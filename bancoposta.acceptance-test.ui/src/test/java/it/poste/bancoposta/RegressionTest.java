package it.poste.bancoposta;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import test.automation.core.XCucumber;
import test.automation.core.annotation.Ui;


@RunWith(XCucumber.class)
@CucumberOptions(plugin = { "json:target/reports/Regression.json" },features = {
		"src/test/resources/it/poste/bancoposta/assistenza/Assistenza_Regression.feature", 
		"src/test/resources/it/poste/bancoposta/bollettino/Bollettino_Regression.feature",
		"src/test/resources/it/poste/bancoposta/bolloAuto/BolloAuto_Regression.feature",
		"src/test/resources/it/poste/bancoposta/bonificoSEPA/BonificoSEPA_Regression.feature",
		"src/test/resources/it/poste/bancoposta/buoni/Buoni_Regression.feature",
		"src/test/resources/it/poste/bancoposta/carte/Carte_Regression.feature",
		"src/test/resources/it/poste/bancoposta/conti/Conti_Regression.feature",
		"src/test/resources/it/poste/bancoposta/girofondo/Girofondo_Regression.feature",
		"src/test/resources/it/poste/bancoposta/home/Home_Regression.feature",
		"src/test/resources/it/poste/bancoposta/impostazioni/Impostazioni_Regression.feature",
		"src/test/resources/it/poste/bancoposta/libretti/Libretti_Regression.feature",
		"src/test/resources/it/poste/bancoposta/loginLogout/LoginLogout_Regression.feature",
		"src/test/resources/it/poste/bancoposta/notifiche/Notifiche_Regression.feature",
		"src/test/resources/it/poste/bancoposta/pfm/PFM_Regression.feature",
		"src/test/resources/it/poste/bancoposta/postagiro/Postagiro_Regression.feature",
		"src/test/resources/it/poste/bancoposta/ricaricaPostePay/RicaricaPostePay_Regression.feature",
		"src/test/resources/it/poste/bancoposta/ricaricaTelefonica/RicaricaTelefonica_Regression.feature",
		"src/test/resources/it/poste/bancoposta/salvadanaio/Salvadanaio_Regression.feature",
		"src/test/resources/it/poste/bancoposta/sconti/Sconti_Regression.feature",
		"src/test/resources/it/poste/bancoposta/UfficiPostali/UfficiPostali_Regression.feature",
		"src/test/resources/it/poste/bancoposta/bacheca/Bacheca_Regression.feature",
"src/test/resources/it/poste/bancoposta/vol/Vol_Regression.feature"},

tags = 
		" @BPINPROAND-1223_fortunato" +
		"", glue= {"classpath:it/poste/bancoposta/"})



@Ui
public class RegressionTest {

}