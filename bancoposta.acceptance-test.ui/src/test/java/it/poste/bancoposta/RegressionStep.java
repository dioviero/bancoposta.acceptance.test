package it.poste.bancoposta;


import static io.appium.java_client.MobileCommand.setSettingsCommand;

import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDateTime;	
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.Random;
import java.util.Set;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import javax.imageio.ImageIO;

import org.openqa.selenium.By;
import org.openqa.selenium.ContextAware;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.html5.Location;
import org.openqa.selenium.html5.LocationContext;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.server.handler.SubmitElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;

import android.Utility;
import automation.core.ui.WaitManager;
import automation.core.ui.pagemanager.UiPageManager;
import automation.core.ui.uiobject.UiPageRepository;
import bean.datatable.*;
import cucumber.api.DataTable;
import cucumber.api.PendingException;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.CommandExecutionHelper;
import io.appium.java_client.ExecutesMethod;
import io.appium.java_client.InteractsWithApps;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import io.appium.java_client.android.nativekey.PressesKey;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.touch.offset.PointOption;
import it.poste.bancoposta.pages.*;
import it.poste.bancoposta.utils.ConnectionUtils;
import it.poste.bancoposta.utils.Parser;
import it.poste.bancoposta.utils.SoftAssertion;
import net.bramp.ffmpeg.FFmpeg;	
import net.bramp.ffmpeg.FFmpegExecutor;	
import net.bramp.ffmpeg.builder.FFmpegBuilder;

import org.json.JSONObject;
import org.junit.Assert;

import test.automation.bug.reporter.BugReporter;
import test.automation.core.UIUtils;
import test.automation.core.cmd.adb.AdbCommandPrompt;
import test.automation.core.cmd.adb.AdbKeyEvent;
import test.automation.core.video.Video;
import test.automation.core.video.VideoManager;
import test.automation.core.video.VideoOption;
import test.automation.core.video.VideoRecorderType;
import test.automation.core.videorecorder.ExecFfmpeg;
import utils.DinamicData;
import utils.Entry;
//import it.poste.bancoposta.tools.WaitManager;
//import it.poste.bancoposta.tools.WaitManagerConfig;

public class RegressionStep {
	public static final int ANDROID = 0;
	public static final int IOS = 1;
	public static final String BUONI_E_LIBRETTI = "buoni e libretti";
	public static final String BUONI_POSTALI = "buoni postali";
	public static final String ANNULLA_OPERAZIONE = "annullamento";
	public static final String CONTI_E_CARTE_SECTION = "conti e carte";
	public static final String OPERAZIONE_VELOCE = "operazioni veloci";
	private static final Object FINGERPRINT = "fingerPrint";

	private int driverType;

	private WebDriver driver;

	private LoginPosteItPage posteIt;
	private LoginPosteIdPage posteId;
	private HomePage homePage;
	private HomepageHamburgerMenu hamburgerMenu;
	private SettingsPage settings;
	private SettingsNotificationPage notification;
	private BuoniLibrettiPage buonilibr;
	private BuoniPostaliPage buoniPostali;
	private FAQPage faq;
	private NumeriUtiliPage numeriUtili;
	private ContiCartePage contiPage;
	private BollettinoChooseOperationPage bollettinoChoose;
	private SelectBolletinoTypesPage selectBollettino;
	private QuickOperationListPage quickPage;
	private GirofondoFormPage girofondo;
	private OKOperationPage thankYouPage;
	private BachecaNotifichePage notifiche;
	private BachecaMessaggiPage messaggi;
	private BachecaMessaggiDetailsPage messageDetails;
	private YourExpencesPage leTueSpese;
	private YourExpencesDetailsPage speseDettaglio;
	private ExclutedOperationsPage speseCategory;
	private ExclutedOperationsDetailsPage speseDetails;
	private PagaConSubMenuBuoniELibretti buoniELibretti;
	private RicaricaLaTuaPostepayChooseOperationPage ricaricaPostepay;
	private PayWithPage payWithPage;
	private PagaConSubMenu pagaConSubMenu;
	private RicaricaPostepayPage ricaricaPostepayPage;
	private SelezionaCartaPage selezionaCartaPage;
	private RiceviSubMenuCartaPage riceviSubMenuCartaPage;

	private String userHeader;
	private GirofondoCreationBean girofondoData;
	private QuickOperationDeletePage deleteQuickOperation;
	private SendBonificoPage bonifico;
	private SendPostagiroPage postagiro;
	private RicaricaTelefonicaPage ricaricaTelefonicaPage;
	private OKOperationPage insertPosteId;
	private PosteChatPage posteChat;
	private ScontiPostePage scontiPostePage;
	private SearchPostalOfficePage searchPostalOfficePage;
	private BolloAutoPage bolloAuto;
	private RicaricaLaTuaPostepayChooseOperationPage RicaricaLaTuaPostepayChooseOperationPage;
	private BolloAutoConfirmPage bolloAutoConfirmPage;
	private SalvadanaioPage salvadanaio;

	private String baseVideoNamePath;
	private String videoName;
	private String scenarioID;
	private String driverName;
	private WebElement pagaButton;
	private WebElement spidOkButton;
	private WebElement signButton;
	private WebElement noAccountButton;
	private WebElement registerAccountSite;
	private WebElement text;
	private WebElement registerCloseButton;
	private WebElement toggle;
	private WebElement accessAut;
	private List<WebElement> record;
	private ContiCartePage conti;
	private WebElement accountDetailButton;
	private WebElement toggleAccount;
	private WebElement confirm;
	private WebElement posteID;
	private List<WebElement> record2;
	private WebElement deleteButton;
	private WebElement popupOkButton;
	private WebElement automaticActiveRechargeButton;
	private String executionDate;
	private String objName;
	private Video dev0;
	private WebElement vediTutto;
	private WebElement ownerNameText;
	private WebElement okpopupbutton;
	private WebElement hamburgerMenuButton;
	private WebElement siAccettoButton;
	private WebElement allowPopupButton;
	private WebElement backButton;
	private WebElement leftMenu;
	private WebElement accediButton;


	@Before
	public void setup(Scenario scenario) throws IOException {
		System.out.println(scenario.getId());
		System.out.println(scenario.getName());
		System.setProperty(UIUtils.SCENARIO_ID, Integer.toString(scenario.getId().hashCode()));
		this.scenarioID = scenario.getName() + "_" + Integer.toString(scenario.getId().hashCode());
		//nuovo test reporter
		SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		//		format.setTimeZone(TimeZone.getTimeZone("UTC"));
		Calendar c=Calendar.getInstance();
		this.executionDate=format.format(c.getTime());
		System.out.println(this.executionDate);
		this.objName="BP obj"+it.poste.bancoposta.utils.Utility.getAlphaNumericString(8);	
		System.out.println("Il valore dell' objective name è:" + objName);
	}
	
	private void restoreDefaultSettings() {
		CommandExecutionHelper.execute((ExecutesMethod) driver, setSettingsCommand("customSnapshotTimeout",15));
		CommandExecutionHelper.execute((ExecutesMethod) driver, setSettingsCommand("snapshotMaxDepth",50));
		CommandExecutionHelper.execute((ExecutesMethod) driver, setSettingsCommand("useFirstMatch",false));
	}

	@SuppressWarnings("unlikely-arg-type")
	@Given("^L'app \"([^\"]*)\" viene avviata e viene eseguito il New Login$")
	public void lapp_something_viene_avviata_e_viene_eseguito_il_new_login(String app, List<CredentialAccessBean> table)
			throws Throwable {
		access = table.get(0);

		// ricavo il tipo di device: ios o android
		// if(app.equalsIgnoreCase("android"))
		// {
		driverType = ANDROID;
		// }
		// else if(app.equalsIgnoreCase("ios"))
		// {
		// driverType=IOS;
		// }
		
		if(access.getWebDriver() != null)
		{
			bpol=UIUtils.ui().driver(access.getWebDriver());
		}

		// ricavo il driver del dispositivo
		driver = (AppiumDriver<?>) UIUtils.ui().driver(access.getDevice());
		
		if(driver instanceof IOSDriver)
		{
			driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
			restoreDefaultSettings();
		}
	
		String loginType = access.getLoginType();
		this.userHeader = access.getUserHeader();

		//connectionsUp();
		//----------------- ANNUNZIATA ----------------------------
		devProp=UIUtils.ui().getCurrentProperties();
		//----------------- ANNUNZIATA ----------------------------

		//		this.baseVideoNamePath = access.getTestIdCheck() + "-" + access.getDevice();
		//		driverName = access.getDevice();
		//		//        driver.setLocation(new Location(49, 123, 10));
		//		startVideoRecording(driverName);

		this.baseVideoNamePath = access.getTestIdCheck() + "-" + access.getDevice();
		driverName = access.getDevice();
		//        driver.setLocation(new Location(49, 123, 10));
		
		if(bpol != null)
		{
			UiPageRepository.get()
			.loadPagesDefinitionSqlLite(new File(RegressionStep.class.getClassLoader().getResource("bp.element.repository.db").toURI()).getAbsolutePath()
					,null, driver,bpol);
		}
		else
		{
			UiPageRepository.get()
			.loadPagesDefinitionSqlLite(new File(RegressionStep.class.getClassLoader().getResource("bp.element.repository.db").toURI()).getAbsolutePath()
					,null, driver);
		}

		
		setConnectionON();

		//		---------------------------------------------------------------------------
		//		                   NUOVA LOGIN - PICA
		NewLoginPreLoginPage p=(NewLoginPreLoginPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("NewLoginPreLoginPage", driver.getClass(), null);
		//Accedi da Welcome Page
		try {


			p.clickOnAccediPrelogin();
			//			UIUtils.ui()
			//			.waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.NewLoginPreLoginPageMolecola.ACCEDIPRELOGINPAGE.getLocator(driver))),15)
			//			.click();

		} catch (Exception e) {
			e.printStackTrace();
		}


		//controllo comparsa popup pre login
		try {

			p.clickOnOkPopup();

			//			UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.NewLoginPreLoginPageMolecola.OKPOPUPBUTTON.getLocator(driver))));
			//			this.okpopupbutton =driver.findElement(By.xpath(Locators.NewLoginPreLoginPageMolecola.OKPOPUPBUTTON.getLocator(driver)));
			//			this.okpopupbutton.click();
			//
			//			WaitManager.get().waitHighTime();

		} catch (Exception e) {
			
		}

		//if(access.getDebug() == null || access.getDebug().equals("n"))
		startVideoRecording(driverName,access.getTestIdCheck());

		//----------ANNUNZIATA PER MIGLIORARE VELOCITA-----------------------------------
		//sessione di login
		try {
			NewLoginPreLoginPage newLogin = (NewLoginPreLoginPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("NewLoginPreLoginPage", driver.getClass(), null);;
					newLogin.checkPage();

					if (access.getOnboarding().equals("nonseitu"))
					{
						newLogin.makeAnewLogin();
					}
					else if(access.getOnboarding().equals("accedi"))
					{
						newLogin.makeAnewLoginAccess();
					}

		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			loginPosteIt = (LoginPosteItPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("LoginPosteItPage", driver.getClass(), null);
			loginPosteIt.checkPage();
			loginPosteIt.loginToTheApp(access.getUsername(), access.getPassword());

		} catch (Exception e) {
			e.printStackTrace();
		}

		login = (LoginWithPosteidPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("LoginWithPosteidPage", driver.getClass(), null);
		login.checkPage();
		login.loginToTheApp(access.getPosteid());
		WaitManager.get().waitLongTime();

		Boolean fatto = false;

		//chiusura popup fingerprint
		p.closeFingerPrintPopup();
		
		
		//chiusura popup notifiche da home
		p.closeNotifichePopup();


		//chiusura popup personalizza esperienza in app
		p.closePersonalizzaEsperienzaInApp();

		homePage = (HomePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("HomePage", driver.getClass(), null);

		homePage.setUserHeader(userHeader);
		homePage.checkPage();

		WaitManager.get().waitMediumTime();
		//----------ANNUNZIATA PER MIGLIORARE VELOCITA-----------------------------------

	}

	@Given("^L'app \"([^\"]*)\" viene avviata e viene eseguito il New Login no onboarding$")
	public void lapp_something_viene_avviata_e_viene_eseguito_il_new_login_no_onboarding(String app, List<CredentialAccessBean> table) throws Throwable {
		access = table.get(0);

		// ricavo il tipo di device: ios o android
		// if(app.equalsIgnoreCase("android"))
		// {
		driverType = ANDROID;
		// }
		// else if(app.equalsIgnoreCase("ios"))
		// {
		// driverType=IOS;
		// }
		
		if(access.getWebDriver() != null)
		{
			bpol=UIUtils.ui().driver(access.getWebDriver());
		}

		// ricavo il driver del dispositivo
		driver = (AppiumDriver<?>) UIUtils.ui().driver(access.getDevice());
		
		if(driver instanceof IOSDriver)
		{
			driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
			restoreDefaultSettings();
		}
		
		
		String loginType = access.getLoginType();
		this.userHeader = access.getUserHeader();

		//connectionsUp();
		//----------------- ANNUNZIATA ----------------------------
		devProp=UIUtils.ui().getCurrentProperties();
		//----------------- ANNUNZIATA ----------------------------

		//		this.baseVideoNamePath = access.getTestIdCheck() + "-" + access.getDevice();
		//		driverName = access.getDevice();
		//		//        driver.setLocation(new Location(49, 123, 10));
		//		startVideoRecording(driverName);

		this.baseVideoNamePath = access.getTestIdCheck() + "-" + access.getDevice();
		driverName = access.getDevice();
		//        driver.setLocation(new Location(49, 123, 10));
		
		if(bpol != null)
		{
			UiPageRepository.get()
			.loadPagesDefinitionSqlLite(new File(RegressionStep.class.getClassLoader().getResource("bp.element.repository.db").toURI()).getAbsolutePath()
					,null, driver,bpol);
		}
		else
		{
			UiPageRepository.get()
			.loadPagesDefinitionSqlLite(new File(RegressionStep.class.getClassLoader().getResource("bp.element.repository.db").toURI()).getAbsolutePath()
					,null, driver);
		}


		setConnectionON();
		
		//		---------------------------------------------------------------------------
		//		                   NUOVA LOGIN - PICA
		NewLoginPreLoginPage p=(NewLoginPreLoginPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("NewLoginPreLoginPage", driver.getClass(), null);
		//Accedi da Welcome Page
		try {


			p.clickOnAccediPrelogin();
			//			UIUtils.ui()
			//			.waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.NewLoginPreLoginPageMolecola.ACCEDIPRELOGINPAGE.getLocator(driver))),15)
			//			.click();

		} catch (Exception e) {
			// TODO: handle exception
		}


		//controllo comparsa popup pre login
		try {

			p.clickOnOkPopup();

			//			UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.NewLoginPreLoginPageMolecola.OKPOPUPBUTTON.getLocator(driver))));
			//			this.okpopupbutton =driver.findElement(By.xpath(Locators.NewLoginPreLoginPageMolecola.OKPOPUPBUTTON.getLocator(driver)));
			//			this.okpopupbutton.click();
			//
			//			WaitManager.get().waitHighTime();

		} catch (Exception e) {

		}

		//if(access.getDebug() == null || access.getDebug().equals("n"))
		startVideoRecording(driverName,access.getTestIdCheck());

		//----------ANNUNZIATA PER MIGLIORARE VELOCITA-----------------------------------
		//sessione di login
		try {
			NewLoginPreLoginPage newLogin = (NewLoginPreLoginPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("NewLoginPreLoginPage", driver.getClass(), null);;
					newLogin.checkPage();

					if (access.getOnboarding().equals("nonseitu"))
					{
						newLogin.makeAnewLogin();
					}
					else if(access.getOnboarding().equals("accedi"))
					{
						newLogin.makeAnewLoginAccess();
					}

		} catch (Exception e) {
			// TODO: handle exception
		}
		
		try {
			loginPosteIt = (LoginPosteItPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("LoginPosteItPage", driver.getClass(), null);
			loginPosteIt.checkPage();
			loginPosteIt.loginToTheApp(access.getUsername(), access.getPassword());

		} catch (Exception e) {
			// TODO: handle exception
		}

		Boolean fatto = false;

		//		//chiusura popup notifiche da home
		//		try {
		//			UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@text='Notifiche di spesa']")));
		//			((AndroidDriver<?>) driver).pressKey(new KeyEvent(AndroidKey.BACK));
		//			
		//		} catch (Exception e) {
		//			
		//		}
		//
		//		//chiusura popup personalizza esperienza in app
		//		try {
		//			this.siAccettoButton = driver.findElement(By.xpath(Locators.NewLoginPreLoginPageMolecola.SIACCETTOPOPUPBUTTON.getLocator(driver)));
		//			this.siAccettoButton.click();
		//			this.allowPopupButton = driver.findElement(By.xpath(Locators.NewLoginPreLoginPageMolecola.ALLOWPOPUPBUTTON.getLocator(driver)));
		//			this.allowPopupButton.click();	
		////			fatto = true;					
		//		} catch (Exception r) {
		//				
		//		}

		//			-----------------------  PICA -------------------------------------------------------------

		WaitManager.get().waitMediumTime();

		EnableYourProductApp enable = (EnableYourProductApp) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("EnableYourProductApp", driver.getClass(), null);
		
		enable.checkPage();
		
		enable.checkEnableYourProductPage();

		//			------------------------ PICA --------------------------------------------------------------
	}


	// Login per verificare che l'accesso con credenziali SPID non avviene nell'app
	@Given("^L'app \"([^\"]*)\" viene avviata e viene eseguita la nuova login$")
	public void lapp_something_viene_avviata_e_viene_eseguita_la_nuova_login(String app,List<CredentialAccessBean> table) throws Throwable {

		CredentialAccessBean access = table.get(0);
		driverType = ANDROID;
		driver = (AppiumDriver<?>) UIUtils.ui().driver(access.getDevice());
		
		if(driver instanceof IOSDriver)
		{
			driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
			restoreDefaultSettings();
		}
		
		
		
		this.baseVideoNamePath = this.scenarioID + "-" + access.getTestIdCheck() + "-" + access.getDevice();
		driverName = access.getDevice();
		
		UiPageRepository.get()
		.loadPagesDefinitionSqlLite(new File(RegressionStep.class.getClassLoader().getResource("bp.element.repository.db").toURI()).getAbsolutePath(),null, driver);

		
		setConnectionON();

		//Accedi da Welcome Page
		NewLoginPreLoginPage p=(NewLoginPreLoginPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("NewLoginPreLoginPage", driver.getClass(), null);

		//Accedi da Welcome Page
		p.clickOnAccediPrelogin();

		startVideoRecording(driverName,access.getTestIdCheck());

		NewLoginPreLoginPage newLogin = (NewLoginPreLoginPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("NewLoginPreLoginPage", driver.getClass(), null);

		newLogin.makeAnewLogin();
		String loginType = access.getLoginType();
		try {
			LoginPosteItPage loginPosteIt = (LoginPosteItPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("LoginPosteItPage", driver.getClass(), null);
			loginPosteIt.checkPage();
			loginPosteIt.loginToTheApp(access.getUsername(), access.getPassword());

		} catch (Exception e) {
			LoginWithPosteidPage login = null;
			login = (LoginWithPosteidPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("LoginWithPosteidPage", driver.getClass(), null);
			login.checkPage();
			login.clickNonSeiTu();
			LoginPosteItPage loginPosteIt = (LoginPosteItPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("LoginPosteItPage", driver.getClass(), null);
			loginPosteIt.checkPage();
			loginPosteIt.loginToTheApp(access.getUsername(), access.getPassword());
		}

	}

	@And("^Viene chiusa l'app$")
	public void viene_chiusa_l_app() throws Throwable {
		WaitManager.get().waitHighTime();
		//		if(driver != null)
		//			driver.quit();
	}

	@And("^Viene effettuato l'onboarding con i seguenti dati$")
	public void viene_effettuato_l_onboarding_con_i_seguenti_dati(List<OnboardingDataBean> table) throws Throwable {
		System.out.println("viene_effettuato_l_onboarding_con_i_seguenti_dati --> METHOD NOT IMPLEMENTED YET");
	}

	@And("^Dal tab menu impostazioni \"([^\"]*)\" abilitare tutte le notifiche relative alle seguenti operazioni$")
	public void dal_tab_menu_impostazioni_abilitare_tutte_le_notifiche_relative_alle_seguenti_operazioni(
			String notifiche, List<NotificationSettingBean> table) throws Throwable {

		WaitManager.get().waitMediumTime();

		// controllo che siamo sulla homepage
		this.homePage = (HomePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("HomePage", driver.getClass(), null);

		homePage.setUserHeader(userHeader);

		homePage.checkPage();

		// apro il menu laterale
		this.hamburgerMenu = homePage.openHamburgerMenu();

		// clicco su impostazioni
		this.settings = hamburgerMenu.gotoSettings();

		// click su notifiche
		notification = settings.gotoNotification();

		NotificationSettingBean n = table.get(0);

		// setto tutte le notifiche in input
		notification.setNotification(n);

		SettingsNotificationPage settingsNotificationPage = (SettingsNotificationPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("SettingsNotificationPage", driver.getClass(), null);
		settingsNotificationPage.gotoSettingsPage();

		settings.checkPage();
		settings.openHamburgerMenu();
		//
		// //apro il menu laterale
		// this.hamburgerMenu = settings.openHamburgerMenu();
		//
		// clicco su homepage
		this.homePage = hamburgerMenu.gotoHomePage(this.userHeader);
	}

	@And("^Vengono effettuate le verifiche dei numeri utili dalla pagina di Login$")
	public void vengono_effettuate_le_verifiche_dei_numeri_utili_dalla_pagina_di_Login(DataTable arg1)
			throws Throwable {

		this.homePage = (HomePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("HomePage", driver.getClass(), null);

		homePage.setUserHeader(userHeader);
		homePage.checkPage();

		faq = homePage.gotoFaq();

		numeriUtili = faq.gotoNumeriUtili();

		numeriUtili.checkNumeriUtili();
	}

	@And("^Dalla pagina \"([^\"]*)\" tab \"([^\"]*)\" vengono compilati i seguenti campi e viene effettuato operazione di \"([^\"]*)\"$")
	public void dalla_pagina_tab_vengono_compilati_i_seguenti_campi_e_viene_effettuato_operazione_di(String pagina,
			String sezione, String operation, List<VoucherVerificationBean> operazione) throws Throwable {

		VoucherVerificationBean c = operazione.get(0);

		if (pagina.equalsIgnoreCase(BUONI_E_LIBRETTI)) {
			this.homePage = (HomePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("HomePage", driver.getClass(), null);

			homePage.setUserHeader(userHeader);
			homePage.checkPage();

			homePage.scrollToBuonieLibrettiSection();

			// accedo alla sezione buoni e libretti
			buonilibr = homePage.gotoBuoniELibrettiPage();

			if (sezione.equalsIgnoreCase(BUONI_POSTALI)) {

				// accedo alla sezione buoni postali
				buoniPostali = buonilibr.gotoBuoniPostali();

				// clicco sul bottone sottoscrivi buono
				buoniPostali = buonilibr.sottoscriviBuono();
				try {
					buonilibr.clickContinua();
				} catch (Exception e) {
					// TODO: handle exception
				}
				// clicco sul conto scelto
				//buoniPostali = buonilibr.clickOnConto(c);
				// clicco sul tipo di buono da effettuare
				buonilibr.clickOnBuono(c);

				//seleziono il metodo di pagamento
				this.payWithPage=(PayWithPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
						.getPageManager("PayWithPage", driver.getClass(), null);

				this.payWithPage.clickOnConto(c.getPayWith());

				// clicco sul metodo di sottoscrizione
				buoniPostali = buonilibr.changeSubscriptionMethod(c);

				// cambio l'importo del buono e controllo che il cambio sia effettuato nel modo giusto
				buoniPostali = buonilibr.changeAmount(c);

				// clicco sul tasto conferma, inserisco il PosteId e arrivo alla ThankYouPage
				this.thankYouPage = buonilibr.clickOnConfirm(c);

				// chiudo la ThankYouPage
				this.thankYouPage.closePage();

				// effettuo le operazioni sui buoni
				// if(operation.equalsIgnoreCase(ANNULLA_OPERAZIONE))
				// {
				//				for(VoucherVerificationBean b : operazione)
				//				{
				//					buoniPostali.execAnnullaSottoscrizioneBuono(b);
				//				}
				// }
			}
		}
	}

	@And("^Viene effettuato un postagiro da \"([^\"]*)\"$")
	public void viene_effettuato_un_postagiro_da(String arg1, List<PaymentCreationBean> table) throws Throwable {
		PaymentCreationBean b = table.get(0);

		DinamicData.getIstance().set("POSTAGIRO_DATA", b);
		
		Properties texts=new Properties();
		
		texts.load(RegressionStep.class.getClassLoader().getResourceAsStream("texts/texts.properties"));

		// navigo verso la sezione conti e carte
		this.homePage = (HomePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("HomePage", driver.getClass(), texts);

		homePage.setUserHeader(userHeader);

		homePage.checkPage();
		WaitManager.get().waitMediumTime();

		this.hamburgerMenu = homePage.openHamburgerMenu();
		WaitManager.get().waitMediumTime();
		// seleziono postagiro
		this.contiPage = hamburgerMenu.gotoContiCarte();
		WaitManager.get().waitMediumTime();
		this.postagiro = this.contiPage.navigateToPostagiro(b);

		// effettuo il postagiro
		this.thankYouPage = this.postagiro.sendPostagiro(b);
		//this.thankYouPage.closePage();

		//this.thankYouPage.check();

	}


	@And("^Viene effettuato un postagiro da \"([^\"]*)\" 2$")
	public void viene_effettuato_un_postagiro_da_something_2(String arg1, List<PaymentCreationBean> table) throws Throwable {

		PaymentCreationBean b = table.get(0);

		DinamicData.getIstance().set("POSTAGIRO_DATA", b);
		Properties p = new Properties();
		p.load(RegressionStep.class.getClassLoader().getResourceAsStream("texts/texts.properties"));
		// navigo verso la sezione conti e carte
		this.homePage =(HomePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("HomePage", driver.getClass(), p);

		homePage.setUserHeader(userHeader);

		homePage.checkPage();
		WaitManager.get().waitMediumTime();

		this.hamburgerMenu = homePage.openHamburgerMenu();
		WaitManager.get().waitMediumTime();
		// seleziono postagiro
		this.contiPage = hamburgerMenu.gotoContiCarte();
		WaitManager.get().waitMediumTime();
		this.postagiro = this.contiPage.navigateToPostagiro(b);

		// effettuo il postagiro
		this.thankYouPage = this.postagiro.sendPostagiro(b);
		this.thankYouPage.closePage();

	}

	@And("^Viene effettuato un postagiro da operazione veloce$")
	public void viene_effettuato_un_postagiro_da_operazione_veloce(List<PaymentCreationBean> table) throws Throwable {

		PaymentCreationBean b = table.get(0);
		try {
			WaitManager.get().waitMediumTime();

			BonificoConfirmPage p=(BonificoConfirmPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("BonificoConfirmPage", driver.getClass(), null);

			p.clikOnModifica();

		} catch (Exception e) {

		}
		Properties texts = new Properties();
		texts.load(RegressionStep.class.getClassLoader().getResourceAsStream("texts/texts.properties"));
		// effettuo il postagiro
		this.postagiro = (SendPostagiroPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("SendPostagiroPage", driver.getClass(), texts);
		this.thankYouPage = this.postagiro.sendPostagiro(b);
		try {
			PostagiroConfirmPage p=(PostagiroConfirmPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("PostagiroConfirmPage", driver.getClass(), null);

			p.clikOnOperazioneEseguitaChiudi();
			OKOperationPage OK=null;
			OK.close();

		} catch (Exception e) {

		}
		try {
			PostagiroConfirmPage p=(PostagiroConfirmPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("PostagiroConfirmPage", driver.getClass(), null);

			p.clikOnOperazioneEseguitaChiudi();
			OKOperationPage OK=null;
			OK.close();
		} catch (Exception e) {

		}
		try {
			QuickOperationListPage p=(QuickOperationListPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("QuickOperationListPage", driver.getClass(), null);
			p.clickBack(userHeader);
		} catch (Exception e) {

		}

	}

	@And("^Viene controllata la ricezione della notifica$")
	public void viene_controllata_la_ricezione_della_notifica(List<NotificationReceptionBean> table) throws Throwable {

		NotificationReceptionBean b = table.get(0);
		// vado alla sezione bacheca messaggi
		this.homePage = (HomePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("HomePage", driver.getClass(), null);
		homePage.setUserHeader(userHeader);
		homePage.checkPage();
		this.hamburgerMenu = homePage.openHamburgerMenu();
		this.messaggi = hamburgerMenu.gotoBachecaSectionMessaggi();

		WaitManager.get().waitMediumTime();

		// cerco il messaggio relativo alla transazione
		this.messaggi.filtraMessaggi(b);
		WaitManager.get().waitLongTime();
		this.messageDetails = this.messaggi.gotoTransactionMessage(table.get(0));
		System.out.println(table.get(0).getMessageText());
		Assert.assertTrue("controllo ricerca messaggio transazione:" + table.get(0).getMessageText(),
				messageDetails != null);

		// controllo messaggio
		System.out.println(b.getNotifyType() + "" + "Questo è notifyType");

		//		if(b.getNotifyType().equals("GIROFONDO"))
		//		{
		//			this.messageDetails.checkGirofondoMessage(b,(GirofondoCreationBean) DinamicData.getIstance().get("GIROFONDO_DATA"));
		//		}
		//		else if(b.getNotifyType().equals("POSTAGIRO"))
		//		{
		//			Object data=DinamicData.getIstance().get("BONIFICO_DATA");
		//
		//			if(data == null)
		//				this.messageDetails.checkPostagiroMessage(b,(PaymentCreationBean)DinamicData.getIstance().get("POSTAGIRO_DATA"));
		//			else
		//				this.messageDetails.checkPostagiroMessage(b,(BonificoDataBean)DinamicData.getIstance().get("BONIFICO_DATA"));
		//
		//		}
		//		else if(b.getNotifyType().equals("BONIFICO"))
		//		{
		//			this.messageDetails.checkBonificoMessage(b,(BonificoDataBean)DinamicData.getIstance().get("BONIFICO_DATA"));
		//		} 	
		//		else if(b.getNotifyType().equals("BOLLETTINO"))
		//		{
		//			this.messageDetails.checkBollettinoMessage(b,(BollettinoDataBean)DinamicData.getIstance().get("BOLLETTINO_DATA"));
		//		}

		// ritorno alla home page
		this.messageDetails.clickBack();

		this.messaggi.checkPage();

		this.hamburgerMenu = this.messaggi.openHamburgerMenu();

		this.homePage = this.hamburgerMenu.gotoHomePage(userHeader);

	}

	@And("^Dalla pagina di creazione di un \"([^\"]*)\" viene modificato il metodo di pagamento$")
	public void dalla_pagina_di_creazione_di_un_viene_modificato_il_metodo_di_pagamento(String arg1, DataTable arg2)
			throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		// For automatic transformation, change DataTable to one of
		// List<YourType>, List<List<E>>, List<Map<K,V>> or Map<K,V>.
		// E,K,V must be a scalar (String, Integer, Date, enum etc)
		throw new PendingException();
	}

	@And("^Viene effettuato un \"([^\"]*)\"$")
	public void viene_effettuato_un(String arg1, List<GirofondoCreationBean> table) throws Throwable {

		GirofondoCreationBean b = table.get(0);
		// this.girofondoData=b;
		DinamicData.getIstance().set("GIROFONDO_DATA", b);

		// navigo verso la sezione conti e carte
		this.homePage = (HomePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("HomePage", driver.getClass(), null);
		homePage.setUserHeader(userHeader);
		homePage.checkPage();

		this.hamburgerMenu = homePage.openHamburgerMenu();

		this.contiPage = hamburgerMenu.gotoContiCarte();

		// seleziono girofondo

		this.girofondo = contiPage.navigateToGirofondo(b);
		WaitManager.get().waitMediumTime();
		// compilo la form e sottometto
		this.thankYouPage = this.girofondo.eseguiGirofondo(b);

		WaitManager.get().waitMediumTime();

		Properties texts=new Properties();
		
		texts.load(RegressionStep.class.getClassLoader().getResourceAsStream("texts/texts.properties"));
		OKOperationPage g = (OKOperationPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("OKOperationPage", driver.getClass(), texts);
		
		g.checkGirofondoOKPage();
//		this.thankYouPage.checkGirofondoOKPage();
		
		if  (b.getTestIdCheck().equals("PBPAPP-901")) {
			g.closePage();
		}

	}

	@And("^Viene effettuato un \"([^\"]*)\" 2$")
	public void viene_effettuato_un_something_2(String arg1, List<GirofondoCreationBean> table) throws Throwable {

		GirofondoCreationBean b = table.get(0);
		// this.girofondoData=b;
		DinamicData.getIstance().set("GIROFONDO_DATA", b);

		// navigo verso la sezione conti e carte
		this.homePage = (HomePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("HomePage", driver.getClass(), null);
		homePage.setUserHeader(userHeader);
		homePage.checkPage();

		this.hamburgerMenu = homePage.openHamburgerMenu();

		this.contiPage = hamburgerMenu.gotoContiCarte();

		// seleziono girofondo

		this.girofondo = contiPage.navigateToGirofondo(b);
		WaitManager.get().waitMediumTime();
		// compilo la form e sottometto
		this.thankYouPage = this.girofondo.eseguiGirofondo(b);

		WaitManager.get().waitMediumTime();

		if  (b.getTestIdCheck().equals("PBPAPP-901")) {
			this.thankYouPage.closePage();
		}
		this.thankYouPage.checkGirofondoOKPage();
		this.thankYouPage.closePage();

	}

	@And("^Viene effettuato un girofondo da operazione veloce$")
	public void viene_effettuato_un_girofondo_da_operazione_veloce(List<GirofondoCreationBean> table) throws Throwable {

		GirofondoCreationBean b = table.get(0);

		// effettuo il girofondo
		this.girofondo = (GirofondoFormPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("GirofondoFormPage", driver.getClass(), null);

		this.thankYouPage = this.girofondo.eseguiGirofondo(b);
	}

	@And("^Viene effettuata una ricarica postepay$")
	public void viene_effettuata_una_ricarica_postepay(List<RicaricaPostepayBean> t) throws Throwable {

		RicaricaPostepayBean b = t.get(0);
		Properties texts=new Properties();
		
		texts.load(RegressionStep.class.getClassLoader().getResourceAsStream("texts/texts.properties"));
		homePage = (HomePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("HomePage", driver.getClass(), texts);
		homePage.setUserHeader(userHeader);
		homePage.checkPage();
		//System.out.println(homePage.get());

		WaitManager.get().waitShortTime();

		this.hamburgerMenu = homePage.openHamburgerMenu();
		this.contiPage = hamburgerMenu.gotoContiCarte();
		//		if (b.getPaymentMethod() == "carta") {
		//			this.contiPage.navigateToContoCarta(t.get(0).getPayWith());
		//		} else {
		//			this.contiPage.navigateToConto(t.get(0).getPayWith());
		//		}
		// this.payWithPage=new PayWithPage(driver, driverType).get();
		// this.payWithPage.clickOnConto(b.getPayWith());
		this.ricaricaPostepayPage = this.contiPage.navigateToRicaricaPostepay(b);

		//		this.pagaConSubMenu = new PagaConSubMenu(driver, driverType).get();

		//		this.ricaricaPostepayPage = new RicaricaPostepayPage(driver, driverType).get();
		// this.selezionaCartaPage = new SelezionaCartaPage(driver, driverType).get();
		// this.selezionaCartaPage.clickOnConto(b.getPayWith());
		this.thankYouPage = this.ricaricaPostepayPage.eseguiRicarica(b);
		//		InsertPosteIDPage posteId = new InsertPosteIDPage(driver, driverType);
		//
		//		posteId.insertPosteID(b.getPosteId());
		//		this.thankYouPage.closePage();

		// this.bollettinoChoose=contiPage.navigateToBollettinoSection(b);

		// this.homePage=new HomePage(driver, driverType, userHeader);
		//
		// this.hamburgerMenu=this.homePage.openHamburgerMenu();
		//
		// this.buonilibr = this.homePage.gotoBuoniELibrettiPage();

		// this.buonilibr = new BuoniLibrettiPage(driver, driverType);
		// Thread.sleep(2000);
		// this.buoniELibretti.get();

		// this.buoniELibretti = this.buoniELibretti.
		// this.buoniELibretti = new PagaConSubMenuBuoniELibretti(driver, driverType);
		// this.buoniELibretti.get();
		// this.buoniELibretti.gotoRicaricaLaTuaPostepay();
		//

	}

	@And("^Viene effettuata una ricarica questa postepay$")
	public void viene_effettuata_una_ricarica_questa_postepay(List<RicaricaPostepayBean> t) throws Throwable {
		Properties texts=new Properties();
		
		texts.load(RegressionStep.class.getClassLoader().getResourceAsStream("texts/texts.properties"));
		RicaricaPostepayBean b = t.get(0);

		homePage = (HomePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("HomePage", driver.getClass(), null);
		homePage.setUserHeader(userHeader);
		homePage.checkPage();

		WaitManager.get().waitShortTime();

		this.hamburgerMenu = homePage.openHamburgerMenu();
		this.contiPage = hamburgerMenu.gotoContiCarte();
		WaitManager.get().waitMediumTime();
		contiPage.getsaldoVecchio();

		this.riceviSubMenuCartaPage = this.contiPage.navigateToRicaricaQuestaPostepay(b);
		this.RicaricaLaTuaPostepayChooseOperationPage = this.riceviSubMenuCartaPage.goToRicaricaQuestaPostepay();
		this.payWithPage = (PayWithPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("PayWithPage", driver.getClass(), null);
		payWithPage.checkPage();
		this.payWithPage.clickOnConto(b.getPayWith());

		RicaricaPostepayPage p = (RicaricaPostepayPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("RicaricaPostepayPage", driver.getClass(), texts);
		p.checkPage();

		p.eseguiRicaricaQuestaPostePay(b);
		
		OKOperationPage g = (OKOperationPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("OKOperationPage", driver.getClass(), texts);
		g.checkPage();
		g.checkRicaricaQuestaOKPage();
		g.closePage();
		this.hamburgerMenu = this.contiPage.openHamburgerMenu();
		this.homePage = this.hamburgerMenu.gotoHomePage(userHeader);
		homePage = (HomePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("HomePage", driver.getClass(), null);
		homePage.setUserHeader(userHeader);
		homePage.checkPage();

	}

	@And("^Viene effettuata una \"([^\"]*)\" da \"([^\"]*)\" section$")
	public void viene_effettuata_una_something_da_something_section(String arg1, String sezione,
			List<RicaricaTelefonicaBean> t) throws Throwable {

		RicaricaTelefonicaBean b = t.get(0);

		homePage = (HomePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("HomePage", driver.getClass(), null);
		homePage.setUserHeader(userHeader);
		homePage.checkPage();
		//System.out.println(homePage.get());

		WaitManager.get().waitShortTime();

		this.hamburgerMenu = homePage.openHamburgerMenu();
		this.contiPage = hamburgerMenu.gotoContiCarte();
		// this.payWithPage=new PayWithPage(driver, driverType).get();
		// this.payWithPage.clickOnConto(b.getPayWith());

		ContiCartePage p=(ContiCartePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("ContiCartePage", driver.getClass(), null);

		p.clickOnPagaButton();

		this.pagaConSubMenu = (PagaConSubMenu) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("PagaConSubMenu", driver.getClass(), null);

		pagaConSubMenu.clickOnRicaricaTelefonica();

		this.ricaricaTelefonicaPage = (RicaricaTelefonicaPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("RicaricaTelefonicaPage", driver.getClass(), null);
		ricaricaTelefonicaPage.checkPage();
		this.thankYouPage = this.ricaricaTelefonicaPage.eseguiRicarica(b);
		// this.thankYouPage.closePage();



	}

	@And("^Viene effettuata una ricarica telefonica da operazione veloce$")
	public void viene_effettuata_una_ricarica_telefonica_da_operazione_veloce(List<RicaricaTelefonicaBean> table)
			throws Throwable {
		RicaricaTelefonicaBean b = table.get(0);

		// effettua una ricarica Postepay
		this.ricaricaTelefonicaPage = (RicaricaTelefonicaPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("RicaricaTelefonicaPage", driver.getClass(), null);
		ricaricaTelefonicaPage.checkPage();
		this.thankYouPage = this.ricaricaTelefonicaPage.eseguiRicarica(b);
	}

	@And("^Salvare l'operazione come \"([^\"]*)\"$")
	public void salvare_l_operazione_come(String arg1, List<QuickOperationCreationBean> table) throws Throwable {

		this.thankYouPage = (OKOperationPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("OKOperationPage", driver.getClass(), null);
		//this.thankYouPage.get();

		this.thankYouPage.saveAsQuickOperation(table.get(0));

		this.contiPage = (ContiCartePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("ContiCartePage", driver.getClass(), null);
		this.contiPage.checkPage();

		WaitManager.get().waitMediumTime();

		QuickOperationCreationBean b=table.get(0);

		contiPage.clickLeftButton();

		if(b.getFromQuickOperation() == null || b.getFromQuickOperation().equals("n"))
		{
			hamburgerMenu=(HomepageHamburgerMenu) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("HomepageHamburgerMenu", driver.getClass(), null);
			
			if(this.driver instanceof IOSDriver)
			{
				System.out.println("ios hamburger load");
				this.hamburgerMenu.checkPage();
			}
			
			this.hamburgerMenu.gotoHomePage(userHeader);
		}

		//		try {
		//			this.quickPage = new QuickOperationListPage(driver, driverType).get();
		//			WebElement backButton = driver
		//					.findElement(By.xpath(Locators.QuickOperationListPageLocator.BACKBUTTON.getLocator(driver)));
		//			backButton.click();
		//		} catch (Exception e) {
		//
		//			// dopo salvataggio ritorno alla home page
		//			this.contiPage = new ContiCartePage(driver, driverType);
		//			this.contiPage.get();
		//
		//			WaitManager.get().waitMediumTime();
		//
		//			this.hamburgerMenu = this.contiPage.openHamburgerMenu();
		//			this.hamburgerMenu.gotoHomePage(userHeader);
		//
		//		}


	}

	@And("^Controlla che l'operazione veloce sia editabile$")
	public void controlla_che_l_operazione_veloce_sia_editabile(List<QuickOperationCreationBean> table)
			throws Throwable {

		QuickOperationCreationBean b = table.get(0);

		// clicco su vedi tutti da operazione veloce
		this.homePage = (HomePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("HomePage", driver.getClass(), null);
		homePage.setUserHeader(userHeader);
		this.homePage.checkPage();
		//		vediTutto = driver.findElement(By.xpath(Locators.HomePageLocator.QUICKPAIMENTSSHOWALLLINK.getAndroidLocator()));
		//		vediTutto.click();
		//		
		this.quickPage = this.homePage.gotoOperazioniVeloci();

		// vado all'operazione veloce
		this.quickPage.gotoQuickOperationPage(b.getQuickOperationName(), false);

		DinamicData.getIstance().set("RICARICAPOSTEPAY_DATA", b);
		// seleziono conto
		this.payWithPage = (PayWithPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("PayWithPage", driver.getClass(), null);
		payWithPage.checkPage();
		this.payWithPage.clickOnConto(b.getPayWith());
		System.out.println(b.getTransferTo());
		// controllo che l'operazione sia modificabile
		this.quickPage.checkModifyOperation(b);

		// ritorno alla homepage
		this.homePage = this.quickPage.clickBack(userHeader);

	}

	@And("^Modifica \"([^\"]*)\" dell'operazione veloce$")
	public void modifica_dell_operazione_veloce(String field, List<QuickOperationModifyBean> table) throws Throwable {

		try {
			PayWithPage p= (PayWithPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("PayWithPage", driver.getClass(), null);

			p.clikOnOkButton();


		} catch (Exception e) {

		}

		// memorizzo nel dinamic data il dato
		DinamicData.getIstance().set("OPERAZIONE_VELOCE_MODIFY", field + ";" + table.get(0).getEditValue());
	}

	@And("^Viene controllata la corretta categorizzazione della transazione$")
	public void viene_controllata_la_corretta_categorizzazione_della_transazione(List<TransactionDetailBean> table)
			throws Throwable {

		TransactionDetailBean b = table.get(0);

		if (b.getTransactionType().toLowerCase().equals("girofondo")) {
			System.err.println("WARN: per la dispositiva " + b.getTransactionType()
			+ ", la rendicontazione dell'operazione non avviene in maniera immediata");
		}

		this.homePage = (HomePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("HomePage", driver.getClass(), null);
		homePage.setUserHeader(userHeader);
		this.homePage.checkPage();

		// vado alla sezione le tue spese
		this.leTueSpese = this.homePage.gotoLeTueSpese();

		WaitManager.get().waitLongTime();

		// clicco sul mese corrente
		this.leTueSpese.tapMeseCorrente();

		WaitManager.get().waitLongTime();

		// mi sposto su vedi tutte le categorie
		this.speseDettaglio = this.leTueSpese.clickVediTutteLeCategorie();

		// vado alla categoria desiderata
		this.speseCategory = this.speseDettaglio.gotoCategory(b);

		// vado sulla sottocategoria desiderata
		this.speseDetails = this.speseCategory.gotoSubCategory();

		// controllo la presenza della transazione
		this.speseDetails.checkTransactionIsPresent();

		// ritorno alla homepage
		this.speseCategory = this.speseDetails.clickBack();

		this.speseDettaglio = this.speseCategory.clickBack();

		this.leTueSpese = this.speseDettaglio.clickBack();

		this.homePage = this.leTueSpese.clickITuoiProdotti(this.userHeader);
	}

	@And("^Viene controllata la presenza di tutti i mesi in cui sono state fatte operazioni$")
	public void viene_controllata_la_presenza_di_tutti_i_mesi_in_cui_sono_state_fatte_operazioni(
			List<TransactionDetailBean> arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		// For automatic transformation, change DataTable to one of
		// List<YourType>, List<List<E>>, List<Map<K,V>> or Map<K,V>.
		// E,K,V must be a scalar (String, Integer, Date, enum etc)
		throw new PendingException();
	}

	@And("^Viene spostata un'operazione da \"([^\"]*)\" ad un'altra categoria$")
	public void viene_spostata_un_operazione_da_ad_un_altra_categoria(String arg1, List<TransactionDetailBean> arg2)
			throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		// For automatic transformation, change DataTable to one of
		// List<YourType>, List<List<E>>, List<Map<K,V>> or Map<K,V>.
		// E,K,V must be a scalar (String, Integer, Date, enum etc)
		throw new PendingException();
	}

	@And("^Viene effettuato un postagiro da \"([^\"]*)\" a \"([^\"]*)\"$")
	public void viene_effettuato_un_postagiro_da_a(String arg1, String arg2, List<GirofondoCreationBean> arg3)
			throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		// For automatic transformation, change DataTable to one of
		// List<YourType>, List<List<E>>, List<Map<K,V>> or Map<K,V>.
		// E,K,V must be a scalar (String, Integer, Date, enum etc)
		throw new PendingException();
	}

	@And("^Vengono effettuati i controlli sulla gestione delle notifiche$")
	public void vengono_effettuati_i_controlli_sulla_gestione_delle_notifiche(List<NotificationSettingBean> table)
			throws Throwable {
		NotificationSettingBean n = table.get(0);

		// controllo che la notifica sia stata settata correttamente
		notification.checkNotificationSetting(n);

		// torniamo alla pagina di settings
		settings = notification.gotoSettingsPage();

		// apro l'hamburger menu
		hamburgerMenu = settings.openHamburgerMenu();

		// ritorno sulla homepage
		homePage = hamburgerMenu.gotoHomePage(this.userHeader);
	}

	@And("^Viene effettuato un postagiro da \"([^\"]*)\" a \"([^\"]*)\" di altro utente$")
	public void viene_effettuato_un_postagiro_da_a_di_altro_utente(String arg1, String arg2,
			List<GirofondoCreationBean> arg3) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		// For automatic transformation, change DataTable to one of
		// List<YourType>, List<List<E>>, List<Map<K,V>> or Map<K,V>.
		// E,K,V must be a scalar (String, Integer, Date, enum etc)
		throw new PendingException();
	}

	@And("^Viene effettuata la login con l'utenza del beneficiario$")
	public void viene_effettuata_la_login_con_l_utenza_del_beneficiario(List<CredentialAccessBean> arg1)
			throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		// For automatic transformation, change DataTable to one of
		// List<YourType>, List<List<E>>, List<Map<K,V>> or Map<K,V>.
		// E,K,V must be a scalar (String, Integer, Date, enum etc)
		throw new PendingException();
	}

	@And("^scegli bollettino dalla pagina \"([^\"]*)\" vengono compilati i seguenti campi e viene effettuata l'operazione di \"([^\"]*)\"$")
	public void scegli_bollettino_dalla_pagina_something_vengono_compilati_i_seguenti_campi_e_viene_effettuata_loperazione_di_something(
			String sezione, String paymentMethod, List<BollettinoDataBean> t) throws Throwable {

		BollettinoDataBean b = t.get(0);
		DinamicData.getIstance().set("BOLLETTINO_BIANCO", b);

		if (sezione.equals(CONTI_E_CARTE_SECTION)) {
			homePage = (HomePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("HomePage", driver.getClass(), null);
			homePage.setUserHeader(userHeader);
			this.homePage.checkPage();

			WaitManager.get().waitShortTime();

			this.hamburgerMenu = homePage.openHamburgerMenu();
			this.contiPage = hamburgerMenu.gotoContiCarte();
			this.bollettinoChoose = contiPage.navigateToBollettinoSection(b, b.getPaymentMethod());
		} else if (sezione.equals(OPERAZIONE_VELOCE)) {
			homePage = (HomePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("HomePage", driver.getClass(), null);
			homePage.setUserHeader(userHeader);
			this.homePage.checkPage();

			this.quickPage = homePage.gotoOperazioniVeloci();
			this.quickPage.gotoQuickOperationPage("Bollettino", false);
			this.bollettinoChoose = (BollettinoChooseOperationPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("BollettinoChooseOperationPage", driver.getClass(), null);
			this.bollettinoChoose.checkPage();
		}

		// controlla se il metodo di pagamento è carta, altrimenti è conto
		try {
			if (b.getPaymentMethod().equals("carta")) {
				ContiCartePage p=(ContiCartePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
						.getPageManager("ContiCartePage", driver.getClass(), null);
				p.clickOnTabCarta();
			}
		} catch (Exception e) {
		}

		try {
			this.bollettinoChoose = contiPage.navigateToBollettinoSection(b, paymentMethod);
		} catch (Exception e) {
		}

		try {
			// clicco su compila manualmente
			this.selectBollettino = this.bollettinoChoose.gotoCompilaManualmenteSection();
		} catch (Exception err) {
		}

		this.selectBollettino = (SelectBolletinoTypesPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("SelectBolletinoTypesPage", driver.getClass(), null);
		this.selectBollettino.checkPage();
		// compilo il bollettino ed in base all'operazione sottometto o chiudo
		this.selectBollettino.creaBollettino(b, sezione.equals(OPERAZIONE_VELOCE));

		WaitManager.get().waitMediumTime();
		InsertPosteIDPage insert = null;
		insert = (InsertPosteIDPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("InsertPosteIDPage", driver.getClass(), null);
		insert.checkPage();
		OKOperationPage OK=insert.insertPosteID(b.getPosteId());

		WaitManager.get().waitMediumTime();

		OK.close();
		//		try {	
		//			
		//				WebElement chiudiButton = driver.findElement(By.xpath(Locators.BollettinoConfirmPageLocator.CHIUDIBUTTON.getAndroidLocator()));
		//				chiudiButton.click(); 
		//		} catch (Exception e) {
		//		}

		//		try {	
		//			
		//			WebElement backButton = driver.findElement(By.xpath(Locators.QuickOperationListPageLocator.BACKBUTTON.getAndroidLocator()));
		//			backButton.click(); 
		//	} catch (Exception e) {
		//	}

		// //se l'operazione � di annullamento allora ritorno alla homepage
		// if(operazione.equals("annullamento") &&
		// sezione.equals(CONTI_E_CARTE_SECTION))
		// {
		// this.hamburgerMenu=this.contiPage.openHamburgerMenu();
		// this.homePage=this.hamburgerMenu.gotoHomePage(userHeader);
		// }
	}

	// @And("^scegli bollettino dalla pagina \"([^\"]*)\" vengono compilati i
	// seguenti campi e viene effettuato operazione di \"([^\"]*)\"$")
	// public void
	// dalla_pagina_scegli_bollettino_vengono_compilati_i_seguenti_campi_e_viene_effettuato_operazione_di_something(String
	// sezione,String operazione,List<BollettinoDataBean> t) throws Throwable
	// {
	// BollettinoDataBean b=t.get(0);
	//
	// if(sezione.equals(CONTI_E_CARTE_SECTION))
	// {
	// homePage=new HomePage(driver, driverType, this.userHeader);
	// homePage.get();
	// System.out.println(homePage.get());
	//
	// this.hamburgerMenu=homePage.openHamburgerMenu();
	// this.contiPage=hamburgerMenu.gotoContiCarte();
	// this.bollettinoChoose=contiPage.navigateToBollettinoSection(b);
	// }
	// else if(sezione.equals(OPERAZIONE_VELOCE))
	// {
	// homePage=new HomePage(driver, driverType, this.userHeader);
	// homePage.get();
	//
	// this.quickPage=homePage.gotoOperazioniVeloci();
	// this.quickPage.gotoQuickOperationPage("Bollettino", false);
	// this.bollettinoChoose=new BollettinoChooseOperationPage(driver, driverType);
	// this.bollettinoChoose.get();
	// }
	//
	// try
	// {
	// //clicco su compila manualmente
	// this.selectBollettino=this.bollettinoChoose.gotoCompilaManualmenteSection();
	//
	// } catch (Exception err) {
	//
	// }
	//
	// //compilo il bollettino ed in base all'operazione sottometto o chiudo
	// //this.selectBollettino.creaBollettino(b,operazione,sezione.equals(OPERAZIONE_VELOCE));
	//
	//
	// //se l'operazione � di annullamento allora ritorno alla homepage
	// if(operazione.equals("annullamento") &&
	// sezione.equals(CONTI_E_CARTE_SECTION))
	// {
	// this.hamburgerMenu=this.contiPage.openHamburgerMenu();
	// this.homePage=this.hamburgerMenu.gotoHomePage(userHeader);
	// }
	// }

	@And("^L'utente sceglie \"([^\"]*)\" dalla pagina \"([^\"]*)\" vengono compilati i seguenti campi e viene effettuata l'operazione$")
	public void lutente_sceglie_something_dalla_pagina_something_vengono_compilati_i_seguenti_campi_e_viene_effettuata_loperazione(
			String sezione, String operazione, List<BollettinoDataBean> t) throws Throwable {

		BollettinoDataBean b = t.get(0);

		homePage =(HomePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("HomePage", driver.getClass(), null);
		homePage.setUserHeader(userHeader);
		homePage.checkPage();

		this.quickPage = homePage.gotoOperazioniVeloci();
		this.quickPage.gotoQuickOperationPage("Bollettino", false);
		this.bollettinoChoose = (BollettinoChooseOperationPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("BollettinoChooseOperationPage", driver.getClass(), null);
		this.bollettinoChoose.checkPage();

		try {
			// clicco su compila manualmente
			this.selectBollettino = this.bollettinoChoose.gotoCompilaManualmenteSection();

		} catch (Exception err) {

		}

		// compilo il bollettino ed in base all'operazione sottometto o chiudo
		this.selectBollettino.creaBollettino(b, sezione.equals(OPERAZIONE_VELOCE));

		// se l'operazione � di annullamento allora ritorno alla homepage
		if (operazione.equals("annullamento") && sezione.equals(CONTI_E_CARTE_SECTION)) {
			this.hamburgerMenu = this.contiPage.openHamburgerMenu();
			this.homePage = this.hamburgerMenu.gotoHomePage(userHeader);
		}
	}

	@And("^Cancella operazione veloce$")
	public void cancella_operazione_veloce(List<QuickOperationCreationBean> table) throws Throwable {
		// throw new PendingException();
		QuickOperationCreationBean b = table.get(0);

		// clicco su vedi tutti da operazione veloce
		this.homePage = (HomePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("HomePage", driver.getClass(), null);
		homePage.setUserHeader(userHeader);
		homePage.checkPage();
		this.quickPage = this.homePage.gotoOperazioniVeloci();

		// clicco su modifica ed elimino l'operazione veloce
		this.deleteQuickOperation = this.quickPage.clickModify();

		this.deleteQuickOperation.deleteQuickOperation(b);

		// ritorno alla homepage
		this.homePage = this.deleteQuickOperation.gotoHomePage(this.userHeader);

		// controllo che l'operazione veloce non sia piu presente
		this.quickPage = this.homePage.gotoOperazioniVeloci();
		this.quickPage.checkIfQuickOperationIsNotPresent(b);

		// ritorno alla homepage
		this.homePage = this.quickPage.clickBack(userHeader);
	}

	@And("^Viene effettuata la transazione da operazione veloce$")
	public void viene_effettuata_la_transazione_da_operazione_veloce(List<QuickOperationCreationBean> table)
			throws Throwable {

		QuickOperationCreationBean b = table.get(0);

		this.homePage = (HomePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("HomePage", driver.getClass(), null);
		homePage.setUserHeader(userHeader);
		homePage.checkPage();
		this.quickPage = this.homePage.gotoOperazioniVeloci();

		// vado all'operazione veloce
		this.quickPage.gotoQuickOperationPage(b.getQuickOperationName(), false);

		try {
			quickPage.clickOnOkPopupButton();
		} catch (Exception e) {

		}

		// seleziono conto
		this.payWithPage = (PayWithPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("PayWithPage", driver.getClass(), null);
		payWithPage.checkPage();
		this.payWithPage.clickOnConto(b.getPayWith());

		// this.bonifico=new SendBonificoPage(driver, driverType);
		//
		//
		// //effettuo la transazione da operazione veloce
		// this.thankYouPage=this.quickPage.submitTransaction(b,(String)
		// DinamicData.getIstance().get("BONIFICO_DATA"));
		//
		// //chiudo la thankyoupage
		// this.thankYouPage.closePage();
		//
		// //gestione differente temporanea per via di un bug su android in fase di
		// valutazione
		// switch(driverType)
		// {
		// case 0://android
		// this.quickPage.get();
		// //ritorno alla home page
		// this.homePage=this.quickPage.clickBack(userHeader);
		// break;
		// case 1://ios
		// this.homePage=new HomePage(driver, driverType, userHeader).get();
		// break;
		// }

	}

	@And("^Prende i dati del postagiro$")
	public void prende_i_dati_del_postagiro(List<PaymentCreationBean> table) throws Throwable {
		PaymentCreationBean b = table.get(0);
		DinamicData.getIstance().set("POSTAGIRO_DATA", b);
		// navigo verso la sezione conti e carte
		this.homePage = (HomePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("HomePage", driver.getClass(), null);
		homePage.setUserHeader(userHeader);
		homePage.checkPage();

		// Apro l'hamburgerMenu
		this.hamburgerMenu = homePage.openHamburgerMenu();

		// vado in conti e carte
		this.contiPage = hamburgerMenu.gotoContiCarte();
		// seleziono il bonifico
		this.postagiro = this.contiPage.navigateToPostagiro(b);
		try {
			postagiro.clickAnnulla();

		} catch (Exception t) {

		}
		contiPage.openHamburgerMenu();
		this.hamburgerMenu.gotoHomePage(userHeader);
	}

	@And("^Prende i dati del bonifico$")
	public void prende_i_dati_del_bonifico(List<BonificoDataBean> table) throws Throwable {

		BonificoDataBean b = table.get(0);
		// this.girofondoData=b;
		DinamicData.getIstance().set("BONIFICO_DATA", b);

		// navigo verso la sezione conti e carte
		this.homePage = (HomePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("HomePage", driver.getClass(), null);
		homePage.setUserHeader(userHeader);
		homePage.checkPage();

		// Apro l'hamburgerMenu
		this.hamburgerMenu = homePage.openHamburgerMenu();

		// vado in conti e carte
		this.contiPage = hamburgerMenu.gotoContiCarte();
		// seleziono il bonifico
		this.bonifico = this.contiPage.navigateToBonifico(b);

		try {
			bonifico.chiudiPopup();


		} catch (Exception t) {

		}

		WaitManager.get().waitMediumTime();
		contiPage.openHamburgerMenu();
		this.hamburgerMenu.gotoHomePage(userHeader);
	}

	@And("^Viene effettuato un bonifico da operazione veloce$")
	public void viene_effettuato_un_bonifico_da_operazione_veloce(List<BonificoDataBean> table) throws Throwable {

		BonificoDataBean b = table.get(0);

		// effettuo il bonifico
		Properties texts=new Properties();
		
		texts.load(RegressionStep.class.getClassLoader().getResourceAsStream("texts/texts.properties"));
		this.bonifico = (SendBonificoPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("SendBonificoPage", driver.getClass(), texts);
		bonifico.checkPage();
		this.thankYouPage = this.bonifico.sendBonifico(b);

		this.thankYouPage.close();
		this.thankYouPage.closeFeedback();
		WaitManager.get().waitShortTime();

		this.quickPage.clickBack(userHeader);
	}

	@And("^Viene effettuata una ricarica Postepay da operazione veloce$")
	public void viene_effettuata_una_ricarica_postepay_da_operazione_veloce(List<RicaricaPostepayBean> table)
			throws Throwable {
		RicaricaPostepayBean b = table.get(0);

		this.homePage = (HomePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("HomePage", driver.getClass(), null);
		homePage.setUserHeader(userHeader);
		homePage.checkPage();

		this.quickPage = this.homePage.gotoOperazioniVeloci();

		// vado all'operazione veloce
		this.quickPage.gotoQuickOperationPage(b.getQuickOperationName(), false);
		
		try {
			quickPage.clickOnOkPopupButton();
		} catch (Exception e) {

		}

		// seleziono conto
		this.payWithPage = (PayWithPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("PayWithPage", driver.getClass(), null);
		payWithPage.checkPage();
		this.payWithPage.clickOnConto(b.getPayWith());
		System.out.println(b.getTransferTo());
		// effettua una ricarica Postepay
		Properties texts=new Properties();
		
		texts.load(RegressionStep.class.getClassLoader().getResourceAsStream("texts/texts.properties"));
		this.ricaricaPostepayPage = (RicaricaPostepayPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("RicaricaPostepayPage", driver.getClass(), texts);
		ricaricaPostepayPage.checkPage();

		this.thankYouPage = this.ricaricaPostepayPage.eseguiRicarica(b);
		//		this.thankYouPage.closePage();
		//		QuickOperationListPage p=new QuickOperationListPage(driver, driverType).get();
		//		p.clickBack(b.getUserHeader());
		//		HomePage q = new HomePage(driver, driverType, userHeader).get();
		//		

	}

	@And("^Viene effettuato un bonifico$")
	public void viene_effettuato_un_bonifico(List<BonificoDataBean> table) throws Throwable {

		BonificoDataBean b = table.get(0);
		// this.girofondoData=b;
		DinamicData.getIstance().set("BONIFICO_DATA", b);
		
		Properties texts=new Properties();
		
		texts.load(RegressionStep.class.getClassLoader().getResourceAsStream("texts/texts.properties"));

		// navigo verso la sezione conti e carte
		this.homePage = (HomePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("HomePage", driver.getClass(), texts);
		homePage.setUserHeader(userHeader);
		homePage.checkPage();

		WaitManager.get().waitMediumTime();

		// Apro l'hamburgerMenu 
		this.hamburgerMenu = homePage.openHamburgerMenu();

		// vado in conti e carte
		this.contiPage = hamburgerMenu.gotoContiCarte();
		// seleziono il bonifico
		this.bonifico = this.contiPage.navigateToBonifico(b);
		
		

		// effettuo il bonifico
		this.thankYouPage = this.bonifico.sendBonifico(b);
	}

	@And("^Viene effettuato un disonboarding dei prodotti$")
	public void viene_effettuato_un_disonboarding_dei_prodotti() throws Throwable {

	}

	@And("^Viene controllato che la transazione sia stata registrata nella lista movimenti$")
	public void viene_controllato_che_la_transazione_sia_stata_registrata_nella_lista_movimenti(
			List<TransactionMovementsBean> table) throws Throwable {
		TransactionMovementsBean b = table.get(0);

		//WaitManager.get().waitMediumTime();

		this.homePage =(HomePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("HomePage", driver.getClass(), null);
		homePage.setUserHeader(userHeader);
		homePage.checkPage();
		//this.homePage.get();

		WaitManager.get().waitMediumTime();

		this.hamburgerMenu = this.homePage.openHamburgerMenu();

		this.contiPage = this.hamburgerMenu.gotoContiCarte();

		//		paymentMethod può essere "carta" se il pagamento avviene con carta o "conto" se il pagamento avviene con conto
		//		if (b.getPaymentMethod() == ("carta")) {
		//			this.contiPage.navigateToContoCarta(table.get(0).getConto());
		//		} else {
		//			this.contiPage.navigateToConto(table.get(0).getConto());
		//		}

		if(b.getConto().contains("conto"))
		{
			this.contiPage.navigateToConto(table.get(0).getConto());
		}
		else
			this.contiPage.navigateToCartaPostePay(table.get(0).getConto());


		this.contiPage.searchTransaction(table.get(0));

		this.contiPage.checkTransaction(table.get(0));

		this.hamburgerMenu = this.contiPage.openHamburgerMenu();

		this.homePage = this.hamburgerMenu.gotoHomePage(userHeader);

	}

	@And("^Controlla che il saldo sia stato decrementato da \"([^\"]*)\"$")
	public void controlla_che_il_saldo_sia_stato_decrementato_da_something(String arg,
			List<TransactionMovementsBean> table) throws Throwable {

		TransactionMovementsBean b = table.get(0);
		this.homePage = (HomePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("HomePage", driver.getClass(), null);
		homePage.setUserHeader(userHeader);
		homePage.checkPage();

		this.hamburgerMenu = this.homePage.openHamburgerMenu();

		this.contiPage = this.hamburgerMenu.gotoContiCarte();

		//		paymentMethod può essere "carta" se il pagamento avviene con carta o "conto" se il pagamento avviene con conto
		//		if (b.getPaymentMethod() == ("carta")) {
		//			this.contiPage.navigateToContoCarta(table.get(0).getConto());
		//		} else {
		//			this.contiPage.navigateToConto(table.get(0).getConto());
		//		}

		if(b.getConto().contains("conto"))
			this.contiPage.navigateToConto(table.get(0).getConto());
		else
			contiPage.navigateToCartaPostePay(table.get(0).getConto());

		WaitManager.get().waitShortTime();
		
		this.contiPage.checkSaldo(table.get(0));

		//this.contiPage.searchTransaction(table.get(0));

		//this.contiPage.checkTransaction(table.get(0));

		this.hamburgerMenu = this.contiPage.openHamburgerMenu();

		this.homePage = this.hamburgerMenu.gotoHomePage(userHeader);

	}

	@And("^L'utente clicca sul pulsante OK$")
	public void lutente_clicca_sul_pulsante_ok() throws Throwable {
		LoginPosteItPage p= (LoginPosteItPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("LoginPosteItPage", driver.getClass(), null);

		p.clickOnSpidButton();
		//WaitManager.get().waitShortTime();
	}

	// Click su pulsante accedi dopo che l'utente non ha effettuato nuvoamente il
	// login
	@And("^L'utente clicca sul pulsante Accedi$")
	public void lutente_clicca_sul_pulsante_accedi() throws Throwable {
		LoginPosteItPage p= (LoginPosteItPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("LoginPosteItPage", driver.getClass(), null);
		p.clickOnSignInButton();


		WaitManager.get().waitShortTime();

	}

	@And("^L'utente clicca su  non ho un account poste$")
	public void lutente_clicca_su_non_ho_un_account_poste() throws Throwable {

		WaitManager.get().waitMediumTime();
		LoginPosteItPage p= (LoginPosteItPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("LoginPosteItPage", driver.getClass(), null);

		p.clickOnNoAccountPoste();

		WaitManager.get().waitShortTime();

	}

	@And("^Viene aperto il browser per permettere la registrazione dell'account poste$")
	public void viene_aperto_il_browser_per_permettere_la_registrazione_dellaccount_poste() throws Throwable {

		WaitManager.get().waitFor(TimeUnit.SECONDS, 20);

		LoginPosteItPage p= (LoginPosteItPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("LoginPosteItPage", driver.getClass(), null);

		p.checkRegistrationPage();



	}

	@And("^Viene cliccato il pulsante dell'assistenza$")
	public void viene_cliccato_il_pulsante_dellassistenza() throws Throwable {
		this.homePage = (HomePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("HomePage", driver.getClass(), null);
		homePage.setUserHeader(userHeader);
		homePage.checkPage();
		WaitManager.get().waitMediumTime();
		System.out.println(homePage);

		homePage.gotoFaq();
	}

	@And("^Viene cliccato il pulsante chatta con noi$")
	public void viene_cliccato_il_pulsante_chatta_con_noi() throws Throwable {
		FAQPage faq =(FAQPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("FAQPage", driver.getClass(), null);
		faq.checkPage();
		faq.clickOnChatWithUsButton();
	}

	@And("^Viene inserito il numero e viene cliccato sul pulsante prenota chiamata$")
	public void viene_inserito_il_numero_e_viene_cliccato_sul_pulsante_prenota_chiamata(List<ChattaConNoiBean> b)
			throws Throwable {
		ChattaConNoiBean c = b.get(0);

		PosteChatPage chat = (PosteChatPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("PosteChatPage", driver.getClass(), null);
		chat.checkPage();

		chat.insertPhoneNumber(b.get(0));

		chat.clickOnRseserveCallButton();
	}

	@And("^viene cliccato il tasto termina e torna alla homepage$")
	public void viene_cliccato_il_tasto_termina_e_torna_alla_homepage() throws Throwable {

		PosteChatPage chat =  (PosteChatPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("PosteChatPage", driver.getClass(), null);
		chat.checkPage();

		chat.clickOnEndButton();
		WaitManager.get().waitMediumTime();

		FAQPage faq =(FAQPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("FAQPage", driver.getClass(), null);
		faq.checkPage();
		faq.clickOnXButton();

		HomePage homepage = (HomePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("HomePage", driver.getClass(), null);
		homePage.setUserHeader(userHeader);
		homePage.checkPage();

	}

	@And("^L'utente naviga alla sezione ScontiPoste$")
	public void lutente_naviga_alla_sezione_scontiposte() throws Throwable {
		//		        HomePage homepage = new HomePage(driver, driverType, userHeader).get();
		//		        
		//		        homepage.openHamburgerMenu();
		//		        this.hamburgerMenu.goToSales();

		this.homePage = (HomePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("HomePage", driver.getClass(), null);
		homePage.setUserHeader(userHeader);
		homePage.checkPage();

		this.hamburgerMenu = this.homePage.openHamburgerMenu();

		this.scontiPostePage = this.hamburgerMenu.goToSales();
	}

	@And("^Vengono visualizzati tutti i luoghi dove sono presenti attivita collegate$")
	public void vengono_visualizzati_tutti_i_luoghi_dove_sono_presenti_attivita_collegate() throws Throwable {

		if (this.scontiPostePage.searchForShopsOnTheMap()) {
			System.out.println("Shop found on the map");
		} else {
			System.out.println("Shop not found on the map");
		}

	}

	@And("^Vengono cliccate le impostazioni dal menu$")
	public void vengono_cliccate_le_impostazioni_dal_menu() throws Throwable {

		WaitManager.get().waitMediumTime();

		this.homePage = (HomePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("HomePage", driver.getClass(), null);
		homePage.setUserHeader(userHeader);
		homePage.checkPage();

		WaitManager.get().waitMediumTime();

		this.hamburgerMenu = this.homePage.openHamburgerMenu();
		this.settings = this.hamburgerMenu.gotoSettings();

	}

	@And("^Viene cliccato Accesso e autorizzazioni$")
	public void viene_cliccato_accesso_e_autorizzazioni() throws Throwable {

		WaitManager.get().waitShortTime();

		this.settings = (SettingsPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("SettingsPage", driver.getClass(), null);
		settings.checkPage();

		System.out.println(settings);

		settings.clickSettingAuth();


		// AccessAuthorizationPage accessAuthorizationPage = new
		// AccessAuthorizationPage(driver,driverType).get();

	}

	@Given("^l'utente clicca sul toggle abilitaDisabilita$")
	public void l_utente_clicca_sul_toggle_abilitaDisabilita() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		WaitManager.get().waitShortTime();

		AccessAuthorizationPage accessAuthorizationPage = (AccessAuthorizationPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("AccessAuthorizationPage", driver.getClass(), null);

		accessAuthorizationPage.checkPage();

		accessAuthorizationPage.clickOnToggle();

	}

	@And("^viene settato a \"([^\"]*)\" il toggle \"([^\"]*)\"$")
	public void viene_settato_a_something_il_toggle_something(String togleFlag, String togleField,List<CredentialAccessBean> table) throws Throwable {
		CredentialAccessBean b = table.get(0);


		if (togleField.equals(FINGERPRINT)) {
			AccessAuthorizationPage accessAuthorizationPage = (AccessAuthorizationPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("AccessAuthorizationPage", driver.getClass(), null);

			accessAuthorizationPage.checkPage();accessAuthorizationPage.toggleFingerPrint(togleFlag, table.get(0));

		}


	}

	@And("^viene inserito il posteId$")
	public void viene_inserito_il_posteid(List<CredentialAccessBean> table) throws Throwable {
		CredentialAccessBean b = table.get(0);

		InsertPosteIDPage posteId = (InsertPosteIDPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("InsertPosteIDPage", driver.getClass(), null);

		posteId.checkPage();

		posteId.insertPosteID(b.getPosteid());

		WaitManager.get().waitMediumTime();
	}

	@And("^viene inserito il fingerPrint$")
	public void viene_inserito_il_fingerprint(List<CredentialAccessBean> table) throws Throwable {
		CredentialAccessBean b = table.get(0);
		FingerPrintPopUp finger = (FingerPrintPopUp) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("FingerPrintPopUp", driver.getClass(), null);

		finger.checkPage();

		WaitManager.get().waitLongTime(); 
		finger.insertFingerPrint(b.getFingerId());

		WaitManager.get().waitLongTime();  


		AccessAuthorizationPage accessAuthorizationPage = (AccessAuthorizationPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("AccessAuthorizationPage", driver.getClass(), null);

		this.homePage = accessAuthorizationPage.gotoHomePage(userHeader);
	}

	@And("^viene effettuato il logout$")
	public void viene_effettuato_il_logout() throws Throwable {
		this.homePage.openHamburgerMenu().logout();
		WaitManager.get().waitLongTime();
	}

	@And("^viene effettuato il login con fingerPrint$")
	public void viene_effettuato_il_login_con_fingerprint(List<CredentialAccessBean> table) throws Throwable {
		CredentialAccessBean b = table.get(0);

		//if (b.getOnboarding().equals("si")) {

		try {
			NewLoginPreLoginPage p=(NewLoginPreLoginPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("NewLoginPreLoginPage", driver.getClass(), null);

			p.clickOnOkPopup();

		} catch (Exception e) {

		}
		NewLoginPreLoginPage newLogin = (NewLoginPreLoginPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("NewLoginPreLoginPage", driver.getClass(), null);

		newLogin.checkPage();

		newLogin.makeAnewLoginAccess();

		//	}else {

		//login senza onboarding

		//	}

		//WaitManager.get().waitHighTime();
		FingerPrintPopUp finger = (FingerPrintPopUp) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("FingerPrintPopUp", driver.getClass(), null);

		finger.checkPage();

		finger.insertFingerPrint(b.getFingerId());

		WaitManager.get().waitLongTime();

		this.homePage = (HomePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("HomePage", driver.getClass(), null);
		homePage.setUserHeader(userHeader);
		homePage.checkPage();
	}

	@And("^Naviga verso la sezione cerca Ufficio Postale$")
	public void naviga_verso_la_sezione_cerca_ufficio_postale() throws Throwable {
		//    	driver.setLocation(new Location(49, 123, 10));
		this.homePage = (HomePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("HomePage", driver.getClass(), null);
		homePage.setUserHeader(userHeader);
		homePage.checkPage();

		this.hamburgerMenu = this.homePage.openHamburgerMenu();

		this.searchPostalOfficePage = this.hamburgerMenu.goToSearchPostalOfficePage();
	}

	@And("^Naviga verso la sezione cerca Ufficio Postale versione negativa$")
	public void naviga_verso_la_sezione_cerca_ufficio_postale_versione_negativa() throws Throwable {
		((LocationContext) driver).setLocation(new Location(49, 123, 10));
		this.homePage = (HomePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("HomePage", driver.getClass(), null);
		homePage.setUserHeader(userHeader);
		homePage.checkPage();

		this.hamburgerMenu = this.homePage.openHamburgerMenu();

		this.searchPostalOfficePage = this.hamburgerMenu.goToSearchPostalOfficePage();
	}

	@And("^Controlla la presenza di un ufficio postale nelle vicinanze$")
	public void controlla_la_presenza_di_un_ufficio_postale_nelle_vicinanze() throws Throwable {

		if (this.searchPostalOfficePage.searchForPostalOfficeOnTheMap()) {
			System.out.println("Shop found on the map");
		} else {
			System.out.println("Shop not found on the map");
		}
	}

	@And("^Controlla la presenza di un ufficio postale nelle vicinanze versione negativa$")
	public void controlla_la_presenza_di_un_ufficio_postale_nelle_vicinanze_versione_negativa() throws Throwable {

		if (this.searchPostalOfficePage.searchForPostalOfficeOnTheMap()) {
			System.out.println("Shop found on the map");
		} else {
			System.out.println("Shop not found on the map");
		}
	}

	@And("^Naviga verso il Bollo Auto ed effettua il pagamento di un bollo auto$")
	public void naviga_verso_il_bollo_auto_ed_effettua_il_pagamento_di_un_bollo_auto(List<BolloAutoBean> bean)
			throws Throwable {
		BolloAutoBean b = bean.get(0);

		this.homePage = (HomePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("HomePage", driver.getClass(), null);
		homePage.setUserHeader(userHeader);
		homePage.checkPage();

		this.hamburgerMenu = this.homePage.openHamburgerMenu();
		this.contiPage = this.hamburgerMenu.gotoContiCarte();

		this.bolloAuto = this.contiPage.goToBolloAuto(b);

		//		Controllo che il tasto in alto a destra porti alla pagina dell'assistenza
		this.bolloAuto.checkAssistenzaPage();

		this.bolloAuto.calculateBollo(b);
		BolloAutoConfirmPage c = (BolloAutoConfirmPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("BolloAutoConfirmPage", driver.getClass(), null);
		c.checkPage();
		c.checkData(b);
		c.goToRiepilogo();
		BolloAutoRiepilogoPage z = (BolloAutoRiepilogoPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("BolloAutoRiepilogoPage", driver.getClass(), null);
		z.checkPage();
		z.checkData(b);

		InsertPosteIDPage p = (InsertPosteIDPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("InsertPosteIDPage", driver.getClass(), null);

		p.checkPage();

		p.insertPosteID(b.getPosteId());

		OKOperationPage t = (OKOperationPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("OKOperationPage", driver.getClass(), null);
		t.checkPage();
		t.closePage();
	}

	@And("^Viene controllato che la transazione sia stata registrata nella lista movimenti due$")
	public void viene_controllato_che_la_transazione_sia_stata_registrata_nella_lista_movimenti_due(
			List<TransactionMovementsBean> table) throws Throwable {
		TransactionMovementsBean b = table.get(0);

		WaitManager.get().waitMediumTime();

		this.homePage = (HomePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("HomePage", driver.getClass(), null);
		homePage.setUserHeader(userHeader);
		homePage.checkPage();

		this.hamburgerMenu = this.homePage.openHamburgerMenu();

		this.contiPage = this.hamburgerMenu.gotoContiCarte();
		this.contiPage.navigateToCartaPostePay(table.get(0).getConto());
		//   		if (b.getPaymentMethod() == "carta") {
		//   			this.contiPage.navigateToContoCarta(table.get(0).getConto());
		//   		} else {
		//   			this.contiPage.navigateToConto(table.get(0).getConto());
		//   		}

	}

	String movTitle;
	String movImport;
	String movDate;
	private String emu;
	private WebElement modifyObjective;
	private WebElement menu;
	private WebElement chiudi;
	private WebElement chiudiSalvadanaio;
	private WebElement closeObjectiveButton;
	private String cardelement;
	private WebElement availableAmount;
	private String availableAmount2;
	private WebElement availableAm;
	private String availableAm2;
	private LoginPosteItPage loginPosteIt;
	private CredentialAccessBean access;
	private LoginWithPosteidPage login;
	private boolean emulator;



	// Recupera il primo movimento utile della carta
	@And("^Viene cliccato il primo risultato disponibile$")
	public void viene_cliccato_il_primo_risultato_disponibile() throws Throwable {

//		this.contiPage=(ContiCartePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
//				.getPageManager("ContiCartePage", driver.getClass(), null);
		contiPage.clickOnFirstItemCarta();
	}

	@Given("^L utente verifica il dettaglio del movimento$")
	public void l_utente_verifica_il_dettaglio_del_movimento() throws Throwable {

//		ContiCartePage p=(ContiCartePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
//				.getPageManager("ContiCartePage", driver.getClass(), null);

		this.contiPage.checkMovimento();

	}

	@And("^Viene controllato che la transazione sia stata registrata nella lista movimenti tre$")
	public void viene_controllato_che_la_transazione_sia_stata_registrata_nella_lista_movimenti_tre(
			List<TransactionMovementsBean> table) throws Throwable {
		TransactionMovementsBean b = table.get(0);

		WaitManager.get().waitMediumTime();

		this.homePage = (HomePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("HomePage", driver.getClass(), null);
		homePage.setUserHeader(userHeader);
		homePage.checkPage();

		this.hamburgerMenu = this.homePage.openHamburgerMenu();

		this.contiPage = this.hamburgerMenu.gotoContiCarte();
		this.contiPage.navigateToConto(table.get(0).getConto());

		//     		if (b.getPaymentMethod() == "carta") {
		//     			this.contiPage.navigateToContoCarta(table.get(0).getConto());
		//     		} else {
		//     			this.contiPage.navigateToConto(table.get(0).getConto());
		//     		}

	}

	@And("^Viene cliccato il primo risultato disponibile su conto$")
	public void viene_cliccato_il_primo_risultato_disponibile_su_conto() throws Throwable {

//		ContiCartePage p=(ContiCartePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
//				.getPageManager("ContiCartePage", driver.getClass(), null);

		this.contiPage.clickOnFirstItemConto();

	}

	@And("^L utente verifica il dettaglio del movimento due$")
	public void l_utente_verifica_il_dettaglio_del_movimento_due() throws Throwable {
//		ContiCartePage p=(ContiCartePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
//				.getPageManager("ContiCartePage", driver.getClass(), null);

		this.contiPage.checkMovimento();
	}

	@Then("^viene mostrata la pagina di posteid$")
	public void viene_mostrata_la_pagina_di_posteid() throws Throwable {

		LoginWithPosteidPage login = null;
		login = (LoginWithPosteidPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("LoginWithPosteidPage", driver.getClass(), null);
		login.checkPage();
		WaitManager.get().waitMediumTime();

		//solo per nuova login
		NewLoginPreLoginPage n = (NewLoginPreLoginPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("NewLoginPreLoginPage", driver.getClass(), null);
		
		n.checkPage();
		n.makeAnewLogin();

		//		loginPosteIt = new LoginPosteItPage(driver, driverType).get();
		//		loginPosteIt.loginToTheApp(access.getUsername(), access.getPassword());


		//		LoginWithPosteidPage login = null;
		//		login = new LoginWithPosteidPage(driver, driverType).get();

		WaitManager.get().waitMediumTime(); 


	}

	@And("^viene messo il device in modalita offline$")
	public void viene_messa_il_device_in_modalita_offline() throws Throwable {


		WaitManager.get().waitHighTime();

		Properties pro = UIUtils.ui().getCurrentProperties();
		String emu = pro.getProperty("emulator");

		//		String emuName = pro.getProperty("android.device.name");
		//		System.out.println("disable wifi");
		//		ConnectionUtils.disableWiFiADB(emuName);
		//		System.out.println("disable data");
		//		ConnectionUtils.disableDataADB(emuName);
		//		//ConnectionUtils.disableWiFi((AndroidDriver<?>) driver);
		//		connectionsUp();
		//		WiFiSettings wifi = new WiFiSettings(driver,driverType);
		//		wifi.disableWIFI();
		if(this.driver instanceof AndroidDriver)
		{
			try {
				Properties p=UIUtils.ui().getCurrentProperties();
				AdbCommandPrompt c=new AdbCommandPrompt(p.getProperty("adb.path"), p.getProperty("ios.udid"));

				c.wifi(false);
				c.mobileData(false);
			} catch (Exception e) {
				// TODO: handle exception
			}catch(AssertionError err)
			{

			}
		}
		else if(this.driver instanceof IOSDriver) 
		{
			DeviceNative d=(DeviceNative) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("DeviceNative", driver.getClass(), null);
			
			WaitManager.get().waitMediumTime();
			//((InteractsWithApps) driver).runAppInBackground(Duration.ofSeconds(1));
			WaitManager.get().waitMediumTime();
			((InteractsWithApps) driver).activateApp("com.apple.Preferences");
			
			d.aereoModeON();
			d.setWifiOFF();
			d.clickOnBackFromWifiSetting();
			
			//((InteractsWithApps) driver).runAppInBackground(Duration.ofSeconds(1));
			
			((InteractsWithApps) driver).activateApp(pro.getProperty("ios.bundle.id"));
			WaitManager.get().waitShortTime();
		}

	}

	@And("^viene messa in background l'applicazione per quindici minuti$")
	public void viene_messa_in_background_lapplicazione_per_quindici_minuti() throws Throwable {
		
		if(driver instanceof AndroidDriver)
		{
			((AndroidDriver) driver).pressKey(new KeyEvent(AndroidKey.HOME));
		}
		else
		{
			((InteractsWithApps) driver).runAppInBackground(Duration.ofSeconds(-1));
		}

		System.out.println("start run in background");
		//    	((AppiumDriver)driver).runAppInBackground(Duration.ofMinutes(15));

		// try {t.interrupt();} catch(Exception err) {}
		try {
			for (int i = 0; i < 15; i++) {
				try {
					Thread.sleep(TimeUnit.MINUTES.toMillis(1));
				} catch (Exception err) {
				}
				System.out.println("sono trascorsi " + (i + 1) + " minuti...");

				if(i == 6 || i == 12)
				{
					try {
						UIUtils.mobile().swipe((MobileDriver) driver, UIUtils.SCROLL_DIRECTION.RIGHT, 1000);
					} catch (Exception e) {
						// TODO: handle exception
					}
				}
			}
		} catch (Exception err) {

		}

		System.out.println("end run in background");
		//    	((AndroidDriver)driver).pressKey(new KeyEvent(AndroidKey.APP_SWITCH));
		//		try {Thread.sleep(3000);} catch (Exception err )  {}
		//		((AndroidDriver)driver).pressKey(new KeyEvent(AndroidKey.APP_SWITCH));
		//		((AppiumDriver)driver).runAppInBackground(Duration.ofSeconds(1));
	}

	@And("^viene fatto un resume dell'app$")
	public void viene_fatto_un_resume_dellapp(List<CredentialAccessBean> table) throws Throwable {
		// connectionsUp();

		CredentialAccessBean access = table.get(0);
		
		DeviceNative d=(DeviceNative) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("DeviceNative", driver.getClass(), null);;
		
		d.resumeApp(access);
		
		

		//    	AndroidDriver d=(AndroidDriver) driver;
		//    	Properties p=UIUtils.ui().getCurrentProperties();
		//    	d.activateApp(p.getProperty("android.app.package"));

		// connectionsUp();
		//		try {
		//
		//			UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.NewLoginPreLoginPageMolecola.OKPOPUPBUTTON.getLocator(driver))));
		//			this.okpopupbutton =driver.findElement(By.xpath(Locators.NewLoginPreLoginPageMolecola.OKPOPUPBUTTON.getLocator(driver)));
		//			this.okpopupbutton.click();
		//			WaitManager.get().waitHighTime();
		//
		//		} catch (Exception e) {
		//
		//		}
		//
		//
		//		if (access.getOnboarding().equals("nonseitu")) {
		//
		//			NewLoginPreLoginPage newLogin = new NewLoginPreLoginPage(driver, driverType);
		//			newLogin.get();
		//			newLogin.makeAnewLogin();
		//
		//		}else if(access.getOnboarding().equals("accedi")) {
		//
		//			NewLoginPreLoginPage newLogin = new NewLoginPreLoginPage(driver, driverType);
		//			newLogin.get();
		//			newLogin.makeAnewLoginAccess();
		//
		//		};	
		//

	}

	@And("^viene ripristinata la connessione$")
	public void viene_ripristinata_la_connessione() throws Throwable {
		//		connectionsUp();
		//
		//		WiFiSettings wifi = new WiFiSettings(driver,driverType);
		//		wifi.enableWIFI();

		if(driver instanceof AndroidDriver)
		{
			try {
				Properties p=UIUtils.ui().getCurrentProperties();
				AdbCommandPrompt c=new AdbCommandPrompt(p.getProperty("adb.path"), p.getProperty("ios.udid"));

				c.wifi(true);
				c.mobileData(true);
			} catch (Exception e) {
				// TODO: handle exception
			}catch (AssertionError e)
			{

			}
		}
		else if(driver instanceof IOSDriver)
		{
			Properties pro=UIUtils.ui().getCurrentProperties();
			
			DeviceNative d=(DeviceNative) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("DeviceNative", driver.getClass(), null);
			
			WaitManager.get().waitMediumTime();
			((InteractsWithApps) driver).runAppInBackground(Duration.ofSeconds(-1));
			WaitManager.get().waitMediumTime();
			((InteractsWithApps) driver).activateApp("com.apple.Preferences");
			
			d.aereoModeOFF();
			d.setWifiON();
			d.clickOnBackFromWifiSetting();
			((InteractsWithApps) driver).runAppInBackground(Duration.ofSeconds(1));
			
			((InteractsWithApps) driver).activateApp(pro.getProperty("ios.bundle.id"));
			WaitManager.get().waitShortTime();
		}

	}

	@And("^viene effettuata una navigazione dell'app$")
	public void viene_effettuata_una_navigazione_dellapp() throws Throwable {
		homePage = (HomePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("HomePage", driver.getClass(), null);
		homePage.setUserHeader(userHeader);
		homePage.checkPage();
		try {
			WaitManager.get().waitLongTime();

			//homePage.clickOnOperazioniVeloci();


			homePage.clickOnOperazioniVeloci();

		} catch (Exception err) {

		}
	}

	@And("^Viene cliccato il pulsante contattaci$")
	public void viene_cliccato_il_pulsante_contattaci() throws Throwable {

		FAQPage faq = (FAQPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("FAQPage", driver.getClass(), null);
		faq.checkPage();
		faq.clickOnContactUsButton();
	}

	@And("^Vengono effettuati i controlli della pagina$")
	public void vengono_effettuati_i_controlli_della_pagina() throws Throwable {
		NumeriUtiliPage p =(NumeriUtiliPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("NumeriUtiliPage", driver.getClass(), null);
		p.checkPage();
		p.checkNumeriUtili();
	}

	@And("^Naviga verso la sezione Buoni e Libretti e seleziona il libretto smart$")
	public void naviga_verso_la_sezione_buoni_e_libretti_e_seleziona_il_libretto_smart(List<BonificoDataBean> bean)
			throws Throwable {

		BonificoDataBean b = bean.get(0);
		// this.girofondoData=b;

		// navigo verso la sezione conti e carte
		this.homePage =(HomePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("HomePage", driver.getClass(), null);
		homePage.setUserHeader(userHeader);
		homePage.checkPage();

		WaitManager.get().waitMediumTime();

		// Apro l'hamburgerMenu
		this.hamburgerMenu = homePage.openHamburgerMenu();

		// vado in conti e carte
		this.buonilibr = hamburgerMenu.goToBuoniLibretti();
		// seleziono il bonifico
		buonilibr.navigateToLibretto(b.getPayWith());

	}

	@And("^Viene controllato che la transazione sia stata registrata nella lista movimenti libretto$")
	public void viene_controllato_che_la_transazione_sia_stata_registrata_nella_lista_movimenti_libretto(
			List<TransactionMovementsBean> table) throws Throwable {

		//((AndroidDriver<?>) driver).pressKey(new KeyEvent(AndroidKey.BACK));
		TransactionMovementsBean b = table.get(0);

		this.buonilibr.searchTransaction(table.get(0));

		this.buonilibr.checkTransaction(table.get(0));

		this.hamburgerMenu = this.buonilibr.openHamburgerMenu();

		this.homePage = this.hamburgerMenu.gotoHomePage(userHeader);

	}

	// Controllo delle voci nell'hamburger menu
	@And("^L utente verifica le voci dell hamburger menu$")
	public void l_utente_verifica_le_voci_dell_hamburger_menu(List<CredentialAccessBean> bean) throws Throwable {

		HomepageHamburgerMenu home = (HomepageHamburgerMenu) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("HomepageHamburgerMenu", driver.getClass(), null);
		home.verifyMenu(bean);

	}

	@And("^Naviga verso la sezione conti e carte$")
	public void naviga_verso_la_sezione_conti_e_carte() throws Throwable {

		// navigo verso la sezione conti e carte
		this.homePage = (HomePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("HomePage", driver.getClass(), null);
		homePage.setUserHeader(userHeader);
		homePage.checkPage();

		// Apro l'hamburgerMenu
		WaitManager.get().waitMediumTime();

		this.hamburgerMenu = homePage.openHamburgerMenu();
		this.conti = hamburgerMenu.gotoContiCarte();

	}

	@And("^Clicca sul pulsante dettaglio del conto$")
	public void clicca_sul_pulsante_dettaglio_del_conto() throws Throwable {

		this.conti= (ContiCartePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("ContiCartePage", driver.getClass(), null);
		conti.checkPage();
		conti.clickOnDettaglio();

	}


	@And("^Clicca sul pulsante Ricariche automatiche attive$")
	public void clicca_sul_pulsante_ricariche_automatiche_attive() throws Throwable 
	{
		conti.clickOnRicaricheAutomaticheAttive();

	}

	@And("^Verifica dell eliminazione della ricarica$")
	public void verifica_dell_eliminazione_della_ricarica(List <CredentialAccessBean> b) throws Throwable {
		CredentialAccessBean s = b.get(0);

		RicaricheAutomatichePage acc= (RicaricheAutomatichePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("RicaricheAutomatichePage", driver.getClass(), null);
		
		acc.checkPage();

		acc.checkEliminazioneRicarica(s);
	}


	@And("^viene controllato il toggle acquistionline$")
	public void viene_controllato_il_toggle_acquistionline(List<CredentialAccessBean> bean) throws Throwable {
		CredentialAccessBean b = bean.get(0);
		
		WaitManager.get().waitHighTime();
		
		UIUtils.mobile().swipe((MobileDriver) driver, UIUtils.SCROLL_DIRECTION.DOWN, 0.5f, 0.8f, 0.5f, 0.5f, 1000);

		WaitManager.get().waitMediumTime();

		UIUtils.mobile().swipe((MobileDriver) driver, UIUtils.SCROLL_DIRECTION.DOWN, 0.5f, 0.8f, 0.5f, 0.5f, 1000);

		WaitManager.get().waitMediumTime();

		UIUtils.mobile().swipe((MobileDriver) driver, UIUtils.SCROLL_DIRECTION.DOWN, 0.5f, 0.8f, 0.5f, 0.5f, 1000);
		
		WaitManager.get().waitMediumTime();

		UIUtils.mobile().swipe((MobileDriver) driver, UIUtils.SCROLL_DIRECTION.DOWN, 0.5f, 0.8f, 0.5f, 0.5f, 1000);
		UIUtils.mobile().swipe((MobileDriver) driver, UIUtils.SCROLL_DIRECTION.DOWN, 0.5f, 0.8f, 0.5f, 0.5f, 1000);
		
		WaitManager.get().waitMediumTime();

		ContiCartePage p=(ContiCartePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("ContiCartePage", driver.getClass(), null);;
		
		p.checkToggleAcquistoOnLine(b);

	}


	@And("^Viene effettuata una ricarica postepay ricarica automatica$")
	public void viene_effettuata_una_ricarica_postepay_ricarica_automatica(List<RicaricaPostepayBean> t) throws Throwable {

		RicaricaPostepayBean b=t.get(0);


		homePage=(HomePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("HomePage", driver.getClass(), null);
		homePage.setUserHeader(userHeader);
		homePage.checkPage();

		//System.out.println(homePage.get());
		WaitManager.get().waitMediumTime();
		this.hamburgerMenu=homePage.openHamburgerMenu();
		this.contiPage=hamburgerMenu.gotoContiCarte();
		this.ricaricaPostepayPage=this.contiPage.navigateToRicaricaPostepay(b);

		this.thankYouPage=this.ricaricaPostepayPage.eseguiRicaricaAutomatica(b);
		this.thankYouPage.checkPage();

		org.springframework.util.Assert.isTrue(this.thankYouPage.checkSaveOperationButton());

		thankYouPage.closePage();

	}

	@And("^Viene effettuata una ricarica postepay ricarica automatica2$")
	public void viene_effettuata_una_ricarica_postepay_ricarica_automatica2(List<RicaricaPostepayBean> t) throws Throwable {

		RicaricaPostepayBean b=t.get(0);


		homePage=(HomePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("HomePage", driver.getClass(), null);
		homePage.setUserHeader(userHeader);
		homePage.checkPage();

		//System.out.println(homePage.get());
		WaitManager.get().waitMediumTime();
		this.hamburgerMenu=homePage.openHamburgerMenu();
		this.contiPage=hamburgerMenu.gotoContiCarte();
		this.ricaricaPostepayPage=this.contiPage.navigateToRicaricaPostepay(b);

		this.thankYouPage=this.ricaricaPostepayPage.eseguiRicaricaAutomatica2(b);
		this.thankYouPage.checkPage();

		org.springframework.util.Assert.isTrue(this.thankYouPage.checkSaveOperationButton());

		thankYouPage.closePage();

	}
	//	----------------------------------------------------------------------------------------
	//	                                    SALVADANAIO

	@And("^Naviga verso il salvadanaio$")
	public void naviga_verso_il_salvadanaio() throws Throwable {

		WaitManager.get().waitMediumTime();
		homePage=(HomePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("HomePage", driver.getClass(), null);
		homePage.setUserHeader(userHeader);
		homePage.checkPage();


		WaitManager.get().waitShortTime();
		this.hamburgerMenu=homePage.openHamburgerMenu();
		this.salvadanaio=hamburgerMenu.goToSalvadanaio();
	}

	@And("^Seleziona un obiettivo$")
	public void seleziona_un_obiettivo(List<SalvadanaioDataBean> bean) throws Throwable {
		SalvadanaioDataBean b = bean.get(0);
		if(b.getObjectiveName().equals("dinamico")){

			b.setObjectiveName(this.objName);

		}
		SalvadanaioPage p = (SalvadanaioPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("SalvadanaioPage", driver.getClass(), null);
		p.checkPage();

		p.chooseObjectiveTwo(b.getObjectiveName());		   
	}

	@And("^Naviga verso la sezione per effettuare un versamento$")
	public void naviga_verso_la_sezione_per_effettuare_un_versamento() throws Throwable {
		SalvadanaioInfoObjectivePage c = (SalvadanaioInfoObjectivePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("SalvadanaioInfoObjectivePage", driver.getClass(), null);
		c.checkPage();

		c.clickOnVersaButton();
	}

	@And("^Effettua i controlli sui campi$")
	public void effettua_i_controlli_sui_campi(List<SalvadanaioDataBean> bean) throws Throwable 
	{
		SalvadanaioDataBean b = bean.get(0);

		String versaType=b.getVersamentoType();

		if(versaType.equals("random"))
			versaType=this.versamentoType;

		switch(versaType)
		{
		case "le tue spese":
			VersaSuObiettivoLeTueSpesePage leTueSpese=(VersaSuObiettivoLeTueSpesePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("VersaSuObiettivoLeTueSpesePage", driver.getClass(), null);
			leTueSpese.checkPage();
			leTueSpese.checkFields(b);
			break;
		case "arrotondamento":
			SalvadanaioDepositArrotondamentoPage p=(SalvadanaioDepositArrotondamentoPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("SalvadanaioDepositArrotondamentoPage", driver.getClass(), null);
			p.checkPage();
			p.checkFields(b);
			break;
		default:
			SalvadanaioDepositPage c =(SalvadanaioDepositPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("SalvadanaioDepositPage", driver.getClass(), null);
			c.checkPage();
			c.checkFields(b.getPayWith());
		}
	}


	@And("^Effettua un accantonamento sull'obiettivo$")
	public void effettua_un_accantonamento_sullobiettivo(List<SalvadanaioDataBean> bean) throws Throwable {
		SalvadanaioDataBean b = bean.get(0);
		SalvadanaioDepositPage c = c =(SalvadanaioDepositPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("SalvadanaioDepositPage", driver.getClass(), null);
		c.checkPage();
		c.effettuaAccantonamento(b.getAmount());
		SalvadanaioConfirmPage v = (SalvadanaioConfirmPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("SalvadanaioConfirmPage", driver.getClass(), null);
		v.checkPage();

		v.checkData(b);	
		InsertPosteIDPage p = (InsertPosteIDPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("InsertPosteIDPage", driver.getClass(), null);
		p.checkPage();
		p.insertPosteId(b.getPosteid());
	}

	@And("^Effettua un accantonamento sull'obiettivo due$")	
	public void effettua_un_accantonamento_sullobiettivo_due(List<SalvadanaioDataBean> bean) throws Throwable {	
		SalvadanaioDataBean b = bean.get(0);
		SalvadanaioSelectVersamentoPage versa=(SalvadanaioSelectVersamentoPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("SalvadanaioSelectVersamentoPage", driver.getClass(), null);
		versa.checkPage();
		
		PayWithPage pay=null;
		String[] pw=null;

		switch(b.getVersamentoType())
		{
		case "singolo":
			versa.clickVersamentoSingolo();
			pay=(PayWithPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("PayWithPage", driver.getClass(), null);
			
			pay.checkPage();

			pw=b.getPayWith().split(";");

			pay.clickOnConto(b.getPayWith());

			WaitManager.get().waitMediumTime();
			break;
		case "ricorrente":
			versa.clickVersamentoRicorrente();
			pay=(PayWithPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("PayWithPage", driver.getClass(), null);
			
			pay.checkPage();

			pw=b.getPayWith().split(";");

			pay.clickOnConto(b.getPayWith());
			WaitManager.get().waitMediumTime();
			break;
		case "arrotondamento":
			versa.clickVersamentoArrotondamento();
			pay=(PayWithPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("PayWithPage", driver.getClass(), null);
			
			pay.checkPage();

			pw=b.getPayWith().split(";");

			pay.clickOnConto(b.getPayWith());
			WaitManager.get().waitMediumTime();
			break;
		case "le tue spese":
			versa.clickVersamentoLeTueSpese();
			WaitManager.get().waitMediumTime();
			//System.out.println(driver.getPageSource());
			//System.out.println(driver.getPageSource());
			break;
		}

		WaitManager.get().waitMediumTime();

		SalvadanaioDepositPage c = (SalvadanaioDepositPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("SalvadanaioDepositPage", driver.getClass(), null);
		
		c.checkPage();

		//c.effettuaAccantonamentoRicorrente(b.getAmount(), b.getPayWith(), b.getObjectiveName());
		c.effettuaAccantonamento(b.getAmount());
		SalvadanaioConfirmPage v = (SalvadanaioConfirmPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("SalvadanaioConfirmPage", driver.getClass(), null);
		v.checkPage();

		//v.checkData3(b);

		v.checkData(b);

		InsertPosteIDPage p = (InsertPosteIDPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("InsertPosteIDPage", driver.getClass(), null);
		p.checkPage();
		p.insertPosteId(b.getPosteid());

		OKOperationPage g = (OKOperationPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("OKOperationPage", driver.getClass(), null);
		g.checkPage();
		g.closePage();

		WaitManager.get().waitShortTime();

		//		SalvadanaioDepositPage c = new SalvadanaioDepositPage(driver, driverType).get();		
		//		c.effettuaAccantonamento(b.getAmount());	
		//		SalvadanaioConfirmPage v = new SalvadanaioConfirmPage(driver, driverType);	
		//		v.get();	
		//
		//		v.checkData(b);		
		//		InsertPosteIDPage p = new InsertPosteIDPage(driver, driverType);	
		//		p.get();	
		//		p.insertPosteId(b.getPosteid());	
		//		this.chiudi=driver.findElement(By.xpath(Locators.SalvadanaioPageLocator.CLOSEBUTTON.getLocator(driver)));	
		//		chiudi.click();			
	}

	@And("^Salva l accantonamento come operazione veloce$")
	public void salva_l_accantonamento_come_operazione_veloce(List<QuickOperationCreationBean> bean) throws Throwable {
		QuickOperationCreationBean b = bean.get(0);
		OKOperationPage p =(OKOperationPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("OKOperationPage", driver.getClass(), null);
		p.checkPage();

		p.saveAsQuickOperation(b);

	}

	@And("^Naviga verso la sezione operazioni veloci$")
	public void naviga_verso_la_sezione_operazioni_veloci(List<QuickOperationCreationBean> bean) throws Throwable {
		QuickOperationCreationBean b = bean.get(0);

		SalvadanaioPage p =(SalvadanaioPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("SalvadanaioPage", driver.getClass(), null);
		p.checkPage();
		p.goToHomepage();

		this.homePage =(HomePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("HomePage", driver.getClass(), null);
		homePage.setUserHeader(userHeader);
		homePage.checkPage();
		this.quickPage = this.homePage.gotoOperazioniVeloci();

		// vado all'operazione veloce
		this.quickPage.gotoQuickOperationPage(b.getQuickOperationName2(), false);
		SalvadanaioConfirmPage v = (SalvadanaioConfirmPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("SalvadanaioConfirmPage", driver.getClass(), null);

		v.checkData2(b);	

		QuickOperationListPage z=(QuickOperationListPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("SalvadanaQuickOperationListPageioConfirmPage", driver.getClass(), null);
		z.checkPage();

		z.clickBack(userHeader);

	}

	@And("^Controlla che il versamento sia presente nella lista dei versamenti$")
	public void controlla_che_il_versamento_sia_presente_nella_lista_dei_versamenti(List<SalvadanaioDataBean> bean) throws Throwable {
		SalvadanaioDataBean b = bean.get(0);
		String f = b.getObjectiveName();
		this.homePage = (HomePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("HomePage", driver.getClass(), null);
		homePage.setUserHeader(userHeader);
		homePage.checkPage();

		this.hamburgerMenu=homePage.openHamburgerMenu();
		this.salvadanaio=hamburgerMenu.goToSalvadanaio();

		SalvadanaioPage p = (SalvadanaioPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("SalvadanaioPage", driver.getClass(), null);
		p.checkPage();

		p.chooseObjective(b.getObjectiveName());
		SalvadanaioInfoObjectivePage c = (SalvadanaioInfoObjectivePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("SalvadanaioInfoObjectivePage", driver.getClass(), null);

		c.clickOnVersamentiListButton();

		SalvadanaioVersamentiListPage v = (SalvadanaioVersamentiListPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("SalvadanaioVersamentiListPage", driver.getClass(), null);
		v.checkPage();

		v.checkVersamento(b);

	}

	@And("^Effettua un accantonamento ricorrente sull'obiettivo$")
	public void effettua_un_accantonamento_ricorrente_sullobiettivo(List<SalvadanaioDataBean> bean) throws Throwable {
		SalvadanaioDataBean b = bean.get(0);
		//		WaitManager.get().waitLongTime();
		//		System.out.println(driver.getPageSource());

		SalvadanaioSelectVersamentoPage versa=(SalvadanaioSelectVersamentoPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("SalvadanaioSelectVersamentoPage", driver.getClass(), null);
		
		versa.checkPage();

		versa.clickVersamentoRicorrente();

		PayWithPage pay=(PayWithPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("PayWithPage", driver.getClass(), null);
		
		pay.checkPage();

		pay.clickOnConto(b.getPayWith());

		WaitManager.get().waitMediumTime();

		SalvadanaioDepositPage c = (SalvadanaioDepositPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("SalvadanaioDepositPage", driver.getClass(), null);
		
		c.checkPage();

		c.effettuaAccantonamentoRicorrente(b.getAmount(), b.getPayWith(), b.getObjectiveName());
		SalvadanaioConfirmPage v = (SalvadanaioConfirmPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("SalvadanaioConfirmPage", driver.getClass(), null);
		v.checkPage();

		v.checkData3(b);

		InsertPosteIDPage p = (InsertPosteIDPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("InsertPosteIDPage", driver.getClass(), null);
		p.checkPage();
		p.insertPosteId(b.getPosteid());

		OKOperationPage g = (OKOperationPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("OKOperationPage", driver.getClass(), null);
		g.checkPage();
		g.closePage();

		WaitManager.get().waitShortTime();

		SalvadanaioPage q = (SalvadanaioPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("SalvadanaioPage", driver.getClass(), null);
		q.checkPage();
		q.goToHomepage();

	}

	@And("^Naviga verso la sezione dei versamenti ricorrenti$")
	public void naviga_verso_la_sezione_dei_versamenti_ricorrenti(List<SalvadanaioDataBean> bean) throws Throwable {
		SalvadanaioDataBean b = bean.get(0);
		SalvadanaioInfoObjectivePage c = (SalvadanaioInfoObjectivePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("SalvadanaioInfoObjectivePage", driver.getClass(), null);
		c.checkPage();

		c.goToModifyVersamentoRicorrente();	
		SalvadanaioGestisciVersamentoRicorrentePage n = (SalvadanaioGestisciVersamentoRicorrentePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("SalvadanaioGestisciVersamentoRicorrentePage", driver.getClass(), null);
		n.checkPage();
		n.checkData(b);
	}


	@And("^Modifica il versamento ricorrente$")
	public void modifica_il_versamento_ricorrente(List<SalvadanaioDataBean> bean) throws Throwable {
		SalvadanaioDataBean b = bean.get(0);
		SalvadanaioDepositPage p =(SalvadanaioDepositPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("SalvadanaioDepositPage", driver.getClass(), null);
		//		p.get();
		SalvadanaioGestisciVersamentoRicorrentePage n = (SalvadanaioGestisciVersamentoRicorrentePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("SalvadanaioGestisciVersamentoRicorrentePage", driver.getClass(), null);

		n.clickModificaElimina();

		p.modifyAccantonamentoRicorrente(b);

		SalvadanaioConfirmPage v = (SalvadanaioConfirmPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("SalvadanaioConfirmPage", driver.getClass(), null);;
		v.checkPage();
		v.checkData4(b);
		InsertPosteIDPage q = (InsertPosteIDPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("InsertPosteIDPage", driver.getClass(), null);
		
		q.checkPage();
		q.insertPosteID(b.getPosteid());
		OKOperationPage g = (OKOperationPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("OKOperationPage", driver.getClass(), null);
		g.checkPage();
		g.closePage();
		SalvadanaioPage z = (SalvadanaioPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("SalvadanaioPage", driver.getClass(), null);
		z.checkPage();
		z.goToHomepage();
	}


	@And("^Modifica il versamento ricorrente e controlla i campi$")
	public void modifica_il_versamento_ricorrente_e_controlla_i_campi(List<SalvadanaioDataBean> bean) throws Throwable {
		SalvadanaioDataBean b = bean.get(0);
		SalvadanaioDepositPage p = (SalvadanaioDepositPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("SalvadanaioDepositPage", driver.getClass(), null);
		p.checkPage();

		p.modifyAccantonamentoRicorrenteDeleteAccantonamento(b);
		InsertPosteIDPage q = (InsertPosteIDPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("InsertPosteIDPage", driver.getClass(), null);
		q.checkPage();
		q.insertPosteID(b.getPosteid());
		OKOperationPage g = (OKOperationPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("OKOperationPage", driver.getClass(), null);
		g.checkPage();
		g.closePage();
		SalvadanaioPage z = (SalvadanaioPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("SalvadanaioPage", driver.getClass(), null);
		z.checkPage();
		//		z.chooseObjective(b.getObjectiveName());
		z.checkCronometro(b.getObjectiveName());
		//z.get();
		z.goToHomepage();
	}

	@And("^Viene controllata l assenza del simbolo del cronometro$")
	public void viene_controllata_l_assenza_del_simbolo_del_cronometro() throws Throwable {
		HomePage p =(HomePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("HomePage", driver.getClass(), null);
		homePage.setUserHeader(userHeader);
		homePage.checkPage();
		this.hamburgerMenu=homePage.openHamburgerMenu();
		this.salvadanaio=hamburgerMenu.goToSalvadanaio();


	}



	@And("^Crea un obiettivo$")
	public void crea_un_obiettivo(List<SalvadanaioDataBean> b) throws Throwable 
	{
		Properties pp=new Properties();
		pp.load(RegressionStep.class.getClassLoader().getResourceAsStream("texts/texts.properties"));
		SalvadanaioPage p = (SalvadanaioPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("SalvadanaioPage", driver.getClass(), pp);
		
		p.checkPage();
		p.createNewTargetButton();
		p.insertImporto(b);
		p.insertData(b);
		p.insertNome(b);
		SalvadanaioCreateObjRiepilogo pp1=p.checkRiepilogoCreazione(b);
		
		pp1.clickConferma();
		
		WaitManager.get().waitMediumTime();
		
//		WaitManager.get().waitHighTime();
//		Parser.parsePageSourceToUix(driver.getPageSource(), "SalvadanaioCreateObjRiepilogoPopup");
//		try {
//			byte[] bytes=((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
//			InputStream is = new ByteArrayInputStream(bytes);
//	        BufferedImage newBi = ImageIO.read(is);
//	        ImageIO.write(newBi, "png", new File("SalvadanaioCreateObjRiepilogoPopup.png"));
//		} catch (Exception e) {
//			// TODO: handle exception
//		}
//		p.insertImporto(b);
//		p.insertData(b);
//		p.insertNome(b);
//		p.controlloPopup();
	}
	
	@Then("^il popup di errore \"([^\"]*)\" viene mostrato$")
	public void il_popup_di_errore_viene_mostrato(String arg1) throws Throwable 
	{
		Properties pp=new Properties();
		pp.load(RegressionStep.class.getClassLoader().getResourceAsStream("texts/texts.properties"));
	    switch(arg1)
	    {
	    case "Importo Massimo Raggiunto":
	    	SalvadanaioCreateObjRiepilogo p = (SalvadanaioCreateObjRiepilogo) UiPageManager.get("it.poste.bancoposta.pages.managers")
			.getPageManager("SalvadanaioCreateObjRiepilogo", driver.getClass(), pp);
	    	
	    	p.controlloPopupImportoMassimo();
	    	break;
	    	default:
	    		throw new Exception(arg1+" non gestito nel metodo");
	    }
	}

	@And("^Viene cliccato il pulsante Crea nuovo obiettivo$")	
	public void viene_cliccato_il_pulsante_crea_nuovo_obiettivo(List <SalvadanaioDataBean> bean) throws Throwable {	
		SalvadanaioDataBean b = bean.get(0);	
		System.out.println(objName);	
		b.setObjectiveName(this.objName); 	

		SalvadanaioPage s =(SalvadanaioPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("SalvadanaioPage", driver.getClass(), null);	
		s.checkPage();	
		s.createObjective(b.getObjectiveName(),b.getPosteid(),b.getEvolutionNumber(),b.getOwner()); 	
	}

	@And("^Viene selezionato l obiettivo$")
	public void viene_selezionato_l_obiettivo(/*List <SalvadanaioDataBean> bean*/) throws Throwable {
		//SalvadanaioDataBean l = bean.get(0);
		CreateTargetPage c = (CreateTargetPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("CreateTargetPage", driver.getClass(), null);
		c.checkPage();

		c.selectObjective(); 	
		//DurationPage

	}

	@And("^Viene selezionata la durata e la data dell obiettivo$")
	public void viene_selezionata_la_durata_e_la_data_dell_obiettivo() throws Throwable {
		DurationTargetPage d =  (DurationTargetPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("DurationTargetPage", driver.getClass(), null);
		d.checkPage();

		d.durationAndDateSelection();
	}

	@And("^Viene inserito l importo$")
	public void viene_inserito_l_importo() throws Throwable {
		AmountTargetPage a = (AmountTargetPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("AmountTargetPage", driver.getClass(), null);
		a.checkPage();

		a.selectAmount(); 		   
	}

	@And("^Viene controllato il riepilogo e inserito il nome obiettivo$")	
	public void viene_controllato_il_riepilogo_e_inserito_il_nome_obiettivo(List <SalvadanaioDataBean> bean) throws Throwable {	
		SalvadanaioDataBean b = bean.get(0);	
		b.setObjectiveName(this.objName);	
		SummaryTargetPage s = (SummaryTargetPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("SummaryTargetPage", driver.getClass(), null);
		s.checkPage();	
		s.InsertObjectiveName(b.getObjectiveName());		  	
	}

	@And("^Si chiude la thank you page cliccando sul pulsante non ora$")	
	public void si_chiude_la_thank_you_page_cliccando_sul_pulsante_non_ora() throws Throwable {	
		ObjectiveCreatedPage o = (ObjectiveCreatedPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("ObjectiveCreatedPage", driver.getClass(), null);
		o.checkPage();	
		o.closeTyp();  	
	}

	@And("^controllo obiettivo creato$")	
	public void controllo_obiettivo_creato(List <SalvadanaioDataBean> bean) throws Throwable {	
		SalvadanaioDataBean b = bean.get(0);	
		b.setObjectiveName(this.objName);	
		SalvadanaioPage s = (SalvadanaioPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("SalvadanaioPage", driver.getClass(), null);
		s.checkPage();	
		s.checkObjective(b.getObjectiveName());  	
	}

	@And("^Modifica l obiettivo creato$")	
	public void modifica_l_obiettivo_creato(List <SalvadanaioDataBean> bean) throws Throwable {	
		SalvadanaioDataBean b = bean.get(0);	
		b.setObjectiveNameNew(this.objName+" 2");	
		SalvadanaioPage p= (SalvadanaioPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("SalvadanaioPage", driver.getClass(), null);
		p.checkModificaObiettivo(b);
	}

	@And("^controllo obiettivo creato due$")	
	public void controllo_obiettivo_creato_due(List <SalvadanaioDataBean> bean) throws Throwable {	
		SalvadanaioDataBean b = bean.get(0);	
		b.setObjectiveNameNew(this.objName+" 2");	
		SalvadanaioPage s = (SalvadanaioPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("SalvadanaioPage", driver.getClass(), null);
		s.checkPage();	
		s.checkObjectiveTwo(b.getObjectiveNameNew(),b.getImporto2());  		
	}

	@And("^viene chiuso l obiettivo$")	
	public void viene_chiuso_l_obiettivo(List <SalvadanaioDataBean> bean) throws Throwable {	
		SalvadanaioDataBean b = bean.get(0);	
		b.setObjectiveNameNew(this.objName+" 2"); 	
		SalvadanaioPage p = (SalvadanaioPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("SalvadanaioPage", driver.getClass(), null);
		p.checkPage();	
		p.chooseObjectiveTwo(b.getObjectiveNameNew());	

		SalvadanaioInfoObjectivePage c = (SalvadanaioInfoObjectivePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("SalvadanaioInfoObjectivePage", driver.getClass(), null);	
		c.checkPage();	
		c.closeObjective(b.getPosteid(),b.getEvolutionNumber(),b.getOwner());	
		p.checkPage();	

		try { 	
			p.chooseObjectiveTwo(b.getObjectiveNameNew());	
		} catch (Exception e) {	
			System.out.println("L'obiettivo è stato eliminato correttamente");	
		}	
	}

	@And("^viene chiuso l obiettivo2$")	
	public void viene_chiuso_l_obiettivo2(List <SalvadanaioDataBean> bean) throws Throwable {	
		SalvadanaioDataBean b = bean.get(0);	
		b.setObjectiveNameNew(this.objName); 	
		SalvadanaioPage p = (SalvadanaioPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("SalvadanaioPage", driver.getClass(), null);
		p.checkPage();	
		p.chooseObjectiveTwo(b.getObjectiveNameNew());	

		SalvadanaioInfoObjectivePage c = (SalvadanaioInfoObjectivePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("SalvadanaioInfoObjectivePage", driver.getClass(), null);	
		c.checkPage();	
		c.closeObjective(b.getPosteid(),b.getEvolutionNumber(),b.getOwner());	
		p.checkPage();	

		try { 	
			p.chooseObjectiveTwo(b.getObjectiveNameNew());	
		} catch (Exception e) {	
			System.out.println("L'obiettivo è stato eliminato correttamente");	
		}	

		p.clickChiudi();
	}

	@And("^Controllo schermata chiudi obiettivo$")	
	public void controllo_schermata_chiudi_obiettivo(List <SalvadanaioDataBean> bean) throws Throwable {	
		SalvadanaioDataBean b = bean.get(0);	
		b.setObjectiveNameNew(this.objName+" 2"); 	
		SalvadanaioInfoObjectivePage c = (SalvadanaioInfoObjectivePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("SalvadanaioInfoObjectivePage", driver.getClass(), null);	
		c.checkPage();	
		
		c.closeObiettivoV2(b);

		
	}	

	@And("^Vengono cliccati i pulsanti Annulla ed Assistenza$")	
	public void vengono_cliccati_i_pulsanti_annulla_ed_assistenza() throws Throwable {	
		CloseObjectivePage o = (CloseObjectivePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("CloseObjectivePage", driver.getClass(), null);	
		o.checkPage();	

		o.clicksOnButtons();	
	}

	@And("^Vengono cliccati i pulsanti Annulla ed Assistenza2$")	
	public void vengono_cliccati_i_pulsanti_annulla_ed_assistenza2() throws Throwable {	
		SalvadanaioConfirmPage s = (SalvadanaioConfirmPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("SalvadanaioConfirmPage", driver.getClass(), null);	
		s.checkPage();	

		s.clicksOnButtons2();	
	}	
	@And("^Viene selezionata la carta$")	
	public void viene_selezionata_la_carta(List <SalvadanaioDataBean> bean) throws Throwable {	
		SalvadanaioDataBean b = bean.get(0);	
		CloseObjectivePage o = (CloseObjectivePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("CloseObjectivePage", driver.getClass(), null);		
		o.checkPage();
		
		SelezionaCartaPage p=(SelezionaCartaPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("SelezionaCartaPage", driver.getClass(), null);
		
		p.clickOnFirstItem();		
	}

	@And("^Vengono controllati i campi nella pagina di riepilogo del chiudi obiettivo$")	
	public void vengono_controllati_i_campi_nella_pagina_di_riepilogo_del_chiudi_obiettivo(List <SalvadanaioDataBean> bean) throws Throwable {	
		SalvadanaioDataBean b = bean.get(0);	
		SalvadanaioConfirmPage s = (SalvadanaioConfirmPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("SalvadanaioConfirmPage", driver.getClass(), null);	
		s.checkPage();	

		s.checkData5(b);	

	}	


	@And("^Check importo disponibile$")	
	public void check_importo_disponibile(List <SalvadanaioDataBean> bean) throws Throwable {	
		SalvadanaioDataBean b = bean.get(0);	
		ContiCartePage c = (ContiCartePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("ContiCartePage", driver.getClass(), null);	
		c.checkPage();	
		c.navigateToCartaPostePay(b.getPayWith());	

		availableAmount2=c.getAvailableAmountCarta();
		
		

	}		
	@And("^Check importo disponibile2$") 	
	public void check_importo_disponibile2(List <SalvadanaioDataBean> bean) throws Throwable {	
		SalvadanaioDataBean b = bean.get(0);	
		ContiCartePage c = (ContiCartePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("ContiCartePage", driver.getClass(), null);	
		c.checkPage();	
		c.navigateToCartaPostePay(b.getPayWith());				
		//		this.record = (List<WebElement>) driver	
		//				.findElements(By.xpath(Locators.ContiCartePageLocator.RECORDSMOVIMENTI.getAndroidLocator()));	
		//		System.out.println(record);	
		//		if (this.record.size() >= 0) {	
		//	
		//			WebElement mov = this.record.get(0);	
		//			WebElement movTitle = mov	
		//					.findElement(By.xpath("//*[@resource-id='posteitaliane.posteapp.appbpol:id/tv_header1']"));	
		//			WebElement movImport = mov	
		//					.findElement(By.xpath("//*[@resource-id='posteitaliane.posteapp.appbpol:id/tv_amount']"));	
		//			WebElement movDate = mov	
		//					.findElement(By.xpath("//*[@resource-id='posteitaliane.posteapp.appbpol:id/tv_date']"));	
		//			this.movTitle2 = movTitle.getText();	
		//			System.out.println(movTitle2);				
		//			movImport2=movImport.getText().replace("+ €", "");				
		//			this.movDate2 = movDate.getText().trim();			
		//			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MMM/yyyy");  //modificare	
		//			LocalDateTime now = LocalDateTime.now();	
		//			String date2 = dtf.format(now); 	
		//			System.out.println(date2);	
		//			String dat = date2.replace("/05/", " MAG "); //cambiare		
		//			Assert.assertTrue(movTitle2.equals("ACCREDITO DA SALVADANAIO"));	
		//			System.out.println(movTitle2);	
		//		    Assert.assertTrue(movImport2.equals(b.getAmount2()));	
		//			System.out.println(movImport2 + " "+b.getAmount2());	
		//			System.out.println("L'importo dispomibile è stato incrementato correttamente di :"+ " " + movImport2 );	
		//			Assert.assertTrue(movDate2.equals(dat)); 	
		//			System.out.println(movDate2 + " "+ dat);	

		availableAm2 = c.getAvailableAmountCarta();	

		System.out.println(availableAmount2);	
		System.out.println(availableAm2);	

		Assert.assertTrue(availableAm2.equals(availableAmount2));  	
		System.out.println("L'importo disponibile è corretto:" + " " + "Saldo iniziale:"+ availableAmount2 + " " + "Saldo disponibile a seguito del rimborso: " + availableAm2); 	
		//}		


	}
	//SALVADANAIO
	//	  ---------------------------------------------------------------------------------------------------

	//    -----------------------------------ADB COMMANDS Domenico------------------------------------------------------------ 


	//Disinstallazione app
	@Given("^L app viene disinstallata$")
	public void lapp_viene_disinstallata(List<CredentialAccessBean> table) throws Throwable {
		CredentialAccessBean c = table.get(0); 

		Properties pro = UIUtils.ui().getProperties(c.getDevice());
		String adbPath = pro.getProperty("adb.path");

		//			Properties pro = UIUtils.ui().getCurrentProperties();
		//			String adbPath = pro.getProperty("adb.path");

		String deviceName = pro.getProperty("android.device.name");

		AdbCommandPrompt adb=new AdbCommandPrompt(adbPath);
		adb.setDeviceName(deviceName);

		String packageApp = pro.getProperty("android.app.package");

		adb.uninstall(packageApp); 
	}

	//installazione apk bancoposta
	@And("^L app viene installata$")
	public void l_app_viene_installata(List<CredentialAccessBean> table) throws Throwable {
		CredentialAccessBean c = table.get(0);

		Properties pro = UIUtils.ui().getProperties(c.getDevice());
		String adbPath = pro.getProperty("adb.path");

		String deviceName = pro.getProperty("android.device.name");

		AdbCommandPrompt adb=new AdbCommandPrompt(adbPath);
		adb.setDeviceName(deviceName);
		adb.install("C:/Users/dpica/Desktop/BancoPostaPFM_13.40.9-PROD.apk");


	}	


	@And("^L app viene aggiornata$")
	public void l_app_viene_aggiornata(List<CredentialAccessBean> table) throws Throwable {

		CredentialAccessBean c = table.get(0);

		Properties pro = UIUtils.ui().getProperties(c.getDevice());
		String adbPath = pro.getProperty("adb.path");

		String deviceName = pro.getProperty("android.device.name");

		String apk = "C:/Users/dpica/Desktop/BancoPostaPFM_13.40.9-PROD.apk";

		AdbCommandPrompt adb=new AdbCommandPrompt(adbPath);
		adb.setDeviceName(deviceName);
		adb.updateApp(apk);





	}


	//  --------------------------------------ADB COMMANDS Domenico------------------------------------------------------------ 

	//	------------------------------------------------------------------------------------------------------
	//	                                       PFM   On Hold causa mancanza tasto visualizzato






	@And("^controlla la presenza del bottone Vedi uscite non contabilizzate$")
	public void controlla_la_presenza_del_bottone_vedi_uscite_non_contabilizzate() throws Throwable {
		ContiCartePage p = (ContiCartePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("ContiCartePage", driver.getClass(), null);	
		p.checkPage();

		boolean presente=p.checkUsciteNonContabilizzateButton();

		if(presente)
		{
			DaContabilizzarePage g = (DaContabilizzarePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("DaContabilizzarePage", driver.getClass(), null);	
			g.checkPage();
		}



	}

	//-----------------------------------------------------------------------------------------------------------
	//    Ricarica PP da libretto

	@And("^Viene effettuata una ricarica postepay da libretto$")
	public void viene_effettuata_una_ricarica_postepay_da_libretto(List<VoucherVerificationBean> bean) throws Throwable {
		VoucherVerificationBean b = bean.get(0);

		this.homePage = (HomePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("HomePage", driver.getClass(), null);
		homePage.setUserHeader(userHeader);
		homePage.checkPage();
		// Apro l'hamburgerMenu
		this.hamburgerMenu = homePage.openHamburgerMenu();
		this.buonilibr = this.hamburgerMenu.goToBuoniLibretti();
		BuoniLibrettiPage p=(BuoniLibrettiPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("BuoniLibrettiPage", driver.getClass(), null);
		p.checkPage();
		p.navigateToLibretto(b.getPayWith());
		p.navigateToRicaricaLaTuaPostepay();



	}



	//-----------------------------------------------------------------------------------------------------------


public void startVideoRecording(String config, String idJira) {
		
		SimpleDateFormat format=new SimpleDateFormat("yyyyMMdd_hhmmss");
		
		Properties d=UIUtils.ui().getCurrentProperties();

		String emulator=d.getProperty("emulator");

		this.emulator = emulator != null && emulator.equals("true");

		if (this.emulator) {
			this.baseVideoNamePath=idJira+"-"+config.toUpperCase()
					+"_"+format.format(Calendar.getInstance().getTime())+".mp4";
		}else {
			this.baseVideoNamePath=idJira+"-"+config.toUpperCase();
		}
		
		System.out.println(this.baseVideoNamePath);
		
		
		if (this.emulator) {

			videoName=UIUtils.ui().videoRecoder(this.baseVideoNamePath,config);
			UIUtils.ui().startVideoRecording();

		}
		else
		{
			videoName=UIUtils.ui().mobileVideoRecorder(this.baseVideoNamePath,config);

			//				((AndroidDriver) d).context("NATIVE_APP");

			UIUtils.ui().startMobileVideoRecording((AppiumDriver<?>) UIUtils.ui().getCurrentDriver());

			//			    ((AndroidDriver) d).context("CHROMIUM");
		}
		
		//		Properties p = UIUtils.ui().getCurrentProperties();
		//		this.emu = p.getProperty("emulator");
		//		if (emu != null && emu.equals("true")) {
		//			SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd_hhmmss");
		//			String date = format.format(Calendar.getInstance().getTime());
		//			this.baseVideoNamePath += "_" + date +".mp4";
		//			videoName = UIUtils.ui().videoRecoder(this.baseVideoNamePath, config);
		//			UIUtils.ui().startVideoRecording();
		//		} else {
		//			videoName = UIUtils.ui().mobileVideoRecorder(this.baseVideoNamePath, config);
		//			this.baseVideoNamePath = videoName.substring(videoName.lastIndexOf('\\') + 1);
		//			UIUtils.ui().startMobileVideoRecording((AppiumDriver<?>) driver);
		//		}

//		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd_hhmmss");
//		String date = format.format(Calendar.getInstance().getTime());
//
//		this.baseVideoNamePath += "_" + date;
//		String workingDir = System.getProperty("user.dir");
//		StringBuilder sb = new StringBuilder();
//		sb.append(workingDir).append(File.separatorChar).append("target").append(File.separatorChar)
//		.append("embedded-videos");
//
//		String videoPath=sb.toString();
//
//		VideoOption o= null;
//
//
//
//		Properties p = UIUtils.ui().getCurrentProperties();
//
//		String emu = p.getProperty("emulator");
//
//		if (emu != null && emu.equals("true")){
//			o = new VideoOption(
//					VideoRecorderType.DESKTOP,//tipo di recorder DESKTOP o MOBILE 
//					this.baseVideoNamePath,//nome video senza estensione 
//					videoPath,//path della cartella dove salvare il video
//					true,//crop video -  ritaglia il video sulla base della properties video.size
//					true, //change frame rate - aggiusta il framerate sul video raccomandato per DESKTOP
//					UIUtils.ui().getCurrentProperties(),//properties del device 
//					driver,//web driver del device 
//					0//screenId vale solo per DESKTOP
//					);
//			dev0=VideoManager.get().createVideoRecorder(o);
//			dev0.init();
//			dev0.start();
//			videoName= videoPath + File.separatorChar + baseVideoNamePath + ".mp4" ;
//
//		} else {
//			o = new VideoOption(
//					VideoRecorderType.MOBILE,//tipo di recorder DESKTOP o MOBILE 
//					this.baseVideoNamePath,//nome video senza estensione 
//					videoPath,//path della cartella dove salvare il video
//					false,//crop video -  ritaglia il video sulla base della properties video.size
//					false, //change frame rate - aggiusta il framerate sul video raccomandato per DESKTOP
//					UIUtils.ui().getCurrentProperties(),//properties del device 
//					driver,//web driver del device 
//					Integer.parseInt(UIUtils.ui().getCurrentProperties().getProperty("screen.id"))//screenId vale solo per DESKTOP
//					);
//			dev0=VideoManager.get().createVideoRecorder(o);
//			dev0.init();
//			dev0.start();
//			videoName= videoPath + "\\" + baseVideoNamePath + ".mp4" ;
//
//			//			videoName = UIUtils.ui().mobileVideoRecorder(this.baseVideoNamePath, config);
//			//			UIUtils.ui().startMobileVideoRecording((AppiumDriver<?>) driver);
//
//		}
	}

	public void stopVideoRecording() {
		Properties p = UIUtils.ui().getCurrentProperties();
		String emu = p.getProperty("emulator");
		if (this.emulator) {
			WaitManager.get().waitMediumTime();
			UIUtils.ui().stopVideoRecording();
			String workingDir = System.getProperty("user.dir");
			workingDir = workingDir + File.separatorChar + "target" + File.separatorChar + "embedded-videos";

			Properties currentProperties=UIUtils.ui().getCurrentProperties();
			try {
				ExecFfmpeg.changeFrameRate(currentProperties, workingDir, this.baseVideoNamePath);
				ExecFfmpeg.cropVideo(currentProperties, workingDir, this.baseVideoNamePath);
			} catch (Exception e) {
				e.printStackTrace();
			}	
		}
		else
		{
			UIUtils.ui().stopMobileVideoRecording();
		}
	}

	@After
	public void runHook(Scenario scenario) {
		WaitManager.get().waitMediumTime();

		//if(access.getDebug() == null || access.getDebug().equals("n"))
		try {
			stopVideoRecording();
		} catch (Exception e) {
			// TODO: handle exception
		}

		Properties p = UIUtils.ui().getCurrentProperties();
		//----------------- ANNUNZIATA ----------------------------
		if(!concatVideo)
		{
			JSONObject obj = new JSONObject();
			obj.put("name", p.getProperty("device.name"));
			obj.put("os", p.getProperty("device.os"));
			obj.put("version", p.getProperty("device.version"));
			obj.put("brand", p.getProperty("device.brand"));
			obj.put("screen_size", p.getProperty("device.screen_size"));
			obj.put("resolution", p.getProperty("device.resolution"));
			obj.put("category", p.getProperty("device.category"));
			obj.put("icon", p.getProperty("device.icon"));
			obj.put("image", p.getProperty("device.image"));
			scenario.write("device:" + obj.toString());
		}
		else
		{
			JSONObject obj = new JSONObject();
			obj.put("name", devProp.getProperty("device.name"));
			obj.put("os", devProp.getProperty("device.os"));
			obj.put("version", devProp.getProperty("device.version"));
			obj.put("brand", devProp.getProperty("device.brand"));
			obj.put("screen_size", devProp.getProperty("device.screen_size"));
			obj.put("resolution", devProp.getProperty("device.resolution"));
			obj.put("category", "mixed smartphone - desktop");
			obj.put("icon", devProp.getProperty("device.icon"));
			obj.put("image",devProp.getProperty("device.image"));
			//			obj.put("image", "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEBLAEsAAD/4R4QRXhpZgAASUkqAAgAAAAFABoBBQABAAAASgAAABsBBQABAAAAUgAAACgBAwABAAAAAgAAADEBAgAMAAAAWgAAADIBAgAUAAAAZgAAAHoAAAAsAQAAAQAAACwBAAABAAAAR0lNUCAyLjEwLjQAMjAyMDowNToxNSAxMDozNDozOQAIAAABBAABAAAAAAEAAAEBBAABAAAAoAAAAAIBAwADAAAA4AAAAAMBAwABAAAABgAAAAYBAwABAAAABgAAABUBAwABAAAAAwAAAAECBAABAAAA5gAAAAICBAABAAAAIh0AAAAAAAAIAAgACAD/2P/gABBKRklGAAEBAAABAAEAAP/bAEMACAYGBwYFCAcHBwkJCAoMFA0MCwsMGRITDxQdGh8eHRocHCAkLicgIiwjHBwoNyksMDE0NDQfJzk9ODI8LjM0Mv/bAEMBCQkJDAsMGA0NGDIhHCEyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMv/AABEIAKABAAMBIgACEQEDEQH/xAAfAAABBQEBAQEBAQAAAAAAAAAAAQIDBAUGBwgJCgv/xAC1EAACAQMDAgQDBQUEBAAAAX0BAgMABBEFEiExQQYTUWEHInEUMoGRoQgjQrHBFVLR8CQzYnKCCQoWFxgZGiUmJygpKjQ1Njc4OTpDREVGR0hJSlNUVVZXWFlaY2RlZmdoaWpzdHV2d3h5eoOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4eLj5OXm5+jp6vHy8/T19vf4+fr/xAAfAQADAQEBAQEBAQEBAAAAAAAAAQIDBAUGBwgJCgv/xAC1EQACAQIEBAMEBwUEBAABAncAAQIDEQQFITEGEkFRB2FxEyIygQgUQpGhscEJIzNS8BVictEKFiQ04SXxFxgZGiYnKCkqNTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqCg4SFhoeIiYqSk5SVlpeYmZqio6Slpqeoqaqys7S1tre4ubrCw8TFxsfIycrS09TV1tfY2dri4+Tl5ufo6ery8/T19vf4+fr/2gAMAwEAAhEDEQA/APf6KKKACiiigAooooAKKKKACiiigArhPGvxW0LwRqMen3kVzc3boJGSBRhFPTJJHX2ru6+XvjFbJffFyW3l3BGhjyVODwmaunB1JKC3ZM5KEXJ7I74ftF+HP+gVqP8A45/jTh+0R4bP/MPvx9QteZXfw1srXQo9S+3SuWCkxYAxu9/x9Kf4e+GVjrsUznUWtip2RhyDvbGeemB055Ptwa3hhXOm6sZLlTs99/uMp11CapyTu1dbbfeemj9obwyf+XK9H4CnD9oTwuettdj/AID/APWrxI+FrEEjfNx/tD/Cj/hF7H+9N/30P8K6/wCx8T5fec39p0PM9u/4aC8Lf88Lr/vk/wCFKf2gvC3/ADwuj/wE/wCFeIf8IvY/3pv++h/hR/wjFj6zf99D/Cj+x8T5feH9p0PM9v8A+GgvC+P9Rdf98/8A1qT/AIaC8L/8+91/3yf8K8S/4Rix9Zv++v8A61H/AAjFj/02/wC+v/rUf2PifL7xf2nQ8z2z/hoPwx/z73P5H/Cj/hoLwxj/AI97j8j/APE14p/wi9h/02/76/8ArUf8IvYf9Nf++/8A61H9jYny+/8A4Af2pQ8z2o/tB+Gv+fa4/X/4mmn9oTw4OlpcH8//AImvGP8AhFrD0l/77pf+EWsPSX/vuj+xsT5ff/wBf2rh/M9ib9ojw+OmnXTfRv8A61RN+0Zog6aPdn/toP8ACvIx4VsPSX/vul/4RWw/uy/990f2PiO6+/8A4Af2rQ8z1dv2j9IH3dDuz/21X/Cmj9o/TCwH9gXO3PJ89eB+VeWDwpp/92X/AL7qnq/h6zstLmuIlcOgGCWz3FTPKq8IuTtp5lQzKjOSir6n114e16z8TaDaaxYF/s9yu5Q4wykHBB9wQRWnXn/wVbd8KtJz2Mo/8iNXoFeYegFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFfM3xUGfjS4/6YJ/6Lr6Zr5n+KGD8an9fIT/0XXTg/48PVfmYYr+BP0f5DZ1v20yMyRv8AZRgK3OParOiaZrV5BdnSoZHTZtm2PtyMHjqMn2GaZJfznSVsQXEJYMSehx2+gzVvRPEN3odvPFbN/rTnlQdpxjcMjg19NauqUlGMb307W8/M+c9pRdWLlJ2tr3vbp5HP7KNlWCmSTz+NASu65wc5X2Uvl1ZEdOEdFxe0Kvl0vl1bEXtT1gJ7Uri5ymIvanCH2q+tv7VKtt7UnMLtmaIPani39q1FtvapVtfaodQpJmSLbPani2PpWuLX2qQWvtUuoWkzHFr7Vl+J7bb4bvGx0Uf+hCuwFr7Vj+L7bb4S1A46Rj/0IVhiKl6Ul5P8jow8f3sfVfmenfBL/klel/78v/oxq9Crzz4I/wDJLNN/35f/AEM16HXyB9cFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFfM/xN5+NsvtAn/ouvpivmn4jru+OE4/6YL/6LFdOD/jw9V+Zz4v/AHefo/yNvVfEUN/4XsdMW32SQYB/eErwMZC44Jx69z61m6ReRWfmtLDFKMqwWQkZIz0wP/rfXs2W1UWsbqmCcYO05PHPt+VS2VpZtBcm6S6MoT9yYdu0N/t57fSvo6dOjUoyjytK+z9fyPllias60Z3s0tPkjMxmnAVbWzJqRbOu7mRxWbKYWpFjq+tgT0qVdPb0qHNFKnIoLGKmVKvppzelWE05vSodRGkaUjOSP2qdI/atFdOb0qdNOb0rJ1EaxoszljHpUyxD0rSTTj6VYTTj6Vm6qNo0WZSwj0qVYPatZdOPpUy6cfSs3VRoqDMdYPasTxrDjwZqhx0h/qK7hdOPpWB48singTWGx0tyf1FY1aqcGvI3o0WpxfmdB8Ef+SW6d/vy/wDoRr0OvOvgf/yS+w/66Sf+hV6LXzx9EFFFFABRRRQAUUUZoAKKTNGaAFoozRQAUUUUAFfOPjZY5Pj3cLK8SILdctK4Vf8AVDueK+jq+a/HUscXx3vGliilQQKNsqB1/wBUOxrWhf2sbb3Mq1vZy5trHYixs7S2huN8Sq4XEtwEW3kyM4RycMR7e9X7bTdIMZY3NteKBk/Z5htQ/rzXLx20Ajt5J9N05IJ+Y5JbSMIfx2+1dDodroOoWkrN4d01ljOwSx28Y3t/dA7mu7ExxMab9+zvv8/6Ry0HhZVFanpbb5b/AKmgmgaXdXUOby0jhC5PlThi/wBTjg/Srlr4Y0oX80i3CzwsuFidhhT68YJ/+vXJHVPDNvdQlNG0ueIjDD7EilB+WCalj1vwnNqMiy6Dp8doEBV/syEsfTG339e1aOOJt1MFLCp9DsIPCdlHbuHl8+ZmzuVggX6L/ifxrIv/AA9qcTEWUds425HmybWz6Y6H86x4tX8KNBIs/h7ThIrfKYLZAHH4rkdqwb8aXelhDaWtqm3AWOBfzJxmtKUMTfV/eZ1Z4W3+R2VjYarGxTU7N0h29bdlPzdieen0q1ZaRqtzLO0kFtbxFCIskna4xjIOCQeenSuF0uz0ezkE9xbQXYRcbJBgbieDjuB6GpYINIhvZ51t4XV4yqLJEpCOQMtjGOucDtmrlCq27ExqUElc7e307Vjp3mSW8KXSnbJGUDBhnqp3DtVa6GvQSuYdMhaHJC4BZsfTP9K5i3j0uKyS2KQ7lO4ymBWZjnpk9BTHsdHmnd8vGrHIRMYX86FConr+QOpSa0/M65brUjJI8WjO0KD7kqMjn3HY/QZNWVn1MIv/ABL7cMzZRSWBIPYjr+eK5P7Noctw0rWxG4DKIwVc+uMcVMlj4f2ANbyMxOSS/QegpOEu39feNVId/wCvuOrF7qQyDo8SHOCC2c/Q5x/KoXvdVAB/s7ABJOwgmsQW3h0kH7BgryMPnP1qP7Bou5SsAGPXBzUqD7f1943Nd/6+46C2v9RlDE2UispHyOm3cPY4rM8dahLL8OtbDWRjY25UgnkDj5vzqtHp2iqwJt0YZ6Moqp4uh0RfBWsCHT4Y5RaPsYKMggVnWho9C6M1zLU6L4HHPwwsvaaT+dej15t8Czn4Y2vtPJ/OvSa8g9cKKKKACiimO2KAMvWvEWnaDHG9/P5fmEhFAyWx1rF/4WR4exn7RJgdflH+Ncl4jeDX9duLy6vntbO2P2eB/s7yK3945HHJzWd/ZWkgnHiBQSc5Ni+f516VLBU3FObd35P/ACOCeLak0rW9V/md6vxK8Nk4+1sPqo/xqYfEHw8VDfbDj1215mPDmjt08Qxf+ATVFrWm28cJmi1SC7ckDy47XyvywMVq8BQ/mf3P/IyWOm+i+9f5nuFhqNtqdlHd2kolgkB2sO+Dj+Yq0DWRoFiNP0WytFGPKiUH645/WtYV478j0x9FIDTWbApjFLYr5i+JEmPjTfuDgiJOf+2a19JSzba+YfiPLn4vai3/AEzj/wDRa1vhv40fUwxKvRkvInuNTmmsYLZnn8qM5QO+VB74GKs6X4ivdMtZ4Ldn2SfMcBTsOMbhkHB9xiqGoaz9r0iztf8AR/3H9xWDdO5IxS6Trx0+wvbcLCfOXguCT9BgH9cV9BGXPC1SPX1PAjBqd72IvtB9aPtFZvn+9Hn+9bc5j7FGr9qOOOKPtbf3qyvP96Xz/ejnD2JrfbXwBu4pwv3H8VY/n0efRzh7E2xqL+tOGot61hefR5/vS5g9kzoF1Rx3qVdXYd65r7R70v2g+tHMg9lLudQusH+9U/8AbS7Fwzbud2en4VyH2g+tH2k+tK6GoSR2S65/tVT13WBN4e1CLd96Bx+lc19pPrVe/uC2n3C56xt/KoqW5H6F0oyU16nvPwHbPwzgHpcy/wAxXpteWfAY/wDFt4x/09S/0r1MV8ufUBRRRQAh6VjeIryW10ic24Zp5Bsj2gkgnqePQZrXc15V4x8U6e/iZ7G8a4a0tI9p+zgEmQkFs57AYH1rWjBzmkZVqihC7MxNLvZQts0syr0CyIyj17jFTJpl4P8ASPOnxCMbwG+UH0P+FYUGu6VbWM8ghuhcszCDcqlNp6ZJ7/QfSo4fEFok0Mcofyy4eXYoyPp/h0r2ued9Evu+88f2UHu39/3HRto0kKuskjKDgsCDz6H3qfTdKGoa7p9uJhLHGTNIOoCKRgYPTJI/I1gN4k0uW8fK3H2ULkNsBYt+mBiu5+HEDXNlcau6BTcMIouP4F/+uTXNiq0402npc2wtGm6vu9Du41wtOpQMDFLivIPXG01smpMUhFAGfdKdpNfLHxNvGs/ijqU4RXKiMYbp9xa+sZo96EV8/wDjb4X6l4j8ZX+pQXlvDDIVADgk5CgHp7iqi2ndCaTVmeZxeL5Yr6a7Om2LmWJ4/LePKLu7qOxH+NMTxXKjhjpmntjyuDCMHYc/+Pfxetdg3wT1UAn+1LU+2xqxP+FX6/8A88n/AO/L/wCFX7Sfch0oPoVJfGzS3Qm/sPSlAuGn2LBgHIxsP+yPT1rDOpsST5QH4104+GGu90cf9sH/AMKePhdrRAPz/wDgNKf/AGWiNScdmDpQe6OV/tNv+ef/AI9S/wBpt/zz/wDHq6sfC7WMfx/+Asv/AMTS/wDCq9ZP8bf+As3/AMTVe3qdxewp9jk/7Tb/AJ5/+PUf2o39w/8AfVdaPhVrH/PU/wDgLN/8TTv+FUav/wA9/wDyVm/+Jo+sVO4vYU+xyP8Aajf3D/31R/arf3D/AN9V13/CqNW/57/+Sk3/AMTR/wAKo1b/AJ7n/wABJv8A4mj6xU7h9Xp9jk01dkdW8rdg5wW4P1qyfEC+bvFhEF80ybN7Yx/d65x+tdEfhTq4/wCWxP8A26Tf/E0n/CqtX/56/wDkrN/8TR9Yqdw9hT7HOjxANpU2UfJB+8exJ9e+cfgKP7eX5P8AQkG05OHb5vY810B+Fmsjo5P/AG7Tf/E00/C7Wh3b/wABpf8A4mj29TuP2FPsYo8QxYcfYVXdjGHJ2/nnrS3HiGCe3uIhYKhlXapDfc+lb9t8J9auZxF5qR8E7pYZFH5lauN8F9cHS/sT+Lf4Ue3qPqHsKa1setfAQ5+HS+13J/SvVBXn3wj0K68OeEpNNvJInmS5Z/3ZyNpAx+ua9AFY9TUdRRRQBnavenT9LursLuaKMso9TjivnCa6vJluYn1bKzyM8oNq/LE5PO3NfTM9vHPG0cqB0bqCKxH8H6AxJOlW+T1wuK1pVXT2MqtGNRWkrng817eXcdtDNrUYhtiGiRrZxgj6R8/jUS3lw2pvfrrVvLORt8wQsR9MFMfpXY+MvD8U3jK10bS7PyFkgU5jQnqTkmvQbLwB4atkULpcbbVC5ZmP8zVrEcq5Ulb0E6Kk+ZvX1PErZ9VeKa1tdRhczF2dAhUsW4bqAMkV9CaDpqaVo1nYoOIIlUn1OOT+dR2vhfRLOVZYNNt0dTkHbnH51rgADis6lTn6Fxhyi0UUVkUHFITS1Uu9RsrIZuryCD/rrIF/nQMnJwCT2rxHxrrWqJrTLY6jLboke5kVsAsxz2I7EeterXHiPSJIJIoNTtJZmUhEjlDEnHbFeO67LLceIbwqxmRG2BVL/LgY7fQ00Ibp+oa4thFdnV5nc/MY3ZjkB8fLl/Y9QfpV+z1PxBeQXE51Tyz5zAI0ROAADwdw9fTFYtndKbiIzXGViB272ZgO+MY6E/zq/qEarLFFNAYIY5QDt5QHI3HGTzj0HamBs2Wqa5qF7cqt6sQG0ojw5GDnuGHp6Vbh1DXJr4wG5iTEIYboCQSNoPQjqTmsG+toprEmKxVbeXEhaNBtYkfKSM4/TvVlraxeyP2fT1ddmyWSCNAJMH+IZB6juOopDNpdS1sXtvbyTQjeSu/yMjPJ9Rxjjua0J7nWLXb5k1uw3AZEBxz07/1rndOt9Pjsdi6WJZ1Qh5IYowwUjlTyCffqPerGmQaerFI9K3T53xhIo1kjAPrkfoTSA6G4k1e0V5mltnjRSSot+D79c5698VOr6q8Ec4a02sobb5B6Ed+evToa5y2sbCC7JfSAFb5I4jFGHDdcjn29fwqVrWxjvfMl0Yhc/vFlhj3Ox98++eSKQHQW8up3VvHOhtQrZ+X7OfXHPJ9DRBNf3okMTW6eTNsf9xndgZxyTxz9eKxLi10+WZJTopXBDu0sMZ3KB0zuP0/rT7qys54dsGitG5wiSPFGVQZ/3j057UXA2zLfTyTW8ZtY5I8ZYQZIz9TUEtzqC3gtc2ocoXyLc9MgH+L39KzJLfTpoMDQmJUFV/dREKTz/eOPWmx2+miEW8mh+Y0fLBIoiCem7G7vj0FFxl6a61S38iF5LQvJ8qubY8kDP9729KZPdajCib5rcBn2hvIz16Z5H0rIW3sLSNba40ou8nz4WKIbvw3DpkjpVc2cAuJHXSsRTDCoscakDJyp+YZ4APei4Gjc3eqx7sXduwPrDgjj2YVh6hrerxKipepu2EFmhCjIPYhvQjr/APqW/wBPtEVXj0WLaQVKGOLcDxg9e/PesW8t1k0qRPsMSJDKsgiYRjOflJHP+7TQjufhvrl1eXlxb3s4llxgEADjqOn416WK8A8C3D6X4oQfZ/s8cgB2nAyR9Pqa99VgQCOhqmBJRRRSAKY60+kIyKAOM14XWl+JoNbt7N7uL7OYJoo/v7M53KO5B7elXLbx34bljy+qRQMPvJODGR9ciuhlt0mXa6gj+VVJtJt7j/XRxzD/AKbRq/8AMUgMqXx74Vi665aMT0Ebbz+QzUf/AAndhMQLCw1W+J6GCzfb+ZwK2ItJt4DmGOOE/wDTKJV/pU/2ND99pH/3nOPyHFAHOP4o1yU4g8NtCOzXl0iZ/wCAgk1E0vjS8X5JrC0z/wA8oGkx+Lla62OCKL/Vxon+6uKlC0AcU3hfX70g33iO8291hcRA/gq5/WsDU/hM13cPPDq0qyuQcuzNjA9SSf1r1XFMYc0AeSWPw3u/D2oW+p3WtG4jiY/uQGAYkH1OOOvSuEuby2nmnuBMA0kjNs3ep9a918WGdtOMNqMzsjtHg4+bGBz+NeS23hbxAZoVktPKUOC0nnLkDPXg1S2A52O4t9wyEI7jfXQwXUOrR3l1cy+X+9UfIVx824kDP+7xzUt34c8RS3tzKtqWWWRmB85emeMc1pHS9et7nI09LyP7MkWJJQVU7ACQCw5BzzQIzrWdp7kW7ybLeOHC+Xt+XbhQTkH2zVmKUwTQW8MjGBpAjEBQxBYnJ4IyMnpUr6RrkdhbxpazMfnLosq/Lk9Dlsdu1S2em60typNjNb4yRIJEwDj2Ynnp+NKwwnZrFJHtbkPn5mbCZI9PTHHXFXxC0EjTwXmZeU+YJlcH6c9O4rJa31S2AaXTJk54LTQjJ/F6cLi8yWewdiepNxAc/wDkSlYDYiea6QTG5xLGflDKnUp14A/vfpUsDzXhdJ7o7o2BxsTDEHPoMdu9ZQv7k436XM2BgZntzx/38qQ6jMVCtpE5A6DzrfA/8i0WYGorTsTbS3hHyhQ2xDng8Yx7U4y3NplBdkRsx2/IpwWYeoP96skalOFKjR59pOSPNt8f+jKVb2eU7V0+W2ABLM0sGHAB+X5XPU49qVgNVzdWUckguSQTkgKvLdM9D7U7ZLGJLlLvLsu5jtTLYHA6YH/16yF1GdOV0mQMOjCWAEf+RKfHPdgBodJkuSw3MEaE+WckYOX9s8ZHNFmMkZZrubzmuR5iMUB2qdu0sCOnv+lDzK+9ZbpwUIAPycHg56fhUFwmpyqs40acsXbfGzRbui4P38Y4PfPtVaW31e5ik3aRPGUAKozR/NzjaMOexJ5x0p8rAbeTKIpHF5K23BBGwdWA/u+9Y7SxSySwSTSzCWJlA3AEN95cceoFaUNlrbSuq6XPbCSJ495ePaMqQCcMT1xWcuneJIp4pYtJuVaNw2fNj59vv00hGDbXUNnqNrdefK5RwSpI4HfnFfR2kXIutLt5QedoB/Cvn/UPDXiI3dwIrCeWMykpL5q8jJx1b39K9i8BzXf9iJb38bR3KAFlYgnPQnj8Kp7AdcOlLTVNOqQCiiigApMUtFACYpcUUUAGKKKKACo3qSo3oA5/WH3agF7LGPzJP+AqlUt6/majcMOgYL+QAqIVS2AWikpaYBS0UYoA5/xU+2K192b+lc6stbfjFtqWX1f/ANlrmFkqHuBoCb3p4n96zhLThLUgaH2jHej7Rx1rP83NL5uKALjTkjg1reHJt1/KmesecfQj/GubMtaXh2fGuQLn7wZf0J/pTW4HbkUhp1Ia0AZTXRXGGUMPQinmkNAEH2eEDiJAP90Ve0h1t9QjAAVXypwPX/6+Kr4pASjq6nBU5FAHZqeafVeKQSRq69GAIqcGoAWiiigAooooAKKKKACiiigAqJzipajZdwoA5TYzyO2Vyzs3LDuTS+S/qn/fYq9ceGYZt4E0iq2cgEj+VYj/AA3s3Yn+09TXPZbuQf8As1O4F/yH9U/77H+NL5D+qf8AfYrN/wCFZ2f/AEFdV/8AAyT/AOKpf+FZ2X/QU1X/AMDJP/iqOYDSFu/qn/fY/wAaXyGH8Sf99j/Gs0fDSy/6Ceq/+Bkn/wAVSj4a2X/QT1T/AMDJP/iqOYCn4k0C81YW32WS3Xyt27zJQOuP8Kwl8EaqOtxZf9//AP61dV/wraz/AOgpqn/gZJ/8VS/8K4tP+gpqv/gZJ/8AFVIHL/8ACGaoP+W1l/3/AP8A61L/AMIZqn/Pay/7/wD/ANauoHw5tP8AoKar/wCBsn/xVOHw7tP+gpqv/gbJ/wDFUAcr/wAIZqn/AD1s/wDv9/8AWo/4QzVf+elp/wB/v/rV1X/Cu7Xtqmq/+Bsn/wAVR/wru2/6Cuq/+Bkn+NAHJnwXqx6Paf8Af7/61T6f4R1a11G3uGa22RyBm2y847/pXSf8K7t/+gtqv/gZJ/jR/wAK8g/6C2q/+Bb/AONAFz7LKei/+PCk+yT/APPM/nVP/hXkH/QX1T/wLf8AxpP+FeQ/9BfVP/Ap/wDGq5gLn2Sf/nmaPsdx/wA8m/Kqn/CvIf8AoL6p/wCBT/40h+HsfbWNUH/b0/8AjRzAWjZ3H/PF/wAqabW47wv+VQp4AVTn+19SP1uW/wAa17Lw5HZJt8+WU+srFj+tHMBe00SLYxrICGGRz6Z4q+pyKhht/KjC5JqYDFID/9n/4Q9aaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLwA8P3hwYWNrZXQgYmVnaW49Iu+7vyIgaWQ9Ilc1TTBNcENlaGlIenJlU3pOVGN6a2M5ZCI/PiA8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJYTVAgQ29yZSA0LjQuMC1FeGl2MiI+IDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+IDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiIHhtbG5zOmlwdGNFeHQ9Imh0dHA6Ly9pcHRjLm9yZy9zdGQvSXB0YzR4bXBFeHQvMjAwOC0wMi0yOS8iIHhtbG5zOnhtcE1NPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvbW0vIiB4bWxuczpzdEV2dD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlRXZlbnQjIiB4bWxuczpzdFJlZj0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlUmVmIyIgeG1sbnM6cGx1cz0iaHR0cDovL25zLnVzZXBsdXMub3JnL2xkZi94bXAvMS4wLyIgeG1sbnM6R0lNUD0iaHR0cDovL3d3dy5naW1wLm9yZy94bXAvIiB4bWxuczpkYz0iaHR0cDovL3B1cmwub3JnL2RjL2VsZW1lbnRzLzEuMS8iIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDo2NTkyNTdENjM2MEExMUU5QjUzMUE4MTIzRjU5REUyQyIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDowNGViNDIxMC0xMmZmLTRkMjktODcwMC0zYTA3NzQ4MTQ4OTUiIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDowMzlGMTk4QTUwMzFFOTExOUEzNEYzQkQ3NDg1MUQ1RSIgR0lNUDpBUEk9IjIuMCIgR0lNUDpQbGF0Zm9ybT0iV2luZG93cyIgR0lNUDpUaW1lU3RhbXA9IjE1ODk1MzE2NzkwOTQ5MjQiIEdJTVA6VmVyc2lvbj0iMi4xMC40IiBkYzpGb3JtYXQ9ImltYWdlL2pwZWciIHhtcDpDcmVhdG9yVG9vbD0iR0lNUCAyLjEwIj4gPGlwdGNFeHQ6TG9jYXRpb25DcmVhdGVkPiA8cmRmOkJhZy8+IDwvaXB0Y0V4dDpMb2NhdGlvbkNyZWF0ZWQ+IDxpcHRjRXh0OkxvY2F0aW9uU2hvd24+IDxyZGY6QmFnLz4gPC9pcHRjRXh0OkxvY2F0aW9uU2hvd24+IDxpcHRjRXh0OkFydHdvcmtPck9iamVjdD4gPHJkZjpCYWcvPiA8L2lwdGNFeHQ6QXJ0d29ya09yT2JqZWN0PiA8aXB0Y0V4dDpSZWdpc3RyeUlkPiA8cmRmOkJhZy8+IDwvaXB0Y0V4dDpSZWdpc3RyeUlkPiA8eG1wTU06SGlzdG9yeT4gPHJkZjpTZXE+IDxyZGY6bGkgc3RFdnQ6YWN0aW9uPSJzYXZlZCIgc3RFdnQ6Y2hhbmdlZD0iLyIgc3RFdnQ6aW5zdGFuY2VJRD0ieG1wLmlpZDpiZWUzNGQwMy0wOWQ2LTRmMTMtODFkMy0wMDBlYmMzY2FlMDYiIHN0RXZ0OnNvZnR3YXJlQWdlbnQ9IkdpbXAgMi4xMCAoV2luZG93cykiIHN0RXZ0OndoZW49IjIwMjAtMDUtMTVUMTA6MzQ6MzkiLz4gPC9yZGY6U2VxPiA8L3htcE1NOkhpc3Rvcnk+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOjAzOUYxOThBNTAzMUU5MTE5QTM0RjNCRDc0ODUxRDVFIiBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOjA2OUYxOThBNTAzMUU5MTE5QTM0RjNCRDc0ODUxRDVFIi8+IDxwbHVzOkltYWdlU3VwcGxpZXI+IDxyZGY6U2VxLz4gPC9wbHVzOkltYWdlU3VwcGxpZXI+IDxwbHVzOkltYWdlQ3JlYXRvcj4gPHJkZjpTZXEvPiA8L3BsdXM6SW1hZ2VDcmVhdG9yPiA8cGx1czpDb3B5cmlnaHRPd25lcj4gPHJkZjpTZXEvPiA8L3BsdXM6Q29weXJpZ2h0T3duZXI+IDxwbHVzOkxpY2Vuc29yPiA8cmRmOlNlcS8+IDwvcGx1czpMaWNlbnNvcj4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPD94cGFja2V0IGVuZD0idyI/Pv/tAEhQaG90b3Nob3AgMy4wADhCSU0EBAAAAAAALBwBWgADGyVHHAI3AAgyMDIwMDUxNRwCAAACAAIcAjwACzEwMzQzOS0xMDM0/9sAQwACAgICAgICAgICAwICAgMEAwICAwQFBAQEBAQFBgUFBQUFBQYGBwcIBwcGCQkKCgkJDAwMDAwMDAwMDAwMDAwM/9sAQwEDAwMFBAUJBgYJDQsJCw0PDg4ODg8PDAwMDAwPDwwMDAwMDA8MDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwM/8IAEQgBOQH0AwERAAIRAQMRAf/EAB0AAAEEAwEBAAAAAAAAAAAAAAABAgMGBAUHCAn/xAAcAQEBAQADAQEBAAAAAAAAAAAAAQIDBAUGBwj/2gAMAwEAAhADEAAAAffwAAAAAAAAAAAAAAAAAAAAAACCiAAAAAAAAgCAADwAAAAAAAAAAAAAAAAAAA+bNefgHkhIPh5OPHxIIFSDIWiGiq4YjICMjtgTFqAgMases0+tOXSwAAAAAAAAAAAAAAAAABh8TNyD3vHmzuDfFJNM1kCgVABaB1gKIFii2EKLSoQtKgOFHWVDxPY+qnS7XoiAAAAAAAAAAAAAAAAAAIz4mc2fQ/13zXcvyf8AR/MH9A/ifpb4L6qh+90OS+/4hRCAAoUsgLRIooqKKjhUWHKsOHyuHHCPgvt/pX4fsekoAAAAAAAAAAAAAAAAAAjPibz47P8Ap/51u+j3676nkbnrdvXc/Bj741EFAVAWFRRUUcio4dDh8PV0OV8PlerpXr56+F+2+jfiez6YgAAAAAAAAAAAAAAAAACM+JnNjt/6R+dXn5z7CgfY/Cdp+P8ArKd7ni0j1PMAhaWFRUUcio4VHSSD4kV+bLK5ZZZJXSySyK+akl80/FfafQzxfa9NwAAAAAAAAAAAAAAAAABGfE/lx3L9K/Muh/MfQ81+y+Y7T8p9bTPZ8Wl+l5aio4VHIqPRyOkeSSyyy5srUmbNNSyySySyTUksksk1JLJL5Y+Q+x99eN7XqCAAAAAAAAAAAAAAAAAAIj4nc3F3X9S/MLJ1uzWe30Zurs5o3fG4VHI5HI+R5JEs1NmzTUssudTTU0s2dTTUssk0+WWWSaklklkmvJXyv1vuvx/Z9RQAAAAAAAAAAAAAAAAABEfEzm4fUv3/AML3v5X6zyv99+c3v4/6LH9fiqHs+A6R6OR6PiWWbOppZpZc6mmsianzqfNyM7mmppZc6klkWWaklklklfNeQPmfqfb3j+v6jUAAAAAAAAAAAAAAAAACE+JnPxd8/UPy3L4ubD5+rseDs4vL101xuRySSSSzZ1PLLNS5ss1PNT51PnU+dTzU81PnU+dZE1LNTZss1LNSSyyyZvjf5z6X2l5Pr+pAAAAAAAAAAAAAAAAAACE+JnPxejvvfyfP+Uul/Qc9C833ah3fKg11ZFlynzuSWWallllnzvJzqfO55qfO8jNmm8jOsjOsjO8nOpZqbNmmps6mllzp6+J/B972B5fqepgAAAAAAAAAAAAAAAAACA+JvPw+mf0f8r3nkc2s9vizuDux8vVyMnSyzWTjeZjknlnzqfO9lxc7NYyM7nzqbO8jOp5rIzqfOps6nmp86nzZ86klkzZFfm+FPK9f1J0e96oAAAAAAAAAAAAAAAAAAID4n83H7U+v+Ttnn+ozm6u/8H28ns9ever5Nx6XdwuTDs6sfDun9/z+beh0cXV6N53f0/Lx7bj5NXy8di4OfC5OLUcvFaOt2IdZy8bkjZcXK25xeXixeTjnzrYcfJZun2vB3R9D051O56qgAAAAAAAAAAAAAAAAACA+JW8+zvT6u28r6LG9f5axfJfTnpefmer5e3miMvOs2MPedXqV3scGFyYzcauHV7Fd7HDuMa1PNxVvs9bX8mN3wc2dnTIx+Xiyc7h1x4fLxa/eN/1O088mef6Psjzu/wCrgAAAAAAAAEAAAUAAAAAMc+JXJj2j9b8xNjVp4ObqPm+hFZVO11rz0+1W+fizca3vFyc69LzeTen5ul3npXQ7+v3x7HOtLy8dl4uXA1jVcvHa+DmSzLxrLzrZ8fIwwObgj1l0bfi5bJ1e14s830/RfU7XrEAAAAAABogkIAoALShC0AAAYx8Q+bh9P/oHwfSODt0Fwdy+M+won0vz3MvV8Vo+Saakpbh5PN7Hh58Xl4Z5qbOnSuWSWaMqWeWWWaWbOppZZZpZpZJfLXh+76t8j2PWwAAAAAAhFGIQoopIPHEkKq04dQAgkYh8R+xwdy+9+AzM8mo5uvb+j6Nf7XTw+ThVHEkFNZCRRHjyRXwqvWQlZmJpqWWaXYcHZi5uCaJ5qWXgnzv0nsTwfoPXgoAAAAARlL5eLw79h8+s02XS8vBscbsHX7GluPb3yP0FzykJLX0CEcRGvPij2ODtn2Xwlg6Gqb7EvTsVbXDgZwAFAsFCKAoIooqLYo5FHI4eOFRxIvLfnPpvcHzv0XsEkoAAAAGmPHmD6Lw6x3utJrEdmLy8LpI7ee8HL78+M+q21jiSn2gwxow41VfF7s8Pa/q/jLl5XPzP6HrdRx3qFydXVzjAABQAAAAQVQQUBAUUAFsCh/P+/wC6/nvd9irNSgAAAhFGjuPIf1Ph4Xc8fI4eWb0OridTjyuPeX2+ynzHv+vvH9TM1AVXWhHGIa40Op8cebj659R85iTW05JruLr7F2cPHFr+Tm1nL1xC1UAFAKUBUAHq4AHDLAkJ4cU3xvY90eH63spZ6UAAAGmPHDfX8bhv1nyeTncsZXHyTyzSwcPP1X437LtfHqahAFQYsCa9app8ctZs3a6sM3t+RrOLrbrPbxs9XW659F2OIRQFCxQClRQADJl3WdxpHU8SrgazsYyiQrXX7Hunr9j2flNTqAABCOMCTy573j1Pu9S39Xta3l4s/G9lnVV7XVw/O9D154nqbUcFIIgIRmItNr4zVhAAAIKIKKKIIAAIACCiCgAogCgOFr6LZntGWSn0oAAwgjiPe6PMvU8yn9vrZWdanp9zc9npY3PjaeZjd9T6D1f5nfyxQAKBEaQrU6+YG08IVY1cBITk5Isioio4fDhwoggCEYgERGNGBW2qeoD2Zl66wfUmigAEMY0nhT3/AJjnfpeT0L5T7znH1vykm8b7wvY1Ho9R/HyezPnPe65x3IsAAAAYrUr1cg21tkoS4I+EJFckwhMr0WJbXQUogoiOWMIZYDVaQkVbqsqtZG7rtHCfUlAABjRzTWfBv0PkssyM6yo1/JmTNh1Nh0+z9H/E9DaWOQBVFVEYRglP1fClUGzrEtdLQPMuN7LgJYpdBZf83WLu0DNCGW5sQ1nxjJIuMQkUuq1Npm6izHNLWk3Keli5HurjubD6UAAxcuacmfInPjCs1i0vh3tUtW5tNT0Jx30FwbnQAVUACMaCc93rwxZqi/w6MqpjJjZw1cwyzZw+MwmHLmxiWbCMGtpLgWZUQ267NLMAkNMJWjMfc5tY7T31LcONJo4AAiiOIEYnnq64Rue9uK51ORSRAAAAGqwaBxvkvjOTKp0dRKHF0NabU2Wbq9L5hXKtcayN5ozNw9SwZaarBGit2cYwysvKtaZ+LVeSbPjsG5ULIdKNqVmvdC9k4klPoABBsREMnBbbInVpZoUWlQFQVhpapBz9eMalOMZe5anBJIjHTp81jyQ23zCn11mWmJblxMxNW/YVHS9ZVS3eoS4mpZcWobWvCobWfjuv1KLVtxa7uP47WOSV/crW5U7O/wB1684ZJbJSgAAIY0UE4LqeisrPmhpapxSa1Iw1UZhboupVKpdY9a/c5NGpR5sy7ywRrKtWbVLOny1Y3JsstJpbc2rWWmNbLPqZebodLDlodLNi6+ys1ssMXbc5tbsrnJNJJNtR6ute8eO7OJqUAABCKMQoGpzVavD43JcIuxtkkNwTjlaRI00Wr473OYJtpdQm8W3xrjfmuh8WBddG4J4zayMp1UVJVeYWpsco6wV2MaXSHLE00hhD9yopsq5tWefQGW9ccm1XAAAA0jhkQGMOJ0nAUcrh1KNIYRKft5B1amlwKnFxNAZkb40ptDLXBSaXISeWcniQbSQ4m0x5EWSo4ltriY5PWhjY6U5NpbR7ME9uL3Thktr6UAAAEEEhAABQFFpQEIYjkoPI0uiqoCooiqKAqczxarhKSEpIPHDoFajSIZXXeaT2oiDRsAhg0xWVkHaOCTrLSgAAAAAgAKAAAAIQRHHM+Rh6jlWiCFClAUU5BxqllLEg8kFHBAJTCIYdn55urWiIg2EEGjVbow6dxTd4s2jhQAAAAAAAAAAAaQZRHJuaPFHAoOAUQVFXifGqWUsSDhwooyCnjRow67yrlsg0aIiDYQQZayrfhdeJPUlKAAAAAAAAAAADSDLErkfLMsdIqraooAKKKcJ41QyePHwooCigNGEZ1PkdL2SmoiojRIQQQbbsDp/AlqalFAAAAAAAAAAAaQxXjn/LJYUVVpQFFFHCnn3jUzB5OPFpQgAQQYMOhbdn5DRES1oIg2EEGjbescLMifRRwAAAAAAAAAADSCSqVW9ppXU4cLC0o+HCDjnmWkiWJBB1KENpsIIR0w3lX3cjVogymAMRsRkdKdAxdtiZFr6cAAAAAAAAAAAwgipWc55JRM2m5sMiKo5FVYfTgHAOh1Oh9PAbIUK0RFAFVHKsSWPACddtF8sullolz4ntlpwAAAAAAAAAACDIjMWIaSFAcKJQOFEAIKUBRwggCCAADRAEHAMGQD6mH2ZEr6cOFAAAAAAAAAAABoDYaEADqUQcAUQBBoAAAAAAAAAAAAgkA6gQUBRT/8QAMhAAAAYBBAEDAgUEAwEBAAAAAAECAwQFBhESExQWBxUwNUAQICUyNiEjMTMXIiRQYP/aAAgBAQABBQL/AOjqQ1IakNyRuSNSP7/OPUO3Xa+W5CPLchHl+QjzDIB5lfjzS+Hmt6Czi+HnV8PO74ee3w88vh55ejzy8Hnl4PPLweeXY88vAWeXZDz26Hnd0POroed3Y85ux5xdg80vDB5fdmPK7oHlF0YPIrcwd7aGImQ28J7Ebly+oPvFf4yAi93KjmGDx6wSXscwJoJyz9imD2KWPYpY9ilj2KUPYpQ9ilD2KUPYpQ9ikj2KSPYZI9hkj2CSPYJI9gkD2CQPYJA9gkDx+QPH5A8fkDx98ePvjx58ePPjx14S4yob/pcrdiP3iv2331mg0KxtVpXXaDFlkzX5j1VWeg0Gg0Gg0Gg0Gg0Gg0Gg0Gg0G0bRtG0bRtGwbBsG0XxaWPpV/E/vFftvfrTCibW/MS63oIkpUQ3T5HNBoNBoNBoNBoNBoNBtG0bRtG0bRtGwbBsGwbBsGwcYyQtLP0o/in3iv23n1phvkU9ROoiiixqJNh39P7NO0Gg0Gg0Gg0Gg0Gg0G0bRsGwbBsGwbBsGwcY4xxjjHGMqLba+k/8AFvvFftvS/WmCVvkWUv27QUd09Wx72fKtJmg0Gg0Gg0Gg2jaNo2jaNg2DYNgJA2DYNg4xxjYNg2DjGYlpbekv8X+8X+27+ttNrdVNprOvbCW1qBoNJ6DT8NBoNBoNBtBJGwbBtGwEgbASBsGwbBsGwbBxjYNgzctLf0k/jH3i/wBlz9boJDEWxySdBRXCrfjNMXL0N2R+XQaDaNBoNARAiGgIgSRoCIbRtG0bRtG0bRtGelpb+kv8Z+8c/Zb/AFpBmk3H1uJDMhTJKPer8dARAkgiGg0GgIgSQRAkgkjQEkEkEQ2jaNo2jYNg2D1CLS39JP41945/rtfrTDO9xaEON7RR0PvIej8L3GY4jHECaHEOIxxGOIE0OEE0CaBNAmgTQJkEyCZBNDiHEOEcI4RwjiHqUjbcekn8b+8c/wBdn9aaL/u/q4jrrBQyBNEQ4hxgmwTYJoEwOAFHC4DjREwCjgo464KOCjgmBwgmQTAJgEyCZHCOEcQ4R6qtG3cekn8c+8c/1zj3XkWMbzrtc5DLRycuNVxmx1auOaqk50h7GE9OmpFMiXRy3pKqWtjx5NauOHbVtoMXDhqqUFaCysUQ017rk6FVWT056RKsYZzrZuIpORuEUqzjxmmb+EoIvmCke/xm3kX0FQLIG3AV23om41Ddop1TVo2kM2UZ4/ViSzJt/ST+O/eOf65Stl2zkJm69Y19SlnOH4pJytDyo+bMQ1t55BKQedQ1hGd1SQWfUw86ojLzTFt/meI7V5LgKx7/AIOEZLibTkfNsTQryXDFTJOU4rIbcyXEVrXZ4WoznYq2uRcYlKbTkONNx28mxzcu4w5ZqyHGHSK+xNKV22JrCLbHW3Xcjxl8RckoIi8/mQ5tj6R/x/7x3/W+rbdqs5TgTYyhFtJKFQrOJOKQ+tp1V2qDLXdQUxaq7YslzL5ECSqfAlRZttFJb5SZKY9e7upDgQBasImoqF9CJVRjiPye5PFjFamqTTNmUhuNKZbrqhKWaupU4iBV8xRqYh0KptKokPaiGykFCZDLMdISuER+rJI7/pF9A+bUa/K7/rkn+rQ3EHKvWoEJuTdPSiqDNugyOV0LVUxbizmL0ZnPRyOe8tPcXt7Q7YTKUYcsFKQUswUox21DtqHcMdwx2wUsFMBTB2x2x2x2x2x2x2h6iOckj0h+hfIp5tA7LA7DA52RytjkQNyfw1+B0/7cn6q0tslvOxVJ3CvtnICJD/K9vG8bwThA3RyDkHKY5RymOcwT5jsGOcc45x2QUoFKHZHaDb7Zt9sFLHbHaIZo5yO+kB/onxGLyyKrre9ZyQT1mbinbNKO1NDEu1dW4/bMOnbT0HQuSV1BHr+bUGYeV/bf+pxWXJb1jUSa5G4VtJIsWpLK4r24bhuG4bhuG4bhuG4bxvG8bxvG8cg5ByDlHMOYc5jnHYMdgxkbnIr0gP8ARi+Ixl7z1rYtYlPUXh1luViNoZeHWwTiFyhTmKXSlMxVOT2UcbZf4/KZhSg+v+279Rr33mpd1ZWUtncKa4mxY0ySt+RuG4bhuG4bhuG4bhvG4bxvG8bxvG8bxvG8bxvG8bxvG8XR6n6QfSC+ExOlJhxY7DM1ch54pLjr6GmXpC2Gps81HOsErclzDaxiEcu6IvzGDCxIV/bV/WwahS3Y7DUiXKVVWSZjTE1xpdXZIrZEGfFEpD8JzspHYSOwkdhI7CR2EjsIHYQOwgc6BzoHOgc6BzoBOko+T/tyanyBS9iuUhypCVbhoobF626VJHpAf6UXwmMtkqcIoTgKvd06CzBQXAUBwFAWJTBxY2GVyoden85kHCEv+jTq9kpu/ltNR7yVFlryu1XNayCwZaXl9s5US8rtpwsL+ZZvdwx3R3R3R3R3R3R3R3SHdId0h3SHdId1Ias1MOLyF1cxi/fjyPfHjbdv3XX28ikNExcqjh6/VJdXkJOP+Qo57ayKyc9HvphfAYdcS03Msorq+7AcVGc448WVHkyDmxZc9cxlbq53IchUma4y2TTZf4/OshOLRl//AHfDoNPtvR76d+cwYzW16VfEnY2yxVz6xc26tKw3O60huonQHRZWkTe/Oq2oeGMqsLNJfn0/AyE9vVtWC3Ulz/ji/H/HF+JmH20J7xqzHjVmCxi1MFilqY8RtgWH3A8Quh4bdmPCrweE3o8IvR4NfjwW/Hgt+PBcgHgt+PBcgHg1+PCL4eEXw8Ivx4XekPDrweH3Y8Ruh4jchnBMgfaPAcjIHguRpHpXVzqxj85gz0F1LbyG/sobMERsahuRIcVqXOt47MF2LR9liurymIs4KILWH1/Qpk/FZf0iSLKrhuLu6Zs03FMsSWMfktlW4wbaafG3EJp8cWGqDH3TKgxxQRjePrMsdoDMsaojHjNHr4zTDxanHi1SPF6geKVQ8WqB4pUjxWoHiVWDxSoIeJ1Q8VqCHitSPFqgeMU4PGqUgrHKNJttQ4ra+uglyYCBWy2UyvzmMosOhVnRSFH7A+FUT6zbxqWQXjsxBnRyeMqKekV1O+7btNk2gv8AHw26/wC3fpRItiZc3O1jkZLyn+lErnHoS0PR2KqM/Lb60thUJMxx02LJh9hViT/StDJwrGOoo9y6y4zZRhF96lN9SyYEZy5kL6VilRO3HYVBsVKkP3UdTsKykiR74whCLGWgmbiOw2uzmJRFsEGj3B8LgSmVOs2ihKRxxpDVk4nGnuGQw5ys/mMZFXvz2JGE2BOnh9iQl41LiR3m2222MZuSbRh10aUYbaKVjOOTYLhJ+O1UXYkmrsoa1XMsnnJD9ZN2LmOwmdk+ZHYcl1cRidZWBNdmMtNpYS3W488pibmzInlWcgis7GKFS7SQ03LsIIRZWTwQuzin7raqWpVnyLuLJKZJ2Uwe6XBI7dnNKM7bxmlWFm8hHuEV9y0sGVG7OaW5KtkodkWoQ09MYdfmCqddKxpXuWH+fQGQ0HqG8bVbjVY3azySNPmulLURKkoWuRKcMnJCjkPSEKcmpcERS2oT8tS1VSuFqVPbNcQ0OzHbmOaFzIkl5Fmw23IsYTrceYiCS7ivESW3HNd5ABzWXXV3cASJsWwNm6iEy/bwXm4tg1CSd5XEGZCIjp3tcg5ryJZ9+OQlvFYMOztSiPm5LeTJr1uOOqcxh8zL4DL8M/ZSdXjFVEg1qfgMyIO2dewJeZ4/ED3qVWay/UK7kn5dNUDylwWjvXxxudEQHp0NwylxhVvst1siYzIRUaFYvS47jbiSN05Lekhotlc8hDDrTZioUTRuEy6b39t5w2XSlJSg4mxcZxpHHVOpBoZBOEmeptpR2J8YSls0yjQhur1NCUNEGX9HHH0hx5vV55oWb2k5+QyosMnGbYL4DGQQkz4NRkMrGzhXdTMInWjBvMpDtxVMk/mmPsB/1Hq0n5nfTAbufTSXQW0o28HbcONhMJkN0BNpkYXCdJz03juGr03htJyWUuPDJxOnbkgpkke4zNlW6p2G6omiq/qi2YbQl/0U5X1zipjBupgQoMlh2AyTceHEIHXQ45IroTUmJXVnHIrYpmxW1ckSq2JIU3BgKUdXHJl6DAikxXRGiXU1zKIsKItxVXCeeTFrzeeahPpciVqRKbidl9mGkWMgn6xTcDbi0wmpERzljfAYWglpl1jbqH8Jpnwv021UXpswGsBxpkR8cxlgMROIdaUsFXRgltCBoNBoNBoLI9sPL1suyF1620NQYSqxqIl0qyDEmSqthdg3MiS6580x66Y21KWgmkmqcpZztspQLVLP/tMNNGTZJnBDD3MlqW2FImrI2XEq2TwaEoQXfMKjuKUk5bYVGmOpeZ4XU95YfaNKXieJ1xqSgTyimwpkzbilBdr0tpfXVOR49jRP8kf4dBoDaSodZkddkE2gvjt1f27ypuJNl7DfByktU03sF6KimtGnW6u/ZMqy7eXaQbRU5MK4IMxLVbkyPZpmcduCK2Ia2pAnrQh2bMdyyHesB3Z4708e4TgRzZDHamjtzh/7XGO/YkGFzJL5e5NIUqxMTY89p402Jg4U9+uXFszFVDn91yotWle22ZDF33SIF8mg0+E/wtFayPjuX3USSkPDsPDneHYdHO6Od0c7o53R2HRzujsOjsug5LwaXyNfnWT4Mnxo9q0bjZpPcX2x/hNVunfHeq/924ajcNw3DcN43jeN43gzG4Vqt8H469zfG+2P8FHvf+O+P9RJQ1Go1G4ajX8dRqNRuFEvdB+Opc/qC+1MOq2IZ/Z8d8f6lqNRqNRuG4bhuG4bhuG4ajG16o+OGvjkgvtTFkvZDQWifjvz/VOQEsEY1Go1/DUajUajUajGnP8A1fH/AIDS+Rv7UxaEa46WHFDqPjpvjqPjpvjpvjpvjpvjpPjpPjpPjpPiwxN6dK8HdBYS6Q8LeHhbw8LeHhbw8MfHhj48MkDwyQPDJA8LkjwqSK7GJMCT1JA6j46r46r46zw6zw67w67w67w4HhwPDgcENKkRwX2hi0accaWcoT3ciQo5+aEPcM2HuGbjv5uO/m47+bjv5sO9mw7uaju5oO9mg72ZDu5kO9mI7uYDuZeO5l47uXju5cO9lo7+Wjv5YO/lY7+Uj3DKR7jlI9zyke65UPd8qHu+VD3jKx73lZD3/KwV9lJhu5yNQhP2b4SS0hjcaAX2ugNlsx12R1mR1mR1mR12h1mh12h12h12h12h12h12h12h12h12h12h12xwNjgaHXaHXaHXaHXaHWaHWaHWaHWaHWaHVZHVZHVZHVZHUZHTZHUZHUZBMNkOJA0/8Awf8A/8QAPREAAQMCAwUDCQcEAgMAAAAAAAECEQMSBAUTFCExQVEVYXEQFiIwMjNAUoEGIEJQkaGxIzRg8CThkMHR/9oACAEDAQE/Af8AKJJ+5JPkkn8j4HbdCYhR2b0271a79Dt2h0UbndF3BHfodu0OinbtDop29Q6Kdu0Oinb1Hop29R6KdvUeinb1Hop29R6KdvUeinb1Hop2/R6Kdv0einb9L5VO36Xyqdv0vlU7fpfKp5wUvlU84KXyqecFL5VPOCl8qnnDS+VTzhpfKp5w0/lU84afyqecNP5VPOGn8qmExKYinenx7+Cn2Vajsxai9/8ABnVFEwVaflXyZPU0msVrZSVu5/Qz5aevLCfuySSSSSSSXFxcXFxJJJPkny5H/bJ4r8e/2VKNbSqXdOm4xOaurMVqud9XKpJhsTpTx+iwVX3OVSSSSSSSSSSSSS4uLiSSSfUZF/bfVfj6nsqYOiyviG03utRVhV6Ga/Z3Atw76mEquc6n7U8F8PJkf2c2+mr1X6Ga4DYqtnlkkkkkkkkkuJJ9VBBBkX9v9fj6nsqMZe6EKmYVaeCWgkb/AGlnepJlGZ18HStSLV74XqZrXqVql74JJJJJJLi4uLvVwQQQQQZH7j6/H1PYXwGtV7obxMRl9eglz27vI1jncEKjHU1hyQpJJJJJPlggggggggggtIIIIIIIMk9z9fj3+wvgpkmJp4fFtfU4GdZlQXCOZul3CP8Af+vJkWNZhF1VXeioqJ4cz7R5izHYrVb6iCCCBE8kEEEEEEEEEEEEGS+6Xx+PrewvgI+10lXFLUSFRPJQxS0kiEXxKj71kkn7kEfcgggRCCCCCCC0tLSC0tMn90vj8fX927wUcsD2af4pLjKcrTGNVyrEGMoaFVWeS1S0sLC0sLBGFhYWFhYWGmaZplhYWFhYWFhYZWkMXx+PxHu3eCjWI50O4D8FTa2WulfA2Z/RSjRfS9pbZFZKyaZpiMEYaZpGkJRH4RzOIlI0hKRpGkaRpGmaZpmmaZpmmaZYaZgWK1q/H1fYXwMPhb32sTepicpxGDS97VRPD/6QtVd6GC+z78RTuWIXqYzANwj7Hpy4ch2XsqulEgfl1JWKiIYXAtppDoVB+CudCIiNHYChELxK2XR7G8c1GpvmSi9vNDC4ZlbnvMQjGJCJ6Rh6SVGKqJ6Scigq1ltXco9KtP0bUK9RtJY4m29EKzmMROajMTSX8KjcUxH2o02xjXcBMXTXkbcjt0G1t3iYsZiFcu4biUTiU8Q1/AwlVKiLHx7uBl9dMJiGVmoktWTO/tG7MaGk5qIlyu58V8eQ2G8DBYuhs6MqLCopmmKZVrSzekQI6F4KK/xEd3Fxf3G43dB1Gkv4f2NnpJwRRKbUWd41zeSfsK2nfdCyPsckKn7DkprxT9h2FoLyU2KkizLipRpVE38esbxEYjY3r4iW85/3xFoUF/Co5tN3FFU06Meyo7CUV5OG4Sm1ZS4fSpv3uRSlTbSWWo4wLLbvj6nsqazhK7+pTrOQp10ePe7kptOm70h1dqNkoYhKvAfibHQLVYrZncYjMGtX0OI7Eq9N6lLfzMJXp097uJiajXpdO8wuIRjF3+kpRfprcu8fUqVUukrq2oqcjQTkpVqNeiIvEa2h3jKVBVG06N0kUDTotFYyIQRiJzNNOoxETiI9hhuC/H191N3gpqqitvduXj3IYuyhD6Szdy4p4oPzJ7uRktbDNpJUqNV09DNsUtGt6G7dw6G0uVZcOxT1lCnin00hqi4uovFRa7jUNQbUXkVMSqpaaheXqahqGoahqGqapqmqapqmqaxqmUvuR3x+J907wUpI270+A9lCPQRZLDC4lKLYlU8CvD3yk/U0zTNMRhYaZplpYpapCnpISoiqSXFxeXlxcUmtcxyq6FTh3lxcXFxkSyj/AKetrVEpsVy8hmYVXpO7ibZWuiB2NrIkwh2pV6DMzrOWEbvFx1ZFhzSlmT31EZHq8R7t3gWj6L6ftpBaUcK6pwHU7VgtLS0tLS0tLS0tLSwsLCwsLDTNM0zTLC1S1S1SDIOD/p63NqjqitoM4rvOzarfxNOzqszLf0HYCsqRKfudmV/m/kTLMSiyjk/VR2WYpd937qZLSc6q5zvw7vr6vEe7d4DaTnKlvErsruhavItMMtVEWwqIs7y0tLS0tLS0tLS0tLS0tLS0tLS0tLS0sLS0sMlSLvWOcjUlTLsLg8e91bE11prO5I5GNfbiVp0KznM6ruMRZToXNruWp05FOrUcyVqqilLE1nLveqD8TWa6EqKpUqVLPemU0NKgnVd/q6ySxfATBuiSngatZyMbvVTEZfUoOsfuUsVvMXLqtl68ClhHVHI1OKmJyyrh3Wvj+f4NnU2dTZ1NnU2dTZ1NnU2dxs7jQcbO40HGg40HGzuNncbO82d/QXDvNneaDjZ3GzvNneZXTVl0+szmsraVicXfwJReJhnp6QtBy8TReNoOTgbO5SnhXVHI1RqQnq3JKQJhY5lGitJ6PY6FQxNBcQ697lVRctavMWk5WWXbh2XoqQriphr4RV4JBsPebD3mw95sPebD3mw95sPebD3mw95sPebD3mwr1NhXqbCvUTBOTmLhnKsygmGeizJsz+ouGeqzKGz1OqDcK9vNB2Fe7oLhXzJsr5ncUaSsmfWYmrQqO37z/jrvSSnTY1oxKT3d4ui5/GP4F053f9C2LvlTBUucz+b4+tp0ljiu5CjTw7d7unCN8+JgNFarlqTbyMxfQVUSiizzLMIxu/8A9mA0nqq1EXuT/ZMwq0lf/RRU/f8AkqWtp/iky6nbSSeK7/zfOcX/AFGs5deSGGWrUqWqiR1QT7MUkhHOXvXkhWotbWsau6eK9xUZ6UJCp3GAy3CYlntuR0b93ot7l3mGy5Kqrv3IVcta1zUReIn5svAXBqu+TYu8XCKvMTAwbCvU2TdAmEcnMw2HVrpX8ogT1ylqlri1fJCkKQo1v+TT5V/MZ8s+qT42SfjI8sCf+MP/xAA9EQABAwIBCQUGBQQBBQAAAAAAAQIRAxIEBRATFCEwMUFRFkBSYXEGIjIzgaEVIEKRsTRQwfAjJGBwwuH/2gAIAQIBAT8B/wC6ILSCC0tLS0gggj+wxKwdm8REy39xPZ6svBzf3OzOJ6t/36C+zddOLm/udmcR1b/v0OzOI6tOzOI6tOzOI6tOzGI6odmMR1Q7MV/Eh2Yr+JDsxX8SHZiv4kOzFfxIdmK3iQ7MVvEh2YreJDsxW8SHZit4kOzFbxIdl6viQ7MVfGh2Yq+NDsxV8SHZir40OzFXxodmKvjQ7MVPGh2YqeNDsxU8aGNwi4Wro1WRe/J8afQ9r3uZkmqreifyh7NYhXZSw6NXntze02Hr4qtokfYkbFmP92ns2lduFtrLKpzTgvp5d0kkkkkkky//AFS+iC9+T5ifQq09JTt6/Uw2Tm0X3Ijfo1EX982JoaWOH1SSmlrUTukkkkkkkmX/AOp+iC9+Z8afQx2MTCYd1VdsIYX2jrJiadDFU7dL8Cp/nN7Q+1LcluRsf58+XkZJykmPoJUTfySSSSSSSXFxJJl3+o+gvfqfxp6mLpMq0FY/gqGCyBSbjW4io971YkMRUhG//c2Wck4fHVLnotyeU/UyZQp0KdjJ+uzdySSSSSSXEkklxcXEkmXPn/QXv1P409UFcjWyvAoY+hXWGOlc2IxtHD/MciFOq2o25iym4kkkkkkkkuLi4uJJJLi4kky1876C9+pfGnqhlfDvr4VzGcTI2T66YlroVETjObLOQ6+PxfGKdvH/ANUT+VMh4F+Dw+jf1/PJJJP5JJzSSSSSSSSSSZY+anoL36l8xvqgqSkFOgjFmVzVaCVOap6DW2pG8nNOec0kk5pJLi4yv8xPQXv1P5rfVDK2Ndg8K6q3in+dhkHL2KrYpKVV1yO8uHPlmyjlJMFb7t0lOqlRiO4SXFxcXlxcXFxeXl5cXFxcXF5eXl5eXl5eXl5lRZenoL36l81vqhXaj6atVJReXUybk2hhKlzKdqrzmTTJ1HYrwJcaWTSF5eK8vNIaQWqNxLX8DSmkNKaU0ppTSGkNIaQ0hpDSGkNIaQ0hjX3OTv8AT+YnqV8RY2567EKOUaWJW1DZSTYplD2kp4apYkynQwWUXY6npGdePBRuOWk33lkblJ6PRV4IYvGq/a2UUpY5GtlVVXfsgmPrucqt4dClj7vi2DWq7gqQVaPRTF1nUOUoYZrqiyq+7BiX6KoiKsNXmYmmlFLm8P8AeBTSlV965TD0XVUlZRPuah5lGk+oq8kH4Sqn6kHYN6suV0Govc3jtFwVVOLj8PVu241J2wXBjsMjU2j8Kq8CphnM4mKpOpqki9+b8SGKa6tSVi8zA5PXD1L55QPS7iZQwGJ1l1Sk1FRU6mScFUo0LakIsqotNVbEoJSROafuOp9FT9zR+n7mi8/uQ6OP3Pe6/cbXrJ+r7ms1uaoLVcqRsHsevxO+4j6iMslIKaPaso77jVqpwX7iYrEJzQ12quxUaU69amuyI6TsFV6unYnoo67lCfb+Bteun6kGuqN4KiC1K3iQbia6c2j8VUckLaMqVGbGqkFWo6qkOVpj33K3v9Pa9DQMNXp9CpQaqFXDLTGU2qm1DVdI33RMO660xGGWlxKeE0jZ59BKL2uiNpQwLlT3+Ayi1i7GlRyJyMZSqVdjeBhkWmtse7/kxVHSPTZ7qFeKjbUKbadLZaUFWmi80NZXoUkdTVVTgOq1vIfXrIgtWtbBdXNNWcJUfMqLUVeRpV6D1VeArHmJ4p3+h8xvqgymizam3l5ryKC1KzXMrNi3nwX0UZgWtMtLiNcbSa5GsXms+pk6glSl723bx6mhREhBKLR9Br+KCUGJyNE0sLBWIMoIiyWFhYhYhozRmjNGaI0RojRGiNEaI0RojKzYVu/gggje4f5rfVB6uj3eI2pWVffVIJMTh9Mu1Gr6lL3Wx/BcSSSSSSSSTnjPBBBBBBUc5rmoiSi8fItLS0tMuJCt3uHorWqIxOYuSqDNm3gLk7D2zKiZNw6rEqfgtDqo/JGHYkq6EG5Nw7my1+wr5JpU6avu4DuO6w/zW+pisU3D071MDlhMQ+xWwpI57W/EsC7CSSSSSSSSSSSSSSSSSSSSSSfyZd/QLvMiU20muxD+CbEPxqi7bDj8YpREOG5WoTwU/GcP4VFyzh1SFav7Dcr4VOX2Mt4hEoo1v6tu7w3zW+pjqLa1JWuWDJmAbTqXK6VzVH0diVeXAV6P2pw/NOaSSSSSSSSSSSSSSSSSSTLn6Rd21quWEMTWxGDY2jRp3JG1fMZcmHvfRS/wpBhUrPqRVosRvVB1Gkjk/wCNFT6GORlBsspI4wTG1mzUoo0bhqSO+WnqZXxGkqr5bBN1QWKjV8xceyYH4+mxtymHxrcQksKuUKTHWu4iZSpXWcxuVqDpVF4FLKVKr8JrbTW2mttNbaa201tprbTW2GtMNaYa0w1phrTDWmGtMNbpmtUzWqfU1qn1NaZ1NZZ1Nap9TWqfU1un1Mr1WvtgXd5Goo6revBP5FqtFxDV2GmanA0rB1Zq8TTtQqYlrGqqFV9y7tFhZNb8h+JvbaqbDDY5cOkMQqYhKj73JtErNvut2jHsZMN4lCslGYTaqyqqsqqmv+Rr3ka95GveRr3ka95GveRrydDXk6GvJ0NeToa8nQ15OhrydBca1eQmKbEQouKYqRBrTOgmKYiRCi4lnRR2LY7komKYnUTFMg1pkQVqqPiBd3haNem2EP8AqE2LaVHvc6OnTgVFqsbwSBNMxkKk/wAiaVE2wNuT9LTHVdkRG7T+xLusnUNLWSeCbSuuKc73Fjb9I/kxmmSmlkXczAMrIirVVI5F2Lc7ZH2Mpaw1qJQiecmFp1EYqv29OX8FDXX1NsR9Ij+TKdS6oqJvIILSO4zuoLSB26wKLQo3RtUpYhXj/amr71rEhF2cZUp1XLSR7k2xwTqU3y2VRU9TE5dWltRk7Yt/WvmiQYnH6JE2bV+xSygr2uVUiCo6V3aZ5/NsJQ2EoShKGwlDYShsJQlCUJQlCUJQlCUJQlC5CS4Xam5ptl0CYxE2Qa6nQTGInEXHoJjmqa2kmttXkYvEo5kJvG7iSM0EnEUmRYJFJORIkCLA2FJgRUVRYQuRR0IXZuKZmi7hiwppGmkaLUaXKaZDSIaRpUqIqQm8b+dSM1uaJIg2FopBsINhEiECJCiwWogqopYLCiJOZuZB+7QXgRv04Z4zQRnjNaoiCtLRUIIIILRELREFaWipJYRsFaMIEzu4btB28gRM67hBRB2ZeGZuZBczs3LM0gbxHccz0IE/Jy3aESRnggjPJJtIzTmX805pzK4kVS4kuEUuLhVLidhcNUuGu2jlL1HqXEi/kQdx3cySpcpJO7Tdrn2EkkkkkknLNMGyBDYuaRURM3IQTPU70380ECoWlpBaWlpYWFhYWFhYWlhYWlgjILCwVpYWkEZ3Js70zeOUlSSSVJUkklSSSVJUlRP7K3hvHb5N47j3lN47fN3j/wCyu3zN4u1O8JvXb5m9Xu7eOeSSSSSS4uLi4UgggjPBBBBBBAmwkkkkkkkkkkkkdx7umZSVJUuJJLiS4uUuLi4uLi4uLi4uLi4uLi8uLi4uLy8vLy8vLhHE/wDgP//EAFQQAAIBAgIDCQoLBAgFAwUAAAECAwARBBITITEFIjIzQVFx0dIUIzA0NWGRkqGxEEBCUnJzgZOiweEkQ2KDFSBjdIKUssIGUFOEoyVE02Ck4vDx/9oACAEBAAY/Av8AmO2ttba2itorUb/H8VgNy8U2CwOEcxXi1PIV1MS1eWMZ98/XXlfF/fP115VxR/nSdqvKWJ+/l7VeUMR9/L268en+/m7deOTH+fN268ak++m/+SvGX+9m7deMN97N2649vvJe3XjDfeS9uvGG+8k7Vce33knarjm+8k7Vcc33knarjD95J2q4z8Unarhg/wCJ+1W1fS/aranpbtVtjPrdqv3XobtVth9X9a4UXqCuNiHRGteML92vVXjI9ReqteL/AAr1V443oFa8ZJ6aXEYbdCeGVTcMrn2jYawO6M65Z5AVn5iy6iR0/HTWLy/KkJ9JoWMevznqq7BVHOb9VcKP0nqqy5GPMLn8q4UfpPVXDj9J6q4cfpPVXDj9J6q4cftrhx+2uMj9tcZH7a4xPbXGJ7a4xPbXGp7a41PbXGp7a41PbXHJ7a45PbXHJ7a45PbXHJ7a45PbXHJ7a45PbXHJ7a49PQa49PRXHr6KMDNmIsbisH/DJIPb8dborEfTrDE21A2v0ViwbcUfT8GFODgd5HmkO6U0S55AFG8S3Majkgi0MskCtjItlpNe0c9vjb/RWofNPL7/AI63RWI+sFRuQSF5Abe2mTLIL7LyE+z4HKGRWb5UblPdTvr3xvrNz6fjbfVrSf3iT8vjrdFYj6wUic9TYmMhlw/GjNs+BcfulPJFBM7JCIrfJ5WvXc6y6eGRBJh5edTz/G/5S0P7y/uHx1uipvpikykKec0+E73v+Plzgsw8w+BIJYYMbhlbSQq8mXIzCji8VkFxljijNwqjk+N/yV/Om/vT/wClfjrdFT/WLSRxqXd9SoNpNLLjMG8Eb7HOz4LqpI5TVmGU8x+Np9QvvNP/AHpv9K/HW6DU/wBYv5VDJiDkQo6CX5hZbBvsqVP6SXHtiIzHDCHzWuVIOrmty/BKmJbNHIwvhxqJ/izVCuCOaGCFI855bfG4f7uvvNP/AHk/6V+Ov0GpfrFpW+brrKVUa73C2+BgFRg23Mt6LWAvyD43h/7sP9Rp/wC8H/Svx1/omn+tWgp2WJtz2pyIBCYxe4a/wYn9sTCdzgHfi97384qSHOsmjbLpE4J6PC7P62z+rs+HCefDD/Uaf6//AGj46/0TTfWr76XfZLfL5q48yW+TktQJQ69mqhprwhuA1jrrVs8BspWdLB9h/qbK2Vs/rbK2fBs+DZWAvqvhf95p/rfy+Ov9E1/PQe2ljiAzv9gty3Nd0R4nDzCPhNC2ky9IoM8F2tYsmon7NYpxjtLci+jXk6aR0leQZcpwjqGDDz7KvhUkgjb5NhYfbapUwuJyTy5TnlUNYrtta22pO75ExcEn7vLY36eStFBCmE3PS2jnUZ31c+u9QxTz2xGwYkiwc9FNktiWjF2SLW3op1OHkSZeCki29NNmw4fVvQNVj5zeiDilw0sZ1wWvmHJYk1oEwzDHRyWZHBsyc9YuaKDS4zDf+1HzSNt6OGnjVZLHK6iwFvnUsL7noUd7I2YHXz3HJSxxxxYibL34qTkVubz1buVAenVUXAxGIbW6xtvbdNDPhHB+VlYNWijwZkR9V73Y9C0AcOVjJtIHHB99HLh2Ove7KMfc2Z77wdHTUhKKGGoRkGmJcDmFqyxsXYi+UAURI2Y32HVR0a58o3+rUK3OaE3C4Ug+uak+t/L47J9E1ny6TLiVOTn32yjHN/w5iMPBOjRTSxK2ZVcWJAyVpNz8DujuvJiIUw7Rvh2hVY01jWE1mrJ/w1iYr7eH2KbET4TEYaZhbIYJWtbz5RTRpuZicUu3SaNh7MtFxudi1duElj7rUD3PPCOUGNr/AOmjpIMQ3NlibqrUuKH8ljW/M1uYwSdVZ+6CslrHvMl7eiiNMhi5bxtb2rWp0W+3IjD8qBixcgYbAgaklfHTI+3I6k++nbDNlkbjDFGRf0UcbHK0eKuS5jUi/PcVoZicm0by2vnFMZZs4P7twLDo10bYmWK/zP1JoMu6uIQjWu9WlEmKZZl24qNFVj9LXakhOKOKycW8qi4+29SGfFNMH2I1nA9Y0WXSIf4Ta34qAeRpyosC2W/pBBpgDLlfbcqbdBJrezTp9q9dJImPmBQ3GpOumeV5mlbbJvR/uovh5ZgXGVw2QqV6CawsuDa6aI3FrW19LVL9YPd8dk+iaDjauJUj7Gq+ksfNqrj21bKjLgsCdjbDVsiK67RQbCTRBm2RkXr9sWPSPtI5qOKaUMoAdgus2NGKO+baWIsBSwNANB8rEjXSz91ZIL8mom1O+DjGlK5VlbX7KkMuKufkRKAq+gUdaIPPy08uMgWaYtaLe3C25afGabNjHktEnIsfNasYFKpjZ96ktrgKKOLxBDNY2T519uatK2LRERwVUKBaoXBihnAtOUXUx568aBboqJJ7CaPUZkUDUKCtpH1b5r29lZiXyKOA3LStlCxxm9ufzU3e7ZqY7WbYB56MaHe3zC45aYZ1s38NbU829ptIFfm1UO9jUOYVuQyALmgfUPpCpvrB+fx2T6Jpj/b/AJ1hVxMuWB2GlI2heWoMXgZQ2mbVDmzLYcoqMMoXRiwC7KixOAw6z43FO6vIFzsrKdS0O5R3OxhDyQf9N34Q1VnkOc+enXNvXtcdFOIXMefhW81ZXlLLzGgubejYK21trUaWIgDJ8O2ttba21trbW2ttba21trbW2ttba3MPNHJ7xU/0x+fhd/IqdJtXHx+sK45PWFcanrCuMX01wx6a4Q8FJ9E0frv91AygsnKF1Gu96bPyZyLfAwixGIw7PwjC9gfsp5M7vn1lpDdifP8A1Nf9bbW2tv8AX21t+CZmmyOgGjjtfNr16621trbW2sB5lf8AKsR9Nf8Ad4TEYq+/AtF9I1pnxd2kci7enmoIHzejq89FxIoC/II3221fJ9SkggXSSNqSNU10IsRAYpMt2Q5V81cIKw5awLYts87R3dj7PAyfRPur+b+dJBHwnNhSyS8FvgMqGyjZs/MinhfavL8Y21trbWF8wb8qxX0l/wB3hMNuLhLMU4QvYZv0re4jDXTUd6eqi2mw1/t6qtpsOOYgt1Vx8Prt1UHSeJWGxg7A+6jmmjlky3sZCSQOmsLhZTreUI99eoGkjGxRbwMn0T7qP1351C+HGaUHer0a6jGLjKRqdTG35dHwSxQRsy8rpa49NSPIMrXsVPxuD7axX0l97eDnxL7Ilv8AbyU+PxGI/aHdmKaQREcxueehHht0J2j1ZiZCbHlF9V6Z13WlZr7yPM1/Pemd92JopV2REvQDboTxrytmY0Qm6MzqNj5mHvoA7pzSF+FHdq7oy3hwZtm/iH6nwUv0T7q6Zfzp8ZHGTh4myvLzG16jwUAMk0nBF7D7SakwHcxbERrnIUqRl5w17ViZocPKY8L4ww+Tal3YfCt3DIdUt1vr5ct72rC90YOSPuwXw1/lVosVC8LkZgDzVy1y1y1y1y1y/By/1wq3YnYAKy2OYfJtVrG/Nar2Ntl7UVcFWG1SLGv0+CwrZVra+aocwttrGDmZfe3g49zoj/aT/kPgz2+2tdctaga2VNiH2RKTRmlN5cUQ/RcX8FL9BvdTSbcr3p4UAEUnDTnqPG4dVSaLZffDmIIPPUuPzqk8q6Nsgyrl5gBWJgjkKx4vXiF56TcRmXuKMi2rf2GsLm5qwfdM2k7g8W81LNi7MyII0tqAUVwPbXA9tcX7a4Htrge2uB7a4HtrgVwK4FcCuBXANcA0ssQKyJrVqfHaJEmfhKqgLstsqTExC0soIfUOWtFr0el02X+KmxBWzsuQ2Gq2yiq7GYs29HyttSaNeM1MCOnrpZZo98i2XKAOW9JiGjOkjtbm3v20uI0bZ12fZ9t6STKVI236APPzVjfpL728E8rmyRgsx6KeWfCyyzSMXZ4ZLauQfYKzrg90VI/tVtRkXCyusuxJWiaT/DrvWVsNjmlj+UZI1UBefXatLBhsZDoz3wZ00Jt0tXesNi00YLModcjelqzKu6Sty2CW9Gatzty80rJjpQ8mlC5sia+RmpI11BRYeCm+g3uqT6X/ACHHfSHvPglwqHv2MNrfwiozMrStoxp43DGQvy5SCFFTnFs8OGvfDKbsAL/Ky66VdzQQ+bfyLmVbdDGrktmHTU7Y6Q51toEObIee5XXUEeDZi6i2JcZshPJlD66zR4jEnFastxYee9YndGXWuFXRRdJ2+DcfOUineCK0bE2Mu9rbh/XPVW3D+ueqtBKqO+UN3u52383mrivYa4r31xXsNcV7DXF+w1xPsPVWqD39VcT7+quI9/VXED29VcQPb1VxA9vVXED29VcQvt6q4hfSequJX0nqriF9J6q4hfSequJX0nqriF9P6V4uvp/SuIX0/pXED0/pXED29VcSPb1VxI9vVSTRwxlJBdd/Xi8frivFFPQ4rdWLGxtHZ4wgP2k28DfmqcPidBg8N3tHpAmI0sjnUAQdX2Vp590dHJYExjLT4ZcWkMCsQuIk1CwpI4sZFi9XDi2UZjjo4wFzMxO9HTWKlZ5WjwxCqIEzs5PMLisK6vMJMRm/Zp0CuAvLqJ21h8wtJP3yT7fBynl2D7dVR4abGgTuBaJVZrX57Cgr4woTsvFJ2a1Y8fdydmkx2KlwkynvayuCT0WtehMI8Joj8vRt1U0yDAaKPjLrrosqYJo14VlPvp7JgWUC+XYenXRywYRrbbXqy4fC35Be1WGHwx6CasMNh/WNW7nw9/pGvFoPWNeK4c/4z114pD6xrVhoD0OeuvFIfWNW7lw9+bP+tX7jhtz5j114pBfmznrrxKL1j11rwkA6XPXXiUXrHrrXhIB0seuvE4PWNa8Jhx/iNasLhz0NeteGw4+01YwYW/ma9JDHLFFFHqRL7KVmxcID8HfVvsfAvS1QtBiY545dTaNr+nwMgVxHNi+8wseQkUWOLiJbWTXjMVARyxJyWzGjmeB+bvlqsZofWvQUPGG5Wzn3VvZ416GNYLCzSCYyuM+Uk2Uc9Ii7EFgPBxR/Oe5+ysU6SyxIDlYLY61FuUULYmW1/wCHqqOXB7p4nD54byBshO+XNl1LUOGz5GhdnfEC13Lc4tbkrDYqHdOaGSW+fMqWFjlPyaxOHGK08srq/duocHky2tymmYboyxzLJox3uPLwS3N5qn02L7qkkiMccgATJm26ra9lSwndJkeNQwtEhGs5fzqN8VjExqJciNVCa9a6yKjwzboBMwOQ6FTsHLUTSborNAcrNEIlRiNtr0cmOWKFnORNEHIzG+3Vz1cbqxlGzKw0AB1EqeWnbDYxYITYsjRiQ3VQu3VzUWi3Vi1W4WHA2qG5/PQbDYxYpGW2Id4w+Y3LXtqtwjTIu6sV1ufFh8lsvzq0wxqd2Zmzz6PekMALZP8ACKSD+koLubD9m/hzfP8ANWmkxqHFKUMEqxWVct/k318LnqMPunB3y2zC87Bfn+ej3Xjo5RlZYdHFo8pNt9tbmoSNupCfMML5r/P81RPNugk+Gaz6IQhCbjn10FTdRVhgUKgMCsco1bb04/pNSisUlTQKrHKbG22nTDYxIIWZnWNow5BbWderlqQf0mAY7Zh3OvKL89Z8Li3iLool72GzFRbN6Ks+Pcjm0IqCTumQYtHMWlyrlIffax9lLmnldRrXvYt7qaJ2Yy5r3Nht1bOm1RyfOUHwCJEt9G2dbbQ1XTcp5F15irbfSRWrcbEA/S//ADpp59zZ4I1sDKzahfV86jZTfk1m9Avubj3dhe++t76BO5+I1/xHt0AdzsTlt8/l9akkxQKRx75I2NyTaw59nhI1JssaEsemp3ixmqRns2hNyG/mUubEby++yw67feU74N9HAy5QkkWu2UKdj0mjeDErKgfeIwsDr5WrC4bDLZ8KmTECZLb7NfVZqXFx9yzGYkaPfhrjbQGgy4xpzLo5ARHlK5eFt5eapXGGweeIhWTSuDr2fIrFy4jDLh8SUUQwgswJVs2+NhbZWibBYRXCki87DUNZ/d1FPi8FHhoow1pI3aS9xYfJWo4H3Ow1tSK5nYX5B+7pM+5sKQ5xmnScvax16sgqSJNzopoAzlJnlKGzMW2ZDz0cm5eHkDDauJJ2i/8A060eHwMeKQqmdnkMZDKLH5LVZdy8OzWJyjFa9Wo/u6jnjwEckr6QTYdpCoXMwIs2U32c1BTuThg7W3vdR5f5fmrutsBGk8bqYsKJSVYAEG75dXC5qzS7kwRgC9zibah/Lo6TAphckbaJ1k0l2NiPkjmrfbjQKBynEnl/l1GG3NhjgLXedJzJYDbqyChh03NhlSEZVmM5W6jYbZDUkS7m4c3GV7Ykm1x9X56n7nwSYqOZs6u0mjtfaLZTy0qnc/D6Rr5Y+6TfVt/d1pocNHJJMmXEQFyoQgkizZde2s0mFw8Y1AA4huXZ+7rfQQj+e3YrGRTtBhmyCSFszMM0Zza96K/dW5CJH7NRGR40D3UtmY6zs+Tz0P4fz1+Dwy6TJEZrzDn1aqwLm7RJLnPNZNdDw+6Rj1vHCQlzbXl5zQdIWRhy6SLt1meIuw5TJF26C6ELm1ZjJFYfjpjg4HxOHgVB3Vh3SwsgHzhTZsPNnbaSY+3WBhw8RxM2SVpYoGRnTM4IuM1d/ixJZdW+yn/fWJn0bNnmjyQ73SNa+xc3nq88GLV/4ojUksYcKuHl40ZNZFgBc0ulgxsa8l4HtWHWLTZtKp38TILX5S2qmheHFbx3u0cLuh3xsQy3HLTIq4rMdn7PJ1Vo5Ip9ccZDQxtINS5SN5fmogpix/20vVUWJMczROZVGSMtIAxBF0G+Gyt+uLB8+Gl7NNioEmaOAxF7xsrGza8qnWdRq7JjB/2s3ZpIcMs5JDh9JC8a2KEbXA5ajWWPGI4UCRe5pTY8usLRii7oMjcAHDyLr6Stqmw06Ym6yuQY4ZJFKsxYa0BHLXBxf+Vl7NLJLDiDFPEmQxxM5uLjfBbkG1q3y4sf9tL2ajx0EcxijnIPeyHysgucnC4QriMWP+2l7NSJhoJ9JDHnGkjaO7IwZQM9q8WxQbl7y/VUEfc81pGyHNGwFm1G96fDSYeUOm8uYywIGw7DQlEUgcEEWiI2dAoIwy6Rb5T635+DjlK3Kyix5r1hZoUBlxUSySS/SHgdZtXfcbCh5s4vW/xmY8yjrtWTCYOfFNyAfpepET9iQX1BLN0X11v91sbflAzddb7G49/W7VK0rkGURh3c89jrNHO0M1+Rn6jQyaGAcyvf3muPj9YVicU0iiGXERxaS+93qs22o0OIw+WLi7FBtqWVZFYQYWZ7q1/k25OmjDHLh2Qm+bMt/fWHCOu+lQb1hymp0zxFzLKHDkXG+IpijJs5GFQh5EzNhYyocjlL6xems0d+e60dJIMvddtZ3vFHVWa8Q6CKl75ve5pDvTzWrbF03F6gZJFHfkvlI+cK3xV2UyIcx16mIpiuUavNRR3VjoYXGc86leX6NHXGPtFLeQZTK8Y16t9Grf7aJ72L+cVvHAyx6TUf+m6tVyU18txSlXUWkTYRym350VkYXhdoiWN+ActHgi4ttFSq0mt1jlFzzrlNvtWuGPTRzHNzb6u93RvnZqMr99XFRJMgY/OW3vFWjgEZ+dmJqK7XMLZD0D9D4OTDvwXFr8x5DQ3J3agdsMni2KTXZfzFDufHwvf5Gazeg1qlU/aK30qDpIq8m6EC/wCMH3Uf23SEcijrtWXDYaXENyf/AKL1/wCn7gzMDy6M++jdY9z42/eSuq29XXX7d/xFnblTDo0p9Jq8rY7F/WyCJfQLmrrgcJGfnPmmP4jasq4owjmgjSP3Cj3wljtZ9dXGNaH6A66Msm6Mz6PfWsBsrC4aNsumN36F/wD7S/tzKSN8Mh1VxzVx7Vo+6pNH8y+qsXpssg0iJGxUXFwxOv7KUYJpcOctp2DEZz6TUcckUcsRSRpBIitsUnlFCWKHRYvUyTRgACtOypNMx1vIoa/TcVMXwcS5GypkjUcimhp+/CIWjEgBsPNWFE2Bw5zLIWIjUNvWAHJTYaNLYS9xhyoy9NrVOjYDDkK0QBaJflOAeSv2GI4Z3GWfKBZhUKnBQOHazZo15fsqDFDCJDiCoZZIkUWNujnqSeSCOaY6y7opufPqptNgIVACkaNFHCQH30uki0qRC0IkAOUc2yhC25+HtpHTNo1vqVWH50cKI8uGJzdz5Vy357WpwMBh2tE0l2jU8Arf2Gi+GhXDmUWfIoFxzbK0ncMD2K3zRrsvr5KlkGEiixGHlYLJGoXLkYijNiIEmkbhuyi5rRtudhsuhVltGvCDMr+6kSeCJkhFogUByjmFbzBwMfPEtSr3FhkSyPCFiXgut+bnvS5YYWuLsNEosebZWCvBCwhkaAsY1JGq62+yr5lB+boFp8LZVQnSLZQPMdnmNRNy2sfs8EVbWDRgxWFXG4U7FPCXoNd4xeJwX9mxDAetV493Cq/V9TV+0buzN0KB+dd+xOJxPmL9QrvG4mnPznBb/VVsJuXh8IOewHuFd8xeUfNjFqu6mY87m9WVQo839aX+Le+msLCDIksKEuymwsx6DzUXaSQqP7VOqp8Y74nTRyCJFDrlJOv5tEqZNXPKo94pYJnxKqwY5lkGrKL/ADaO5O5kTqWkOIGJeWz6hbXqtspsPisTNmj4WV4z/spyGmxbaHJ31lW2kXXbIo5DWkXFz6NecxcnSlIcRip5YgwzoNGL/grFdy47EYdM++jGjIzAAG2ZTVjupiCPow//AB1ho4MVNh5MOGBnGQl85ubgravK+I9SHsViBLi5pZZsuWbeKUym4sAtq8rzfdw9iopcRj5cQsTZtEVjW/2qt6yw7pSpEOBGUiNhzXy0Q26b2P8AZRdmlbC42TDWjWNlyo98mw75a8qv91F2as2Ml7r7oVhiMqbWUjg2tawqw3SY/wAqLqpu7MS84MbJkyonD5d6Kji/pea7MY1vFFyLm25eassm6kjxNqZdHELjpy1ip4cbLh0kzTPCqRvrUa7ZhW93Tc6r8VF1VDfFSRTwZ9JPZDmDm53trVLEu6Up0LZXvHGNdr83npc+NlGcZl3sez0VgZklnXENGYXO9sdHz6vPTy6WYols7bzl/wANbo4eeWe6KMQGGUnvfNqoRxtOztwRvOqoHklmtfIdS/K1a6K8o19fhNag1xYrixWpAPBwx/Of3VNNBg9NCwURvmtsFeTv/IKhgTDq2JbEmaaHNsGXKNdq8nj7wdVTy4jDiC2HkWHfXu7Cw2CgU3PKsuxllFZZMBlznfytIDbz1O0GAOIha2SQMB8kDYa8kv661Ej7mvCudc0jOtgAdeysUY9znnjeQskqsusHptXkeb14+uvI0/rx9qvI2I9aPtV5FxPrRdqvIeJ9aLtV5DxXrRduvIWL9MXbryHi/TF268iYz0xduvImM/8AF26xU39HywOhhMcDlM75GJa1iRsNeSMX/wCPt15Ixf8A4+3RxIwEqvDOrphmK52GUq1tduXnre7j4wfbF26RJdzp8PGc2kkkKWsVI5GPPSxNuXiJGjGUyKY8ptyjf1b+iMVr+r7daVcFJiBiIozJoiu9kC5WBuRXkrFfg7VMe45Elw+IEkcLFQzKVytbXXkvE/h7VKsuAmiglV45ne1srL000fcEz5NWdbEHz7a8n4gf4ahEylHdQHU85F/f8agT5qlvCIiysq6MGykjlNcfJ6xrjpPWNcdJ6xrjpPWNcdJ6xrjpPWNcdJ6xrjX9Y1xr+sa46T1jXHSesa42T1jWqeS/JvjUcnz1DenwG8dB0r+tGzp5tVcJLdFKzEFla9xqoMNjC/xmT+BQvhAP7JfefDYY/wANvRq8InOm9PxnEPzyH2eEPmiX8/DW/wCnIw/P8/CSxf4h8Ydvmi9X59fhJPoJ7vDYuPmZW9It+XhIzz70/b8YnPOLemlHm8JP9FP9PhsRH86K/qn9fCX5qR/nD4vkG0sK1DXzVwK4FcCuBXArgVwa4FcCuDXBp8TptHntdbX2aq8Z/D+teNfh/WvGfw/rXjP4f1rxr8P614z+H9a8Z/D+teM/h/WvGfw/rXjP4f1rxn8P614z+H9a8Y/D+td0aXSb0rltbbXAri64s1xZrizXANcWa4s1xZrizXANa1IpFbb8XGTapvWsAnnIq+CEDJ81wb++vFcKfsbtV4phfQ3arxXC+hu1XiuF9D9qvFcL6H7VeK4X0N2q8Vw34u1XiuG/F2q8Vw34u1XieG/F2q8Sw34+1XiOF/F114jhvxddeIYb8faryfhvx9qvJ2G9L9qvJ2G9L9qvJ2H9Z+1Xk3D+tJ2q8mQevJ2q8lw/eSdqvJcX3svaryXH99L2q8lL9/L2q8lD/MTddeSv/uZuuvJZ/wAzN115LP8AmZeuvJZ/zMteSz/mZK8ln/MSV5KP+YkryUfv3rXuWfv3rfYAr/Nau/h4uhzV87k+ck1vtvxjWgrgCuLHorixXFiuLFcWK4sVxYrgCuAK4ArgCuAK4ArgCuAK4ArgCuAK4ArgCuAK4ArgCuAK4ArgCuAK4ArixXAFcAVwBXFiuAK4ArUtbP8A6E//xAAqEAEAAgECBAUFAQEBAAAAAAABABEhMUEQUWFxgZGh8PEgQLHB0TDhUP/aAAgBAQABPyH/AMu5ZLJZzlnOWc50E6CdN5z5KfJQ0x2P34L/AO923IF6VHmk4c+bnfTBbkYckDq0Ie85sYt/hppJIe89Z82exB0XtG877x5LnMnoMHjUHQS653Eav70jntEH+RC7eA6gHjhW/fd5aaj2sTWR7RLt1PSL9AQauF8h/UwbCCI0orHgneV7fQUn2ql/eqn5DGHhiXmy+rMuDoMkdoAE4LFsdVa3pCWEk1PoCCAA+e/ifPfxPmP4nzH8T5n+J7X+J7/+J7H+J73+J7P+J7P+Po1rXxsfBx8HHwUfHR8dwQfOJ8uhiC6EZjWtm+9+sT0C/OUpVnqXqCazl+DgIAcfrBCy7laRwAkdBoqKf6AB7fp/snZOydnHIOJ9mtI7PkvR976xNL7LmdKih6bDSAu0zPq7yteAAqhZ6365zQeVm5rX6gds7fq92fSJJwDp4Toh0wk6JTOa/mOx5fe71j8SuD2MyZXP4QgkWQWmolGZUC7laTcwcXjBOjdkVp4hP8wP7eIQQQdMOmEnTCToh0Q6IdEIvD25iEjb71fXPxKK9FzzjBrOFo03ZmeqsroAArlwNNnaeYHCJ0TWWFZmdO4d/wDIOEHRCSCDphPRh0w6YdMIOiEnTDph0Sn6/ljyn3o+rfiZB7MRBbBto0AIu+qKNuSl095UBQLAGLZfvq0VrxK+gEknAkEHTCOjCejCTphJJ0w6YdEJOmd/ffIv925TP2eINq8l0tjZyYBwUC6kcNTOUqUjcVu4DFdPxGRGpIu+t8rrjUqVCCSCSTiQ4UIHlwQw6YGHTDphJ0w6Z3dD0nV8/vb2rlLNm/6oAqtAvJiUJWUSXqkqUR1oWnvEQ3AKPAlSoEPoIkED5ReAvECU4C8oOEkjhA+UHynf0VX97F7JymXV/IQN/BasF1cG8a2LbVNhDoh5fRUNyKjnWaxdO65Q5UORBcp0Zc2hyIciC5R+UFyi8ovLgOjOjE5RuU6PBD5QfKCh0QUC5QfLgRPqX3p7pymXMfxpn73aC4xJZZCL1uBAE11Z7QgkRdgc2kxhw0YcVdiu0s2h0y/S0okK7h/E6M6E6EOIh0IdM6M6EDlxSOmEDpgnSKnWvirHl+v733TlLr2KTJWA1RAtaAAZg3JYlJwYd3fSWt60UbOIeAQG3JjUXRDvKYqrHC9Ble9xaXslOsKdMCB0nNTonOZuOrQmrB2YVdix0OeyJT3sLoIx63BsYUruzp8IdjMQ/wCUNg9xFm9orkS/mVxtGmM3riYBF45GN1Xf0hV8oehwDqUdt5enJzBdyd4eXFzq5BDumRIWXhwwPolEL4aXoRiJoA2izijAsfYAX6M0V0UWDGAhgWmWhvWctWiMcrejL4Vwvrdv+E0ZlQSuhvK1e5Y+RLrIFBo1xFb36Lz1qMlA1mo5zcQ8sPMYxA2H3r3DlF1WTrxD5ouOHCuBAp1Y0eynkRA4SVZ7uhr8ZP6pkMFILtImyaea6grwGrbKROyPbqAZFvLAOsefn+Qj1vGNbYWfMDPnlDedH4hYpqq6L6CVqSWZV7pDDWl4Zd0pJ28CN0FRyZs4Nxtd8TLxII66jJh7RNdEukPEGOu2xiEijMLneYmSL8ARF6Czyufo5eOQwmhqCHjvrHoQ3ZeAEVyZLh6sPFjICYWKtwTzgl05qL45gtvem51lTQF7F7YTJCcYtRbfnMQvzoe0JPHy/M+99r5RcI7gBHTCZGWMJbC4l5Jh4nmbkmf9TDhUhL5y3oO9cexDN3JRoYIqlWixtb1j0eBSOeQSxJuS/Y7zCShaFueSOd3RJyoIzCA4Aij9KYoIXs5yxc9VVlujFMwA+shNjnbMXqFot5uQtnsPBoFbHWLmXRLi484YeYVjDwqr1YwqowDOSMugJSCWqCeQHEWEa1vGG7lACE6W9TXrLByVxUyvTvKUxihxoZQlLThL7dZCkF2bUolig1vCUR1vow915f6Lly+JcuXLly5cv6Li93tE5wPpPOZN4f0j75abHQbC8Z1m3QwKdpmiiJ2q2EItlvJMPQSGl1tcEv32ElVYvxWgmICbYrshYkbE3u4ZV8kg4FCgWTmyWbzmJzc5kBYtwRCOFlJSSZIUDgUCgnly6U3f+d2LMWh0/wAp7Q/c92fuG03s5wfTyc+FQbAnQSXwXD6bnunaZp0wXOjYyVjMrNo1uB2Cd0YdFkcpaE8ZktrPs1VvxgQrqe0RluAPA1Bc2BaQDvgot1Z3Q64dcFznU4AkdUtRNuZaDkozAQEAgkNg3T9ZwfsuB/yUqmK/z0vLWbvC13pZy8BNguvFaQb8qAc4sqtyqNWZ+aA5tsKnoXNVkFGuVbZi7kAHLsxZwupN7vCUIQ4seB3/AOdHd+mcp5vExBty0XXTkvPgPeyhqdrgfsX0aJ9YffO/j93Ev9G7p3zulucvznUloISSxewxiPdcr/FYotOSbIltvQS5XjQ6U50uIBdN6S0rWgE7RvDQ2gioxeuZvaPkGhGTF2MBib74hHaGDtDQ4kuLxj2fuizRr7utRb1O0rSh3hDGTHfCPTGrFpqK7xv1rIIjm+t/V/vnfO/6qd07p3cHdwd3B3fQ++dmn6xY5L/xUTovkd9jxZY4HW/wFu3LSKjp0hVsOdShPgd/Og4Mb3LRzKiw0yO8RWL0ZjkMrT9qDwyhYEASoebpHugjWI3Xs0lyfRcuKOKU+7y4RUGwZK0WrvSahlQCAqpAALVmYn+3tJYMeAaYgtV52hWhB2zULUl1qLBMqBi8sw2coeq0TOZ0Y6cdOPeJ7xwXc8p7BO55TrPlOs+U6z5TrPlOs+UOhOnFXkEqo2iNl2a4gVKnUq8a4lK3LUVfKbgv2HcYKWXRq2g2j6MFbgXVVK0HJ0nykmuRt8uMCz61imroqny/MzZWQQsBa74ZFnvKW48YTIOpEapmIHNeex4sWkkmcPUK5h1fqeBgZb2zKKIWrD0ZfufvCna5hcQRaIgFIGkmEEYA7SqAi+zQOt1YyvUAiWxS0Jgmd9HIXxloy4hLH5yhAnS+3aW938lvZ/Jb3fye34cUqfOdT5zqfOdR5zrvOfPT5iMc2gmGdroSL8MyJIyFa8MXW3WYVu8rg9zQDAbFdodJVyttKdmtJoaDSEqhjzQTxhWdQvMPR0MOYYgumMpthWr+aFEnXaDuvFPE8In1qX6rPYFs1UGMfWA2Aivlqog84+L/AM+jYU47Qgqsrd0yo/MQbZm8EVfTyi2/0F7sEKa0Qck9+lQKl4gBYIIVRTtRAkpw7QUPorhUSWjPYTVPWvz/AJClo/aE5zGE7f1hD6lFMQ+rM73mw7ZxTjQA5ZgtI+U46zHKBuOEQzS03faWGti1SpXVS/Fzutg0gkIEc14BiNV5u1hbB0pZZUar3IsT6alcCS6J/wBYCpZ3Eo38sswaXTgSObA+wgwW0g5j2dJ7x/E3T7ek9kfxB9Pd7cHWLF4kuWMuQL7cJoWIJM+flf8A04EinhiwnEM+Zf34Q18zDqvOQl/dCGvnwGf3w5si53QSNdSpuhLlh5n9xgiA6LRHMZ14EPpUIKaBawJ7N/QYsPWFC4hfqvRCtUYMr1q+UyuQTLKXvUtTpsK8XWFgXTuipcvKtl1KCs2xVo2QpQjD2SlUu7e8z0h3lcalfSQlrFOrP2iWpk/YEwMEkVg9nPgDqiv+AG3aOekRh0MDE3ciZgL0wg57Sn7eV3poilpzkg1yZJfvBAgN97m3mkSOTUX+GJFi4yH5Yho7GG/DP31/3mydgn4jv/t3l3iVr8RZ7n1mfK3WX5Rg9EMFwZWmR5RbemaxeSKD8wbC84nQa7i/MDgiDkRLPOI+oB+WX6PzQY7M9XaVsLUifgwueUFQ8pqNVjb8CbpOmD9S+KOM1deReB9SlpYXdCSq9iOAByMrOr+LDM1FiW87YijawrV12mSNqV+i44NKbgdeSXLzpF+CLkszGW2TF1iF1RekCCh9a8ajco6FZb43WuqHMOcCRPQSzKImwJwOYRxFcIEA2UYGkMt1zytCrUxozkRDNDMJXPSySxu0hooWOXCqZrHSJoIHFNG+oh+CfcYFB0HSVGGEkGdxsRPOcYqxLqO6rJaemVqYNWPjHYG3KUBVmcAE6wQA1a9NifLAihhgKstqQCSIr5wz1xLesOGuSxXcgU3p8y7ZdsIySC57byEMDG2Bl/R0zD9MGlK13UiiI5loiXCgLPiabTFprDywOOENF0by6V42xJXTTWNlKCIkWwtLxoCg6RTx1Kx2RP0YpR6QdTuhhiKmHtRiwGBW/eEBNToNqhK/GqAZSgQAreO9Zh9QiZKi6BokswiigTvlQ5NsIK9Ix6MDR00fOW1qVrXrcu14UpvSqOJdEi6RqQodS2HNKUndw6m1QleX6b4vFfQY5av1MxrR2E3eGow7lg84VeqIaGuRpd1NhynK5EJ0WvaX/Qo6qbZxbowU9DubgKSsWQuHKFhoWy1QjCiujeRmV8SrMZLBwwMPBd7n0DrLNaDrcAedzm9RJ0ZKtVz8i0GuThxcFWoqM4xUw1gyZNEFRrpY2LuDWkGYxMvhaFhmDlWUAmEJlojVhrHDtRBm1eO0tFuwQejkYZaSzSqw2VXolxpGyX3arL1wGWoErKo8zEDCthLhn3SayzbxzipjNa5lkz01j2nRaMW2YUyXdWbveWfmMguGHIpCB1ZFDs0VcGkuWzZxbgDmqWMvhq3UxIQzuiCbQVdyihQ7E7k5HewUOcckMt5eDoYIfSkeIOUmZt1VSsu7MPCHfur6y+hpwVA/wY8THKHAo3YDLNLIZ38MDxBW0waWYBrN1i9zNkkhtjJyl6YivVXVZtA5JuKTY2uLC0sLFqp4mZr1BWRu5TSItV9S9XnCkqWPXwl6RASBR2BtVkH0FVTAVECu8aENeqYFMRR7KFPPdkM2k2qUIonNAiR1hCe/rAgEFe0t9iHgMqUbJiSlHBMHkRZEaGpDMgNRiUMhBBMLA55MOSvBpvZYhndYUAqswTSFJbDZyiQLVWbUPRXmURA2ZApTWiyMsA7QZ5KLcOhoUgFaYACVhPvVCwtBrw3AGjLrS2C2FoyFx6EAHejAEWkx3AoA9RLh9ScBIGI7TbQlwfUY1sFDlmEq9/ov6B7Ic3Evwhq/QDcsQ9tmfQgrk6DXyyjN+3mkt+Igz3yc6WAnmGun5l84q0zSztMOKmgquxEhku0WvS7tIZz7jrLyTCC3QGqSjr0tgahRzzzDVN6qU8rQbHmcuBo9kc7M7HCYpmeqQ1Ail5O/SHc0XsssePcEuhGDLLRX6ysWE0clDVUqDCrd+sFsAKw7muINBzCda3vrDQRStN4MMdSiYEc+dZkI23DEXQCJuwcosIsZx/SAiiquYJrWqnMAKP6StYjOvRq66XLM6Lg8AwPL0eUFCgHK1V32lasmSbky/Kex6wfOVP0pZSuaAU+spmBbwtdqDMKjqaVVkEbwke2EUMSslM3AwHdsPR8Ic+f+BghW53m3gMVuzWyHl+DSWsmYgXetmWC6f2hFim4v3EVO1L3kmVdGC/tS6IWf8QMNgr/qa9IQbqNHYka3aCAoJ3tEKiCNn3xyjJWKqP5vWVo7UF1dZkiNhrytDDxsRuqmiadeBqiY85v7cFhbld5jdg+gyz+2FNep/aUkYvrUOl6CemOJEVV5R2WOYJ56EoGvsDQugMbxqlCo6vmLlRuzqKZYBuscR4clrGINsf8AmvuhZvNnCebBcWUwAQqb0MRZlhaBjBpdw1RicVDKmVJ9uyCXiqijXciitjd1/TmZjFxv8lsIMrixPUNcmAheIgbC4YJcebJjnRNoHsndyR8tQoLE8iDzVPIVCukUa9p06tZlXHEUtgY2RKBaf/IKwTbKXgF+WYsFTg4gXmgemvXQmUwrfAGtYoG3lHWW9U5etkBxkjNnLdTwmY781whD6zDyoUkQz1h8XWPCWt46dolL9YHaYA+hNaDufmHM25vQHwlheV+plUo8v07y/dKlNy/624V6w70uIRT2wriMPA69APHUC7CNKAUw4k4tI3poX3gU9eEfcwTMBQpVwuOzLIzzWznfTBCGJM1rnKoIVq2NidH9pbmxuKjqJwZiQKzKUPEsRUSRhelgfWN4eprIFnWOccxCQJaETAuvUYogUWkYhFDjN0Gs8x4ZlUHAGRpYYFvI6y2LLNdZol4xGS6KVVZCmuUtnjFTLV00d0DTWXBI4CWhMfhA02t0iB5SHnX0RZ2PJkspY1jFOEDm6zmr1gAqMHImleKqMv7JMVMltbRDDkqMeQdyW8mLAoxd5uLrK2lmDgNLog9UTyGqoDfN7S+uPN0XuNiDoX20iNrYuWe6Hnx6iEPrSMM+taTnwDh+PqBylMqVA4seGAbanQrKvkQcBEROdw5A9nKYAyC9mzDVgH8dm8GwJplCA+DQwYvCyA1WLYp4Xi0oYOU1f2/WKxKOBikS6Qo+bx0DRAt3gWAbim1CVDjQdZbmR7w1k6f7zRnkn2IULSVaLPnz0DB896qPRu2TRBa1LSNlTa8z89EZuu6LhYhp7QVxxWu+C982KEDsg04FdxZBrGAzzYDVHhIpoZSUGct6jNs2vR4om6NZGGj8psBlGMiD/FUqVK4lSpUr63WSvFr9fVf1Fe9GltjjtPbP8wSvd+MN73fWHtv8z3n+4e4/zPcf7nu/9z2f+4+4/wAz2H+5t+N/3gbQDxfnPhrl8Lj9KBuLxc48BGFA7r/2b5Lc1/mEcsBpjvcPRgHjBh9o8Fz2Wtf3D/OjkpJBJxBwsJXnEc+DkxkbnT17+nCv8KjNT3Z4GnpBh9o8DvOicO2H+mE4EEn07W94NfRWNT36n9M/SkSUI3r8LCKH2bwAvu+QlkPVq8f9MHyKCCB8JTi93B3cLDNqOmnGH/Cpt+OAi4X9k8FEa0/ROig/0EC9PwJTZuI6Tmw4i/oDNecZ7Eq9n/A8WXYGqs8IQ+ywftGKcsSHOmHbTDF27gDnPqQDXtPB+2y3RE06UNVnKH1+ZcYhkH6ASlp1qS6OR5YK/lN7eUednVR4YDwxvjJ8LH/gx/5M+Dn7UmIEXQ8oRfZsDEAroGs2Suq84w6dW8QzHEmq04SpHHCQpH/Fl3DtLc9OWf8ATxpEyXn/AE4fNwOGoNRBda4RIwVREZ7fCGL/ANJ8DWv+IQ5Sqeo956w2ZtXDvFWPf9pph9nKNhzfP+po6b2fzHBjafvIwtrLlgQV9mxIl1mcR4Rb+fA3wc+BJ8bPhZ8aT42fFz4OfBz4OfFz4OfBz4OfDT4yfET4KfBT4ufFz4efDz46fHz46fBz4mfBT4qP/Cj/AMThxA0QIFtgTAYlSpX2r/42/wBP/9oADAMBAAIAAwAAABAAAAAAAAAAAAAAAAAAQSSSSQASQCAAAAAAAAAAAAAAASSALCK6TBKCuuEnmTAAAAAAAAAAAAAACHf9SN8a1Lo/A8P7WoAAAAAAAAAAAAACT1Mor/qPj/cUHh1U1AAAAAAAAAAAAAAQxeCGEG5ro2IMOdDDQAAAAAAAAAAAAACTkZk8pvEiYMd0n8QeAAAAAAAAAAAAAABe/wD+65pPkHGybW+TIAAAAAAAAAAAAAAnA+/X7rBDF2uhFKX0gAAAAAAAAAAAAAA6rLnSAm52IKR/oBPnAAAAAAAAAAAAAABH+/lIxSSSXM4jgBfQAAAAAAAAAAAAAAWoD4e/5zVDk+UsVdqAAAAAAAAAAAAAAnSjp4yj7sKUCwVUxZwAAAAAAAAAAAAAEDtk1AP4b0h21W9nXsAAAAAAAAAAAAAAizUHnirdJVfq3KbrUwAAAAAAEkkAAAAEQ7ivq2KruDwhdRHfiAAAAAEm0SUGAkgjDDSnQSiAPK6/KM27wAAAAAE7pASkAEWaaW2TD5k8/mZSTHScAAAAAh1IQ83AAQHxxDbM0pov2sO++/WSAAAAEEWFigxAEwkzm7/7bu2+TW/aWNPEAAAmcqE0bbUi26BHOK8N/UAb6Syqn44AAAAnkrfTWbzHUtpCsFPValN+w5YhSEWAAETiVAsCUmSu2gAEEFsAAkkEEgkAAakAAgIzZNGy2A70iwWSUgpbb/7ffTVw2yAAG7AUPQz22zCbtz7RrB4wklG1tWMGUgAAij7HmSf3HebVUE4qpFr8UDiqodyZygAGPAqxbba6RT0GQmuAndkrqNkNPOTg0AAm7CEPbbbeb0UhrNwtLIJdUwr2VsYyQAE0SsSTzWF3ocNHO2Oef6oegmdr6De4gAAEVOQyIQx2DR5K+JBq34akmKfHlcYgAAEwoDuxb+z8ppClL2QHSejNyhmAvXTAAAAGWzX6SSyotX/9qqK3Y+VmIESBgKygAAECyECUAg98EpAAAfoEn1DPWYyFm0gAAAAEkAAAAiTQWUEAUSy02gmqTfFuCOkAAAAAAAAAmBcIgggGG2W2i2WCWZBIgyAAAAAAAAAAw0aAEkkC0S0myWyHzvFoikAAAAAAAAAkwn0EEgAWy2y2WWU+38J6igAAAAAAAAAL+AiEwgEy0mmyEBkSyq14WAAAAAAAAAgtIotHlpjrknIltFCtNikmwAAAAAAAAAQUyyQkEigkEAkgAgg0WCugAAAAAAAAAAECSEgQCQAAAAAAAAAiAAAg/8QAKxEBAAIBAgQEBwEBAQAAAAAAAQARIRAxQVFhcSCBkdEwQKGxwfDx4VBg/9oACAEDAQE/EP8A0F/AKTsly5egpL0Wly3/AIK0tg1CvQPeCU5zaH1Z/NPeIU56B+zEMPonvP5J7z+Ce8/knvP4p7z+Ke8/jnvP1z3j/L95+2e8/bPefpnvP0z3n6pP3T3n7hP2Cfok/RI/yJ+wT+gT+gT+sT+sT+kT+kQ0SCpT0+f+iYHo4387VGMlWF1jGK89pcHLkVBNYKbliI9IkCllo4TOLDjX40XLly/kgcGXouXFfz4fSP2jmbtO5XkmSLkjyDzFp0HzjzPsgm3S8W3zXf4wv58RFy9R0IsP02+f+gftKBTpzRauv3rH7BgSg3UBXM3uXEzJuBqy63ePTGOuIuUoll7nBHtLl6j4XuudK2DDUIGgQggj7/8AHz/079ol8Crlwc4Ml+FI3QABRWOLvoeOw3oF7FpTy7bQBQHADdBz6t+AMsukw6FuhAgQIECBAhBBBBBGLOr8fPrL1fmUyVNAbsHqFxw13q6izEs5rAuXYxxeEWuDgiJ5OdRhhhldCCSCCSSSSSTRJIJJJI3+77Hz/wC86zbYvHY736ec4ItsWubd1DI0UOUWExvVjRuvA4Bu74jChsymMqteQ1oxYuoQggg0AQEIIIINMkkkkkkV3n2IfPYd99mMB3G87Snxm7Cn1ixcAO9LlqwL4GDyNDCy4ENAIQIQQaEkgkkgcIIIJIIFfo2Pn/0HJlzz9L4zGTI1kp7lLGBs9i/ySska4m0p0BsIuQctBRYOEkkEDgoQQSKCg4SSSTUv1j5/9RyYWSlu1deUdKDqB7L6zALm2w57QjKdjSjz25R03Z35w1lVgoQQ7gIeJw6JPRh0wkkgMJJBCQ8odEIJE7QGefz5sXm+0pr+IAy3wxBvNJRnlgXEByVw9snpUIPYoFVp0pz7SnR5AKHZ2PeX1jpt9oaQXO109Nq9Y9JeaTN995WBGyAvnBka2Xtb2MRW+hxiqo5ET6sRefGKxnr/AIXAG0Dhz5V+Y6aBqm6Tne37tL3nMV2bjkaOmMb3xISJBcOHPPbbvDoBVmroYUwJ54+pFaxNwceU3TqU2e8tRR83yGphmouRKr7s26/LaGfu3g226v2mJICYqk+nGEiqHLH0lKutXWNiMIrfSvWohjdGcfecuLXz5t9owAAHBpumqxiA9qKtd3e1yBRDgMvvYUpzd1SDW++5M2wFq5Xzz5wFSztEeA8oXFek6R9GZbr0ZvsG+zAJydn2m6ein4m8B2uGln1H8kMq9u9L2iqS5wT15x2HXd9Im2JybH+RK6O1/mUQB32/yHAtOCi71h9IeeHZGTzxHdqh4Jf5R1QHpZ6UykKTay313+sCRgfPPRbZsYdv9hlrDexGFNeNfpMsZw4sTqNwwgQW6SuHDL8+6bo/aLtjUXhUUFhsE4u0eAWywXG8xyv7QyRjnNhcJt3M7RfePAMEva6n5gzktjpUY3rcZwHaHuHE6EZ3s8Od73L0IDgCvLtESxVmtmWm6zb5bpyIChtzbl6VoNl3iaMBnjnpm4DeN+8SKuXl1lw2N+0vQ3Q5KWQrh03lVD59qD9CbeGLG6bv+8N43rSd1gHcKcdbhpYE9IzvtZXqU39DGcyhYm1uJ7YuicbIJY0/iLEF/iC0sWAuNPfGPFDaArlBQbOtBQUuQ0Tqh1Q6odWi8JIJ85H5+Ncsly5cuXL+F+k5MHgXkN4Soeqq+mipFO9iumG/XeFDK+OSsYtLStzFOoIKIwD4zqwYL76JcVFy0IHByvcRH1HfaXg4ODnnr8vi7KQuUSgaZ7W8Ngg9If3/AEiXacM3vW3Xh0lfD6MNnVwp95gdaurO3OVZ2tb+r6QND4H1z7TeAWqAc1wESN1tkb48NAqi67fmOi4fKh/DFpY8FA4K735fEYhRuugbX9/KYhk6F/Uekqqu7p9KOidp0/r7JSiedDnfhLtBevoWkuzfMpYfQGEYfA+ufaG2widzJC6FLFc3GcuilmNmJfFxltFpaWlpaW+T4D2reX8y/hoMQFvYmxMCV1xd42M432gWkaLrHFrtGC0s8Ive93HGg4RgY3hvbh3vptxgcE52tStM57feEutXgf5M45rz2+kH4LKnzX2mOpRxnKLgWq9sxONPBG+e1X9IprG+V1na0wecJSyzefWUY2AXgbaMtG83mEspAnRsM65OuTrk65OzOzOzOxOxO1OxO1rQrQQvqsy5oNIJpJ0JfwnBqdKcKpTBV1+Y/D6DT8vXaAwKec4o78feK3Z7wUq2u7MvY7RRbljUOU3+sMg2MEZWpL0vW25oIoddouDKEwj33iVZ4u/Le72jV25dENr51LC5G3Dlv+JYgm3bt77xDiSAGwee7xZT+f8AZX+f9lf5/wBlf5/2V/mfvU/epf8AmX/mX/mX5PSftJ0npOkjNh9J9mxjoDPSNdU3vZ7+k5AFcYAV9CXVZO8QFcO8ULF+c4h9UaVM1t0+Gy2Oxyv8SlwftLINjwQXy4yuKo54CufBmaFTfn9fxA8iVmrbeuGFKH5Y9KmTadTNvty8NStBj8UIn/Ad35w+xmCbhuSXxOdByAe5KQUZA9eKW484UhfLNU7cs+UyIWjfe/iFnAdxCu4LHaVkDkr2FgX0Irw7NWUXxHh2pYruYee30+Dt4Ll+Mlx0qVKlHOVKlSpUqVKlaVKlfGAc1alCpxWvI3OPKKa4GSxXYvbq8pbzUsKuq90e28WFyBgK4sc+EAvADPNyevnLG3BwiF5EKvSWzmUJx652IrRtkeRu4hAo20uXLly5ejpcXW5ejpmZhGZhemZvKjMzMzojKZmZmZTK+Ixok3jisnpB8npNooecZZR+kQ4IvE16/abPT1li7ao/XR8FaLKjoEdEDSpcC9N9aiQJUqVCVcSmVGVpUSBZqVMR+GVKmPedaFuWURkyasRhti+AZcvx4jK0rQ1qZmZUuVM6ZjeudMy2Fy2FkXR0D4W2ht4Km3wCPgqUwvwmtxZcWXLly5cuXLly5cvReZei/jVCX4ala1KlQI+B0qG2hLJvowqWQSCaU5xRlkRLJZMXBJxlkXEJZFLi6OhH4V1NoeKpWjoIXS5tFYsFlwcTMHQdSVpWlXKgQIGYBEJSJKuU0VK8Fw+IWSyYmNLixZelakvRlyiY5SiKQm3jqVKlSpUqOlabcZtHx41Pj3KleNhElRJUCVKiSmBpmUzOmYXLZctlumZbLlxZct8FQKlZiaHzDNvCw8Lpctly/ARly5ehGbQdK8df9atL14+Dj8yuly4fLPhYSv8Au5nGMI/PB83XgrVh8wy4fFvS/g2S/g1E0vRg/LJiWkGYmNMa1KmJRK66Y5yjnKIkrrKlaYlEo5x0dK+Ay4wP+7X/AIT/xAAqEQEAAgECBAUFAQEBAAAAAAABABEhEDFBUWFxIIGRofAwQMHR8bHhUP/aAAgBAgEBPxD/ANO//CCYmIsGX0ly9KlSusCVKlSpUpmXFlubO9j1s72d76ynN9Z3PrDviZSAgEqJX3wwHGj1nJDu/Ufpb0T+J/SYDsPdH4lp+Zj+8/qf3n9T+0/qH/Tf1P6b+p8V/U+K/qfJf1Pgv6nyX9T57+of358tny2fLZ/DZ/HZ/FZ/FZ/HZ/NZ/JZ/JZ/MZ/EZ/AY4AgGzr99YfX/CMAjwd6SduFXfSJAupRc73eWyt+FaL9x3pHo8XiORBgmUIoAWpBbXXnUuXL0uXBly5cuXL8Ny/EB0DI3/AH3vfwlljhWQHmOEg0tPJGaAJcuKRsOQffaKhVhwKPI4dpcuXFly5cuXLly5cuXLiy5cYde6/N/Leb/vs+/+ECKl1HFcB5rD8Z7F5C63cOONMxVoKjkiAMtirCirg1ttYnRHW9Lly5cuXLlxi6HRdN17qWXfl/n797d/pCVWohv0TqOYwkC62ZVzdNtg5aBgFvNamAHMur5TauMqGy8c9q6eG5cuXLlxisZda6TppjDqWVZej8zd99n8zJLdgC1cAR2cOHHuXv5aFje7W1fznDxF2RseyS5fguXodFhllhlhh0GWXSYZZVjs/M3fffGcyblMMc6ykdtptY8uF+7pbgsL3Nl1wFueAOLduJAVG9G3vV+ely5cuLGGGGLixYwwsYYYdB02XQd/Ju/fsA+FkF1xKlfJiqVT0dHRUHNW8qLLXFy+bpUdFixYxYsWMLGFixhYwyysYZdBX8m/374DmQzbIDiCgL0LuZzH4BQNhpjFI84wSzy7NVVdG94ARAuncvgykpGWKsTGExERGGWWWWURlEYYZYYposXxn798BzIfslK2DwiYYN14ci7q+mecXspjfO04AUclmPWKyd46LpExlkDdjiLJERMYVGHqjLDDLL1x64yywk4wWHKP3yy9H+kBeIMx89vKzJzsYLjBfHNeeF81jEEFqLoetmPaXwoeYCcN3ntskoornx/2X0tmBqx2ve69Y+UviOK7bPpLK3uK+RwmYDdxUd3P4hAztxdvX8x0Ucw3+4oGPnecdDn3YuVc48udlPltKJV2sqx5de3vOyYF0m5XA68JYZeS8t7cj295ZAkMlJjlV5e0UpYxYWOfT3iHK3yz7VAKkdlMy0CjhYn7JWgjyPNzFVLgwjv54J74ZfmY4Whxd9+h/u0yAVHN2N/o7StAF55l+IF1eatggmj1uDU6txnL26d4YHKX9+VdwjyNDj8ZWSs5HDt+Ymbv0ggSQUFJvYpe22yekuKmgJi+2PSAFRzuGbkIwLu76Ew2Ho/cCoiuz9xW3u7P3AuLuH/WWFI7pEwx6If5AABDaw+ly2m4RRrlT/kq6vsccnmQEoHmUXvzhgKd6/FSopG27/2M1pcVh2vJ2uPjl3MD5ZJSA0OI2/we0GBQ659bJZVjycejZ7RKOQ7l9wA9puqd7/EUlRK3eMEAxwu/xcwZjJTSPc/UUCigljd544I/fAD1IEUlnWJZDHARS9yZx94DdUR9BnaDWP3GosNx/VmBjibH7gQCublgRVmUGOmeb0hisBlrLzXLq23Da39Q0OjnyraovVKmVb+MPKbMXudO0Qc1JukuDzYlsB0qVgC13Db1xDWZXHbrFqzt2lgKwc+kxBnaUi7OseYlNSo8doUBj9e5cuXLly5f0DZfCybySNHYDB7suQAVTIvAw4zgK9GPqK3EvKB2FmRWAwlr0qZ3KoBdA2cgpuXxqGzMEjRZ+Y0JNc4jYLgCoZlJSEMxUVblYjWkSsTEsYYYYdB6NTyc/iP1LgLLcpaWlpTKfBfhqM+U5xWcOraArHRd++mtQDajZ5n+QIICuGBXTwN5fA6SkozEalCUSiUlJSViYzeRCL2PZzKSkRo8hv4j9Tf3Vdji+RG8cLY70G+6/wCMHaHxOfR9IV3nHFbXv049Z/WIreG6pR7S1dtV1Zz5ZxGOqC7GXgbcWMtWp4Ll6e1f7HQ2GAN1diZnq0zY1lOFNaAxqO2Fut9pm9/J+4AAHxcs03+b8aD6SzsLheXevYlGlT13rpdweyjufuCtf3pn8g/cswHJCY6XDAGF8mL40P4leWXRYZ92oNw8NxdAns3+yvgwjyTZqUOkNAIF4Vvj0lx1S8n8rlJwBVbVwly5cuXov7EABb66v1/jQfRWG8ytHdhkyyMispXfnOAz6i8GXB1mMZXgW+A7l86hgKcSlPHDy9+EMJVrBt1QF9CIkF4xuc6cnnGiBW2DfY3lAHGDy3978IZfgCC5wEFYNvCKhQN3EqupzwHrdQGacidazyzMOunDH7hrFZuOHM5naGLZppKpHekcjSOec6bOmzps6LO76TuzuzuTvek73pO96Tvek6r6TqvpE8rMF3OtpBiyPiGfMM+AZ8gz4Bgtt1f40H0Wdgb3bem8flCcG9oEqgdJU3RflODnvAFFVKmsEuV3vPeHivUlfyRS2m+8Nhq3zKkw5bnPaucJWRndzm888wS3nX/kKHINrg5HKF3lSBDFq8gA5AS39/8AJ87/AOT53/yHzf8Ak+dyv9yv9zrfWdZOsnWes6yddOuhNKqD2zuQkljtC27bVwneA4RLcKC8HaE0HsgEUzgD7QoWxz1HjdKwgPOt/OYaFfrBiKeJJ5uD7y2ALllb5bJ+JiVbbl9PzNpq8Xiz0t6XKdKHfPm3ANy6OMem7xz4Lly9dtT/AOAajxrA6hPLb1YapHFjsVWXVfKEN8BbcM1eI0JTDF+aRo0Bcch/uJ3OQC8LoUObGAFW4Mq42QL6sQwmxZlzKpb8s77wNkGPTf3vwVpUrTfQ3mcvLRpKlSpiYmJZMSznLJZLJZLJZLJSWS5cuXLly5cGy0vAn0HShbZ66G37hKpQd4leMAu7q6KrnVRaOJcyLc8o10Fcbu/aH0Fis79NkUdXzjFMS0WvNUP6DhHddiM6wPoGhzLl6FJiWSyWMuBJcWOgwsaSjTvd4EhNtITS4cyVGaG8JVq4CUlUDxMAltCgYHWdZ6zaC+kbsJ7zgDDKR9v9jvk+krnRdv426xbh4c6XqM6rTLIoS40FwEpvBDMAsoZgilBiFmANRKbQADE1VTGUrVQK2DbECVUCtpsCNNtoo2mBRG3CD0gkDtMMQ0w8Z3Mz6dLF3OowHCsOfoUAlfQNdjrRKzGmJUAlR47mKqFoIQGBAKNmIGIYmrjgSqXBpUDxSpAOLgQsYpyxjeVq7jAzDF3AOcBtAm6HN+NPBujDqhov6LDUaK0I3ehTKYiktAYwcYl3BR3aXikqLgqSW4ypzLRRjKptBR6VLQpWLcFCOg0yqlw3bQ8VRNFmLOt+OpWgU3iBKmOISyJGWw2mdCRiYkmZaFWWqZl7gbZTctiUxtlFYGklpci+yA3GUYKFrMCbwgWjQ+gwWx82LN5TKZaX0UTEaS0y4zOBUpBzN+l6XFojA3B1BlDC8qJaGE2S+YMFcrYq47kvEblyK5RRalkQBIJ3YLnHylsGyVFTBSh9B0pu3lfWdIl4tlrrXjOSO8TSjRmItSjeoVATBwijOyB2qU5QJwlOUpyiXhKcoUxjlDlRMlSm4QtyQC9pRwkQ2lnKNNpSU0INnQ+hX0wlStTmJKZUqVqqwghvHwdbXvFy8KlS0XDGpeMri4KIy0EFQZCAYjLtJ9KpUrSpUqV4XQbx+mhoWl+el1pfnL851pfnLaHWhJWeBZcHS9CJcSoP2zHQfp75fjYaVK0ePokHQ1oPtkhwSo/S3SvDWjDwvEHw14Bgw4vQ+3uLL+lu1qVK8dabIeJPAMNmh9qw5IuhL+jv0vQ1fGoOl+FPAQU19s6DiWSkpKSsrKykpKSky0U1KlfQBkUBKSkDKysrKSkpKaK6D7V0yxjradKdKW5S/KX5S3KX5S3KdCW5S3KW5S3KW5S3KW5S/KX5S/KX5S/KX5S/KW5S/Kdsvyl+U7JblOydk7J2TtiPCW1PtKlfSqVKlaVKlSpUqVKlSpUqVKlSpUCVKiQJUDSvuGPgJxjoacY+B+yYfQ//xAApEAEAAgICAQIFBQEBAAAAAAABABEhMUFRYXGBEJGhsdEgQMHw8eEw/9oACAEBAAE/EP29nfws7+Fks7/RcuWdyzuWSzuWdyzuWdyzuWdkp2TyE8hPA+c8b5zx/nP9An+wRDZ+yf4T8xLZez8y5x26H7fvnEue3U2oKcoa4LbuXc2Fc3+9Rkr+xmH/AEF4MpLq4z/AZpZNYPLv8MH55z+AMDoF5SKALq5VkFZk7T7yANRuwVC9qPQiBkvN3HHp/Qv5zET1xG5ghezxNNt7sNHtK3N3bRbJ/Fv7w9il/wBRkqfGeX1UU3Luy4cB9VT7qKbB3b93G0nZq6n5owjkkf3c+jEZJAbOFJuQ9Hn2m+5p+IMFWjDVPsEIwtO1DTKjkCQMtyEGBwlBxdfvfJi+RLnKh6mXqSvVrWyvQBFC9ackdZay+ExrFwANtKxRRFICJwnwSLZjuP4hal7Ql9MhOvhv5/eA2x94coNcQP8Apw7B+BYLvwY/CC/g/CYvqV+Eu4v6dS3+h9J/cP4h95/w+Dx1fUhYB1ETlGL0+gKBsKeoMO7N4G/z+9/q+mEAKtam7eftCUCkBA3AeR1AqSkSYCq1cjVMp1KcgcyrfI2UeblWUJVK10VuCy838SnXwe6U8z0Qn0y7ipbkuW7QfUK7Mwg98PJ8AbB6U9EPGJ0xemFOGD6lArnhV26j5n8/vf6jpgwKaDD2HMJEgnehso3iIWHdIUXShieiHOsnJWioy8wRwgJna+cu13M34K9ML8St6v0IdFXwBdEt4g+69oSeqEinFSjqDxiC6nhngh4zkpDw+kwYhIWshKPL0mGawHqRlDUTq7/z+9/q+0SSiNyd0u5TaILBmi1HlqiXw7ocDpAN4Uwz0xz7mSOZIsAjy0SoFX6CAAwIjWHct1PRMoR6ZnuHhCK9Q8PSD4gUG8SysfKCdyysb5YlmhE/xFeI54ReoFcppwlOWEa4xXiYSq3lgLrpVUq/z+9/t+0LFB4eRf1uZ+FVnVn1CKO2P/QTQwFbXNy16hlyZ3tmqM2A0biUZ8wMKl21XaspA/iHhcvV1RBYxPqhJ4QE6CWx4anUTvJogmsTwIruD6T/AKp4IEeP6QY/qJ2CV+qtwQld01/e2H9vKPZYKrvd/wCYsW19QLIqsHRo4GWOhuqQ/wBxyaiBGgXVvEbjgWCoWNINJqAgOoeMIDAyzjMyy3iaeYWqni+GbGK+ALGMM/6GXZr4LTggdL+gKaMEP8IZaq8/JgCnla9X/eqn6+4llOVgUryDL5QgEqZNmo0opaz40rz2wttgHiYAYHcy2FS4FalNWsSADCEoysBPZjFQLgahT8Qk6Es4l1YhY1PH7wg16zXRCHtmmzMI4g1kmJZMTCuGC8ToWw2Op8oBFkeGHGmCpFLmFQl7F9/h/vcH/taNOl6/WMJtEpyUSx3qYjdBUKAFoDzBFd8xmPK+uAOR7R7q7CP0MEKwm3iI8RFMRMcsonTGM/AEYqvMbGJ0q8x8UXEKXcdppnQiaqpwoZsjx+D3qdKM8RkakKdKsQEIav8Akfx+9x/o5zC9P6MLWzzRNdGrqrpqVyTb8TXrbspXGYvbPM4BvW+oxG1zuEN+iE9xcMS1FkuH4aMetleKhSzB1iXCt5tYes56iwzhZpSDn7EW/qiQ2Mn2mBU4cIZ+gi3hZ5qYnwY6rE9p57OJSewzg/REIlKvkUFVYtq9n+P3v9b3gRt5C/MAHCBXLDDRl8wHaFinXLlRnqD7uxAaHJM7NQC4EEbQemtQSrbVPHD7xmlLrom9l+Yg2Fp3EMn0gReHE2q/KX4axaCp4CI4HOBs02afDOM+mYNb9JZss6/fUTD9koDkzTr7TX9hLdZ+k7KwG7e0E6e0GKJMgf8AMdBFWiBuMxY2AcTwmvz/AAfvf7/vNSExHTRxg89pAFSQpoAlJLFXwHAShGW0xMDnUNinPoAKgQPgaAUlDL29YvzvlN1lCFmc45E4hCucpLJ3SrcfVXTK33RZHiBhfu4GtHiysjTsltjLHQKpc7KrlWNNwo8R2CaAHZWALAFDyLeXD29YnJk8KJmyI3q2zzL9/oKwrIbNjdcTB4oq3lopQWxXcWOI8QUqClF2BsrKyqxr1qsFFLUMAG2GvDcoGpa40NMckFQUR6AGkJTXnppj2aNUs6AAqIziGH7Y9a2qa7/7jnogJAlnLpN3wRsLnPFYoOL1gsVZ3LLiWuy794crIR7VNjfAEtiDQ7VFI1VXW4OGXgfBTUiHRtoHc2TdXacltnlDDDpa0uL0CJ73Hlvgz8jw6M1xGMDaEzRBT8ynJ2Zpl7M+nXiWm+dUBKMjVwxKy/XF+373+77xQWRpatEBvCtRvAY+x9I2EHXNLey9JXTebSNihSi20rKHUGVQLcDKxkJWURIqhLA0RofXrTwTAdt95lmvlGW5bob7sFxMQuI7CxumAoOaomPDFHxaMXhGlAkzn/uRSEvQq3zGuUOeiqaB9ZZFrFwmxPsInrS3JxkvyZqEWnnOMLj/AEBqZAyxvNQuYSy+LYPkL5hIRRU7oQzyMygT5bfFKFXmkl7RL1AYo/OIGsCCUgN5sg9RJWvF1N7phvd2aHlZ3dUScZxwkFwVY7GT0L+Es1XgEFagPUZZ2cZXlE0BtEhdGUgaDXLjDVkBrKXLw2H+kXB/rtsCYRxyPpH9ZgGMD4O6CPEbhIlKZGgesp7YcVbQxN3s8EJ5C/n/AO5ZLJcsln/k6/uZxb7PINImPUjcygAmzJZL9ELyhG6aiFRYwAsWNR+amiWJi01ZcVKzrC1EtzDhabKNWAoQIJ7jAG3IXOSI0rohQ0Y+ANWNDSyi1l3A60NXRtwYWaxcHZflB1OKHdRQJspL7CCv0lv3qphsNFX3B+mxkQVdK69o1kiBxqRoVjHeWVN2kAXLVJKV3TWIHIpXb1sFpdpz3DDwPpx5QDBaFHVCtcXUq05lKEbohMdvdzEs6w2eVYDncYbYCjYaL5y2MxtJRaZbOHMx9s5NMKJHOiK1YxWMiAsHTrEDKJBK8JeAIqZFJHpFDW7K6g1+Ll1OzOvaUAfkufTEHQGKaBvjFyozTU2rS4MEPWQ4AYFtF7ZSoMqvyP4/9GHziZTueqfPK/CHueqer4gnpLO5ZHpP7ZzhNFE9mvtDzhdVBQzktxKOjQCwyAgVZtaOEFNBFVgpQcvMvDUMgXx3gu7VKgRcpm7oYUGbYeIEY0IVQvH1gENhW3ivRc3A9eYdjPMLdq6FKH55l02is4tuoG+frA7WfMUklwDM+pDXavlJiZ+am0NO4LVqcWy0xt1fMAcsXmG5fP8A7OK655hjK+coYXzgEtR1r7ojV6+ZxrnrK4/MM531n5mNt9Yt/ZcZ2+zW+SKF0U/M/wAf+aolcYFzQ0+yJfk9Sa3S+kyeS6SNsvRf5gml/t3FbW8l9mFOYeUL+Yv0LUfOCZ/9sXlpKNWD5CwtUzN1gtfFmTnPJMf5zC3rosZsHTPEAGqyAL5ksFK9zSb8y9EAaO3ERw0cE8tvcEN3j1ggkZtGl5zLOfM3Ns54mCwerO3YwMU2PoOIPlfOf0MUeMFX8oml/L8QLo+qPz65Vxh5soU4gafM3qcr2RVV6MzzHvDdPeCsyg8tL1t030/4ln/lSRYJL6wUqO9vZMkZYakT0GMObyYmAZEwqItlVaBbQ2yyogmOEGAlLrEpQ05DW/kxnUhytgcOeuY0Z96KpCF1HKZ8yxlN6S1tIZ6p3LaLKVpbXkqNxrEqo8/Bv4KUZgnMo1KgWlx5yaLRi6tA+agR1JcFe6VmvQ+c9Uyb3PYbKFWJi5UCJbdhLKvqvWer6z1T+sy/bPUy3fwDyS/DB8PrLdvSW7gzmeSeTDowXbDzQXeCOcBkOSiipA9UmzbxmD3b3juN1/nCi1i+va/iWFf+NU3xEwJhcisB6rfMWjKFpJYLUwp6SxgbZF41lWoJJEKWGcrVC0SGlFi/jzpYgEvnMXc+lCoLPAuLYPRLSkHWV0FYzB+FfooFSgc8waYPzixmeE3ynmUXmUZ+e4WeAWD8PoBgM44Q56lZuilKgCMgy9fCZN4xQZGxRVBqFRbZbVHzkXzPWT1k9RPWS/ZL+Uv0r4XrJ6ia7nqJ6ph/1P6XLdJZxZP7XLd/Wel85aty/SXvf1h5fWeqXhEbo/WSLXL8MNfrXHrKiHuKpWKvdAjmQ0VKW+4mPl1HW/Xxl2YHGoeuYuyj1mWqAUQK3ExCYibrmLcVoyweoJj1SCpbSiYxuEDS2m08yveIwjY9KurllyjDKXLmhuKnut/EYQzvm/7RwczZ5haTCGfWVg8KxXJDvwSxGq/jcGyAAMB5UpddlKCCOxNjKsA12KlRb4RnkxQEOYEB0VM5ICDT1xRRU5LGkISRh5ni8Aolk/wz8zm+mfmf4J+Z4fkfmeH5H5n9Qfmeb5X5nk/p6zyfK/M7h+KRP9JP9JCVUYOaAWq+IICITnQaWJzLox2MdgLxzBCUdfzaqr8R8G5aSWUBN8xIzsoQK3moMqobT8EVqtylhdXmuYoAAIcKOqvuD0svS8W6pzGg2ILftlu05Hhb+Zw9Jp/42zqgro2r1yHpDUMjVKRUOgF32zuIgP3ZajaYBSXSuoTSNNh7YiVqcrbAEQktklFfKBF5pFoYinFjY4A4nC+CU/G2NyzqMjLUCWPqANta+Bb6flLWyiRNiVCwrDL0KCl8OWkFIzFEUsrpxxUrlVtWY2Agd8xhbzHhUGRRdLHgozQEGAZBeANTcAVykwrzOq0A7VVW1WY5FvGoHQ+AS0nqf16SnK/r0nkf36Tgcnj+J/i/xD/nvxP8PP8AMz/KS9UlBorSU2NMq9UMAlIGhar233BqIhmRQsqqKR9bzETU4pNtga5K7iAfALccpZzeNlURlsCxmMwYvzSoK2gyxS4xJf0jrm9kZLuXDQm2VYpc8FHABGhlMc0p3YNc5eYOrgaJWVY1kq1zEI1Wx4JqguL6/qWiUEH/AK1CifIjV0E0TSBiHrlizpKDVA2se7BVeos1XbLkZHKy5h/JBjbVi6XS2AEeT2bCoMsYPEIm5uohwOGzVNxu6JqZXNdF8WwivfSimRC16jqERC3AACVzxn4UhqUMfd8EPib5oEbAzOV2Fbfmj4/TzfUKzZeKPD3NfBGhifFHcFc5/V9IlypX/ZUDuUFszesfD2lfE1Bm+IG7JH0W2MTO4+IN/po+GR2IwAINrqig9BhFhGTpydcI03mFQ4qyENDlQtu1cZrImH80Gd+wFd8L1FNG9inL3r7wsDAykxZ0EUD3mXYSoCL1BWLdDiiOMUm1dscA8h3ejN136CXi85plC4NysfxA7jqY8T1S0VuWfwyklQ3lZP1/5QdbIzuObRemqTMKSuf7KUbWnBxzQDxAS0Gwyi84NZjHXMHTPpSDmFQBFy8rPSU7in0gWQhNoID3maKpFoWFxRB016ujINCPVz0HoXPhz8kJrPRaT4ro7GNqXmj6LOp9oacSgxMjgEeKyyAtE3UbgWWbDK+iwvicMgAM3N32lK6CFpcNkSEqrJgiZJ9m34oIqDYc9hESAcxU9Q1BFliGoGa/8CA04toAyq+IZoCKukJTCini4Do5eWOau1YZtT2zZ0pQHLuJiWcXUhZhh3iYUiTOhG5vS3N3GNfJDMCXbsOTMIamc8CFEWTTE9vmbAmsdqWbimhS2npSPT318FHUolJSURGnMbLiXuWNIO3okJysOTpxDdLqnSQw+mwmh0ZUZFXSVHbK4RC21Usa2iKG+YvVUKzZHvs6lSyaBbhQFtNrpCb/AE3Jqm4DrWYNVYGys9BoxzAA422geUMQQktjKFoZGJr9WZutoJCa0pX24MiEQWmUVicWumDqYTYIeVcZQ2IrzOTKdwuqscVSY1MbCDZkWsjKw43I8TNSQFgqaq7yckddDCKnqwkDUX3LuqV80zMHMBg+UUF8ill0YBvZEqVxo/NRnkyqStLVgaIGbwbJpEYjEa4GzC9W0kLjZEzZglqYYw2xlQUdqsYlALQEsu6rO5SXLNcWi1i2UaQoMJtyOOcQ8araQJ4vOIqlQUw6FUkc/wAx76lcY/VuliF6hlZkpgrNpH+/RzNrdcwvPacfyj+2kIq5CBsODEWdwVpEsqtwJ7yi6yitT5IiDYRpGWkLs9K8xtEeS/WmWX4RcBRgVFzmAdMrRSCVTXfwrFyoFfBT1lWCLb8FSY8u+/KgV3kmlh0OnhmIgIFarb11iAI8/mDqt5cW3xHFyVhSnABTFSmAsbrcUVOaGCIZCAYAgSVyrY4JT0jRoq8Ao+ajoma4LQgYboVzKEdfQ6gP2l9RAgDHIZWlpuzMrp4mjRZspdb4gq+Exq5UgKC1LZX/AN8bUoqy6jfyXaYG1b0pAyMqJg8UkjzfcpDsVOrKp0HriCMiBcld4wUpDiAF3RjbvOF+jLfVB9AlIEEvY3uoDY1nRz2IKEsxfbdg50kYSqS5Y9vtXOJxTXgt4lME4Wbyi1gLM3HxBofT8lwqtsKetFlSpg3Azq5QFsSdlbMuhdTHlzJAjIXVHGY9Mw2MlVIlll7lrn2RLyKylbsTiUVRSkk2IoIKUKjlIpGUZ02RjNyyRLRjXLRgz8LbaUiIqteIOnCoJAUEyX/EUa/TCFPncWfSGT9LMVyserEVYmFLMwQlKBFlLa74JXJDeQ8WD94muUkRVuOdAhgXBDLoBLlY/rdkQ2b5F878QommdWGhKvVxafAClsru0kuvOH0omYaFo4MswE1x+i/EtG0q09ItR+9y/hiVQlgKyeBzDQ4Cm1LDIsLNhqIKhkpm6XDV4iNTzPNSh0SXHLdpbNwotD4s7iAETUPloFEVoMy9t4HWGCsWGslRFOpPeGbAb4ZquYlTmXRQFywm+IWR5ju2dB2S+IO26/fyCFFq+LYocgUZKgyV6agYRRpIXD4zFtXVs0nK9KCMeSKLeZzusguGosHdX4hvSFeAoWpF9GU6HCQERwo2OxJYHuuowIaI+pB6AT5C7rYGzRxmuNJnQrDn9AzZB2qbLlU4FucAF6pALRrQbWY151ojAYLk9EJWLBqbr5Ah8e1IAIFsCGXipkO4yYoZiUFm8suV4+kgAXipeRO4GmiYiRsxAZFKxFKQgnpKAL8nce+SXPMHHZbBMarSxCdGMlIPSCYmBGECsdGY+usFGqcBFtLjEQ6zdiGgK46jq4DR6mBaclIKrAbclQeypNo7z+m6ElPznQz1yyXULYTai0HV8TWU6rlzs1Cq3uEgUKAOAgQM6hWcr/4PE2gNxXrFbhZ75cDydhUPMu0AFEA5O41N5YlaK1gm2PmIYRgCSBtaZfu7vn58JhyxVxxwWYSobKrmXXE1BIqseVRxcv8AkBuMRJkXyzEvcIoGQYC7aaGBq3DLsm5LctNrlgutdOUmamKhoLFNuRTqJmkDtmkAufAtxCYbYJySW4R5zTMo7VlFAgD1WcV+28gZpKYqrjTHY1kfM7Gk6IX4EdbC4jX2yV79pRnjMDns4G6LcS0FUAwo0TnyhuTy6hAq9UMVQOBwApYOUHYsX1RdQ0paFgDaxMsHw17BhXYiJMydYsfvOruNsJRAD2bGWkNkliloRtoEBhZzWqLl/rcJvq2gruJ6zizMB5bq4K4cmoFA5nSxqpp5TS8XAfbKXClEKWkKEfDyQvrig5cKVRoh31lw7Aej2EPoj1+lOphmBiX5iWbZQth43XvCXUJaTdulMht3DVbY+A6+BFnwuOC8tcB7saCxUuHmfRFxzy5dD95HKGTfDgy/aYLMFAHOAJq68qjd1G3wARYrMpLfFWEwpIkCbaI5RjbzUWa1QNVrBdnn09Yp48ZasLTYKrzA3oyaxyAZ2s8ZveNxQypU4oZsnI2rzKF4sdQUlA8oO50KgPCrpLztW3gUZu3iJLh1CfDVWAOFWCN5IMjBBqweoJHTQmQ68RZIFFYnOKgctxDJhyXQ1e4VomiGS2+6vSOJ+aAEG2KDfljUeA2qsXkb29CBmFwIRSxtahy2XH6eygG5ZM4l6Lq7hgqa4mpkLtZy3GNdIBhfUNAoLXachR5y9x4lhVAXnEEQ5WgU4WsbeKxCQUhKrszcbjgaCh8LG0xA97gqKGq75gFfkRoU0j5jlx+6JyS6GYxflLDzZ/7whOpLNhm9sCV5i7tHPGaQoDm7gpY5HkYUeodsvUMZQA0DaX4uCqPrc0M7zDx4RCAbBY+sUNfqAmpUykE9JbgH6Ax0N9neSuWtm2RXAqFxY8LpBueoKKmBV9oVZCIQ90QlEbIj3D6Sz+1QZGqEH1ihpGgovg7vhg5iFBjxR8e2MgdFMurAeYtygLbnSgfMjIpXzWUB8otmYzz2pW+SYe6C/onJ3NLKFUt2qqrGFnZR9gAi+flJ3htNUxNBcks4tWCjXUGACO4mcTB5n07ifKCpa+fzJastZc5N8fpCEjrg3j7JvGyPpTvQVsIOaK8vAg2DRU2iIeTyQAN2zTkpStEqtU3ZRBbPcF6ju+JS4IbCm4LTb64gOGQuXE9MGCalp3Db2L3bVwfL2VDWHlOdY7mKx3Sig5xdOYHOBaux5XgOKSK3egrDwu6iwDPq1s5ZNjYTQBFqwj0G4GTEMFgGrXtMR0ANQAZeQYDEs0eqL2HpxmiXwOHkKCyPKrhdaxBQMO/cgwUfX6o4rPYWmNcHC0e0DTU6li+JIAbSgrm8wgDySgLzVjbFTskYBJVnoNwT3T+KBQxGjGJSI5UGHRVqMEYAYubAuBfiDfQCwLdtAzZiIJSLI3lGBALR5t/iEaTJbAVdBzt4y5GX2UJQu8B1aXzgG537+UcVn6nUsgOVkR5RglzvAT1ET+jNCGFDBm4+jAvmohs3oXq5VQZwL4jY+sp6BWTU5jCfEozzlMA3LwkOixCYbnb+7Psgk4YOPoQvKVqBC69oeFSmFHtqv6LNqZbMDA7Xiit8NlIW6koMWRkErN1MIuLJddVKvtEvxV5OCGwHb/KQyudUNIqRGeqSszZiRUVa03LfmW/RDo0rfpp4HpAZEFDdCgCFNN0QfJBB0FVgh3Wc7plKKqpdgdfgPkmJEKYhxVTHK2iYozkUu9wZ3lxIi+JpVoxDhbGVaTCfjhtwVFzKrUWUCeVZrZUy+6xDmhLrOIjErYukaYWhs0ZYoWCNTThpk1DKs42wqpsDRiIaWHGZVedJ9/gpbbBuFu3tyzjdt7FrBIMC1ealpA+GG2pXwy3MKGognltKWZLxCw9JQjamrpYu63H5aLLbVTae4zDcfsJ6aLgqq5Y7jutOjDyBHmJtkFyxAU9ozPTAhapFrEgpxiClDYS6FEOadEJ1xY1zWaqA2saj7mo1bDyHZzKQBy0AKq2WahWrTnkfS+pFqP8AX/pBzAfPcxXoAxdsb4Kmk90v7yhwnISUmAehLtw98OFfAriUwWQ1j4OZR/ugLSFacB8mmGq94gz6ycZa6WshXSrTztgKV3TAX0Rt0nwFtr7THnw5cWALlYs6qHVNUGcZZYM7PK0KEXMw30mfsezigODQG5XDM3+lvBpxxCs+ghplQ8RUrUN1dNTb7qS5vBzPW4JukjGUHsaNmn3oC3i6hBVYcSggKUXDmass4wZi73qhBU43GV94wwPqZ9vCgF21iVM14Ay80eEJUF3KBcqFV5MBKioXjqbLDnJGVYkNVCssvTwunkBW2HMcyR6Jkm0+uTfAiIBaRAO5fF6OJqXBqrDdAEYOUkx0ywBfvnMaVKyFiacSLMIKWx4sLji/gUNfrQx7EpKynUpKyspDw94AfF18PfXUp1wI8kPug3XRLuEs7vEGszzg+3mC/wAy+L1LYPbBtbGoxBlqAzKNMeFbD452sLmfB+ZPWGaXlwp3CcT1994ufK1X3K7dHEq9oXU7dQ3cUoLlV1DTSzco9x7oK+8z3Fd6ia3Fiy+93iNfmWwptLL4ORW2DCdvVlO7dt08Yx5mQ6iAqW82NVx3mOTA2gxLFljOYvloPgWfeZiLj/yo6lEo6lHUo6lHX6VF8ojMwdsC8KLhde8Mw488THVzPpCVmVK96nMJoYfrZWczGNtk9whXvzCu3c202QWc13E7O9wRQFERf0RLgMS1hMq1UEtm76swkcsxJW8xPES/aPmPpiIue44MF8SzvsCv/ITNFgYN/tZ1bqK43dO0r9oLxmHp7wqf25bOL5JWbnFwPrKgaj0cVX3V/MQ5K3Uv5qCI3AiZ1AYrNMzzcWtX/SbivMXK9bhh/Edka7i+4QZc8ysVnogyCdRP+xIl7IG7lXE74iRrHibd/AFVoLyf8Isy4/aOc+qKRQs+oxfVFJrCF/8APhhrqB8uIV7zxC+ZTuUwOSHkf9h/MvbNkXv2gnfMolW28SpSuMVDy1FFAwag+qPTXZMrvIi+7iK9dRu8Tbp40RT9JdTwnH3i1OaiBrPw34eIkfLMuBt14oVn3qKkmYg1PR+y5ek3lqqb6rP5ziyh71CjeYPEw3Kn9xKrh9ZTAxApYemYEcFKv1f5hlMTbqK0Nc3LDgzY8es8Wr3A03frO3c8nEHdXbHWnMwRIboxXDzKrmSO1CEiL6xuJEWeflEvXzg+fc1z7y+5kS+PmQ7VzKIN+qZ+szQbD9m9zdATl67WEp9ILLRFYR9J/KLIdp9yPwN7kCrL5k8rO8ky5XzIM0e+yD9r8k/Gwgy8/UhZrfoiBYzIjE5RaBxieiaalfdJryX8VgMqwemLtLzjH9Zl4r+2emrqspK/alw0Ki8RajbQ18QclXc5TlIQyBuVqQwRdlbpGPc+MTKgnl+RA5w+kUa+nEokvvR/1iG/IiHJ7ocSO0VD9e7wmyyPMuP2d6e50INtCOrkqAVwMfZVMTlTCSe4fKMPCLYHU/Nk6vAuiNDYTzAHyXDk5CNeOfJPYi3pwETswFrb5pjiXOYD9U7Q3obmTFxzAa7OpmhYEnU3dDOxfxgYQVcDgoo28XI0VgvtBWz1sAjISu8bT0h3rv6VQkY8NFIx4+FCfV1/JPSjqYwj5/8AeDGG7tX73EeE92PMT1PuSwUQdkq1D1uyTVThfyIaHtJvnSVunaYPoql7RLkbdxMYuML/AGY+stuGoBOmL2B2oimW9s/zX4nhfb+IX3n8figf4sfAen8fCo/5GHTe38T/ADP4n+Z/E/zs9C9P4n+d/EOq9v4l/wCL+ID+HP8AP/if5f8AEf8Akp/k5/nY57+R/Es/En+Uj/w34n+cn+ej/wApP8RDi+Wln40TdzJ/Ch1ftB1RehB69EIpYz6QkpDRDxhSH7I6fj/D49fA4jpjtho/cun/AMBo+H//2Q==");
			scenario.write("device:" + obj.toString());
		}
		//----------------- ANNUNZIATA ----------------------------
		scenario.write("video:" + videoName);
		//nuovo test reporter
		scenario.write("execDate:"+this.executionDate);
		//connectionsUp();
		// TestEnvironment.get().writeDeviceInfo(scenario);
		// if (scenario.isFailed()){
		// takeScreenshotOnFailure(scenario);
		// }else
		// {
		// embedScreenshot(scenario);
		// }

		setConnectionON();
		
		if(driver instanceof IOSDriver)
			restoreDefaultSettings();
		
		if (driver != null)
			driver.quit();
		try {
			if (bpol != null) {
				bpol.close();
			}
		} catch (Exception ale) {

		}

		try {
			if(scenario.isFailed())
				BugReporter.get(
						this.getClass().getClassLoader().getResourceAsStream("bug.reporter.config.properties"))
				.reportBug(scenario, "it.poste.bancoposta", AppVersion.version,videoName);
		} catch (Exception e) {
			// TODO: handle exception
		}


		try {
			SoftAssertion.get().getAssertion().assertAll();
		} catch (AssertionError e) {
			BugReporter.get(
					this.getClass().getClassLoader().getResourceAsStream("bug.reporter.config.properties"))
			.reportBug(scenario, e, AppVersion.version,videoName);
			throw e;
		}
		//----------------- ANNUNZIATA ----------------------------
		//		if(concatVideo)
		//		{
		//			concatVideos(this.videoName,this.bpolVideo0,this.deviceVideo,this.bpolVideo1);
		//		}
		//----------------- ANNUNZIATA ----------------------------
	}

	private void setConnectionON() 
	{
		if(driver instanceof AndroidDriver)
		{
			try {
				Properties p=UIUtils.ui().getCurrentProperties();
				AdbCommandPrompt c=new AdbCommandPrompt(p.getProperty("adb.path"), p.getProperty("ios.udid"));

				c.wifi(true);
				c.mobileData(true);
			} catch (Exception e) {
				// TODO: handle exception
			} catch(AssertionError err)
			{

			}
		}
		else if(driver instanceof IOSDriver)
		{
			try
			{
				Properties pro=UIUtils.ui().getCurrentProperties();
				
				DeviceNative d=(DeviceNative) UiPageManager.get("it.poste.bancoposta.pages.managers")
						.getPageManager("DeviceNative", driver.getClass(), null);
				
				//WaitManager.get().waitMediumTime();
				//((InteractsWithApps) driver).runAppInBackground(Duration.ofSeconds(1));
				WaitManager.get().waitMediumTime();
				((InteractsWithApps) driver).activateApp("com.apple.Preferences");
				
				d.aereoModeOFF();
				d.setWifiON();
				d.clickOnBackFromWifiSetting();
				//((InteractsWithApps) driver).runAppInBackground(Duration.ofSeconds(1));
				
				((InteractsWithApps) driver).activateApp(pro.getProperty("ios.bundle.id"));
				WaitManager.get().waitShortTime();
			}
			catch(Exception err)
			{
				err.printStackTrace();
			}
		}
	}

	private void connectionsUp() {
		Properties pro = UIUtils.ui().getCurrentProperties();
		String emu = pro.getProperty("emulator");

		try {
			if(emu != null && emu.equals("true")){
				AdbCommandPrompt adb = new AdbCommandPrompt(pro.getProperty("adb.path"));
				adb.setDeviceName(pro.getProperty("ios.udid"));
				adb.startActivity("com.android.settings/.wifi.WifiSettings");
				WaitManager.get().waitLongTime();

			}else {
				String emuName = pro.getProperty("android.device.name");
				ConnectionUtils.disableAirModeADB(emuName);
				ConnectionUtils.enableDataADB(emuName);
				ConnectionUtils.enableWiFiADB(emuName);
				ConnectionUtils.enableWiFi((AndroidDriver<?>) driver);
				ConnectionUtils.enableAirModeADB(emuName);
			}


		} catch (Exception err) {
		}
	}

	//-------------------------------- ANNUNZIATA ------------------------------------

	private WebDriver bpol;
	private BPOLPage bpolPage;
	private String bpolVideo0;
	private String bpolVideo1;
	private String bpolDriverName;
	private String deviceVideo;
	private boolean concatVideo=false;
	private Properties devProp;
	private Object chiudiTuttoButton;
	private TutorialPopupPage tutorialPopup;
	private VolLibrettoSmartPage volLibretto;
	private VetrinaVolEvolutaPage volEvoluta;
	private ContoVolSubPage contoVol;
	private VolProductDetailsPage volProductDetails;
	private LibrettoVolSubPage librettoVol;
	private PurchaseProductVolPage purchaseVol;
	private SummaryPurchaseVolPage summaryPurchaseVol;
	private String pageSource;
	private List<String[]> speseDettaglioList;
	private CheckTransactionBean transactionBean;
	private String versamentoType;
	private SelectBonificoTypePage selectBonifico;

	@Then("^la notifica push viene ricevuta dall app$")
	public void la_notifica_push_viene_ricevuta_dall_app(List<NotificaPushBean> table) throws Throwable 
	{
		WaitManager.get().waitMediumTime();

		if(table.get(0).getBpol() == null)
		{
			bpol.manage().window().setPosition(new Point(0, 2000));
			UIUtils.ui().stopVideoRecording();

			((AndroidDriver<?>)driver).openNotifications();

			restartVideoRecording(table.get(0).getDevice(),"mobile-video0.mp4");

			long time=TimeUnit.MINUTES.toMillis(2);
			DeviceNative dev=(DeviceNative) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("DeviceNative", driver.getClass(), null);
			boolean find=false;

			while(time > 0)
			{
				try
				{
					dev.clickOnAutorizzaAccessoNotification("BancoPosta");
					find=true;
					break;
				}
				catch(Exception err)
				{
					Thread.sleep(TimeUnit.SECONDS.toMillis(10));
					time -=TimeUnit.SECONDS.toMillis(10);
					System.out.println("notifica non trovata attendo 10 sec...");
				}
			}

			if(!find)
				((AndroidDriver<?>) driver).pressKey(new KeyEvent(AndroidKey.BACK));

			Assert.assertTrue("notifica push non arrivata", find);
		}
		else
		{
			((AndroidDriver<?>)driver).openNotifications();

			long time=TimeUnit.MINUTES.toMillis(2);
			DeviceNative dev=(DeviceNative) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("DeviceNative", driver.getClass(), null);
			boolean find=false;

			while(time > 0)
			{
				try
				{
					dev.clickOnAutorizzaAccessoNotification(table.get(0).getNotificaTitle());
					find=true;
					break;
				}
				catch(Exception err)
				{
					Thread.sleep(TimeUnit.SECONDS.toMillis(10));
					time -=TimeUnit.SECONDS.toMillis(10);
					System.out.println("notifica non trovata attendo 10 sec...");
				}
			}

			if(!find)
				((AndroidDriver<?>) driver).pressKey(new KeyEvent(AndroidKey.BACK));

			Assert.assertTrue("notifica push non arrivata", find);
		}

	}

	private void restartVideoRecording(String config, String video) 
	{
		if (emu != null && emu.equals("true")) {

			this.deviceVideo =video;
			deviceVideo = UIUtils.ui().videoRecoder(video, config);
			UIUtils.ui().startVideoRecording();
		} else {
			deviceVideo = UIUtils.ui().mobileVideoRecorder(video, config);
			UIUtils.ui().startMobileVideoRecording((AppiumDriver<?>) driver);
		}
	}


	@Then("^dal portale BPOL si viene indirizzati alla homepage di bancoposta$")
	public void dal_portale_bpol_si_viene_indirizzati_alla_homepage_di_bancoposta() throws Throwable 
	{
		//bpolVideo1=UIUtils.ui().videoRecoder("bpol-1.mp4", bpolDriverName);
		//UIUtils.ui().startVideoRecording();
		bpolPage.checkIsBancoPostaPage();
	}
	@And("^utente Accede a BPOL$")
	public void utente_accede_a_bpol(List<CredentialAccessBean> table) throws Throwable 
	{
		concatVideo=true;
		CredentialAccessBean b=table.get(0);
		devProp=UIUtils.ui().getCurrentProperties();
		
		if(bpol == null) {
			try {
				bpol=UIUtils.ui().driver(b.getDevice());
			}catch(Exception e) {
				e.printStackTrace();
			}
			if(devProp.getProperty("device.os").equals("ios")) {
				bpol=UIUtils.ui().driver("default_ita");
			}
		}
		
//		UiPageRepository.get()
//		.loadPagesDefinitionSqlLite(new File(RegressionStep.class.getClassLoader().getResource("bp.element.repository.db").toURI()).getAbsolutePath(),null, driver, bpol);
		
		bpol.manage().window().maximize();
//		bpolDriverName=b.getDevice();
//		bpolVideo0=UIUtils.ui().videoRecoder("bpol"+Calendar.getInstance().getTimeInMillis()+".mp4", b.getDevice());
//		UIUtils.ui().startVideoRecording();
		bpolPage=(BPOLPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("BPOLApp", bpol.getClass(), null);
		
		bpolPage.setDriver(bpol);

		bpolPage.gotoAreaPersonale();

		bpolPage.login(b);
	}
	@And("^accede alla sezione Servizi online Bancoposta online$")
	public void accede_alla_sezione_servizi_online_bancoposta_online() throws Throwable 
	{
		bpolPage.gotoBancoPostaOnline();
	}
	@And("^invia la notifica push all app$")
	public void invia_la_notifica_push_all_app() throws Throwable 
	{
		bpolPage.inviaNotificaPush();
	}
	@And("^utente clicca su \"([^\"]*)\"$")
	public void utente_clicca_su_something(String op,List<CredentialAccessBean> table) throws Throwable 
	{
		//TODO:da implementare
		BPOnlineNotifica notifica=(BPOnlineNotifica) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("BPOnlineNotifica", driver.getClass(), null);

		if(op.equals("AUTORIZZA"))
		{
			notifica.clickAutorizza(table.get(0));
			notifica.clickChiudi();
		}
		else if(op.equals("NEGA"))
		{
			notifica.clickNega(table.get(0));
		}
		else if(op.equals("SPESE"))
		{
			notifica.clickSpese(table.get(0));
		}

		WaitManager.get().waitLongTime();

		if(bpol != null)
			bpol.manage().window().maximize();
		//stopVideoRecording();
	}
	@And("^Utente naviga il portale$")
	public void utente_naviga_il_portale() throws Throwable 
	{
		bpolPage.gotoContoCarteFromBP();
		WaitManager.get().waitLongTime();
		try {Thread.sleep(3000);} catch(Exception err) {}
		UIUtils.ui().stopVideoRecording();
	}
	@And("^Viene chiuso il portale$")
	public void viene_chiuso_il_portale() throws Throwable 
	{
		if(bpol != null)
			bpol.quit();

		System.out.println(this.videoName);
		System.out.println(this.bpolVideo0);
		System.out.println(this.deviceVideo);
		System.out.println(this.bpolVideo1);
	}

	@And("^L utente imposta come preferito il device$")
	public void l_utente_imposta_come_preferito_il_device(List<CredentialAccessBean> table) throws Throwable 
	{
		this.homePage=(HomePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("HomePage", driver.getClass(), null);
		homePage.setUserHeader(userHeader);
		homePage.checkPage();
		WaitManager.get().waitMediumTime();

		SettingsNotificationPage notification=homePage.openHamburgerMenu()
				.gotoSettings()
				.gotoNotification();

		WaitManager.get().waitMediumTime();
		
		notification.setPreferito(table.get(0));

		WaitManager.get().waitMediumTime();

		notification
		.gotoSettingsPage()
		.openHamburgerMenu()
		.gotoHomePage(this.userHeader);

		WaitManager.get().waitMediumTime();

		//stopVideoRecording();
	}

	//	private void concatVideos(String...files)
	//	{  	
	//		String workingDir = System.getProperty("user.dir");
	//		Properties deviceProp=UIUtils.ui().getCurrentProperties();
	//
	//		try {
	//
	//			StringBuilder sb = new StringBuilder();
	//			sb.append(workingDir).append(File.separatorChar).append("target").append(File.separatorChar)
	//			.append("embedded-videos");
	//
	//			ExecFfmpeg.concatVideos(deviceProp, sb.toString(), this.baseVideoNamePath, files);
	//			ExecFfmpeg.changeFrameRate(deviceProp, sb.toString(), this.baseVideoNamePath);
	//
	//		} catch (Exception e) {
	//			// TODO Auto-generated catch block
	//			e.printStackTrace();
	//		}
	//	}

	@And("^L utente mette l app in background$")
	public void l_utente_mette_l_app_in_background() throws Throwable 
	{
		Properties p = UIUtils.ui().getCurrentProperties();
		if(p.getProperty("device.os").equals("ios")) {
			((InteractsWithApps) driver).runAppInBackground(Duration.ofSeconds(-1));
		} else {
			((AndroidDriver) driver).pressKey(new KeyEvent(AndroidKey.HOME));
			try {Thread.sleep(2000);} catch (Exception e) {}
		} 
	}
	@And("^L utente chiude l app$")
	public void l_utente_chiude_l_app() throws Throwable 
	{

		//		new Actions(driver).sendKeys(AndroidKey.APP_SWITCH);
		////		driver.pressKeyCode_app_switch();
		//		switch(this.driverType)
		//		{
		//			case 0://android driver
		//				this.chiudiTuttoButton=driver.findElement(By.xpath(Locators.LoginPosteItPageLocator..getAndroidLocator()));
		//			break;
		//			case 2://web driver
		//			
		//			break;
		//		}
		//driver.quit();

		Properties p=UIUtils.ui().getCurrentProperties();
		AdbCommandPrompt c=new AdbCommandPrompt(p.getProperty("adb.path"),p.getProperty("ios.udid"));

		c.forceStop(p.getProperty("android.app.package"));

		WaitManager.get().waitMediumTime();

		//		driver = (AppiumDriver<?>) UIUtils.ui().driver("fake_browser");
		//		try {Thread.sleep(2000);} catch (Exception e) {}
	}

	//------------------------------02/10/2020 VOL ---------------------------------------
	@Given("^Naviga sul carosello dei biscotti verso$")
	public void naviga_sul_carosello_dei_biscotti_verso(List<BiscottoDataBean> table) throws Throwable 
	{
		homePage.navigaVersoIlBiscotto(table.get(0));
	}

	@Given("^Clicca su Scopri dal biscotto$")
	public void clicca_su_Scopri_dal_biscotto() throws Throwable 
	{
		homePage.clickOnScopriDaBiscotto();
	}

	@Then("^Viene mostrata la pagina di tutorial da biscotto$")
	public void viene_mostrata_la_pagina_di_tutorial_da_biscotto(List<TutorialPopUpBean> list) throws Throwable 
	{
		try {
			tutorialPopup=(TutorialPopupPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("TutorialPopupPage", driver.getClass(), null);
			tutorialPopup.checkTutorialPage(list.get(0));
		} catch (Exception e) {
			System.err.println("il tutorial non è presente");
		}
	}

	@Then("^clicca su Scopri da tutorial di biscotto$")
	public void clicca_su_Scopri_da_tutorial_di_biscotto() throws Throwable 
	{
		try {
			tutorialPopup.clickApriButton();
		} catch (Exception e) {
			System.err.println("il tutorial non è presente");
		}
		WaitManager.get().waitLongTime();
	}

	@Then("^La vetrina vol viene mostrata$")
	public void la_vetrina_vol_viene_mostrata(List<BiscottoDataBean> table) throws Throwable 
	{
		//volLibretto=new VolLibrettoSmartPage(driver, driverType).get();
		pageSource=driver.getPageSource();
		volEvoluta=(VetrinaVolEvolutaPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("VetrinaVolEvolutaPage", driver.getClass(), null);
		volEvoluta.getLight(pageSource);
		volEvoluta.clickOnTab(table.get(0).getBiscotto());
		volEvoluta.checkSubPage(table.get(0));
	}


	@Then("^Naviga verso il prodotto$")
	public void naviga_verso_il_prodotto(List<BiscottoDataBean> table) throws Throwable 
	{
		BiscottoDataBean b=table.get(0);
		if(b.getBiscotto().equals("Conto"))
		{
			contoVol=(ContoVolSubPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("ContoVolSubPage", driver.getClass(), null);
			contoVol.navigateToProduct(b.getProdotto(),pageSource);
		}
		else if(b.getBiscotto().equals("Libretto"))
		{
			librettoVol=(LibrettoVolSubPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("LibrettoVolSubPage", driver.getClass(), null);
			librettoVol.navigateToProduct(b.getProdotto());
		}
	}

	@Then("^Da pagina di dettaglio prodotto vol clicca su FID$")
	public void da_pagina_di_dettaglio_prodotto_vol_clicca_su_FID(List<BiscottoDataBean> table) throws Throwable 
	{
		//System.out.println(driver.getPageSource());
		volProductDetails=(VolProductDetailsPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("VolProductDetailsPage", driver.getClass(), null);
		volProductDetails.checkPage();
		volProductDetails.checkTitlePage(table.get(0));
	}

	@Then("^Il documento FID viene mostrato$")
	public void il_documento_FID_viene_mostrato(List<BiscottoDataBean> table) throws Throwable 
	{
		volProductDetails.clickOnFID();
		volProductDetails.checkFID(table.get(0));
	}

	@Then("^Clicca sul link Scopri l intera offerta$")
	public void clicca_sul_link_Scopri_l_intera_offerta() throws Throwable 
	{
		volEvoluta.clickOnScopriOfferta();
	}

	@Then("^la pagina web \"([^\"]*)\" viene mostrata$")
	public void la_pagina_web_viene_mostrata(String url) throws Throwable 
	{
		//String currentUrl=driver.getCurrentUrl();
		WebElement e=driver.findElement(By.xpath("//*[@resource-id='com.android.chrome:id/url_bar' or @resource-id='com.sec.android.app.sbrowser:id/location_bar_edit_text']"));
		String currentUrl=e.getText();
		org.springframework.util.Assert.isTrue(url.contains(currentUrl), "pagina errata attuale:"+currentUrl+", atteso:"+url);
	}

	@Then("^Clicca sul link Approfondisci l intera gamma di offerte$")
	public void clicca_sul_link_Approfondisci_l_intera_gamma_di_offerte(List<BiscottoDataBean> table) throws Throwable 
	{
		String pageSource=driver.getPageSource();
		System.out.println(pageSource);

		BiscottoDataBean b=table.get(0);

		if(b.getBiscotto().equals("Conto"))
		{
			volProductDetails=(VolProductDetailsPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("VolProductDetailsPage", driver.getClass(), null);
			volProductDetails.checkPage();
			volProductDetails.clickOnInteraGamma();
		}
		else if(b.getBiscotto().equals("Libretto"))
		{
			librettoVol=(LibrettoVolSubPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("LibrettoVolSubPage", driver.getClass(), null);
			librettoVol.checkPage();
			librettoVol.clickOnInteraGamma(pageSource);
		}


	}


	@Then("^Clicca su richiedi da pagina di dettaglio vol$")
	public void clicca_su_richiedi_da_pagina_di_dettaglio_vol(List<BiscottoDataBean> table) throws Throwable 
	{
		BiscottoDataBean b=table.get(0);
		if(b.getBiscotto().equals("Conto"))
		{
			contoVol.clickRichiedi();
		}
	}

	@Then("^La pagina di richiesta acquisto viene mostrata$")
	public void la_pagina_di_richiesta_acquisto_viene_mostrata(List<BiscottoDataBean> table) throws Throwable 
	{
		purchaseVol=(PurchaseProductVolPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("PurchaseProductVolPage", driver.getClass(), null);
		purchaseVol.checkPage();
	}

	@Then("^Clicca su Continua da pagina di acquisto vol$")
	public void clicca_su_Continua_da_pagina_di_acquisto_vol(List<BiscottoDataBean> table) throws Throwable 
	{
		purchaseVol.clickContinua();
	}

	@Then("^la pagina di riepilogo acquisto vol viene mostrata$")
	public void la_pagina_di_riepilogo_acquisto_vol_viene_mostrata() throws Throwable 
	{
		summaryPurchaseVol=(SummaryPurchaseVolPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("SummaryPurchaseVol", driver.getClass(), null);
		summaryPurchaseVol.checkPage();
	}

	@Then("^Clicca su Annulla acquisto vol$")
	public void clicca_su_Annulla_acquisto_vol() throws Throwable 
	{
		summaryPurchaseVol.clickAnnulla();
	}

	@Then("^La card in home Conto in apertura viene mostrata$")
	public void la_card_in_home_Conto_in_apertura_viene_mostrata() throws Throwable 
	{
		homePage=(HomePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("HomePage", driver.getClass(), null);
		homePage.setUserHeader(userHeader);
		homePage.checkPage();
		homePage.checkContoInApertura();
	}


	@Then("^Clicca sulla card Conto in apertura$")
	public void clicca_sulla_card_Conto_in_apertura() throws Throwable 
	{
		homePage.clickOnContoInApertura();
	}

	@Then("^Clicca su Annulla Acquisto VOL$")
	public void clicca_su_Annulla_Acquisto_VOL() throws Throwable 
	{
		summaryPurchaseVol=(SummaryPurchaseVolPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("SummaryPurchaseVol", driver.getClass(), null);
		summaryPurchaseVol.checkPage();
		summaryPurchaseVol.clickCancellaAcquisto();
		WaitManager.get().waitMediumTime();
		volEvoluta=(VetrinaVolEvolutaPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("VetrinaVolEvolutaPage", driver.getClass(), null);
		volEvoluta.checkPage();
	}


	@Given("^L'app \"([^\"]*)\" viene avviata$")
	public void l_app_viene_avviata(String app, List<CredentialAccessBean> table) throws Throwable 
	{
		access = table.get(0);

		// ricavo il tipo di device: ios o android
		// if(app.equalsIgnoreCase("android"))
		// {
		driverType = ANDROID;
		// }
		// else if(app.equalsIgnoreCase("ios"))
		// {
		// driverType=IOS;
		// }

		// ricavo il driver del dispositivo
		driver = (AppiumDriver<?>) UIUtils.ui().driver(access.getDevice());

		//connectionsUp();
		//----------------- ANNUNZIATA ----------------------------
		devProp=UIUtils.ui().getCurrentProperties();
		//----------------- ANNUNZIATA ----------------------------

		//		this.baseVideoNamePath = access.getTestIdCheck() + "-" + access.getDevice();
		//		driverName = access.getDevice();
		//		//        driver.setLocation(new Location(49, 123, 10));
		//		startVideoRecording(driverName);

		this.baseVideoNamePath = access.getTestIdCheck() + "-" + access.getDevice();
		driverName = access.getDevice();
		
		UiPageRepository.get()
		.loadPagesDefinitionSqlLite(new File(RegressionStep.class.getClassLoader().getResource("bp.element.repository.db").toURI()).getAbsolutePath(),null, driver);


		try {

			NewLoginPreLoginPage p=(NewLoginPreLoginPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("NewLoginPreLoginPage", driver.getClass(), null);;
			p.clickOnOkPopup();

		} catch (Exception e) {

		}

		if(access.getDebug() == null || access.getDebug().equals("n"))
			startVideoRecording(driverName,access.getTestIdCheck());

		if (access.getOnboarding().equals("nonseitu")) {

			NewLoginPreLoginPage newLogin = (NewLoginPreLoginPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("NewLoginPreLoginPage", driver.getClass(), null);
			newLogin.checkPage();
			//newLogin.makeAnewLogin();

		}else if(access.getOnboarding().equals("accedi")) {

			NewLoginPreLoginPage newLogin = (NewLoginPreLoginPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("NewLoginPreLoginPage", driver.getClass(), null);
			newLogin.checkPage();
			//newLogin.makeAnewLoginAccess();

		};	


		// Thread.sleep(3000);
		// InsertPosteIDPage insertPosteId = new InsertPosteIDPage(driver,
		// driverType).get();
		// insertPosteId.clickNonSeiTu();

		String loginType = access.getLoginType();
		this.userHeader = access.getUserHeader();

		try {
			FingerPrintPopUp popup=(FingerPrintPopUp) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("FingerPrintPopUp", driver.getClass(), null);
			popup.clickCancelButton();
		}catch(Exception err) {System.out.println("il popup fingerprint non è visibile");}
	}

	@Given("^Clicca su Scopri i prodotti da prelogin$")
	public void clicca_su_Scopri_i_prodotti_da_prelogin() throws Throwable 
	{
		NewLoginPreLoginPage newLogin = (NewLoginPreLoginPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("NewLoginPreLoginPage", driver.getClass(), null);
		newLogin.checkPage();
		newLogin.clickScopriProdotti();
	}

	//----------------------02/10/2020 VOL ------------------------------------------------
	//--------------------- BACHECA ------------------------------------------------
	@Given("^Naviga verso la sezione Bacheca$")
	public void naviga_verso_la_sezione_Bacheca() throws Throwable 
	{
		this.homePage = (HomePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("HomePage", driver.getClass(), null);
		homePage.setUserHeader(userHeader);
		homePage.checkPage();
		this.hamburgerMenu = homePage.openHamburgerMenu();
		this.messaggi = hamburgerMenu.gotoBachecaSectionMessaggi();
	}


	//--------------------- BACHECA ------------------------------------------------
	//--------------------- PFM ----------------------------------------------------
	@Given("^Naviga verso la sezione le tue spese$")
	public void naviga_verso_la_sezione_le_tue_spese() throws Throwable 
	{
		this.leTueSpese=homePage.gotoLeTueSpese();
	}

	@Given("^Clicca su Vedi il tuo andamento$")
	public void clicca_su_Vedi_il_tuo_andamento() throws Throwable 
	{
		this.leTueSpese.clickOnMesePrecedente();
		this.leTueSpese.checkRiepilogoDiv();
		this.leTueSpese.clickOnVediIltuoAndamento();
	}

	@Then("^Viene controllata la sezione Il tuo Andamento$")
	public void viene_controllata_la_sezione_Il_tuo_Andamento() throws Throwable 
	{
		IlTuoAndamentoPage andamento=(IlTuoAndamentoPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("IlTuoAndamentoPage", driver.getClass(), null);
		andamento.checkPage();
		andamento.checkEntrateTab();
		andamento.checkUsciteTab();
		andamento.checkDifferenzaTab();
	}

	@Given("^Clicca su Vedi tutte le categorie$")
	public void clicca_su_Vedi_tutte_le_categorie() throws Throwable 
	{
		WaitManager.get().waitLongTime();
		// mi sposto su vedi tutte le categorie
//		this.leTueSpese.clickOnMesePrecedente();
		this.speseDettaglio = this.leTueSpese.clickVediTutteLeCategorie();
		WaitManager.get().waitLongTime();
		this.speseDettaglioList=this.speseDettaglio.readSpeseDettaglio();
	}

	@Given("^Viene cliccato l'opzione esporta come csv$")
	public void viene_cliccato_l_opzione_esporta_come_csv(List<TransactionDetailBean> table) throws Throwable 
	{
		TransactionDetailBean t =table.get(0);

		if(t.getCsvPage().equals("tutteLeCategorie"))
		{
			this.speseDettaglio.clickRightMenu();
			this.speseDettaglio.esportaCSV();
		}
		else if(t.getCsvPage().equals("Giroconti e ricariche"))
		{
			ExclutedOperationsDetailsPage dp=(ExclutedOperationsDetailsPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("ExclutedOperationsDetailsPage", driver.getClass(), null);
			dp.setBean(t);
			this.speseDettaglioList=dp.readSpeseDettaglio();
			dp.clickRightMenu();
			dp.esportaCsv();
		}
		else if(t.getCsvPage().equals("ilTuoAndamento"))
		{
			IlTuoAndamentoPage andamento=(IlTuoAndamentoPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("IlTuoAndamentoPage", driver.getClass(), null);
			WaitManager.get().waitShortTime();
			andamento.clickRightMenu();
			andamento.esportaCsv();
		}
	}

	@Then("^il file viene esportato$")
	public void il_file_viene_esportato(List<TransactionDetailBean> table) throws Throwable 
	{
		TransactionDetailBean t =table.get(0);

		if(t.getCsvPage().equals("tutteLeCategorie"))
		{
			this.speseDettaglio.readandCheckCsv(this.speseDettaglioList);
			this.speseDettaglio.removeCsv();
		}
		else if(t.getCsvPage().equals("Giroconti e ricariche"))
		{
			ExclutedOperationsDetailsPage dp=(ExclutedOperationsDetailsPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("ExclutedOperationsDetailsPage", driver.getClass(), null);
			dp.setBean(t);
			dp.readAndCheckCsv(this.speseDettaglioList);
			dp.removeCsv();
		}
		else if(t.getCsvPage().equals("ilTuoAndamento"))
		{
			IlTuoAndamentoPage andamento=(IlTuoAndamentoPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("IlTuoAndamentoPage", driver.getClass(), null);
			andamento.readAndCheckCsv();
			andamento.removeCsv();
		}
	}

	@Then("^Clicca su una sottocategoria$")
	public void clicca_su_una_sottocategoria(List<TransactionDetailBean> table) throws Throwable 
	{
		TransactionDetailBean t =table.get(0);

		if(t.getCsvPage().equals("tutteLeCategorie"))
		{
			// vado alla categoria desiderata
			this.speseCategory = this.speseDettaglio.gotoCategory(t);

			// vado sulla sottocategoria desiderata
			this.speseDetails = this.speseCategory.gotoSubCategory();
		}
		else if(t.getCsvPage().equals("MovimentiEsclusi"))
		{
			ExclutedOperationsPage p= (ExclutedOperationsPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("ExclutedOperationsPage", driver.getClass(), null);
			p.setBean(t);
			p.gotoSubCategory();
		}
	}

	@Given("^Clicca su Movimenti esclusi$")
	public void clicca_su_Movimenti_esclusi(List<TransactionDetailBean> table) throws Throwable 
	{
		this.speseDettaglio.clickMovimentiEsclusi();
		ExclutedOperationsPage p= (ExclutedOperationsPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
		.getPageManager("ExclutedOperationsPage", driver.getClass(), null);
		p.setBean(table.get(0));
		p.checkPage();
	}

	@Then("^Clicca sul tab \"([^\"]*)\"$")
	public void clicca_sul_tab(String tab) throws Throwable 
	{
		IlTuoAndamentoPage andamento=(IlTuoAndamentoPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("IlTuoAndamentoPage", driver.getClass(), null);

		if(tab.equals("Uscite"))
		{
			andamento.clickUsciteTab();
		}
		else if(tab.equals("Entrate"))
		{
			andamento.clickEntrateTab();
		}
		else if(tab.equals("Differenza"))
		{
			andamento.clickDifferenzaTab();
		}

		WaitManager.get().waitMediumTime();
	}

	@Then("^I pin del grafico \"([^\"]*)\" vengono aggiornati dopo click sulla sidebar del grafico$")
	public void i_pin_del_grafico_vengono_aggiornati_dopo_click_sulla_sidebar_del_grafico(String tab) throws Throwable 
	{
		IlTuoAndamentoPage andamento=(IlTuoAndamentoPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("IlTuoAndamentoPage", driver.getClass(), null);

		if(tab.equals("Uscite"))
		{
			andamento.checkGraphSeekBarUscite();
		}
		else if(tab.equals("Entrate"))
		{
			andamento.checkGraphSeekBarEntrate();
		}
		else if(tab.equals("Differenza"))
		{
			andamento.checkGraphSeekBarAndamento();
		}
	}

	//--------------------- PFM ----------------------------------------------------
	//----------------------- LIBRETTO ---------------------------------------------
	@Given("^Viene controllato che la transazione sia presente nel libretto$")
	public void viene_controllato_che_la_transazione_sia_presente_nel_libretto(List<TransactionMovementsBean> table) throws Throwable 
	{
		TransactionMovementsBean b = table.get(0);
		((AndroidDriver<?>) driver).pressKey(new KeyEvent(AndroidKey.BACK));
		this.buonilibr.searchTransaction(table.get(0));

		this.buonilibr.checkTransaction(table.get(0));
	}

	@Given("^Viene cliccato sulla transazione$")
	public void viene_cliccato_sulla_transazione(List<TransactionMovementsBean> table) throws Throwable 
	{
		this.transactionBean=this.buonilibr.clickOnTransaction(table.get(0));
	}

	@Then("^la pagina di dettaglio movimenti viene mostrata$")
	public void la_pagina_di_dettaglio_movimenti_viene_mostrata(List<TransactionMovementsBean> table) throws Throwable 
	{
		DettaglioMovimentoPage dettaglio=(DettaglioMovimentoPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("DettaglioMovimentoPage", driver.getClass(), null);
		dettaglio.checkPage();
		dettaglio.checkDettaglio(transactionBean);
	}


	//----------------------- LIBRETTO ---------------------------------------------
	//----------------------- Bonifici Sepa ----------------------------------------
	@Given("^Accede alla sezione Invia Bonifico$")
	public void accede_alla_sezione_Invia_Bonifico(List<BonificoDataBean> table) throws Throwable 
	{
		// Apro l'hamburgerMenu 
		this.hamburgerMenu = homePage.openHamburgerMenu();

		// vado in conti e carte
		this.contiPage = hamburgerMenu.gotoContiCarte();
		// seleziono il bonifico
		this.bonifico = this.contiPage.navigateToBonifico(table.get(0));
	}

	@Then("^il campo Paese di residenza viene prepopolato con i seguenti dati$")
	public void il_campo_Paese_di_residenza_viene_prepopolato_con_i_seguenti_dati(List<BonificoDataBean> table) throws Throwable 
	{
		this.bonifico.checkPaeseDiResidenza(table.get(0).getCity());
	}
	//------------------------Bonifico Sepa ----------------------------------------
	//------------------------ Girofondo -------------------------------------------
	@Given("^Naviga verso la sezione girofondo$")
	public void naviga_verso_la_sezione_girofondo(List<GirofondoCreationBean> table) throws Throwable 
	{
		this.hamburgerMenu = homePage.openHamburgerMenu();

		this.contiPage = hamburgerMenu.gotoContiCarte();

		// seleziono girofondo

		GirofondoCreationBean b=table.get(0);
		this.girofondo = contiPage.navigateToGirofondo(b);
	}

	@Then("^controlla che nella lista Trasferisci su siano mostrati i seguenti dati$")
	public void controlla_che_nella_lista_Trasferisci_su_siano_mostrati_i_seguenti_dati(List<GirofondoCreationBean> table) throws Throwable 
	{
		this.girofondo.clickTrasferisciSu();

		for(GirofondoCreationBean b : table)
		{
			this.girofondo.checkTrasferisciSu(b.getTransferTo());
		}
	}


	//------------------------ Girofondo -------------------------------------------
	//------------------------ Carte -----------------------------------------------
	@Given("^Naviga verso la sezione postepay$")
	public void naviga_verso_la_sezione_postepay(List<RicaricaPostepayBean> table) throws Throwable 
	{
		this.hamburgerMenu = homePage.openHamburgerMenu();

		this.contiPage = hamburgerMenu.gotoContiCarte();

		this.contiPage.navigateToCartaPostePay(table.get(0).getPayWith());

		this.contiPage.clickOnPostePayCard();
	}

	@Then("^viene controllata la pagina postepay$")
	public void viene_controllata_la_pagina_postepay(List<RicaricaPostepayBean> table) throws Throwable 
	{
		PostePayCardDetailsPage postepay=(PostePayCardDetailsPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("PostePayCardDetailsPage", driver.getClass(), null);
		postepay.checkPage();
	}

	//------------------------ Carte -----------------------------------------------
	//------------------------ Ricariche Postepay ----------------------------------
	@Then("^Verifica La presenza del tasto INIZIA o RICARICA$")
	public void verifica_La_presenza_del_tasto_INIZIA_o_RICARICA() throws Throwable 
	{
		WaitManager.get().waitMediumTime();
		RicaricheAutomatichePage a=(RicaricheAutomatichePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("RicaricheAutomatichePage", driver.getClass(), null);
		System.out.println(driver.getPageSource());

		if(a.RicaricaIsPresent())
		{
			a.clickOnRicarica();
			RicaricaPostepayPage r=(RicaricaPostepayPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("RicaricaPostepayPage", driver.getClass(), null);
			r.check();
		}
	}
	//------------------------ Ricariche Postepay ----------------------------------
	//------------------------ Home ------------------------------------------------
	@Given("^L utente verifica le voci delle operazioni veloci$")
	public void l_utente_verifica_le_voci_delle_operazioni_veloci(List<QuickOperationCheckBean> table) throws Throwable 
	{
		QuickOperationListPage quickOp=homePage.gotoOperazioniVeloci();

		for(QuickOperationCheckBean bean : table)
		{
			quickOp.checkQuickOperationFunnel(bean);
		}
		WaitManager.get().waitMediumTime();
		quickOp.clickBack(userHeader);
	}
	//------------------------ Home ------------------------------------------------
	//------------------------ Postagiro BPOL --------------------------------------
	@Then("^Utente naviga verso Postagiro$")
	public void utente_naviga_verso_Postagiro() throws Throwable 
	{
		bpolPage.navigaBersoPostagiro();
	}

	@Then("^Compila Postagiro su BPOL$")
	public void compila_Postagiro_su_BPOL(List<PaymentCreationBean> table) throws Throwable 
	{
		bpolPage.compilaPostagiro(table.get(0));
	}

	@Then("^la notifica di autorizzazione postagiro viene ricevuta$")
	public void la_notifica_di_autorizzazione_postagiro_viene_ricevuta(List<NotificaPushBean> table) throws Throwable 
	{
		BPOnlineNotifica notifica=(BPOnlineNotifica) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("BPOnlineNotifica", driver.getClass(), null);
		notifica.checkPage();
		notifica.checkNotificaPushPostagiroBpol(table.get(0));
		//throw new PendingException();
	}

	@Then("^l operazione va a buon fine su BPOL$")
	public void l_operazione_va_a_buon_fine_su_BPOL(List<NotificaPushBean> table) throws Throwable 
	{
		bpolPage.checkOperationNotificaOK(table.get(0));
	}

	@Then("^La sezione le tue spese viene aperta$")
	public void laSezioneLeTueSpeseVieneAperta() throws Throwable 
	{
		//controllo che sia mostrato il wizard le tue spese,
		//esso viene mostrato solo al primo accesso alla sezione
		YourExpencesWizardPage wizard=(YourExpencesWizardPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("YourExpencesWizardPage", driver.getClass(), null);

		try
		{
			WaitManager.get().waitMediumTime();
			wizard.clickSalta();
		}
		catch(Exception err)
		{

		}

		try
		{
			//wizard.get();
			wizard.clickContinua();
			WaitManager.get().waitMediumTime();
			wizard.clickContinua();
			WaitManager.get().waitMediumTime();
			wizard.clickContinua();
			WaitManager.get().waitMediumTime();
			wizard.clickContinua();
			WaitManager.get().waitMediumTime();
			wizard.clickContinua();
			WaitManager.get().waitMediumTime();
		}
		catch(Exception err)
		{

		}

		YourExpencesPage p=(YourExpencesPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("YourExpencesPage", driver.getClass(), null);

		p.checkPage();
	}

	//------------------------ Postagiro BPOL --------------------------------------
	//------------------------ LoginLogout -----------------------------------------
	@Then("^viene mostrata la pagina di login$")
	public void viene_mostrata_la_pagina_di_login() throws Throwable 
	{
		WaitManager.get().waitMediumTime();
		System.out.println(driver.getPageSource());
		NewLoginPreLoginPage newLogin = (NewLoginPreLoginPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("NewLoginPreLoginPage", driver.getClass(), null);
		newLogin.checkPage();
	}

	@Given("^L app viene avviata$")
	public void l_app_viene_avviata(List<CredentialAccessBean> table) throws Throwable 
	{
		access = table.get(0);

		// ricavo il tipo di device: ios o android
		// if(app.equalsIgnoreCase("android"))
		// {
		driverType = ANDROID;
		// }
		// else if(app.equalsIgnoreCase("ios"))
		// {
		// driverType=IOS;
		// }

		// ricavo il driver del dispositivo
		driver = (AppiumDriver<?>) UIUtils.ui().driver(access.getDevice());

		String loginType = access.getLoginType();
		this.userHeader = access.getUserHeader();

		//connectionsUp();
		//----------------- ANNUNZIATA ----------------------------
		devProp=UIUtils.ui().getCurrentProperties();
		//----------------- ANNUNZIATA ----------------------------

		//		this.baseVideoNamePath = access.getTestIdCheck() + "-" + access.getDevice();
		//		driverName = access.getDevice();
		//		//        driver.setLocation(new Location(49, 123, 10));
		//		startVideoRecording(driverName);

		this.baseVideoNamePath = access.getTestIdCheck() + "-" + access.getDevice();
		driverName = access.getDevice();
		
		UiPageRepository.get()
		.loadPagesDefinitionSqlLite(new File(RegressionStep.class.getClassLoader().getResource("bp.element.repository.db").toURI()).getAbsolutePath(),null, driver);


		WaitManager.get().waitShortTime();

		((AndroidDriver<?>) driver).pressKey(new KeyEvent(AndroidKey.BACK));



	}

	@Then("^la popup di errore credenziali viene mostrata$")
	public void la_popup_di_errore_credenziali_viene_mostrata() throws Throwable 
	{
		loginPosteIt = (LoginPosteItPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("LoginPosteItPage", driver.getClass(), null);
		loginPosteIt.checkErrorPopup();

	}

	@Then("^L utente clicca su recupera$")
	public void l_utente_clicca_su_recupera() throws Throwable 
	{
		loginPosteIt.clickRecuperaPopupErrorLogin();
		WaitManager.get().waitLongTime();
	}

	@Then("^Viene aperto il browser per permettere il recupero password$")
	public void viene_aperto_il_browser_per_permettere_il_recupero_password() throws Throwable 
	{
		Set<String> contexts = ((ContextAware) driver).getContextHandles();
		System.out.println("context:"+contexts);

		for(String s : contexts)
		{
			if(s.contains("WEBVIEW"))
				((ContextAware) driver).context(s);

		}

		UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[text()='Recupera le credenziali']")));

		//System.out.println();

		//throw new PendingException();
	}


	@Given("^seleziona il versamento sul salvadanaio$")
	public void seleziona_il_versamento_sul_salvadanaio(List<SalvadanaioDataBean> bean) throws Throwable {
		SalvadanaioDataBean b = bean.get(0);
		//		WaitManager.get().waitLongTime();
		//		System.out.println(driver.getPageSource());

		SalvadanaioSelectVersamentoPage versa=(SalvadanaioSelectVersamentoPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("SalvadanaioSelectVersamentoPage", driver.getClass(), null);
		
		versa.checkPage();

		PayWithPage pay=null;
		String[] pw=null;

		String versaType=b.getVersamentoType();

		if(versaType.equals("random"))
		{
			System.out.println("metodo di selezione random");
			String[] V=new String[] {
					"singolo",
					"ricorrente",
					"arrotondamento",
					"le tue spese"
			};

			Random r = new Random();
			int low = 0;
			int high = 4;
			int result = r.nextInt(high-low) + low;

			versaType=V[result > 3 ? 3 : result];

			System.out.println("metodo selezionato:"+versaType);
			this.versamentoType=versaType;
		}

		switch(versaType)
		{
		case "singolo":
			versa.clickVersamentoSingolo();
			pay=(PayWithPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("PayWithPage", driver.getClass(), null);

			pay.checkPage();
			pw=b.getPayWith().split(";");

			switch(pw[0])
			{
			case "conto":
				pay.clickOnConto(b.getPayWith());
				break;
			}
			WaitManager.get().waitMediumTime();
			break;
		case "ricorrente":
			versa.clickVersamentoRicorrente();
			pay=(PayWithPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("PayWithPage", driver.getClass(), null);

			pay.checkPage();
			pw=b.getPayWith().split(";");

			switch(pw[0])
			{
			case "conto":
				pay.clickOnConto(b.getPayWith());
				break;
			}
			WaitManager.get().waitMediumTime();
			break;
		case "arrotondamento":
			versa.clickVersamentoArrotondamento();
			pay=(PayWithPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("PayWithPage", driver.getClass(), null);

			pay.checkPage();
			pw=b.getPayWith().split(";");

			switch(pw[0])
			{
			case "conto":
				pay.clickOnConto(b.getPayWith());
				break;
			}
			WaitManager.get().waitMediumTime();
			break;
		case "le tue spese":
			versa.clickVersamentoLeTueSpese();
			WaitManager.get().waitMediumTime();
			//System.out.println(driver.getPageSource());
			//System.out.println(driver.getPageSource());
			break;
		}

	}

	@Given("^Elimina il versamento ricorrente$")
	public void elimina_il_versamento_ricorrente(List<CredentialAccessBean> table) throws Throwable 
	{
		CredentialAccessBean c=table.get(0);
		SalvadanaioGestisciVersamentoRicorrentePage n = (SalvadanaioGestisciVersamentoRicorrentePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("SalvadanaioGestisciVersamentoRicorrentePage", driver.getClass(), null);

		n.eliminaVersamentoRicorrente(c.getPosteid());
	}

	@Given("^Naviga verso la sezione dei versamenti ricorrenti ed elimina l accantonamento se presente$")
	public void naviga_verso_la_sezione_dei_versamenti_ricorrenti_ed_elimina_l_accantonamento_se_presente(List<SalvadanaioDataBean> bean) throws Throwable 
	{
		try {
			SalvadanaioDataBean b = bean.get(0);
			SalvadanaioInfoObjectivePage c = (SalvadanaioInfoObjectivePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("SalvadanaioInfoObjectivePage", driver.getClass(), null);
			c.checkPage();

			c.goToModifyVersamentoRicorrente();	
			SalvadanaioGestisciVersamentoRicorrentePage n = (SalvadanaioGestisciVersamentoRicorrentePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("SalvadanaioGestisciVersamentoRicorrentePage", driver.getClass(), null);
			n.checkPage();
			n.checkData(b);
			n.eliminaVersamentoRicorrente(b.getPosteid());

			if(b.getObjectiveName().equals("dinamico")){

				b.setObjectiveName(this.objName);

			}
			SalvadanaioPage p = (SalvadanaioPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("SalvadanaioPage", driver.getClass(), null);
			p.checkPage();

			p.chooseObjectiveTwo(b.getObjectiveName());		
		} catch (Exception e) 
		{
			System.err.println("[WARN] - accantonamento ricorrente non trovato");
		}
	}

	@Given("^accede ad operazione veloce e seleziona l operazione da eseguire$")
	public void accede_ad_operazione_veloce_e_seleziona_l_operazione_da_eseguire(List<QuickOperationCreationBean> table) throws Throwable 
	{
		QuickOperationCreationBean b=table.get(0);
		boolean saved=b.getQuickOperationSaved() != null && b.getQuickOperationSaved().equals("y") ? true : false;
		this.quickPage = homePage.gotoOperazioniVeloci();
		this.quickPage.gotoQuickOperationPage(b.getQuickOperationName(), saved);
		this.payWithPage=(PayWithPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("PayWithPage", driver.getClass(), null);
		payWithPage.checkPage();
		this.payWithPage.clickOnConto(b.getPayWith());
	}
	@Given("^accede ad operazione veloce e seleziona Bonifico$")
	public void accede_ad_operazione_veloce_e_seleziona_Bonifico(List<QuickOperationCreationBean> table) throws Throwable {
		QuickOperationCreationBean b=table.get(0);
		boolean saved=b.getQuickOperationSaved() != null && b.getQuickOperationSaved().equals("y") ? true : false;
		this.quickPage = homePage.gotoOperazioniVeloci();
		this.quickPage.gotoQuickOperationPage(b.getQuickOperationName(), saved);
		this.selectBonifico=(SelectBonificoTypePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("SelectBonificoTypePage", driver.getClass(), null);
		selectBonifico.clickOnBonificoSepa();
		this.payWithPage=(PayWithPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("PayWithPage", driver.getClass(), null);
		payWithPage.checkPage();
		this.payWithPage.clickOnConto(b.getPayWith());
	}
	
	@Given("^Viene cliccato il toogle di bonifico istantaneo$")
	public void viene_cliccato_il_toogle_di_bonifico_istantaneo(List<CredentialAccessBean> table) throws Throwable {
		CredentialAccessBean b = table.get(0);
		conti.clickOnToogleBonificoIstantaneo(b);
		WaitManager.get().waitMediumTime();

		InsertPosteIDPage posteId = (InsertPosteIDPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("InsertPosteIDPage", driver.getClass(), null);
		posteId.checkPage();
		posteId.insertPosteId(b.getPosteid());
		WaitManager.get().waitMediumTime();
	}
	
	@Then("^Viene controllato il messaggio di attivazione del bonifico istantaneo$")
	public void viene_controllato_il_messaggio_di_attivazione_del_bonifico_istantaneo() throws Throwable {
		WaitManager.get().waitShortTime();
		Properties texts=new Properties();
		texts.load(RegressionStep.class.getClassLoader().getResourceAsStream("texts/texts.properties"));
			((AndroidDriver<?>)driver).openNotifications();

			long time=TimeUnit.MINUTES.toMillis(2);
			DeviceNative dev=(DeviceNative) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("DeviceNative", driver.getClass(), texts);
			boolean find=false;

			while(time > 0)
			{
				try
				{
					dev.controlloHeaderToogleBonifico("SERVIZIO ATTIVATO");
					if(dev.controlloDescrizioneToogleBonifico()==true) {
					find=true;
					break;
					}
				}
				catch(Exception err)
				{
					Thread.sleep(TimeUnit.SECONDS.toMillis(10));
					time -=TimeUnit.SECONDS.toMillis(10);
					System.out.println("notifica non trovata attendo 10 sec...");
				}
			}
	}
	
	@Then("^Utente clicca back da il tuo andamento$")
	public void utente_clicca_back_da_il_tuo_andamento() throws Throwable {
		IlTuoAndamentoPage andamento=(IlTuoAndamentoPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("IlTuoAndamentoPage", driver.getClass(), null);
		andamento.clickOnBack();
		YourExpencesPage p=(YourExpencesPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("YourExpencesPage", driver.getClass(), null);
		p.checkPage();
		
	}

	@Then("^Utene effettua uno swipe indietro tra i mesi$")
	public void utene_effettua_uno_swipe_indietro_tra_i_mesi() throws Throwable {
		this.leTueSpese.clickOnMesePrecedente(2);
		this.leTueSpese.checkPage();
		this.leTueSpese.clickOnMesePrecedente(3);
		this.leTueSpese.checkPage();
	}

	@And("^Utente verifica la NON obbligatorietà del campo \"([^\"]*)\"$")
	public void utente_verifica_la_NON_obbligatorietà_del_campo(String g, List<RicaricaPostepayBean> t) throws Throwable {
		RicaricaPostepayBean b=t.get(0);
		this.contiPage=(ContiCartePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("ContiCartePage", driver.getClass(), null);
		this.ricaricaPostepayPage=this.contiPage.navigateToRicaricaPostepay(b);
		this.thankYouPage=this.ricaricaPostepayPage.eseguiRicaricaAutomaticaFinoAl(b);
	}

	@Then("^Verifica popup della sezione ricarica automatica$")
	public void verifica_popup_della_sezione_ricarica_automatica() throws Throwable {
		this.thankYouPage=this.ricaricaPostepayPage.checkPopupRicaricaAutomatica();
	}
	
	@Given("^Utente clicca su chiudi della thankYouPage e torna in homePage$")
	public void utente_clicca_su_chiudi_della_thankYouPage_e_torna_in_homePage(List<QuickOperationCreationBean> table) throws Throwable {
		this.thankYouPage = (OKOperationPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("OKOperationPage", driver.getClass(), null);
		thankYouPage.closePage();
//		OKOperationPage OK=null;
//		OK.close();
		this.contiPage = (ContiCartePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("ContiCartePage", driver.getClass(), null);
		this.contiPage.checkPage();

		WaitManager.get().waitMediumTime();

		QuickOperationCreationBean b=table.get(0);

		contiPage.clickLeftButton();

	}

	//------------------------ LoginLogout -----------------------------------------
	
	//------------------------ VOL NUOVO ENTRY POINT -------------------------------
	@Given("^Utente accede alla sezione VOL da hamburger menu$")
	public void utente_accede_alla_sezione_VOL_da_hamburger_menu(List<BiscottoDataBean> table) throws Throwable {
		homePage = (HomePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("HomePage", driver.getClass(), null);
		
		homePage.openHamburgerMenu().clickOnApriunContolibretto();
		
	}
	//------------------------ VOL NUOVO ENTRY POINT -------------------------------
}
