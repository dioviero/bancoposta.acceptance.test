@Postagiro
Feature: Postagiro
  L'utente compila e sottomette un postagiro verso conte POSTE, da sottomenu paga o da operazione veloce

  @BPINPROAND-1588
  Scenario Outline: Postagiro da carta a conto
    Viene effettuato un postagiro da carta a conto

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck     | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | BPINPROAND-1588 | <onboarding> |
    And Viene effettuato un postagiro da "conto bancoposta"
      | iban   | cc   | owner   | amount   | description   | posteid   | payWith   | beneficiario   | commissioni   | reference   | testIdCheck | paymentMethod   |
      | <iban> | <cc> | <owner> | <amount> | <description> | <posteID> | <payWith> | <beneficiario> | <commissioni> | <reference> | -           | <paymentMethod> |
    And Salvare l'operazione come "operazione veloce"
      | quickOperationName   | useAdb | device   |
      | <quickOperationName> | true   | <device> |
    And Viene controllato che la transazione sia stata registrata nella lista movimenti
      | conto     | amount   | operation | transactionType   | transactionDescription   | commissioni   | paymentMethod   |
      | <payWith> | <amount> | -         | <transactionType> | <transactionDescription> | <commissioni> | <paymentMethod> |
    And Controlla che il saldo sia stato decrementato da "conto bancoposta"
      | conto     | amount   | operation | transactionType   | transactionDescription   | commissioni   | paymentMethod   |
      | <payWith> | <amount> | -         | <transactionType> | <transactionDescription> | <commissioni> | <paymentMethod> |
    Then Viene controllata la ricezione della notifica
      | messageText   | testIdCheck | notifyType        |
      | <messageText> | -           | <transactionType> |
    And Viene chiusa l'app

    @BPINPROAND-1588_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | payWith                   | iban                        | cc                          | city   | owner              | amount | description | address                     | citySender | reference     | refecenceEffective | quickOperationName | transactionType | transactionDescription | messageText                               | categoryTransation | editValue | posteID | commissioni | bonificoType | beneficiario       | notifyType | paymentMethod | testIdCheck     | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | postepay;5333171077147177 | IT15L0760103400001044920229 | IT51T0306903491100000001379 | Italia | vincenzo fortunato |   0.01 | postagiro   | Via degli Ulivi Gialli n.23 | Agrigento  | PETRUCCI LUCA | Mariana Salvino    | postagiro uno      | POSTAGIRO       | TRN                    | Bancoposta: Conferma Postagiro effettuato | Varie              |       0.2 | prisma  |        0.50 | postagiro    | vincenzo fortunato | postagiro  | carta         | BPINPROAND-1588 | nonseitu   |

  #Per eseguire il test ci vuole una postepay Evolution
  @BPINPROAND-1731
  Scenario Outline: Postagiro da conto a conto
    Viene effettuato un postagiro da conto a conto

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck     | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | BPINPROAND-1731 | <onboarding> |
    And Viene effettuato un postagiro da "conto bancoposta"
      | iban   | cc   | owner   | amount   | description   | posteid   | payWith   | beneficiario   | commissioni   | reference   | testIdCheck | paymentMethod   |
      | <iban> | <cc> | <owner> | <amount> | <description> | <posteID> | <payWith> | <beneficiario> | <commissioni> | <reference> | -           | <paymentMethod> |
    And Salvare l'operazione come "operazione veloce"
      | quickOperationName   | useAdb | device   |
      | <quickOperationName> | true   | <device> |
    And Viene controllato che la transazione sia stata registrata nella lista movimenti
      | conto     | amount   | operation | transactionType   | transactionDescription   | commissioni   |
      | <payWith> | <amount> | -         | <transactionType> | <transactionDescription> | <commissioni> |
    And Controlla che il saldo sia stato decrementato da "conto bancoposta"
      | conto     | amount   | operation | transactionType   | transactionDescription   | commissioni   |
      | <payWith> | <amount> | -         | <transactionType> | <transactionDescription> | <commissioni> |
    Then Viene controllata la ricezione della notifica
      | messageText   | testIdCheck | notifyType        |
      | <messageText> | -           | <transactionType> |
    And Viene chiusa l'app

    @BPINPROAND-1731_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | payWith            | iban         | cc                 | city   | owner           | amount | description | address                     | citySender | reference           | refecenceEffective | quickOperationName | transactionType | transactionDescription | messageText                               | categoryTransation | editValue | posteID | commissioni | bonificoType | beneficiario    | notifyType | paymentMethod | testIdCheck     | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | conto;001044548509 | 001044618914 | conto;001044618914 | Italia | Mariana Salvino |   0.01 | postagiro   | Via degli Ulivi Gialli n.23 | Agrigento  | FORTUNATO Salvatore | vincenzo fortunato | postagiro quattro  | POSTAGIRO       | TRN                    | Bancoposta: Conferma Postagiro effettuato | Varie              |       0.2 | prisma  |        0.50 | postagiro    | Mariana Salvino | postagiro  | conto         | BPINPROAND-1731 | nonseitu   |

  @BPINPROAND-1732
  Scenario Outline: Postagiro da conto a carta
    Viene effettuato un postagiro da conto a carta

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck     | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | BPINPROAND-1732 | <onboarding> |
    And Viene effettuato un postagiro da "conto bancoposta"
      | iban   | cc   | owner   | amount   | description   | posteid   | payWith   | beneficiario   | commissioni   | reference   | testIdCheck | paymentMethod   |
      | <iban> | <cc> | <owner> | <amount> | <description> | <posteID> | <payWith> | <beneficiario> | <commissioni> | <reference> | -           | <paymentMethod> |
    And Salvare l'operazione come "operazione veloce"
      | quickOperationName   | useAdb | device   |
      | <quickOperationName> | true   | <device> |
    And Viene controllato che la transazione sia stata registrata nella lista movimenti
      | conto     | amount   | operation | transactionType   | transactionDescription   | commissioni   |
      | <payWith> | <amount> | -         | <transactionType> | <transactionDescription> | <commissioni> |
    And Controlla che il saldo sia stato decrementato da "conto bancoposta"
      | conto     | amount   | operation | transactionType   | transactionDescription   | commissioni   |
      | <payWith> | <amount> | -         | <transactionType> | <transactionDescription> | <commissioni> |
    Then Viene controllata la ricezione della notifica
      | messageText   | testIdCheck | notifyType        |
      | <messageText> | -           | <transactionType> |
    And Viene chiusa l'app

    #Per eseguire il test ci vuole una postepay wallet
    @BPINPROAND-1732_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | payWith            | iban                        | cc                          | city   | owner              | amount | description | address                     | citySender | reference     | refecenceEffective | quickOperationName | transactionType | transactionDescription | messageText                               | categoryTransation | editValue | posteID | commissioni | bonificoType | beneficiario       | notifyType | paymentMethod | testIdCheck     | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | conto;001044548509 | IT15L0760103400001044920229 | IT51T0306903491100000001379 | Italia | vincenzo fortunato |   0.01 | postagiro   | Via degli Ulivi Gialli n.23 | Agrigento  | PETRUCCI LUCA | Mariana Salvino    | postagiro uno      | POSTAGIRO       | TRN                    | Bancoposta: Conferma Postagiro effettuato | Varie              |       0.2 | prisma  |        0.50 | postagiro    | vincenzo fortunato | postagiro  | conto         | BPINPROAND-1732 | nonseitu   |

    @BPINPROAND-1732_fortunato
    Examples: 
      | username                | password        | loginType | userHeader | device               | payWith            | iban                        | cc                          | city   | owner             | amount | description | address                     | citySender | reference     | refecenceEffective | quickOperationName | transactionType | transactionDescription | messageText                               | categoryTransation | editValue | posteID | commissioni | bonificoType | beneficiario       | notifyType | paymentMethod | testIdCheck     | onboarding |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | poste.it  | Vincenzo   | samsungGalaxyS8_plus | conto;001044920229 | IT06W0760103400001044548509 | IT51T0306903491100000001379 | Italia | salvatore musella |   0.01 | postagiro   | Via degli Ulivi Gialli n.23 | Agrigento  | PETRUCCI LUCA | Mariana Salvino    | postagiro uno      | POSTAGIRO       | TRN                    | Bancoposta: Conferma Postagiro effettuato | Varie              |       0.2 | prisma  |        0.50 | postagiro    | vincenzo fortunato | postagiro  | conto         | BPINPROAND-1732 | nonseitu   |

  @BPINPROIOS-1582
  Scenario Outline: Postagiro da conto a carta
    Viene effettuato un postagiro da conto a carta

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck     | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | BPINPROIOS-1582 | <onboarding> |
    And Viene effettuato un postagiro da "conto bancoposta"
      | iban   | cc   | owner   | amount   | description   | posteid   | payWith   | beneficiario   | commissioni   | reference   | testIdCheck | paymentMethod   |
      | <iban> | <cc> | <owner> | <amount> | <description> | <posteID> | <payWith> | <beneficiario> | <commissioni> | <reference> | -           | <paymentMethod> |
    And Salvare l'operazione come "operazione veloce"
      | quickOperationName   | useAdb | device   |
      | <quickOperationName> | true   | <device> |
    And Viene chiusa l'app

    @BPINPROIOS-1582_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | payWith            | iban                        | cc                          | city   | owner              | amount | description | address                     | citySender | reference     | refecenceEffective | quickOperationName | transactionType | transactionDescription | messageText                               | categoryTransation | editValue | posteID | commissioni | bonificoType | beneficiario       | notifyType | paymentMethod | testIdCheck     | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | conto;001044548509 | IT15L0760103400001044920229 | IT51T0306903491100000001379 | Italia | vincenzo fortunato |   0.01 | postagiro   | Via degli Ulivi Gialli n.23 | Agrigento  | PETRUCCI LUCA | Mariana Salvino    | postagiro uno      | POSTAGIRO       | TRN                    | Bancoposta: Conferma Postagiro effettuato | Varie              |       0.2 | prisma  |        0.50 | postagiro    | vincenzo fortunato | postagiro  | conto         | BPINPROIOS-1582 | nonseitu   |

  @BPINPROAND-1589
  Scenario Outline: Postagiro da carta a carta
    Viene effettuato un postagiro da carta a carta

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck     | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | BPINPROAND-1589 | <onboarding> |
    And Viene effettuato un postagiro da "conto bancoposta"
      | iban   | cc   | owner   | amount   | description   | posteid   | payWith   | beneficiario   | commissioni   | reference   | testIdCheck | paymentMethod   |
      | <iban> | <cc> | <owner> | <amount> | <description> | <posteID> | <payWith> | <beneficiario> | <commissioni> | <reference> | -           | <paymentMethod> |
    And Salvare l'operazione come "operazione veloce"
      | quickOperationName   | useAdb | device   |
      | <quickOperationName> | true   | <device> |
    And Viene controllato che la transazione sia stata registrata nella lista movimenti
      | conto     | amount   | operation | transactionType   | transactionDescription   | commissioni   |
      | <payWith> | <amount> | -         | <transactionType> | <transactionDescription> | <commissioni> |
    And Controlla che il saldo sia stato decrementato da "conto bancoposta"
      | conto     | amount   | operation | transactionType   | transactionDescription   | commissioni   |
      | <payWith> | <amount> | -         | <transactionType> | <transactionDescription> | <commissioni> |
    Then Viene controllata la ricezione della notifica
      | messageText   | testIdCheck | notifyType        |
      | <messageText> | -           | <transactionType> |
    And Viene chiusa l'app

    #Per eseguire il test ci vuole una postepay wallet
    @BPINPROAND-1589_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | payWith                   | iban                        | cc                     | city   | owner           | amount | description | address                     | citySender | reference     | refecenceEffective | quickOperationName | transactionType | transactionDescription | messageText                               | categoryTransation | editValue | posteID | commissioni | bonificoType | beneficiario    | notifyType | paymentMethod | testIdCheck     | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | postepay;5333171077147177 | IT13P3608105138280234980240 | carta;4023600959866981 | Italia | Luisa Barbarino |   0.01 | postagiro   | Via degli Ulivi Gialli n.23 | Agrigento  | PETRUCCI LUCA | Mariana Salvino    | test postagiro     | POSTAGIRO       | TRN                    | Bancoposta: Conferma Postagiro effettuato | Varie              |       0.2 | prisma  |        0.50 | postagiro    | Luisa Barbarino | postagiro  | carta         | BPINPROAND-1589 | nonseitu   |

  # | musellasal@gmail.com| Musabp67!|
  #Ci vogliono le credenziali per una postepay evolution
  @BPINPROAND-2165
  Scenario Outline: Postagiro da CC-P a Postepay Evo su stessa utenza
    Viene effettuato CC-P a Postepay Evo su stessa utenza

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck     | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | BPINPROAND-2165 | <onboarding> |
    And Viene effettuato un postagiro da "conto bancoposta"
      | iban   | cc   | owner   | amount   | description   | posteid   | payWith   | beneficiario   | commissioni   | reference   | testIdCheck | paymentMethod   |
      | <iban> | <cc> | <owner> | <amount> | <description> | <posteID> | <payWith> | <beneficiario> | <commissioni> | <reference> | -           | <paymentMethod> |
    And Salvare l'operazione come "operazione veloce"
      | quickOperationName   | useAdb | device   |
      | <quickOperationName> | true   | <device> |
    And Viene controllato che la transazione sia stata registrata nella lista movimenti
      | conto     | amount   | operation | transactionType   | transactionDescription   | commissioni   |
      | <payWith> | <amount> | -         | <transactionType> | <transactionDescription> | <commissioni> |
    And Controlla che il saldo sia stato decrementato da "conto bancoposta"
      | conto     | amount   | operation | transactionType   | transactionDescription   | commissioni   |
      | <payWith> | <amount> | -         | <transactionType> | <transactionDescription> | <commissioni> |
    Then Viene controllata la ricezione della notifica
      | messageText   | testIdCheck | notifyType        |
      | <messageText> | -           | <transactionType> |
    And Viene chiusa l'app

    @BPINPROAND-2165_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | payWith            | iban                        | cc                          | city   | owner             | amount | description | address                     | citySender | reference         | refecenceEffective | quickOperationName | transactionType | transactionDescription | messageText                               | categoryTransation | editValue | posteID | commissioni | bonificoType | beneficiario      | notifyType | paymentMethod | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | conto;001044548509 | IT15E3608105138271495971507 | IT15E3608105138271495971507 | Italia | salvatore musella |   0.01 | postagiro   | Via degli Ulivi Gialli n.23 | Agrigento  | Musella Salvatore | salvatore musella  | test postagiro     | POSTAGIRO       | TRN                    | Bancoposta: Conferma Postagiro effettuato | Varie              |       0.2 | prisma  |        0.50 | postagiro    | salvatore musella | postagiro  | conto         | nonseitu   |

  #Ci vogliono le credenziali per una postepay evolution
  @BPINPROAND-2167
  Scenario Outline: Postagiro da Postepay Evo a CC-P su utenze differenti
    Viene effettuato un postagiro da Postepay Evo a CC-P su utenze differenti

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck     | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | BPINPROAND-2167 | <onboarding> |
    And Viene effettuato un postagiro da "conto bancoposta"
      | iban   | cc   | owner   | amount   | description   | posteid   | payWith   | beneficiario   | commissioni   | reference   | testIdCheck | paymentMethod   |
      | <iban> | <cc> | <owner> | <amount> | <description> | <posteID> | <payWith> | <beneficiario> | <commissioni> | <reference> | -           | <paymentMethod> |
    And Salvare l'operazione come "operazione veloce"
      | quickOperationName   | useAdb | device   |
      | <quickOperationName> | true   | <device> |
    And Viene controllato che la transazione sia stata registrata nella lista movimenti
      | conto     | amount   | operation | transactionType   | transactionDescription   | commissioni   |
      | <payWith> | <amount> | -         | <transactionType> | <transactionDescription> | <commissioni> |
    And Controlla che il saldo sia stato decrementato da "conto bancoposta"
      | conto     | amount   | operation | transactionType   | transactionDescription   | commissioni   |
      | <payWith> | <amount> | -         | <transactionType> | <transactionDescription> | <commissioni> |
    Then Viene controllata la ricezione della notifica
      | messageText   | testIdCheck | notifyType        |
      | <messageText> | -           | <transactionType> |
    And Viene chiusa l'app

    @BPINPROAND-2167_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | payWith                   | iban         | cc           | city   | owner           | amount | description | address                     | citySender | reference           | refecenceEffective | quickOperationName | transactionType | transactionDescription | messageText                               | categoryTransation | editValue | posteID | commissioni | bonificoType | beneficiario    | notifyType | paymentMethod | testIdCheck     | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | postepay;5333171077147177 | 001044618914 | 001044618914 | Italia | Mariana Salvino |   0.01 | postagiro   | Via degli Ulivi Gialli n.23 | Agrigento  | FORTUNATO Salvatore | vincenzo fortunato | test postagiro     | POSTAGIRO       | TRN                    | Bancoposta: Conferma Postagiro effettuato | Varie              |       0.2 | prisma  |        0.50 | postagiro    | Mariana Salvino | postagiro  | carta         | BPINPROAND-2167 | nonseitu   |

  @BPINPROAND-1553
  Scenario Outline: Postagiro Operazione veloce
    Viene effettuato un postagiro da operazione veloce

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck     | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | BPINPROAND-1553 | <onboarding> |
    #    And Prende i dati del postagiro
    #     | iban   | cc  | owner   | amount    | description   | posteid   | payWith   | beneficiario   | commissioni   | reference   | testIdCheck | paymentMethod |
    #     | <iban> |<cc> | <owner> | <amount> | <description> | <posteID> | <payWith> | <beneficiario> | <commissioni> | <reference> | -		    | <paymentMethod> |
    And Viene effettuata la transazione da operazione veloce
      | quickOperationName   | transactionType   | testIdCheck     | payWith   | iban   | city   | owner    | amount   | description   |
      | <quickOperationName> | <transactionType> | BPINPROAND-1553 | <payWith> | <iban> | <city> | <owner > | <amount> | <description> |
    And Viene effettuato un postagiro da operazione veloce
      | payWith   | iban   | cc   | city   | owner   | amount   | description   | address   | citySender   | reference   | refecenceEffective   | posteid   | commissioni   | testIdCheck             | bonificoType   | beneficiario   |
      | <payWith> | <iban> | <cc> | <city> | <owner> | <amount> | <description> | <address> | <citySender> | <reference> | <refecenceEffective> | <posteID> | <commissioni> | PBPAPP-2105;PBPAPP-2108 | <bonificoType> | <beneficiario> |
    And Viene controllato che la transazione sia stata registrata nella lista movimenti
      | conto     | amount   | operation | transactionType   | transactionDescription   | commissioni   | paymentMethod   |
      | <payWith> | <amount> | -         | <transactionType> | <transactionDescription> | <commissioni> | <paymentMethod> |
    And Controlla che il saldo sia stato decrementato da "conto bancoposta"
      | conto     | amount   | operation | transactionType   | transactionDescription   | commissioni   |
      | <payWith> | <amount> | -         | <transactionType> | <transactionDescription> | <commissioni> |
    Then Viene controllata la ricezione della notifica
      | messageText   | testIdCheck | notifyType        |
      | <messageText> | -           | <transactionType> |
    And Viene chiusa l'app

    @BPINPROAND-1553_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | payWith            | iban         | cc           | city   | owner           | amount | description     | address                     | citySender | reference           | refecenceEffective  | quickOperationName | transactionType | transactionDescription | messageText                               | categoryTransation | editValue | posteID | commissioni | bonificoType | beneficiario  | notifyType | paymentMethod | testIdCheck     | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | conto;001044548509 | 001044618914 | 001044618914 | Italia | Mariana Salvino |   0.01 | test automatico | Via degli Ulivi Gialli n.23 | Agrigento  | FORTUNATO Salvatore | FORTUNATO Salvatore | Postagiro     | POSTAGIRO       | TRN                    | Bancoposta: Conferma Postagiro effettuato | Varie              |       0.2 | prisma  |         0.5 | bonifico     | PETRUCCI LUCA | postagiro  | carta         | BPINPROAND-1553 | nonseitu   |

    @BPINPROAND-1553_fortunato
    Examples: 
      | username                | password        | loginType | userHeader | device               | payWith            | iban         | cc           | city   | owner           | amount | description     | address                     | citySender | reference           | refecenceEffective  | quickOperationName | transactionType | transactionDescription | messageText                               | categoryTransation | editValue | posteID | commissioni | bonificoType | beneficiario  | notifyType | paymentMethod | testIdCheck     | onboarding |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | poste.it  | Vincenzo   | samsungGalaxyS8_plus | conto;001044920229 | 001044618914 | 001044618914 | Italia | Mariana Salvino |   0.01 | test automatico | Via degli Ulivi Gialli n.23 | Agrigento  | FORTUNATO Salvatore | FORTUNATO Salvatore | Postagiro     | POSTAGIRO       | TRN                    | Bancoposta: Conferma Postagiro effettuato | Varie              |       0.2 | prisma  |         0.5 | bonifico     | PETRUCCI LUCA | postagiro  | carta         | BPINPROAND-1553 | nonseitu   |

  @PBPAPP-900
  Scenario Outline: Postagiro da cc a evo con autorizzazione poste id
    Verificare che è possibile effettuare l'autorizzazione tramite app BP del Postagiro eseguito da portale BancoPosta

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | PBPAPP-900  | <onboarding> |
    And Viene effettuato un postagiro da "conto bancoposta"
      | iban   | cc   | owner   | amount   | description   | posteid   | payWith   | beneficiario   | commissioni   | reference   | testIdCheck | paymentMethod   |
      | <iban> | <cc> | <owner> | <amount> | <description> | <posteID> | <payWith> | <beneficiario> | <commissioni> | <reference> | -           | <paymentMethod> |
    And Viene controllato che la transazione sia stata registrata nella lista movimenti
      | conto     | amount   | operation | transactionType   | transactionDescription   | commissioni   |
      | <payWith> | <amount> | -         | <transactionType> | <transactionDescription> | <commissioni> |
    And Controlla che il saldo sia stato decrementato da "conto bancoposta"
      | conto     | amount   | operation | transactionType   | transactionDescription   | commissioni   |
      | <payWith> | <amount> | -         | <transactionType> | <transactionDescription> | <commissioni> |
    Then Viene controllata la ricezione della notifica
      | messageText   | testIdCheck | notifyType        |
      | <messageText> | -           | <transactionType> |
    And Viene chiusa l'app

    @PBPAPP-900
    Examples: 
      | username             | password  | loginType | userHeader | device               | payWith                   | iban                        | cc                          | city   | owner              | amount | description | address                     | citySender | reference     | refecenceEffective | quickOperationName | transactionType | transactionDescription | messageText                               | categoryTransation | editValue | posteID | commissioni | bonificoType | beneficiario       | notifyType | paymentMethod | testIdCheck | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | postepay;5333171077147177 | IT15L0760103400001044548509 | IT51T0306903491100000001379 | Italia | vincenzo fortunato |   1.00 | postagiro   | Via degli Ulivi Gialli n.23 | Agrigento  | PETRUCCI LUCA | Mariana Salvino    | test postagiro     | POSTAGIRO       | TRN                    | Bancoposta: Conferma Postagiro effettuato | Varie              |       0.2 | prisma  |        0.50 | postagiro    | vincenzo fortunato | postagiro  | carta         | PBPAPP-900  | nonseitu   |

  @BPINPROAND-2217
  Scenario Outline: Postagiro da CC a Postepay Evo, da BPOL acceduto da mobile
    Postagiro da CC a Postepay Evo, da BPOL acceduto da mobile

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | webDriver | posteid   | testIdCheck     | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | chrome    | <posteID> | BPINPROAND-2217 | <onboarding> |
    And L utente imposta come preferito il device
      | posteid   |
      | <posteID> |
    And utente Accede a BPOL
      | username   | password   | loginType   | userHeader   | device | posteid   | testIdCheck   |
      | <username> | <password> | <loginType> | <userHeader> | chrome | <posteID> | <testIdCheck> |
    And accede alla sezione Servizi online Bancoposta online
    And invia la notifica push all app
    Then la notifica push viene ricevuta dall app
      | device   |
      | <device> |
    And utente clicca su "AUTORIZZA"
      | posteid   |
      | <posteID> |
    Then dal portale BPOL si viene indirizzati alla homepage di bancoposta
    And Utente naviga verso Postagiro
    And Compila Postagiro su BPOL
      | iban   | cc   | owner   | amount   | description   | posteid   | payWith   | beneficiario   | commissioni   | reference   | testIdCheck | paymentMethod   |
      | <iban> | <cc> | <owner> | <amount> | <description> | <posteID> | <payWith> | <beneficiario> | <commissioni> | <reference> | -           | <paymentMethod> |
    And invia la notifica push all app
    Then la notifica push viene ricevuta dall app
      | device   |
      | <device> |
    And la notifica di autorizzazione postagiro viene ricevuta
      | payWith            | iban                        | type              | importo  | commissioni |
      | Conto 001044548509 | IT70M3608105138207391907401 | POSTAGIRO SU IBAN | 0,01 EUR | 0,50 EUR    |
    And utente clicca su "AUTORIZZA"
      | posteid   |
      | <posteID> |
    Then l operazione va a buon fine su BPOL
      | title                                        |
      | Il Postagiro è stato effettuato con successo |
    And Viene chiuso il portale
    And Viene chiusa l'app

    @BPINPROAND-2217_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | payWith            | iban                        | cc                          | city   | owner              | amount | description | address                     | citySender | reference          | refecenceEffective | quickOperationName | transactionType | transactionDescription | messageText                               | categoryTransation | editValue | posteID | commissioni | bonificoType | beneficiario       | notifyType | paymentMethod | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | conto;001044548509 | IT70M3608105138207391907401 | IT15E3608105138271495971507 | Italia | vincenzo fortunato |   0,01 | postagiro   | Via degli Ulivi Gialli n.23 | Agrigento  | FORTUNATO VINCENZO | vincenzo fortunato | test postagiro     | POSTAGIRO       | TRN                    | Bancoposta: Conferma Postagiro effettuato | Varie              |       0.2 | prisma  |        0.50 | postagiro    | vincenzo fortunato | postagiro  | conto         | nonseitu   |
