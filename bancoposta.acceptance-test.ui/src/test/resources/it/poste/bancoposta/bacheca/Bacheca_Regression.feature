@Bacheca
Feature: Bacheca 
L utente accede all'area bacheca

  @BPINPROAND-1572
  Scenario Outline: Bacheca
      Verificare l accesso alla Bacheca
      
   Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck | onboarding   |debug|
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | <testIdCheck>  | <onboarding> | y|
    And Naviga verso la sezione Bacheca
    And Viene chiusa l'app
 
    @BPINPROAND-1572_musella
    Examples: 
      | username                | password       | loginType | userHeader     | device               | posteID  | testIdCheck |onboarding| biscotto|
			| musellasal@gmail.com | Musabp67! | poste.it  | Salvatore       | samsungGalaxyS8_plus | prisma  | BPINPROAND-1572 |nonseitu| Libretto |		