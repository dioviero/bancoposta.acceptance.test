@Girofondo
Feature: Girofondo
   L'utente effettua un girofondo

  @BPINPROAND-1622
  Scenario Outline: Girofondo - Salvare l operazione come Operazione veloce
    L'utente effettua un girofondo e lo salva come operazione veloce

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck     | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | BPINPROAND-1622 | <onboarding> |
    And Viene effettuato un "girofondo"
      | payWith   | transferTo   | owner   | amount   | posteid   | favoreDi   | commissioni   | fromContoDetail   | toContoDetail   | testIdCheck |
      | <payWith> | <transferTo> | <owner> | <amount> | <posteID> | <favoreDi> | <commissioni> | <fromContoDetail> | <toContoDetail> | PBPAPP-2276 |
    And Salvare l'operazione come "operazione veloce"
      | quickOperationName   | useAdb | device   |
      | <quickOperationName> | true   | <device> |
    And Viene chiusa l'app

    @BPINPROAND-1622_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | payWith            | transferTo                  | owner             | amount | favoreDi          | commissioni | fromContoDetail  | toContoDetail    | messageText                                    | quickOperationName | editValue | categoryTransation | month | categoryDestination | posteID | transactionType | transactionDescription | testIdCheck     | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | conto;001044548509 | libretto smart;000049494216 | Salvatore Musella |   0.01 | MUSELLA SALVATORE |        0.50 | Conto BancoPosta | Libretto Postale | Conferma Girofondo da Conto BancoPosta a Libre | girofondo veloc    |      0.20 | banca e finanza    | month | categoryDestination | prisma  | GIROFONDO       | DR 000050028091        | BPINPROAND-1622 | nonseitu   |

    @BPINPROAND-1622_fortunato
    Examples: 
      | username                | password        | loginType | userHeader | device               | payWith            | transferTo                  | owner              | amount | favoreDi           | commissioni | fromContoDetail  | toContoDetail    | messageText                                    | quickOperationName | editValue | categoryTransation | month | categoryDestination | posteID | transactionType | transactionDescription | testIdCheck     | onboarding |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | poste.it  | Vincenzo   | samsungGalaxyS8_plus | conto;001044920229 | libretto smart;000049580610 | Vincenzo Fortunato |   0.01 | FORTUNATO VINCENZO |        0.50 | Conto BancoPosta | Libretto Postale | Conferma Girofondo da Conto BancoPosta a Libre | girofondo veloc    |      0.20 | banca e finanza    | month | categoryDestination | prisma  | GIROFONDO       | DR 000050028091        | BPINPROAND-1622 | nonseitu   |

  @BPINPROAND-1881
  Scenario Outline: Girofondo da conto a libretto - in lavorazione OK
    L'utente effettua un girofondo da conto a libretto

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck     | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | BPINPROAND-1881 | <onboarding> |
    And Viene effettuato un "girofondo"
      | payWith   | transferTo   | owner   | amount   | posteid   | favoreDi   | commissioni   | fromContoDetail   | toContoDetail   | testIdCheck |
      | <payWith> | <transferTo> | <owner> | <amount> | <posteID> | <favoreDi> | <commissioni> | <fromContoDetail> | <toContoDetail> | PBPAPP-2996 |
    #And Salvare l'operazione come "operazione veloce"
    #| quickOperationName   | useAdb | device   |
    #| <quickOperationName> | true   | <device> |
    And Viene chiusa l'app

    @BPINPROAND-1881_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | payWith            | transferTo                  | owner             | amount | favoreDi          | commissioni | fromContoDetail  | toContoDetail    | messageText                                    | quickOperationName | editValue | categoryTransation | month | categoryDestination | posteID | transactionType | transactionDescription | testIdCheck     | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | conto;001044548509 | libretto smart;000049494216 | Salvatore Musella |   0.01 | MUSELLA SALVATORE |        0.50 | Conto BancoPosta | Libretto Postale | Conferma Girofondo da Conto BancoPosta a Libre | girofondo veloc    |      0.20 | banca e finanza    | month | categoryDestination | prisma  | GIROFONDO       | DR 000050028091        | BPINPROAND-1881 | nonseitu   |

  @BPINPROAND-1755
  Scenario Outline: Girofondo Operazione Veloce - Modifica dati
    Viene effettuata la modifica dati di un girofondo

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck     | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | BPINPROAND-1755 | <onboarding> |
    And Controlla che l'operazione veloce sia editabile
      | quickOperationName   | testIdCheck | transactionType   | amount   | transferTo   | payWith   |
      | <quickOperationName> | -           | <transactionType> | <amount> | <transferTo> | <payWith> |
    And Viene chiusa l'app

    @BPINPROAND-1755_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | payWith            | transferTo                  | owner             | amount | messageText                                    | quickOperationName | editValue | categoryTransation | month | categoryDestination | posteID | description                         | testIdCheck     | transactionType | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | conto;001044548509 | libretto smart;000049494216 | Salvatore Musella |   0.01 | Conferma Girofondo da Conto BancoPosta a Libre | Girofondo    |      0.02 | varie              | month | categoryDestination | prisma  | Verifica i dati prima di procedere. | BPINPROAND-1755 | GIROFONDO       | nonseitu   |

    @BPINPROAND-1755_fortunato
    Examples: 
      | username                | password        | loginType | userHeader | device               | payWith            | transferTo                  | owner              | amount | messageText                                    | quickOperationName | editValue | categoryTransation | month | categoryDestination | posteID | description                         | testIdCheck     | transactionType | onboarding |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | poste.it  | Vincenzo   | samsungGalaxyS8_plus | conto;001044920229 | libretto smart;000049580610 | Vincenzo Fortunato |   0.01 | Conferma Girofondo da Conto BancoPosta a Libre | Girofondo    |      0.02 | varie              | month | categoryDestination | prisma  | Verifica i dati prima di procedere. | BPINPROAND-1755 | GIROFONDO       | nonseitu   |

  @BPINPROAND-1877
  Scenario Outline: Girofondo da libretto a libretto - presa in carico OK
    L'utente effettua un girofondo e lo salva come operazione veloce

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck     | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | BPINPROAND-1877 | <onboarding> |
    And Viene effettuato un "girofondo"
      | payWith   | transferTo   | owner   | amount   | posteid   | favoreDi   | commissioni   | fromContoDetail   | toContoDetail   | testIdCheck | paymentMethod   |
      | <payWith> | <transferTo> | <owner> | <amount> | <posteID> | <favoreDi> | <commissioni> | <fromContoDetail> | <toContoDetail> | PBPAPP-2992 | <paymentMethod> |
    And Viene chiusa l'app

    @BPINPROAND-1877_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | payWith            | transferTo                  | owner             | amount | favoreDi          | commissioni | fromContoDetail  | toContoDetail    | messageText                                     | quickOperationName | editValue | categoryTransation | month | categoryDestination | posteID | transactionType | transactionDescription | paymentMethod               | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | conto;001044548509 | libretto smart;000050028091 | Salvatore Musella |   0.01 | MUSELLA SALVATORE |        0.50 | Conto BancoPosta | Libretto Postale | BancoPosta: Conferma Trasferimento Fondi da App | girofondo          |      0.20 | banca e finanza    | month | categoryDestination | prisma  | GIROFONDO       | DR 000050028091        | libretto smart;000049494216 | nonseitu   |

  @BPINPROAND-2235
  Scenario Outline: Girofondo da cc a libretto con autorizzazione poste id
    Verificare che è possibile eseguire l'autorizzazione tramite app BP del Girofondo eseguito da portale BancoPosta

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck     | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | BPINPROAND-2235 | <onboarding> |
    And Viene effettuato un "girofondo"
      | payWith   | transferTo   | owner   | amount   | posteid   | favoreDi   | commissioni   | fromContoDetail   | toContoDetail   | testIdCheck |
      | <payWith> | <transferTo> | <owner> | <amount> | <posteID> | <favoreDi> | <commissioni> | <fromContoDetail> | <toContoDetail> | PBPAPP-901  |
    And Viene controllato che la transazione sia stata registrata nella lista movimenti
      | conto     | amount   | operation | transactionType   | transactionDescription   |
      | <payWith> | <amount> | -         | <transactionType> | <transactionDescription> |
    And Viene controllata la corretta categorizzazione della transazione
      | categoryTransation   | amount   | subCategoryTransaction | transactionType   | operation |
      | <categoryTransation> | <amount> | Prodotti di risparmio  | <transactionType> | -         |
    Then Viene controllata la ricezione della notifica
      | messageText   | testIdCheck | notifyType        |
      | <messageText> | PBPAPP-901  | <transactionType> |
    And Viene chiusa l'app

    @BPINPROAND-2235_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | payWith            | transferTo                  | owner             | amount | favoreDi            | commissioni | fromContoDetail  | toContoDetail    | messageText                                    | quickOperationName | editValue | categoryTransation | month | categoryDestination | posteID | transactionType | transactionDescription | testIdCheck | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | conto;001044548509 | libretto smart;000049494216 | Salvatore Musella |   0.01 | Fortunato Salvatore |        0.50 | Conto BancoPosta | Libretto Postale | Conferma Girofondo da Conto BancoPosta a Libre | girofondo          |      0.20 | banca e finanza    | month | categoryDestination | prisma  | GIROFONDO       | DR 000050028091        | PBPAPP-901  | nonseitu   |

  @BPINPROAND-1512 @BPINPROIOS-1362
  Scenario Outline: Verificare le opzioni di accredito nella schermata "Trasferisci su" del Girofondo
    Verificare le opzioni di accredito nella schermata "Trasferisci su" del Girofondo

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck     | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | BPINPROAND-1512 | <onboarding> |
    And Naviga verso la sezione girofondo
      | payWith   | transferTo   | owner   | amount   | posteid   | favoreDi   | commissioni   | fromContoDetail   | toContoDetail   | testIdCheck |
      | <payWith> | <transferTo> | <owner> | <amount> | <posteID> | <favoreDi> | <commissioni> | <fromContoDetail> | <toContoDetail> | PBPAPP-901  |
    Then controlla che nella lista Trasferisci su siano mostrati i seguenti dati
      | transferTo                  |
      #| Libretto Ordinario 000049573986 |
      | Libretto Smart 000049494216 |
    And Viene chiusa l'app

    @BPINPROAND-1512_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | payWith            | transferTo                  | owner             | amount | favoreDi            | commissioni | fromContoDetail  | toContoDetail    | messageText                                    | quickOperationName | editValue | categoryTransation | month | categoryDestination | posteID | transactionType | transactionDescription | testIdCheck     | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | conto;001044548509 | libretto smart;000049494216 | Salvatore Musella |   0.01 | Fortunato Salvatore |        0.50 | Conto BancoPosta | Libretto Postale | Conferma Girofondo da Conto BancoPosta a Libre | girofondo          |      0.20 | banca e finanza    | month | categoryDestination | prisma  | GIROFONDO       | DR 000050028091        | BPINPROAND-1512 | nonseitu   |

    @BPINPROIOS-1362_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | payWith            | transferTo                  | owner             | amount | favoreDi            | commissioni | fromContoDetail  | toContoDetail    | messageText                                    | quickOperationName | editValue | categoryTransation | month | categoryDestination | posteID | transactionType | transactionDescription | testIdCheck     | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | conto;001044548509 | libretto smart;000049494216 | Salvatore Musella |   0.01 | Fortunato Salvatore |        0.50 | Conto BancoPosta | Libretto Postale | Conferma Girofondo da Conto BancoPosta a Libre | girofondo          |      0.20 | banca e finanza    | month | categoryDestination | prisma  | GIROFONDO       | DR 000050028091        | BPINPROIOS-1362 | nonseitu   |
