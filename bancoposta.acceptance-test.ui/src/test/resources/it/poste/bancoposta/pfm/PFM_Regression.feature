@PFM
Feature: PFM
  L'utente Verifica il bottone Vedi uscite non contabilizzate

  @BPINPROAND-2138
  Scenario Outline: Verifica il bottone Vedi uscite non contabilizzate
    L'utente Verifica il bottone Vedi uscite non contabilizzate

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck     | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | BPINPROAND-2138 | <onboarding> |
    And Naviga verso la sezione conti e carte
    And controlla la presenza del bottone Vedi uscite non contabilizzate
    #And clicca sul bottone Vedi uscite non contabilizzate e controlla la lista dei movimenti da contabilizzare
    And Viene chiusa l'app

    @BPINPROAND-2138_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | payWith                   | iban                        | cc                          | city   | owner              | amount | description | address                     | citySender | reference     | refecenceEffective | quickOperationName | transactionType | transactionDescription | messageText                               | categoryTransation | editValue | posteID | commissioni | bonificoType | beneficiario       | notifyType | paymentMethod | testIdCheck     | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | postepay;5333171077147177 | IT15E3608105138271495971507 | IT51T0306903491100000001379 | Italia | vincenzo fortunato |   0.01 | postagiro   | Via degli Ulivi Gialli n.23 | Agrigento  | PETRUCCI LUCA | Luca Petrucci      | postagiro uno      | POSTAGIRO       | TRN                    | Bancoposta: Conferma Postagiro effettuato | Varie              |       0.2 | prisma  |        0.50 | postagiro    | vincenzo fortunato | postagiro  | carta         | BPINPROAND-2138 | nonseitu   |

  #Per eseguire il test ci vuole una postepay Evolution
  @BPINPROAND-2030 @BPINPROIOS-1880
  Scenario Outline: Riepilogo - Vedi il tuo Andamento
    L'utente fa tap sul bottone Vedi il tuo Andamento

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck     | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | BPINPROAND-2030 | <onboarding> |
    And Naviga verso la sezione le tue spese
    And Clicca su Vedi il tuo andamento
    Then Viene controllata la sezione Il tuo Andamento
    And Viene chiusa l'app

    @BPINPROAND-2030_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | payWith                   | iban                        | cc                          | city   | owner              | amount | description | address                     | citySender | reference     | refecenceEffective | quickOperationName | transactionType | transactionDescription | messageText                               | categoryTransation | editValue | posteID | commissioni | bonificoType | beneficiario       | notifyType | paymentMethod | testIdCheck     | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | postepay;5333171077147177 | IT15L0760103400001044920229 | IT51T0306903491100000001379 | Italia | vincenzo fortunato |   0.01 | postagiro   | Via degli Ulivi Gialli n.23 | Agrigento  | PETRUCCI LUCA | Luca Petrucci      | postagiro uno      | POSTAGIRO       | TRN                    | Bancoposta: Conferma Postagiro effettuato | Varie              |       0.2 | prisma  |        0.50 | postagiro    | vincenzo fortunato | postagiro  | carta         | BPINPROAND-2030 | nonseitu   |

    @BPINPROIOS-1880_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | payWith                   | iban                        | cc                          | city   | owner              | amount | description | address                     | citySender | reference     | refecenceEffective | quickOperationName | transactionType | transactionDescription | messageText                               | categoryTransation | editValue | posteID | commissioni | bonificoType | beneficiario       | notifyType | paymentMethod | testIdCheck     | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | postepay;5333171077147177 | IT15L0760103400001044920229 | IT51T0306903491100000001379 | Italia | vincenzo fortunato |   0.01 | postagiro   | Via degli Ulivi Gialli n.23 | Agrigento  | PETRUCCI LUCA | Luca Petrucci      | postagiro uno      | POSTAGIRO       | TRN                    | Bancoposta: Conferma Postagiro effettuato | Varie              |       0.2 | prisma  |        0.50 | postagiro    | vincenzo fortunato | postagiro  | carta         | BPINPROAND-2030 | nonseitu   |

  @BPINPROAND-2026 @BPINPROIOS-1876
  Scenario Outline: Child page Spese del mese
    Child page Spese del mese

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck     | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | BPINPROAND-2026 | <onboarding> |
    And Naviga verso la sezione le tue spese
    And Clicca su Vedi tutte le categorie
    And Viene cliccato l'opzione esporta come csv
      | csvPage          |
      | tutteLeCategorie |
    Then il file viene esportato
      | csvPage          |
      | tutteLeCategorie |
    And Clicca su una sottocategoria
      | csvPage          | categoryTransation | subCategoryTransaction |
      | tutteLeCategorie | Varie              | Da Categorizzare       |
    And Viene chiusa l'app

    @BPINPROAND-2026_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | payWith                   | iban                        | cc                          | city   | owner              | amount | description | address                     | citySender | reference     | refecenceEffective | quickOperationName | transactionType | transactionDescription | messageText                               | categoryTransation | editValue | posteID | commissioni | bonificoType | beneficiario       | notifyType | paymentMethod | testIdCheck     | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | postepay;5333171077147177 | IT15L0760103400001044920229 | IT51T0306903491100000001379 | Italia | vincenzo fortunato |   0.01 | postagiro   | Via degli Ulivi Gialli n.23 | Agrigento  | PETRUCCI LUCA | Luca Petrucci      | postagiro uno      | POSTAGIRO       | TRN                    | Bancoposta: Conferma Postagiro effettuato | Varie              |       0.2 | prisma  |        0.50 | postagiro    | vincenzo fortunato | postagiro  | carta         | BPINPROAND-2026 | nonseitu   |

   @BPINPROAND-2026_fortunato
    Examples: 
      | username                | password        | loginType | userHeader | device               | onboarding | posteID |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | poste.it  | Vincenzo   | samsungGalaxyS8_plus | nonseitu   | prisma  |
      
    @BPINPROIOS-1876_fortunato
    Examples: 
      | username                | password        | loginType | userHeader | device               | onboarding | posteID |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | poste.it  | Vincenzo   | samsungGalaxyS8_plus | nonseitu   | prisma  |

    @BPINPROIOS-1876_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | posteID | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | prisma  | nonseitu   |

  @BPINPROAND-2083
  Scenario Outline: Verifica il file csv di export dei movimenti esclusi dalle tue spese
    Verifica il file csv di export dei movimenti esclusi dalle tue spese

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck     | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | BPINPROAND-2083 | <onboarding> |
    And Naviga verso la sezione le tue spese
    And Clicca su Vedi tutte le categorie
    And Clicca su Movimenti esclusi
      | csvPage          | categoryTransation | subCategoryTransaction |
      | MovimentiEsclusi | Movimenti esclusi  | Giroconti e ricariche  |
    And Clicca su una sottocategoria
      | csvPage          | categoryTransation | subCategoryTransaction |
      | MovimentiEsclusi | Movimenti esclusi  | Giroconti e ricariche  |
    And Viene cliccato l'opzione esporta come csv
      | csvPage               |
      | Giroconti e ricariche |
    Then il file viene esportato
      | csvPage               |
      | Giroconti e ricariche |
    And Viene chiusa l'app

    @BPINPROAND-2083_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | payWith                   | iban                        | cc                          | city   | owner              | amount | description | address                     | citySender | reference     | refecenceEffective | quickOperationName | transactionType | transactionDescription | messageText                               | categoryTransation | editValue | posteID | commissioni | bonificoType | beneficiario       | notifyType | paymentMethod | testIdCheck     | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | postepay;5333171077147177 | IT15L0760103400001044920229 | IT51T0306903491100000001379 | Italia | vincenzo fortunato |   0.01 | postagiro   | Via degli Ulivi Gialli n.23 | Agrigento  | PETRUCCI LUCA | Luca Petrucci      | postagiro uno      | POSTAGIRO       | TRN                    | Bancoposta: Conferma Postagiro effettuato | Varie              |       0.2 | prisma  |        0.50 | postagiro    | vincenzo fortunato | postagiro  | carta         | BPINPROAND-2083 | nonseitu   |

    @BPINPROAND-2083_fortunato
    Examples: 
      | username                | password        | loginType | userHeader | device               | onboarding | posteID |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | poste.it  | Vincenzo   | samsungGalaxyS8_plus | nonseitu   | prisma  |
      
  @BPINPROAND-2011 @BPINPROIOS-1861
  Scenario Outline: Mese non corrente - Hai speso di pi? in
    Mese non corrente - Hai speso di pi? in

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck     | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | BPINPROAND-2011 | <onboarding> |
    And Naviga verso la sezione le tue spese
    And Clicca su Vedi tutte le categorie
    And Viene chiusa l'app

    @BPINPROAND-2011_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | payWith                   | iban                        | cc                          | city   | owner              | amount | description | address                     | citySender | reference     | refecenceEffective | quickOperationName | transactionType | transactionDescription | messageText                               | categoryTransation | editValue | posteID | commissioni | bonificoType | beneficiario       | notifyType | paymentMethod | testIdCheck     | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | postepay;5333171077147177 | IT15L0760103400001044920229 | IT51T0306903491100000001379 | Italia | vincenzo fortunato |   0.01 | postagiro   | Via degli Ulivi Gialli n.23 | Agrigento  | PETRUCCI LUCA | Luca Petrucci      | postagiro uno      | POSTAGIRO       | TRN                    | Bancoposta: Conferma Postagiro effettuato | Varie              |       0.2 | prisma  |        0.50 | postagiro    | vincenzo fortunato | postagiro  | carta         | BPINPROAND-2011 | nonseitu   |

    @BPINPROIOS-1861_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | posteID | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | prisma  | nonseitu   |

    @BPINPROIOS-1861_fortunato
    Examples: 
      | username                | password        | loginType | userHeader | device               | posteID | onboarding |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | poste.it  | Vincenzo   | samsungGalaxyS8_plus | prisma  | nonseitu   |

  @BPINPROAND-2067 @BPINPROIOS-1917
  Scenario Outline: Andamento Differenza-Il Tuo Andamento-tab Differenza-Header
    Andamento Differenza-Il Tuo Andamento-tab Differenza-Header

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck     | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | BPINPROAND-2067 | <onboarding> |
    And Naviga verso la sezione le tue spese
    And Clicca su Vedi il tuo andamento
    Then Viene controllata la sezione Il tuo Andamento
    And Clicca sul tab "Uscite"
    Then I pin del grafico "Uscite" vengono aggiornati dopo click sulla sidebar del grafico
    And Clicca sul tab "Entrate"
    Then I pin del grafico "Entrate" vengono aggiornati dopo click sulla sidebar del grafico
    And Clicca sul tab "Differenza"
    Then I pin del grafico "Differenza" vengono aggiornati dopo click sulla sidebar del grafico
    And Viene cliccato l'opzione esporta come csv
      | csvPage        |
      | ilTuoAndamento |
    Then il file viene esportato
      | csvPage        |
      | ilTuoAndamento |
    And Viene chiusa l'app

    @BPINPROAND-2067_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | payWith                   | iban                        | cc                          | city   | owner              | amount | description | address                     | citySender | reference     | refecenceEffective | quickOperationName | transactionType | transactionDescription | messageText                               | categoryTransation | editValue | posteID | commissioni | bonificoType | beneficiario       | notifyType | paymentMethod | testIdCheck     | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | postepay;5333171077147177 | IT15L0760103400001044920229 | IT51T0306903491100000001379 | Italia | vincenzo fortunato |   0.01 | postagiro   | Via degli Ulivi Gialli n.23 | Agrigento  | PETRUCCI LUCA | Luca Petrucci      | postagiro uno      | POSTAGIRO       | TRN                    | Bancoposta: Conferma Postagiro effettuato | Varie              |       0.2 | prisma  |        0.50 | postagiro    | vincenzo fortunato | postagiro  | carta         | BPINPROAND-2030 | nonseitu   |

    @BPINPROAND-2067_fortunato
    Examples: 
      | username                | password        | loginType | userHeader | device               | posteID | onboarding |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | poste.it  | Vincenzo   | samsungGalaxyS8_plus | prisma  | nonseitu   |

    @BPINPROIOS-1917_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | posteID | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | prisma  | nonseitu   |

  @BPINPROAND-1375
  Scenario Outline: Verifica che cliccando su le tue spese in una notifica push si viene reindirizzati alle tue spese
    Verifica che cliccando su le tue spese in una notifica push si viene reindirizzati alle tue spese

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck     | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | BPINPROAND-1375 | <onboarding> |
    And Viene effettuato un postagiro da "conto bancoposta" 2
      | iban   | cc   | owner   | amount   | description   | posteid   | payWith   | beneficiario   | commissioni   | reference   | testIdCheck | paymentMethod   |
      | <iban> | <cc> | <owner> | <amount> | <description> | <posteID> | <payWith> | <beneficiario> | <commissioni> | <reference> | -           | <paymentMethod> |
    Then la notifica push viene ricevuta dall app
      | device   | bpol | notificaTitle    |
      | <device> | n    | Hai un movimento |
    And utente clicca su "SPESE"
      | posteid   |
      | <posteID> |
    Then La sezione le tue spese viene aperta
    And Viene chiusa l'app

    @BPINPROAND-1375_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | payWith                   | iban         | cc           | city   | owner              | amount | description | address                     | citySender | reference         | refecenceEffective | quickOperationName | transactionType | transactionDescription | messageText                               | categoryTransation | editValue | posteID | commissioni | bonificoType | beneficiario       | notifyType | paymentMethod | testIdCheck     | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | postepay;5333171077147177 | 001044920229 | 001044920229 | Italia | Vincenzo Fortunato |   0.01 | postagiro   | Via degli Ulivi Gialli n.23 | Agrigento  | Musella Salvatore | salvatore musella  | test postagiro     | POSTAGIRO       | TRN                    | Bancoposta: Conferma Postagiro effettuato | Varie              |       0.2 | prisma  |        0.50 | postagiro    | Vincenzo Fortunato | postagiro  | carta         | BPINPROAND-1375 | nonseitu   |

 		@BPINPROAND-1375_fortunato
 		 Examples:
 				 | username                | password        | loginType | userHeader | device               | payWith                   | iban         | cc           | city   | owner             | amount | description | address                     | citySender | reference          | refecenceEffective | quickOperationName | transactionType | transactionDescription | messageText                               | categoryTransation | editValue | posteID | commissioni | bonificoType | beneficiario      | notifyType | paymentMethod | testIdCheck | onboarding |
 				 | vincenzo.fortunato-4869 | D@niela22Lili10 | poste.it  | Vincenzo   | samsungGalaxyS8_plus        | postepay;5333171077144679 | 001044548509 | 001044548509 | Italia | Salvatore Musella |   0.01 | postagiro   | Via degli Ulivi Gialli n.23 | Agrigento  | Fortunato Vincenzo | vincenzo fortunato | test postagiro     | POSTAGIRO       | TRN                    | Bancoposta: Conferma Postagiro effettuato | Varie              |       0.2 | prisma  |        0.50 | postagiro    | Salvatore Musella | postagiro  | carta         | PBPAPP-880  | nonseitu   |
  
  @BPINPROAND-4900
  Scenario Outline: Andamento - Uscite
    Viene controllare la navigazione nella sezione PFM il tuo andamento

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck     | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | BPINPROAND-4900 | <onboarding> |
    And Naviga verso la sezione le tue spese
    And Clicca su Vedi il tuo andamento
    Then Viene controllata la sezione Il tuo Andamento
    And Clicca sul tab "Uscite"
    Then I pin del grafico "Uscite" vengono aggiornati dopo click sulla sidebar del grafico
    And Utente clicca back da il tuo andamento
    And Utene effettua uno swipe indietro tra i mesi
    And Clicca su Vedi il tuo andamento
    And Viene cliccato l'opzione esporta come csv
      | csvPage        |
      | ilTuoAndamento |
    Then il file viene esportato
      | csvPage        |
      | ilTuoAndamento |
    And Viene chiusa l'app

    @BPINPROAND-4900_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | payWith                   | iban                        | cc                          | city   | owner              | amount | description | address                     | citySender | reference     | refecenceEffective | quickOperationName | transactionType | transactionDescription | messageText                               | categoryTransation | editValue | posteID | commissioni | bonificoType | beneficiario       | notifyType | paymentMethod | testIdCheck     | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | postepay;5333171077147177 | IT15L0760103400001044920229 | IT51T0306903491100000001379 | Italia | vincenzo fortunato |   0.01 | postagiro   | Via degli Ulivi Gialli n.23 | Agrigento  | PETRUCCI LUCA | Luca Petrucci      | postagiro uno      | POSTAGIRO       | TRN                    | Bancoposta: Conferma Postagiro effettuato | Varie              |       0.2 | prisma  |        0.50 | postagiro    | vincenzo fortunato | postagiro  | carta         | BPINPROAND-2030 | nonseitu   |

    @BPINPROAND-4900_fortunato
    Examples: 
      | username                | password        | loginType | userHeader | device               | payWith                   | iban                        | cc                          | city   | owner              | amount | description | address                     | citySender | reference     | refecenceEffective | quickOperationName | transactionType | transactionDescription | messageText                               | categoryTransation | editValue | posteID | commissioni | bonificoType | beneficiario       | notifyType | paymentMethod | testIdCheck     | onboarding |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | poste.it  | Vincenzo   | samsungGalaxyS8_plus | postepay;5333171077147177 | IT15L0760103400001044920229 | IT51T0306903491100000001379 | Italia | vincenzo fortunato |   0.01 | postagiro   | Via degli Ulivi Gialli n.23 | Agrigento  | PETRUCCI LUCA | Luca Petrucci      | postagiro uno      | POSTAGIRO       | TRN                    | Bancoposta: Conferma Postagiro effettuato | Varie              |       0.2 | prisma  |        0.50 | postagiro    | vincenzo fortunato | postagiro  | carta         | BPINPROAND-2030 | nonseitu   |
