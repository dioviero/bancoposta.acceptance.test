Feature: Buoni
  L'utente effettua l emissione di buoni

  @BPINPROAND-1695
  Scenario Outline: Emissione BFP 3X2 da conto
    Viene effettuato un BFP 3X2 da conto

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck     | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | BPINPROAND-1695 | <onboarding> |
    And Dalla pagina "buoni e libretti" tab "buoni postali" vengono compilati i seguenti campi e viene effettuato operazione di "conferma"
      | voucherType   | amount   | payWith   | posteid   | testIdCheck | value   | amount2   |
      | <voucherType> | <amount> | <payWith> | <posteid> | PBPAPP-2421 | <value> | <amount2> |
    #    Then Viene controllata la ricezione della notifica
    #      | messageText   | testIdCheck |
    #      | <messageText> | PBPAPP-463  |
    And Viene chiusa l'app

    @BPINPROAND-1695_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | voucherType | amount | amount2 | payWith            | sender             | messageText | quickOperationName | Value | posteID | testIdCheck | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | BFP 3X2     |  50.00 |  100.00 | conto;001044548509 | vincenzo fortunato | messageText | quickOperationName | Value | prisma  | BPINPROAND-1695 | nonseitu   |

   @BPINPROAND-1696
  Scenario Outline: Emissione BFP 3X4 da conto
    Viene effettuato un BFP 3X4 da conto

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck     | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | BPINPROAND-1696 | <onboarding> |
    And Dalla pagina "buoni e libretti" tab "buoni postali" vengono compilati i seguenti campi e viene effettuato operazione di "conferma"
      | voucherType   | amount   | payWith   | posteid   | testIdCheck     | value   | amount2   |
      | <voucherType> | <amount> | <payWith> | <posteid> | BPINPROAND-1696 | <value> | <amount2> |
    #    Then Viene controllata la ricezione della notifica
    #      | messageText   | testIdCheck |
    #      | <messageText> | PBPAPP-463  |
    And Viene chiusa l'app

    @BPINPROAND-1696_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | voucherType                     | amount | amount2 | payWith            | sender             | messageText | quickOperationName | value | posteID | testIdCheck | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | BUONO FRUTTIFERO POSTALE BFP3X4 |  50.00 |  100.00 | conto;001044548509 | vincenzo fortunato | messageText | quickOperationName | value | prisma  | BPINPROAND-1696  | nonseitu   |

  @BPINPROAND-1697
  Scenario Outline: Emissione Buono Ordinario da conto
    Viene effettuata l'emissione di un Buono Ordinario da conto

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck     | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | BPINPROAND-1697 | <onboarding> |
    And Dalla pagina "buoni e libretti" tab "buoni postali" vengono compilati i seguenti campi e viene effettuato operazione di "conferma"
      | voucherType   | amount   | payWith   | posteid   | testIdCheck     | value   | amount2   |
      | <voucherType> | <amount> | <payWith> | <posteID> | BPINPROAND-1697 | <value> | <amount2> |
    #    Then Viene controllata la ricezione della notifica
    #      | messageText   | testIdCheck |
    #      | <messageText> | PBPAPP-463  |
    And Viene chiusa l'app

    @BPINPROAND-1697_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | voucherType     | amount | amount2 | payWith            | sender             | messageText | quickOperationName | value | posteID | testIdCheck | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | BUONO ORDINARIO |  50.00 |  100.00 | conto;001044548509 | vincenzo fortunato | messageText | quickOperationName | value | prisma  | BPINPROAND-1697  | nonseitu   |

  #    OBSOLETO
  #   @PBAPP-2424
  #  Scenario Outline: Gestione buoni: creazione buoni, check ricezione notifica e salvataggio in operazioni veloci
  #    L'utente deve avere un conto bp e la carta onboardati
  #
  #    Given L'app "android" viene avviata e viene eseguita la login
  #      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck |
  #      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | PBPAPP-2832 |
  #    And Dalla pagina "buoni e libretti" tab "buoni postali" vengono compilati i seguenti campi e viene effettuato operazione di "conferma"
  #      | voucherType   | amount   | payWith   | posteid   | testIdCheck                                     |
  #      | <voucherType> | <amount> | <payWith> | <posteid> | PBPAPP-2421;PBPAPP-2422;PBPAPP-2423;PBPAPP-2423 |
  #    Then Viene controllata la ricezione della notifica
  #      | messageText   | testIdCheck |
  #      | <messageText> | PBPAPP-463  |
  #    And Viene chiusa l'app
  #    @Test-PBAPP-2424
  #    Examples:
  #      | username           | password     | loginType | userHeader | device   | voucherType | amount | payWith            | sender      | messageText | quickOperationName | editValue | posteID |
  #      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore       | samsungGalaxyS8_plus| BFP 3X2     |  50.00 | conto;001044548509 | Salvatore Acalli | messageText | quickOperationName | editValue | prisma  |
  #
  #
  @BPINPROAND-1813
  Scenario Outline: Sottoscrizione buono 3X4 da libretto - PIC Diurna
    Viene effetuata la sottoscrizione buono 3X4 da libretto - PIC Diurna

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck     | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | BPINPROAND-1813 | <onboarding> |
    And Dalla pagina "buoni e libretti" tab "buoni postali" vengono compilati i seguenti campi e viene effettuato operazione di "conferma"
      | voucherType   | amount   | payWith   | posteid   | testIdCheck     | value   | amount2   |
      | <voucherType> | <amount> | <payWith> | <posteid> | BPINPROAND-1813 | <value> | <amount2> |
    And viene mostrato il messaggio di presa in carico
      | messageText   | testIdCheck   |
      | <messageText> | <testIdCheck> |
    #    Then Viene controllata la ricezione della notifica
    #      | messageText   | testIdCheck |
    #      | <messageText> | PBPAPP-463  |
    And Viene chiusa l'app

    @BPINPROAND-1813_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | voucherType                     | amount | amount2 | payWith            | sender             | messageText | quickOperationName | value | posteID | testIdCheck | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | BUONO FRUTTIFERO POSTALE BFP3X4 |  50.00 |  100.00 | conto;001044548509 | vincenzo fortunato | messageText | quickOperationName | value | prisma  | BPINPROAND-1813 | nonseitu   |

	 @BPINPROAND-1815
  	Scenario Outline: Sottoscrizione buono 4X4 da libretto - PIC Diurna
    Viene effetuata la sottoscrizione buono 4X4 da libretto - PIC Diurna

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck     | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | BPINPROAND-1815 | <onboarding> |
    And Dalla pagina "buoni e libretti" tab "buoni postali" vengono compilati i seguenti campi e viene effettuato operazione di "conferma"
      | voucherType   | amount   | payWith   | posteid   | testIdCheck     | value   | amount2   |
      | <voucherType> | <amount> | <payWith> | <posteid> | BPINPROAND-1815 | <value> | <amount2> |
    And viene mostrato il messaggio di presa in carico
      | messageText   | testIdCheck |
      | <messageText> | PBPAPP-463  |
    #Then Viene controllata la ricezione della notifica
      #| messageText   | testIdCheck |
      #| <messageText> | PBPAPP-463  |
    And Viene chiusa l'app

    @BPINPROAND-1815_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | voucherType | amount | amount2 | payWith            | sender             | messageText | quickOperationName | value | posteID | testIdCheck | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | BFP 4X4     |  50.00 |  100.00 | conto;001044548509 | vincenzo fortunato | messageText | quickOperationName | value | prisma  | BPINPROAND-1815  | nonseitu   |

  @BPINPROAND-1816
  Scenario Outline: Sottoscrizione buono ordinario da libretto - PIC Diurna
    Viene effetuata la sottoscrizione buono ordinario da libretto - PIC Diurna

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck     | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | BPINPROAND-1816 | <onboarding> |
    And Dalla pagina "buoni e libretti" tab "buoni postali" vengono compilati i seguenti campi e viene effettuato operazione di "conferma"
      | voucherType   | amount   | payWith   | posteid   | testIdCheck     | value   | amount2   |
      | <voucherType> | <amount> | <payWith> | <posteid> | BPINPROAND-1816 | <value> | <amount2> |
    And viene mostrato il messaggio di presa in carico
      | messageText   | testIdCheck |
      | <messageText> | PBPAPP-463  |
    #Then Viene controllata la ricezione della notifica
      #| messageText   | testIdCheck |
      #| <messageText> | PBPAPP-463  |
    And Viene chiusa l'app

    @BPINPROAND-1816_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | voucherType | amount | amount2 | payWith            | sender             | messageText | quickOperationName | value | posteID | testIdCheck | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | BFP 4X4     |  50.00 |  100.00 | conto;001044548509 | vincenzo fortunato | messageText | quickOperationName | value | prisma  | BPINPROAND-1816 | nonseitu   |

  @BPINPROAND-1817
  Scenario Outline: Sottoscrizione buono 3X2 da libretto - PIC Diurna
    Viene effetuata la sottoscrizione buono 3X2 da libretto - PIC Diurna

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck     | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | BPINPROAND-1817 | <onboarding> |
    And Dalla pagina "buoni e libretti" tab "buoni postali" vengono compilati i seguenti campi e viene effettuato operazione di "conferma"
      | voucherType   | amount   | payWith   | posteid   | testIdCheck     | amount2   | value   |
      | <voucherType> | <amount> | <payWith> | <posteid> | BPINPROAND-1817 | <amount2> | <value> |
    And viene mostrato il messaggio di presa in carico
      | messageText   | testIdCheck |
      | <messageText> | PBPAPP-2812 |
    #Then Viene controllata la ricezione della notifica
      #| messageText   | testIdCheck |
      #| <messageText> | PBPAPP-2812 |
    And Viene chiusa l'app

    @BPINPROAND-1817_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | voucherType | amount | amount2 | payWith            | sender             | messageText | quickOperationName | value | posteID | testIdCheck | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | BFP 3X2     |  50.00 |  100.00 | conto;001044548509 | vincenzo fortunato | messageText | quickOperationName | value | prisma  | BPINPROAND-1817  | nonseitu   |

 @BPINPROAND-1838
  Scenario Outline: Emissione BFP 4X4 da conto
    Viene effetuata l'emissione BFP 4X4 da conto

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck     | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | BPINPROAND-1838 | <onboarding> |
    And Dalla pagina "buoni e libretti" tab "buoni postali" vengono compilati i seguenti campi e viene effettuato operazione di "conferma"
      | voucherType   | amount   | payWith   | posteid   | testIdCheck     |
      | <voucherType> | <amount> | <payWith> | <posteid> | BPINPROAND-1838 |
    And Viene controllata la ricezione della notifica
      | messageText   | testIdCheck |
      | <messageText> | PBPAPP-463  |
    And Viene controllata la corretta categorizzazione della transazione
      | categoryTransation   | amount   | testIdCheck |
      | <categoryTransation> | <amount> | -           |
    And Viene chiusa l'app

    @BPINPROAND-1838_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | voucherType | amount | payWith            | sender           | messageText | quickOperationName | editValue | posteID | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | BFP 3X2     |  50.00 | conto;001044548509 | Salvatore Acalli | messageText | quickOperationName | editValue | prisma  | nonseitu   |

 @BPINPROAND-1198
  Scenario Outline: Sottoscrizione buono 3X4 da libretto - PIC Notturna
    Viene effetuata la sottoscrizione buono 3X4 da libretto - PIC Notturna

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck     | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | BPINPROAND-1198 | <onboarding> |
    And Dalla pagina "buoni e libretti" tab "buoni postali" vengono compilati i seguenti campi e viene effettuato operazione di "conferma"
      | voucherType   | amount   | payWith   | posteid   | testIdCheck     |
      | <voucherType> | <amount> | <payWith> | <posteid> | BPINPROAND-1198 |
    And viene mostrato il messaggio di presa in carico
      | messageText   | testIdCheck |
      | <messageText> | PBPAPP-3067 |
    #Then Viene controllata la ricezione della notifica
      #| <messageText> | PBPAPP-463 |
    #Then Viene controllata la ricezione della notifica
      #| messageText   | testIdCheck |
      #| <messageText> | PBPAPP-3067 |
    And Viene chiusa l'app

    @BPINPROAND-1198_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | voucherType | amount | payWith            | sender           | messageText | quickOperationName | editValue | posteID | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | BFP 3X2     |  50.00 | conto;001044548509 | Salvatore Acalli | messageText | quickOperationName | editValue | prisma  | nonseitu   |
