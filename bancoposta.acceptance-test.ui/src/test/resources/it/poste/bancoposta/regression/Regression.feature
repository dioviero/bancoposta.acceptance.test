@Regression
Feature: Regression Test

  @DONE
  Scenario Outline: Login con credenziali poste.it
    L'utente deve avere un conto bp onboardato

    Given L'app "android" viene avviata e viene eseguita la login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | PBPAPP-2832 |
    Then Viene chiusa l'app

    Examples: 
      | username           | password     | loginType | userHeader | device               | posteID |
      | luca.petrucci-abmo | NLWbnsx6#st0 | poste.it  | Luca       | samsungGalaxyS8_plus | prisma  |

  @DOOOOOOONE
  Scenario Outline: Gestione notifiche
    L'utente deve avere un conto bp e la carta onboardati

    Given L'app "android" viene avviata e viene eseguita la login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | PBPAPP-2832 |
    And Dal tab menu impostazioni "notifiche" abilitare tutte le notifiche relative alle seguenti operazioni
      | operationType   | value   | amount   | testIdCheck |
      | <operationType> | <value> | <amount> | PBPAPP-492  |
    And Vengono effettuate le verifiche dei numeri utili dalla pagina di Login
      | testIdCheck |
      | PBPAPP-255  |
    And Viene chiusa l'app

    Examples: 
      | username           | password     | loginType | userHeader | device               | operationType        | value | amount | posteID |
      | luca.petrucci-abmo | NLWbnsx6#st0 | poste.it  | Luca       | samsungGalaxyS8_plus | Movimenti in entrata | si    |    100 | prisma  |

  #        | luca.petrucci-abmo | NLWbnsx6#st0 | poste.it  | Luca    | iphone6 | Movimenti in entrata | si    |    100 |
  #        | luca.petrucci-abmo | NLWbnsx6#st0 | poste.it  | Luca    | iphone6 | Movimenti in uscita  | si    |    100 |
  #        | luca.petrucci-abmo | NLWbnsx6#st0 | poste.it  | Luca    | huaweiP20lite | Resoconto mensile    | si    |    100 |
  #        | luca.petrucci-abmo | NLWbnsx6#st0 | poste.it  | Luca    | huaweiP20lite | Soglie per categoria | si    |    100 |
  #        | luca.petrucci-abmo | NLWbnsx6#st0 | poste.it  | Luca    | huaweiP20lite | Movimenti in entrata | si    |     50 |
  #        | luca.petrucci-abmo | NLWbnsx6#st0 | poste.it  | Luca    | huaweiP20lite | Movimenti in uscita  | si    |     50 |
  #        | luca.petrucci-abmo | NLWbnsx6#st0 | poste.it  | Luca    | huaweiP20lite | Resoconto mensile    | si    |     50 |
  #        | luca.petrucci-abmo | NLWbnsx6#st0 | poste.it  | Luca    | huaweiP20lite | Soglie per categoria | si    |     50 |
  #        | luca.petrucci-abmo | NLWbnsx6#st0 | poste.it  | Luca    | huaweiP20lite | Movimenti in entrata | no    |    100 |
  #        | luca.petrucci-abmo | NLWbnsx6#st0 | poste.it  | Luca    | huaweiP20lite | Movimenti in uscita  | no    |    100 |
  #        | luca.petrucci-abmo | NLWbnsx6#st0 | poste.it  | Luca    | huaweiP20lite | Resoconto mensile    | no    |    100 |
  #        | luca.petrucci-abmo | NLWbnsx6#st0 | poste.it  | Luca    | huaweiP20lite | Soglie per categoria | no    |    100 |
  #        | luca.petrucci-abmo | NLWbnsx6#st0 | poste.it  | Luca        | samsungGalaxyS8_plus | Movimenti in uscita  | si    |    100 | prisma |
  #        | luca.petrucci-abmo | NLWbnsx6#st0 | poste.it  | Luca        | samsungGalaxyS8_plus | Resoconto mensile    | si    |    100 | prisma |
  #        | luca.petrucci-abmo | NLWbnsx6#st0 | poste.it  | Luca        | samsungGalaxyS8_plus | Soglie per categoria | si    |    100 | prisma |
  #        | luca.petrucci-abmo | NLWbnsx6#st0 | poste.it  | Luca        | samsungGalaxyS8_plus | Movimenti in entrata | si    |     50 | prisma |
  #        | luca.petrucci-abmo | NLWbnsx6#st0 | poste.it  | Luca        | samsungGalaxyS8_plus | Movimenti in uscita  | si    |     50 | prisma |
  #        | luca.petrucci-abmo | NLWbnsx6#st0 | poste.it  | Luca        | samsungGalaxyS8_plus | Resoconto mensile    | si    |     50 | prisma |
  #        | luca.petrucci-abmo | NLWbnsx6#st0 | poste.it  | Luca        | samsungGalaxyS8_plus | Soglie per categoria | si    |     50 | prisma |
  #        | luca.petrucci-abmo | NLWbnsx6#st0 | poste.it  | Luca        | samsungGalaxyS8_plus | Movimenti in entrata | no    |    100 | prisma |
  #        | luca.petrucci-abmo | NLWbnsx6#st0 | poste.it  | Luca        | samsungGalaxyS8_plus | Movimenti in uscita  | no    |    100 | prisma |
  #        | luca.petrucci-abmo | NLWbnsx6#st0 | poste.it  | Luca        | samsungGalaxyS8_plus | Resoconto mensile    | no    |    100 | prisma |
  #        | luca.petrucci-abmo | NLWbnsx6#st0 | poste.it  | Luca        | samsungGalaxyS8_plus | Soglie per categoria | no    |    100 | prisma |
 
  @DONE
  Scenario Outline: Bollettino manuale da conto
      L'utente deve avere un conto bp e la carta onboardati

    Given L'app "android" viene avviata e viene eseguita la login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | PBPAPP-2832 |
    And scegli bollettino dalla pagina "conti e carte" vengono compilati i seguenti campi e viene effettuata l'operazione di "esecuzione"
      | bollettinoType   | payWith   | bollettinoCode   | cc   | amount   | owner   | description   | expireDate   | senderName   | senderLastName   | senderAdress   | senderCity   | senderCAP   | senderProv   | testIdCheck | posteid   |
      | <bollettinoType> | <payWith> | <bollettinoCode> | <cc> | <amount> | <owner> | <description> | <expireDate> | <senderName> | <senderLastName> | <senderAdress> | <senderCity> | <senderCAP> | <senderProv> | PBPAPP-2129 | <posteID> |
    And Salvare l'operazione come "operazione veloce"
      | quickOperationName   |
      | <quickOperationName> |
    And Viene controllata la ricezione della notifica
      | messageText   | testIdCheck |
      | <messageText> | PBPAPP-463  |
    And Viene chiusa l'app

    Examples: 
      | username           | password     | loginType | userHeader | device               | bollettinoType    | payWith            | bollettinoCode     | cc       | amount | owner              | description | expireDate | senderName | senderLastName | senderAdress                | senderCity | senderCAP | senderProv | posteID |
      | luca.petrucci-abmo | NLWbnsx6#st0 | poste.it  | Luca       | samsungGalaxyS8_plus | Bollettino Bianco | conto;001046919773 | 099992009000150736 | 40512204 | 102.73 | Croce Rosa Celeste | description | expireDate | Gianni     | Test           | Via degli Ulivi Gialli n.23 | Agrigento  |     92100 | Agrigento  | prisma  |

  @DONE
  Scenario Outline: Bollettino Operazione veloce
      L'utente deve avere un conto bp e la carta onboardati

    Given L'app "android" viene avviata e viene eseguita la login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | PBPAPP-2832 |
    When scegli bollettino dalla pagina "operazioni veloci" vengono compilati i seguenti campi e viene effettuata l'operazione di "esecuzione"
      | bollettinoType   | payWith   | bollettinoCode   | cc   | amount   | owner   | description   | expireDate   | senderName   | senderLastName   | senderAdress   | senderCity   | senderCAP   | senderProv   | testIdCheck | 
      | <bollettinoType> | <payWith> | <bollettinoCode> | <cc> | <amount> | <owner> | <description> | <expireDate> | <senderName> | <senderLastName> | <senderAdress> | <senderCity> | <senderCAP> | <senderProv> | PBPAPP-762  |
    Then Viene controllata la ricezione della notifica
      | messageText   | testIdCheck |
      | <messageText> | PBPAPP-463  |
    And Viene chiusa l'app

    Examples: 
      | username           | password     | loginType | userHeader | device               | bollettinoType    | payWith            | bollettinoCode     | cc       | amount | owner              | description | expireDate | senderName | senderLastName | senderAdress                | senderCity | senderCAP | senderProv | posteID |quickOperationName |
      | luca.petrucci-abmo | NLWbnsx6#st0 | poste.it  | Luca       | samsungGalaxyS8_plus | Bollettino Bianco | conto;001046919773 | 099992009000150736 | 40512204 | 102.73 | Croce Rosa Celeste | description | expireDate | Gianni     | Test           | Via degli Ulivi Gialli n.23 | Agrigento  |     92100 | Agrigento  | prisma  | bollettino bianco |

  #    | luca.petrucci-abmo      | NLWbnsx6#st0    | poste.it  | Luca            | iphone6              | Bollettino Precompilato 896 | conto;001046214688 | 099992009000150736 | 000030168405 | 102.73 | owner | description | expireDate | Gianni     | Test           | Via degli Ulivi Gialli n.23 | Agrigento  | 92100     | Agrigento  | prisma  |
  #    | luca.petrucci-abmo      | NLWbnsx6#st0    | poste.it  | Luca            | samsungGalaxyS8_plus | Bollettino Precompilato 896 | conto;001044920229 | 099992009000150736 | 000030168405 | 102.73 | owner | description | expireDate | Gianni     | Test           | Via degli Ulivi Gialli n.23 | Agrigento  | 92100     | Agrigento  | prisma  |
  
  @DONE
  Scenario Outline: Gestione buoni: creazione buoni, check ricezione notifica e salvataggio in operazioni veloci
    L'utente deve avere un conto bp e la carta onboardati

    Given L'app "android" viene avviata e viene eseguita la login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | PBPAPP-2832 |
    And Dalla pagina "buoni e libretti" tab "buoni postali" vengono compilati i seguenti campi e viene effettuato operazione di "conferma"
      | voucherType   | amount   | payWith   | posteid   | testIdCheck                                     |
      | <voucherType> | <amount> | <payWith> | <posteid> | PBPAPP-2421;PBPAPP-2422;PBPAPP-2423;PBPAPP-2423 |
    Then Viene controllata la ricezione della notifica
      | messageText   | testIdCheck |
      | <messageText> | PBPAPP-463  |
    And Viene chiusa l'app
    
    Examples: 
      | username           | password     | loginType | userHeader | device   | voucherType | amount | payWith            | sender      | messageText | quickOperationName | editValue | posteID |
      #      | luca.petrucci-abmo | NLWbnsx6#st0 | poste.it  | Luca       | samsungGalaxyS8_plus | BUONO FRUTTIFERO POSTALE BFP3X4 |  50.00 | conto;001009052257 | Luca Acalli | messageText | quickOperationName | editValue | prisma  |
      #      | luca.petrucci-abmo | NLWbnsx6#st0 | poste.it  | Luca       | samsungGalaxyS8_plus | BUONO ORDINARIO                 |  50.00 | conto;001009052257 | Luca Acalli | messageText | quickOperationName | editValue | prisma  |
      #      | luca.petrucci-abmo | NLWbnsx6#st0 | poste.it  | Luca       | samsungGalaxyS8_plus | BUONO INDICIZZATO A INFLAZIONE  |  50.00 | conto;001009052257 | Luca Acalli | messageText | quickOperationName | editValue | prisma  |
      | luca.petrucci-abmo | NLWbnsx6#st0 | poste.it  | Luca       | samsungGalaxyS8_plus| BFP 3X2     |  50.00 | conto;001046919773 | Luca Acalli | messageText | quickOperationName | editValue | prisma  |

  @DONE
  Scenario Outline: Gestione postagiro: creazione postagiro, check ricezione notifica e salvataggio in operazioni veloci
    L'utente deve avere un conto bp e la carta onboardati

    Given L'app "android" viene avviata e viene eseguita la login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | PBPAPP-2832 |
    And Viene effettuato un postagiro da "conto bancoposta"
      | iban   | owner   | amount   | description   | posteid   | payWith   | beneficiario   | commissioni   | reference   | testIdCheck |
      | <iban> | <owner> | <amount> | <description> | <posteID> | <payWith> | <beneficiario> | <commissioni> | <reference> | PBPAPP-459  |
    And Salvare l'operazione come "operazione veloce"
      | quickOperationName   |
      | <quickOperationName> |
    And Viene controllato che la transazione sia stata registrata nella lista movimenti
      | conto     | amount   | operation | transactionType   | transactionDescription   | commissioni   |
      | <payWith> | <amount> | -         | <transactionType> | <transactionDescription> | <commissioni> |
    And Controlla che il saldo sia stato decrementato da "conto bancoposta"
      | conto     | amount   | operation | transactionType   | transactionDescription   | commissioni   |
      | <payWith> | <amount> | -         | <transactionType> | <transactionDescription> | <commissioni> |
    Then Viene controllata la ricezione della notifica
      | messageText   | testIdCheck | notifyType        |
      | <messageText> | PBPAPP-463  | <transactionType> |
    And Viene chiusa l'app

    Examples: 
      | username           | password     | loginType | userHeader | device   | payWith            | iban                        | city   | owner              | amount | description     | address                     | citySender | reference     | refecenceEffective | quickOperationName | transactionType | transactionDescription | messageText                               | categoryTransation | editValue | posteID | commissioni | bonificoType | beneficiario       |
      | luca.petrucci-abmo | NLWbnsx6#st0 | poste.it  | Luca       | samsungGalaxyS8_plus | conto;001046919773 | IT15L0760103400001044920229 | Italia | Vincenzo Fortunato |   0.01 | test automatico | Via degli Ulivi Gialli n.23 | Agrigento  | PETRUCCI LUCA | Luca Petrucci      | test postagiro     | POSTAGIRO       | TRN                    | Bancoposta: Conferma Postagiro effettuato | Varie              |       0.2 | prisma  |        0.50 | postagiro    | Vincenzo Fortunato |

  @DONE
  Scenario Outline: Postagiro Operazione veloce
      L'utente deve avere un conto bp e la carta onboardati

    Given L'app "android" viene avviata e viene eseguita la login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | PBPAPP-2832 |
    And Viene effettuata la transazione da operazione veloce
      | quickOperationName   | transactionType   | testIdCheck | payWith   | iban   | city   | owner    | amount   | description   |
      | <quickOperationName> | <transactionType> | PBPAPP-2121 | <payWith> | <iban> | <city> | <owner > | <amount> | <description> |
    And Viene effettuato un postagiro da operazione veloce
      | payWith   | iban   | cc   | city   | owner   | amount   | description   | address   | citySender   | reference   | refecenceEffective   | posteid   | commissioni   | testIdCheck             | bonificoType   | beneficiario   |
      | <payWith> | <iban> | <cc> | <city> | <owner> | <amount> | <description> | <address> | <citySender> | <reference> | <refecenceEffective> | <posteID> | <commissioni> | PBPAPP-2105;PBPAPP-2108 | <bonificoType> | <beneficiario> |
    Then Viene controllata la ricezione della notifica
      | messageText   | testIdCheck |
      | <messageText> | PBPAPP-463  |
    And Viene chiusa l'app

    Examples: 
      | username           | password     | loginType | userHeader | device   | payWith            | iban                        | cc                          | city   | owner              | amount | description     | address                     | citySender | reference | refecenceEffective | quickOperationName | transactionType | transactionDescription | messageText                                   | categoryTransation | editValue | posteID | commissioni | bonificoType | beneficiario  |
      | luca.petrucci-abmo | NLWbnsx6#st0 | poste.it  | Luca       | samsungGalaxyS8_plus | conto;001046919773 | IT15L0760103400001044920229 | IT15L0760103400001044920229 | Italia | Vincenzo Fortunato |   0.01 | test automatico | Via degli Ulivi Gialli n.23 | Agrigento  | Luca      | Luca Petrucci      | postagiro          | POSTAGIRO       | TRN                    | BancoPosta: Conferma Bonifico SEPA effettuato | Varie              |       0.2 | prisma  |         1.0 | postagiro    | PETRUCCI LUCA |

  @DONE
  Scenario Outline: Gestione girofondi: creazione girofondo, check ricezione notifica e salvataggio in operazioni veloci
    L'utente deve avere un conto bp e la carta onboardati

    Given L'app "android" viene avviata e viene eseguita la login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | PBPAPP-2832 |
    And Viene effettuato un "girofondo"
      | payWith   | transferTo   | owner   | amount   | posteid   | favoreDi   | commissioni   | fromContoDetail   | toContoDetail   | testIdCheck |
      | <payWith> | <transferTo> | <owner> | <amount> | <posteID> | <favoreDi> | <commissioni> | <fromContoDetail> | <toContoDetail> | PBPAPP-2276 |
    And Salvare l'operazione come "operazione veloce"
      | quickOperationName   |
      | <quickOperationName> |
    And Viene controllato che la transazione sia stata registrata nella lista movimenti
      | conto     | amount   | operation | transactionType   | transactionDescription   |
      | <payWith> | <amount> | -         | <transactionType> | <transactionDescription> |
    And Viene controllata la corretta categorizzazione della transazione
      | categoryTransation   | amount   | subCategoryTransaction | transactionType   | operation |
      | <categoryTransation> | <amount> | Prodotti di risparmio  | <transactionType> | -         |
    Then Viene controllata la ricezione della notifica
      | messageText   | testIdCheck | notifyType        |
      | <messageText> | PBPAPP-463  | <transactionType> |
    And Viene chiusa l'app

    Examples: 
      | username           | password     | loginType | userHeader | device   | payWith            | transferTo                  | owner         | amount | favoreDi      | commissioni | fromContoDetail  | toContoDetail    | messageText                                     | quickOperationName | editValue | categoryTransation | month | categoryDestination | posteID | transactionType | transactionDescription |
      | luca.petrucci-abmo | NLWbnsx6#st0 | poste.it  | Luca       | samsungGalaxyS8_plus | conto;001046919773 | libretto smart;000050028091 | Luca PETRUCCI |   0.01 | PETRUCCI LUCA |        0.50 | Conto BancoPosta | Libretto Postale | BancoPosta: Conferma Trasferimento Fondi da App | test girofondi     |      0.20 | banca e finanza    | month | categoryDestination | prisma  | GIROFONDO       | DR 000050028091        |

  #			| angelo.casoria-xell | Ang_cas83 | poste.it  | Angelo     | samsungGalaxyS8_plus | conto;000084627371 | libretto smart;000049492562 | ANGELO CASORIA |   0.10 | CASORIA ANGELO |        0,00 | Conto BancoPosta | Libretto Postale | BancoPosta: Conferma Trasferimento Fondi da App | test girofondi     |      0.20 | banca e finanza    | month | categoryDestination |  555555 | GIROFONDO       | DR 000049492562     |
  #     | luca.petrucci-abmo | NLWbnsx6#st0 | poste.it  | Luca        | samsungGalaxyS8_plus | conto;001009052257 | libretto smart 000034029414 | owner | 0.1 | BancoPosta: Conferma Trasferimento Fondi da App | test girofondi | editValue | categoryTransation | month | categoryDestination | 555555   |
  
  @DONE
  Scenario Outline: Girofondo da operazione veloce
    Given L'app "android" viene avviata e viene eseguita la login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | PBPAPP-2832 |
    And Viene effettuata la transazione da operazione veloce
      | quickOperationName   | transactionType   | testIdCheck | payWith   | iban   | city   | owner    | amount   | description   |
      | <quickOperationName> | <transactionType> | PBPAPP-2121 | <payWith> | <iban> | <city> | <owner > | <amount> | <description> |
    And Viene effettuato un girofondo da operazione veloce
      | payWith   | transferTo    |  amount   | posteid   | testIdCheck             | 
      | <payWith> | <transferTo > | <amount>  | <posteID> | PBPAPP-2105;PBPAPP-2108 |
    Then Viene controllata la ricezione della notifica
      | messageText   | testIdCheck | notifyType        |
      | <messageText> | PBPAPP-463  | <transactionType> |
    And Viene chiusa l'app

    Examples: 
      | username           | password     | loginType | userHeader | device   | payWith            | transferTo                  | amount | favoreDi      | commissioni | fromContoDetail  | toContoDetail    | messageText                                     | quickOperationName | editValue | categoryTransation | month | categoryDestination | posteID | transactionType | transactionDescription |
      | luca.petrucci-abmo | NLWbnsx6#st0 | poste.it  | Luca       | samsungGalaxyS8_plus | conto;001046919773 | libretto smart;000050028091 |  0.01 | PETRUCCI LUCA |        0.0 | Conto BancoPosta | Libretto Postale | BancoPosta: Conferma Trasferimento Fondi da App | girofondo          |      0.20 | banca e finanza    | month | categoryDestination | prisma  | GIROFONDO       | DR 000050028091        |

  #			| angelo.casoria-xell | Ang_cas83 | poste.it  | Angelo     | samsungGalaxyS8_plus | conto;000084627371 | libretto smart;000049492562 | ANGELO CASORIA |   0.10 | CASORIA ANGELO |        0,00 | Conto BancoPosta | Libretto Postale | BancoPosta: Conferma Trasferimento Fondi da App | test girofondi     |      0.20 | banca e finanza    | month | categoryDestination |  555555 | GIROFONDO       | DR 000049492562     |
  #     | luca.petrucci-abmo | NLWbnsx6#st0 | poste.it  | Luca        | samsungGalaxyS8_plus | conto;001009052257 | libretto smart 000034029414 | owner | 0.1 | BancoPosta: Conferma Trasferimento Fondi da App | test girofondi | editValue | categoryTransation | month | categoryDestination | 555555   |
  
  @DONE
  Scenario Outline: Gestione bonifico: creazione bonifico, check ricezione notifica e salvataggio in operazioni veloci
    L'utente deve avere un conto bp e la carta onboardati

    Given L'app "android" viene avviata e viene eseguita la login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | PBPAPP-2102 |
    And Viene effettuato un bonifico
      | payWith   | iban   | city   | owner   | amount   | description   | address   | citySender   | reference   | refecenceEffective   | posteId   | commissioni   | testIdCheck             | bonificoType   | beneficiario   |
      | <payWith> | <iban> | <city> | <owner> | <amount> | <description> | <address> | <citySender> | <reference> | <refecenceEffective> | <posteID> | <commissioni> | PBPAPP-2105;PBPAPP-2108 | <bonificoType> | <beneficiario> |
    And Salvare l'operazione come "operazione veloce"
      | quickOperationName   |
      | <quickOperationName> |
    And Viene controllato che la transazione sia stata registrata nella lista movimenti
      | conto     | amount   | operation | transactionType   | transactionDescription   | commissioni   |
      | <payWith> | <amount> | -         | <transactionType> | <transactionDescription> | <commissioni> |
    And Viene controllata la corretta categorizzazione della transazione
      | categoryTransation   | amount   | subCategoryTransaction | transactionType   | operation |
      | <categoryTransation> | <amount> | Da Categorizzare       | <transactionType> | -         |
    Then Viene controllata la ricezione della notifica
      | messageText   | testIdCheck | notifyType        |
      | <messageText> | PBPAPP-463  | <transactionType> |
    And Viene chiusa l'app

    Examples: 
      | username           | password     | loginType | userHeader | device               | payWith            | iban                        | city   | owner            | amount | description     | address                     | citySender | reference | refecenceEffective | quickOperationName | transactionType | transactionDescription | messageText                                   | categoryTransation | editValue | posteID | commissioni | bonificoType | beneficiario  |
      | luca.petrucci-abmo | NLWbnsx6#st0 | poste.it  | Luca       | samsungGalaxyS8_plus | conto;001046919773 | IT21S0310401625000000206536 | Italia | ERRIQUEZ RODOLFO |   0.01 | test automatico | Via degli Ulivi Gialli n.23 | Agrigento  | Luca      | Luca Petrucci      | test bonifico      | BONIFICO        | TRN                    | BancoPosta: Conferma Bonifico SEPA effettuato | Varie              |       0.2 | prisma  |         1.0 | bonifico     | PETRUCCI LUCA |

  #      | luca.petrucci-abmo | NLWbnsx6#st0 | poste.it  | Luca       | samsungGalaxyS8_plus2 | conto;001046214688 | IT67M3608105138276813676823 | Italia | PETRUCCI LUCA      |   0.01 | test automatico | Via degli Ulivi Gialli n.23 | Agrigento  | Luca      | Luca Ioviero       | test postagiro     | POSTAGIRO       | TRN                    | Bancoposta: Conferma Postagiro effettuato     | Varie              |       0.2 | prisma  |        0.50 | postagiro    | PETRUCCI LUCA    |
  #      | luca.petrucci-abmo | NLWbnsx6#st0 | poste.it  | Luca       | samsungGalaxyS8_plus  | conto;001046214688 | IT15L0760103400001044920229 | Italia | Vincenzo Fortunato |   0.01 | test automatico | Via degli Ulivi Gialli n.23 | Agrigento  | Luca      | Luca Ioviero       | test bonifici      | BONIFICO        | TRN                    | BancoPosta: Conferma                          | Varie              | 0.2       | prisma  | 1.0         | bonifico     | PETRUCCI LUCA    |
  #      | luca.petrucci-abmo | NLWbnsx6#st0 | poste.it  | Luca       | samsungGalaxyS8_plus  | conto;001046214688 | IT21S0310401625000000206536 | Italia | Rodolfo Erriquez   | 0.01   | test automatico | Via degli Ulivi Gialli n.23 | Agrigento  | Luca      | Luca Ioviero       | test bonifici      | transactionType | transactionDescription | messageText                                   | categoryTransation | editValue | 555555  | 1.0         |
  
  @DONE
  Scenario Outline: Bonifico Operazione veloce
      L'utente deve avere un conto bp e la carta onboardati

    Given L'app "android" viene avviata e viene eseguita la login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | PBPAPP-2832 |
    And Viene effettuata la transazione da operazione veloce
      | quickOperationName   | transactionType   | testIdCheck | payWith   | iban   | city   | owner    | amount   | description   |
      | <quickOperationName> | <transactionType> | PBPAPP-2121 | <payWith> | <iban> | <city> | <owner > | <amount> | <description> |
    And Viene effettuato un bonifico da operazione veloce
      | payWith   | iban   | city   | owner   | amount   | description   | address   | citySender   | reference   | refecenceEffective   | posteid   | commissioni   | testIdCheck             | bonificoType   | beneficiario   |
      | <payWith> | <iban> | <city> | <owner> | <amount> | <description> | <address> | <citySender> | <reference> | <refecenceEffective> | <posteID> | <commissioni> | PBPAPP-2105;PBPAPP-2108 | <bonificoType> | <beneficiario> |
    Then Viene controllata la ricezione della notifica
      | messageText   | testIdCheck |
      | <messageText> | PBPAPP-463  |
    And Viene chiusa l'app

    Examples: 
      | username           | password     | loginType | userHeader | device   | payWith            | iban                        | city   | owner            | amount | description     | address                     | citySender | reference | refecenceEffective | quickOperationName | transactionType | transactionDescription | messageText                                   | categoryTransation | editValue | posteID | commissioni | bonificoType | beneficiario  |
      | luca.petrucci-abmo | NLWbnsx6#st0 | poste.it  | Luca       | samsungGalaxyS8_plus | conto;001046919773 | IT21S0310401625000000206536 | Italia | ERRIQUEZ RODOLFO |   0.01 | test automatico | Via degli Ulivi Gialli n.23 | Agrigento  | Luca      | Luca Petrucci      | bonifico           | BONIFICO        | TRN                    | BancoPosta: Conferma Bonifico SEPA effettuato | Varie              |       0.2 | prisma  |         1.0 | bonifico     | PETRUCCI LUCA |

  #   | luca.petrucci-abmo      | NLWbnsx6#st0    | poste.it  | Luca            | iphone6              | Bollettino Precompilato 896 | conto;001046214688 | 099992009000150736 | 000030168405 | 102.73 | owner | description | expireDate | Gianni     | Test           | Via degli Ulivi Gialli n.23 | Agrigento  | 92100     | Agrigento  | prisma  |
  #   | luca.petrucci-abmo      | NLWbnsx6#st0    | poste.it  | Luca            | samsungGalaxyS8_plus | Bollettino Precompilato 896 | conto;001044920229 | 099992009000150736 | 000030168405 | 102.73 | owner | description | expireDate | Gianni
  
  @DONE
  Scenario Outline: Gestione ricarica postepay: creazione ricarica postepay, check ricezione notifica e salvataggio in operazioni veloci
    L'utente deve avere un conto bp e la carta onboardati

    Given L'app "android" viene avviata e viene eseguita la login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | PBPAPP-2832 |
    And Viene effettuata una ricarica postepay
      | payWith   | transferTo   | owner   | amount   | testIdCheck            | description   | posteid   |
      | <payWith> | <transferTo> | <owner> | <amount> | PBPAPP-185;PBPAPP-2113 | <description> | <posteID> |

    #    And Salvare l'operazione come "operazione veloce"
    #      | quickOperationName   |
    #      | <quickOperationName> |
    #    And Viene controllata la ricezione della notifica
    #      | messageText   |
    #      | <messageText> |
    #    And Controlla che l'operazione veloce sia editabile
    #      | quickOperationName   | testIdCheck |
    #      | <quickOperationName> | PBPAPP-198  |
    #    And Modifica "metodo di pagamento" dell'operazione veloce
    #      | editValue   | testIdCheck |
    #      | <editValue> | PBPAPP-201  |
    #    And Viene effettuata la transazione da operazione veloce
    #      | quickOperationName   | testIdCheck |
    #      | <quickOperationName> | PBPAPP-2131 |
    #    And Viene controllata la ricezione della notifica
    #      | messageText   | testIdCheck |
    #      | <messageText> | PBPAPP-463  |
    #    And Cancella operazione veloce
    #      | quickOperationName   | testIdCheck |
    #      | <quickOperationName> | PBPAPP-201  |
    #    And Viene controllata la corretta categorizzazione della transazione
    #      | categoryTransation   | amount   | testIdCheck |
    #      | <categoryTransation> | <amount> | PBPAPP-455  |
    #    Then Viene controllata la ricezione della notifica
    #      | messageText   | testIdCheck |
    #      | <messageText> | PBPAPP-463  |
    #    And Viene chiusa l'app
    Examples: 
      | username           | password     | loginType | userHeader | device   | payWith            | transferTo       | owner         | amount | messageText     | quickOperationName | editValue | categoryTransation | month | categoryDestination | posteID | description |
      | luca.petrucci-abmo | NLWbnsx6#st0 | poste.it  | Luca       | samsungGalaxyS8_plus | conto;001046919773 | 4023600948770922 | LUCA PETRUCCI |   0.01 | test automatico | quickOperationName | editValue | categoryTransation | month | categoryDestination | prisma  | verifica i dati prima di procedere.        |

	@DONE
  Scenario Outline: Ricarica PostePay Operazione veloce
      L'utente deve avere un conto bp e la carta onboardati

    Given L'app "android" viene avviata e viene eseguita la login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | PBPAPP-2832 |
    And Viene effettuata la transazione da operazione veloce
      | quickOperationName   | transactionType   | testIdCheck | payWith   | transferTo   | owner    | amount   | description   |
      | <quickOperationName> | <transactionType> | PBPAPP-2121 | <payWith> | <transferTo> | <owner > | <amount> | <description> |
    And Viene effettuata una ricarica Postepay da operazione veloce
      | payWith   | transferTo   | owner   | amount   | description   | posteid   | testIdCheck             |
      | <payWith> | <transferTo> | <owner> | <amount> | <description> | <posteID> | PBPAPP-2105;PBPAPP-2108 |
    Then Viene controllata la ricezione della notifica
      | messageText   | testIdCheck |
      | <messageText> | PBPAPP-463  |
    And Viene chiusa l'app

    Examples: 
      | username           | password     | loginType | userHeader | device   | payWith            | transferTo       | owner         | amount | description     | reference | refecenceEffective | quickOperationName | transactionType   | transactionDescription | messageText                                   | categoryTransation | editValue | posteID | commissioni | bonificoType | beneficiario  |
      | luca.petrucci-abmo | NLWbnsx6#st0 | poste.it  | Luca       | samsungGalaxyS8_plus | conto;001046919773 | 4023600948770922 | LUCA PETRUCCI |   0.01 | test automatico | Luca      | Luca Petrucci      | ricarica postepay  | Ricarica Postepay | TRN                    | BancoPosta: Conferma Bonifico SEPA effettuato | Varie              |       0.2 | prisma  |         1.0 | bonifico     | PETRUCCI LUCA |

  #   | luca.petrucci-abmo      | NLWbnsx6#st0    | poste.it  | Luca            | iphone6              | Bollettino Precompilato 896 | conto;001046214688 | 099992009000150736 | 000030168405 | 102.73 | owner | description | expireDate | Gianni     | Test           | Via degli Ulivi Gialli n.23 | Agrigento  | 92100     | Agrigento  | prisma  |
  #   | luca.petrucci-abmo      | NLWbnsx6#st0    | poste.it  | Luca            | samsungGalaxyS8_plus | Bollettino Precompilato 896 | conto;001044920229 | 099992009000150736 | 000030168405 | 102.73 | owner | description | expireDate | Gianni

  @DONE
  Scenario Outline: Gestione ricarica telefonica: creazione ricarica telefonica, check ricezione notifica e salvataggio in operazioni veloci
    L'utente deve avere un conto bp e la carta onboardati

    Given L'app "android" viene avviata e viene eseguita la login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | PBPAPP-2832 |
    And Viene effettuata una "ricarica telefonica" da "conti e carte" section
      | payWith   | amount   | testIdCheck            | posteid   | phoneNumber   | phoneCompaign   |
      | <payWith> | <amount> | PBPAPP-185;PBPAPP-2113 | <posteID> | <phoneNumber> | <phoneCompaign> |
    #    And Salvare l'operazione come "operazione veloce"
    #      | quickOperationName   |
    #      | <quickOperationName> |
    #    And Viene controllata la ricezione della notifica
    #      | messageText   |
    #      | <messageText> |
    And Controlla che l'operazione veloce sia editabile
      | quickOperationName   | testIdCheck |
      | <quickOperationName> | PBPAPP-198  |

    #    And Modifica "metodo di pagamento" dell'operazione veloce
    #      | editValue   | testIdCheck |
    #      | <editValue> | PBPAPP-201  |
    #    And Viene effettuata la transazione da operazione veloce
    #      | quickOperationName   | testIdCheck |
    #      | <quickOperationName> | PBPAPP-2131 |
    #    And Viene controllata la ricezione della notifica
    #      | messageText   | testIdCheck |
    #      | <messageText> | PBPAPP-463  |
    #    And Cancella operazione veloce
    #      | quickOperationName   | testIdCheck |
    #      | <quickOperationName> | PBPAPP-201  |
    #    And Viene controllata la corretta categorizzazione della transazione
    #      | categoryTransation   | amount   | testIdCheck |
    #      | <categoryTransation> | <amount> | PBPAPP-455  |
    #    Then Viene controllata la ricezione della notifica
    #      | messageText   | testIdCheck |
    #      | <messageText> | PBPAPP-463  |
    #    And Viene chiusa l'app
    Examples: 
      | username           | password     | loginType | userHeader | device   | payWith            | transferTo       | owner         | amount | messageText     | quickOperationName | editValue | categoryTransation | month | categoryDestination | posteID | description | phoneNumber | phoneCompaign |
      | luca.petrucci-abmo | NLWbnsx6#st0 | poste.it  | Luca       | samsungGalaxyS8_plus | conto;001046919773 | 4023600948770922 | PETRUCCI LUCA |  15,00 | test automatico | quickOperationName | editValue | categoryTransation | month | categoryDestination | prisma  | Test        |  3804678064 | WIND          |

  @DONE
  Scenario Outline: Ricarica telefonica Operazione veloce
      L'utente deve avere un conto bp e la carta onboardati

    Given L'app "android" viene avviata e viene eseguita la login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | PBPAPP-2832 |
    And Viene effettuata la transazione da operazione veloce
      | quickOperationName   | transactionType   | testIdCheck | payWith   | amount   | posteid   |
      | <quickOperationName> | <transactionType> | PBPAPP-2121 | <payWith> | <amount> | <posteID> |
    And Viene effettuata una ricarica telefonica da operazione veloce
      | payWith   | amount   | posteid   | testIdCheck             | phoneNumber   | phoneCompaign   |
      | <payWith> | <amount> | <posteID> | PBPAPP-2105;PBPAPP-2108 | <phoneNumber> | <phoneCompaign> |
    Then Viene controllata la ricezione della notifica
      | messageText   | testIdCheck |
      | <messageText> | PBPAPP-463  |
    And Viene chiusa l'app

    Examples: 
      | username           | password     | loginType | userHeader | device               | payWith            | amount | description     | reference | refecenceEffective | quickOperationName  | transactionType     | transactionDescription | messageText                                   | categoryTransation | editValue | posteID | commissioni | bonificoType | beneficiario  | phoneNumber | phoneCompaign |
      | luca.petrucci-abmo | NLWbnsx6#st0 | poste.it  | Luca       | samsungGalaxyS8_plus | conto;001046919773 |  15,00 | test automatico | Luca      | Luca Petrucci      | ricarica telefonica | Ricarica telefonica | TRN                    | BancoPosta: Conferma Bonifico SEPA effettuato | Varie              |       0.2 | prisma  |         1.0 | bonifico     | PETRUCCI LUCA |  3804678064 | WIND          |
