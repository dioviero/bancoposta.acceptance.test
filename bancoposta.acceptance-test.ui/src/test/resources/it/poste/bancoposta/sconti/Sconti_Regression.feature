@Sconti
Feature: Sconti
   L'utente naviga alla sezione ScontiPoste

  @BPINPROAND-1532
  Scenario Outline: Sconti Poste
    L'utente naviga alla sezione ScontiPoste

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | BPINPROAND-1532 | <onboarding> |
    And L'utente naviga alla sezione ScontiPoste
    Then Vengono visualizzati tutti i luoghi dove sono presenti attivita collegate
    And Viene chiusa l'app

    @BPINPROAND-1532_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | testIdCheck | posteID | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | PBPAPP-2096 | prisma  | nonseitu   |
