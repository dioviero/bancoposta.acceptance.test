@Impostazioni
Feature: Impostazioni
   L'utente accetta il pop up per abilitare la personalizzazione dell'app

  @BPINPROAND-1828
  Scenario Outline: Impostazioni - Personalizza la tua esperienza in app
    L'utente va nelle impostazioni dell'app per abilitare il toggle di "Personalizza La Tua Esperienza in app"

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | BPINPROAND-1828 | <onboarding> |
    And Vengono cliccate le impostazioni dal menu
    And Viene cliccato Accesso e autorizzazioni
    And l'utente clicca sul toggle abilitaDisabilita
    And Viene chiusa l'app

    @BPINPROAND-1828_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | payWith                   | iban                        | city   | owner        | amount | description     | address                     | citySender | reference | refecenceEffective | quickOperationName | transactionType | transactionDescription | messageText                                   | categoryTransation | editValue | posteID | commissioni | bonificoType | beneficiario  | paymentMethod | testIdCheck | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | postepay;5333171077147177 | IT15E3608105138271495971507 | Italia | REGA GENNARO |   0.01 | test automatico | Via degli Ulivi Gialli n.23 | Agrigento  | Gennaro   | Gennaro Rega       | test bonifico      | BONIFICO        | TRN                    | BancoPosta: Conferma Bonifico SEPA effettuato | Varie              |       0.2 | prisma  |         1.0 | bonifico     | PETRUCCI LUCA | carta         | BPINPROAND-1828 | nonseitu   |
