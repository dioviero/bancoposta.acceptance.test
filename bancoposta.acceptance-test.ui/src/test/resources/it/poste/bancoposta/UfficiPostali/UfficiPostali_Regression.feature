@UfficiPostali
Feature: Uffici Postali
  L utente cerca un ufficio postale intorno a lui

  @BPINPROAND-1570
  Scenario Outline: Cerca Ufficio Postale
      Viene cercato un Ufficio Postale nella relativa sezione

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck   | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | <testIdCheck> | <onboarding> |
    And Naviga verso la sezione cerca Ufficio Postale
    Then Controlla la presenza di un ufficio postale nelle vicinanze
    And Viene chiusa l'app

    @BPINPROAND-1570_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | posteID | testIdCheck | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | prisma  | BPINPROAND-1570 | nonseitu   |

  @PBPAPP-????
  Scenario Outline: Cerca Ufficio Postale test negativo
      Viene cercato un Ufficio Postale nella relativa sezione e viene restituito un risultato negativo

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck   | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | <testIdCheck> | <onboarding> |
    And Naviga verso la sezione cerca Ufficio Postale versione negativa
    Then Controlla la presenza di un ufficio postale nelle vicinanze versione negativa
    And Viene chiusa l'app

    @Test-PBPAPP-????
    Examples: 
      | username             | password  | loginType | userHeader | device               | posteID | testIdCheck | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | prisma  | BPINPROAND-1570 | nonseitu   |
