@Bollettino
Feature: Bollettino
  L'utente compila e sottomette un bollettino, da sottomenu paga o da operazione veloce

	@BPINPROAND-1564 @BPINPROIOS-1414
  Scenario Outline: Bollettino manuale da conto
    Viene effettuato un bollettino manuale da conto

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck     | onboarding   | owner   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | BPINPROAND-1564 | <onboarding> | <owner> |
    And scegli bollettino dalla pagina "conti e carte" vengono compilati i seguenti campi e viene effettuata l'operazione di "esecuzione"
      | bollettinoType   | payWith   | bollettinoCode   | cc   | amount   | owner   | description   | expireDate   | senderName   | senderLastName   | senderAdress   | senderCity   | senderCAP   | senderProv   | testIdCheck | posteid   | paymentMethod   | provinciaCode   |
      | <bollettinoType> | <payWith> | <bollettinoCode> | <cc> | <amount> | <owner> | <description> | <expireDate> | <senderName> | <senderLastName> | <senderAdress> | <senderCity> | <senderCAP> | <senderProv> | PBPAPP-2129 | <posteID> | <paymentMethod> | <provinciaCode> |
    And Salvare l'operazione come "operazione veloce"
      | quickOperationName   |
      | <quickOperationName> |
    And Viene controllato che la transazione sia stata registrata nella lista movimenti
      | conto     | amount   | operation | transactionType   | transactionDescription   | commissioni   |
      | <payWith> | <amount> | -         | <transactionType> | <transactionDescription> | <commissioni> |
    And Controlla che il saldo sia stato decrementato da "conto bancoposta"
      | conto     | amount   | operation | transactionType   | transactionDescription   | commissioni   |
      | <payWith> | <amount> | -         | <transactionType> | <transactionDescription> | <commissioni> |
    Then Viene controllata la ricezione della notifica
      | messageText   | notifyType        |
      | <messageText> | <transactionType> |
    And Viene chiusa l'app

    @BPINPROAND-1564_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | bollettinoType    | payWith            | bollettinoCode     | cc       | amount | owner              | description | expireDate | senderName | senderLastName | senderAdress   | senderCity            | senderCAP | senderProv | posteID | paymentMethod | testIdCheck | commissioni | transactionType | transactionDescription | messageText                               | onboarding | provinciaCode |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | Bollettino Bianco | conto;001044548509 | 099992009000150736 | 40512204 |    2.0 | Croce Rosa Celeste | bollettino  | expireDate | SALVATORE  | MUSELLA        | VIA ROMA 32 32 | GIUGLIANO IN CAMPANIA |     80014 | Napoli     | prisma  | conto         | BPINPROAND-1564 |        1.00 | BOLLETTINO      | Bollettino             | BancoPosta: Conferma pagamento Bollettino | nonseitu   | NA            |

    @BPINPROIOS-1414_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | bollettinoType    | payWith            | bollettinoCode     | cc       | amount | owner              | description | expireDate | senderName | senderLastName | senderAdress   | senderCity            | senderCAP | senderProv | posteID | paymentMethod | testIdCheck | commissioni | transactionType | transactionDescription | messageText                               | onboarding | provinciaCode |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | Bollettino Bianco | conto;001044548509 | 099992009000150736 | 40512204 |    2.0 | Croce Rosa Celeste | bollettino  | expireDate | SALVATORE  | MUSELLA        | VIA ROMA 32 32 | GIUGLIANO IN CAMPANIA |     80014 | Napoli     | prisma  | conto         | BPINPROIOS-1414 |        1.00 | BOLLETTINO      | Bollettino             | BancoPosta: Conferma pagamento Bollettino | nonseitu   | NA            |

    @BPINPROIOS-1414_fortunato
    Examples: 
      | username                | password        | loginType | userHeader | device               | bollettinoType    | payWith            | bollettinoCode     | cc       | amount | owner              | description | expireDate | senderName | senderLastName | senderAdress   | senderCity            | senderCAP | senderProv | posteID | paymentMethod | testIdCheck | commissioni | transactionType | transactionDescription | messageText                               | onboarding | provinciaCode |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | poste.it  | Vincenzo   | samsungGalaxyS8_plus | Bollettino Bianco | conto;001044920229 | 099992009000150736 | 40512204 |    2.0 | Croce Rosa Celeste | bollettino  | expireDate | VINCENZO  | FORTUNATO        | VIA ROMA 32 32 | GIUGLIANO IN CAMPANIA |     80014 | Napoli     | prisma  | conto         | BPINPROIOS-1414 |        1.00 | BOLLETTINO      | Bollettino             | BancoPosta: Conferma pagamento Bollettino | nonseitu   | NA            |

 	@BPINPROAND-1568
  Scenario Outline: Bollettino manuale da carta
    Viene effettuato un bollettino manuale da carta postepay

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck     | onboarding   | owner   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | BPINPROAND-1568 | <onboarding> | <owner> |
    And scegli bollettino dalla pagina "conti e carte" vengono compilati i seguenti campi e viene effettuata l'operazione di "esecuzione"
      | bollettinoType   | payWith   | bollettinoCode   | cc   | amount   | owner   | description   | expireDate   | senderName   | senderLastName   | senderAdress   | senderCity   | senderCAP   | senderProv   | testIdCheck | posteid   | paymentMethod   |
      | <bollettinoType> | <payWith> | <bollettinoCode> | <cc> | <amount> | <owner> | <description> | <expireDate> | <senderName> | <senderLastName> | <senderAdress> | <senderCity> | <senderCAP> | <senderProv> | PBPAPP-2129 | <posteID> | <paymentMethod> |
    And Salvare l'operazione come "operazione veloce"
      | quickOperationName   |
      | <quickOperationName> |
    And Viene controllato che la transazione sia stata registrata nella lista movimenti
      | conto     | amount   | operation | transactionType   | transactionDescription   | commissioni   |
      | <payWith> | <amount> | -         | <transactionType> | <transactionDescription> | <commissioni> |
    And Controlla che il saldo sia stato decrementato da "conto bancoposta"
      | conto     | amount   | operation | transactionType   | transactionDescription   | commissioni   |
      | <payWith> | <amount> | -         | <transactionType> | <transactionDescription> | <commissioni> |
    Then Viene controllata la ricezione della notifica
      | messageText   | notifyType        |
      | <messageText> | <transactionType> |
    And Viene chiusa l'app

    @BPINPROAND-1568_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | bollettinoType    | payWith                   | bollettinoCode     | cc       | amount | owner              | description | expireDate | senderName | senderLastName | senderAdress                | senderCity | senderCAP | senderProv | posteID | paymentMethod | testIdCheck | commissioni | transactionType   | transactionDescription | messageText                               | quickOperationName | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | Bollettino Bianco | postepay;5333171077147177 | 099992009000150736 | 40512204 |   0.01 | Croce Rosa Celeste | bollettino  | expireDate | Gianni     | Francucci      | Via degli Ulivi Gialli n.23 | Agrigento  |     92100 | Agrigento  | prisma  | carta         | BPINPROAND-1568 |        1.00 | BOLLETTINO ONLINE | Bollettino             | Bancoposta: Conferma pagamento Bollettino | bollettino2129     | nonseitu   |

  #000030168405
  @BPINPROAND-1782
  Scenario Outline: Bollettino Operazione Veloce
    Viene effettuato un bollettino da operazione veloce

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck     | onboarding   | owner   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | BPINPROAND-1782 | <onboarding> | <owner> |
    When scegli bollettino dalla pagina "operazioni veloci" vengono compilati i seguenti campi e viene effettuata l'operazione di "esecuzione"
      | bollettinoType   | payWith   | bollettinoCode   | cc   | amount   | owner   | description   | expireDate   | senderName   | senderLastName   | senderAdress   | senderCity   | senderCAP   | senderProv   | testIdCheck | posteid   | paymentMethod   |
      | <bollettinoType> | <payWith> | <bollettinoCode> | <cc> | <amount> | <owner> | <description> | <expireDate> | <senderName> | <senderLastName> | <senderAdress> | <senderCity> | <senderCAP> | <senderProv> | PBPAPP-762  | <posteID> | <paymentMethod> |
    And Viene controllato che la transazione sia stata registrata nella lista movimenti
      | conto     | amount   | operation | transactionType   | transactionDescription   | commissioni   |
      | <payWith> | <amount> | -         | <transactionType> | <transactionDescription> | <commissioni> |
    And Controlla che il saldo sia stato decrementato da "conto bancoposta"
      | conto     | amount   | operation | transactionType   | transactionDescription   | commissioni   |
      | <payWith> | <amount> | -         | <transactionType> | <transactionDescription> | <commissioni> |
    Then Viene controllata la ricezione della notifica
      | messageText   | notifyType        |
      | <messageText> | <transactionType> |
    And Viene chiusa l'app

    @BPINPROAND-1782_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | bollettinoType    | payWith            | bollettinoCode     | cc       | amount | owner              | description | expireDate | senderName | senderLastName | senderAdress                | senderCity | senderCAP | senderProv | posteID | messageText                               | conto | amount | operation | transactionType   | transactionDescription | commissioni | testIdCheck | messageText                               | paymentMethod | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | Bollettino Bianco | conto;001044548509 | 099992009000150736 | 40512204 |   2.01 | Croce Rosa Celeste | bollettino  | expireDate | Gianni     | Francucci      | Via degli Ulivi Gialli n.23 | Agrigento  |     92100 | Agrigento  | prisma  | BancoPosta: Conferma pagamento bollettino | conto |   2.01 | operation | BOLLETTINO ONLINE | Bollettino             |        1.00 | BPINPROAND-1782 | BancoPosta: Conferma pagamento Bollettino | conto         | nonseitu   |
