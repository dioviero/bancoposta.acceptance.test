@LoginLogout
Feature: LoginLogout
   L'utente effettua la nuova login all app

  @BPINPROAND-1826
  Scenario Outline: Login con credenziali poste.it
    L'utente deve avere un conto bp onboardato

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck     | onboarding   | owner   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | BPINPROAND-1826 | <onboarding> | <owner> |
    Then Viene chiusa l'app

    @BPINPROAND-1826_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | posteID | testIdCheck | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | prisma  | BPINPROAND-1826 | nonseitu   |

		@BPINPROAND-1826_fortunato
    Examples: 
      | username             | password  | loginType | userHeader | device               | posteID | testIdCheck | onboarding |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | poste.it  | Vincenzo  | samsungGalaxyS8_plus | prisma  | BPINPROAND-1826 | nonseitu   |
 
  @BPINPROAND-1718 @BPINPROIOS-1568
  Scenario Outline: Verificare il funzionamento del CTA Non sei tu?
    Viene verificato che il pulsante non sei tu? sia funzionante

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck   | onboarding   | owner   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | <testIdCheck> | <onboarding> | <owner> |
    Then Viene chiusa l'app

    @BPINPROAND-1718_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | posteID | testIdCheck     | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | prisma  | BPINPROAND-1718 | nonseitu   |

    @BPINPROIOS-1568_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | posteID | testIdCheck     | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | prisma  | BPINPROIOS-1568 | nonseitu   |

  @BPINPROAND-1831 @Canceled
  Scenario Outline: Login con Fingerprint
    Viene verificato la login con fingerPrint

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck   | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | <testIdCheck> | <onboarding> |
    And Vengono cliccate le impostazioni dal menu
    And Viene cliccato Accesso e autorizzazioni
    And viene settato a "si" il toggle "fingerPrint"
      | posteid   |
      | <posteID> |
    #And viene inserito il posteId
    #| posteid   |
    #| <posteID> |
    And viene inserito il fingerPrint
      | fingerId   |
      | <fingerId> |
    And viene effettuato il logout
    And viene effettuato il login con fingerPrint
      | fingerId   | onboarding   |
      | <fingerId> | <onboarding> |
    And Vengono cliccate le impostazioni dal menu
    And Viene cliccato Accesso e autorizzazioni
    And viene settato a "no" il toggle "fingerPrint"
      | posteid   |
      | <posteID> |
    #And viene inserito il posteId
    #| posteid   |
    #| <posteID> |
    Then Viene chiusa l'app

    @BPINPROAND-1831_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | posteID | testIdCheck     | fingerId | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Luca       | samsungGalaxyS8_plus | prisma  | BPINPROAND-1831 |        1 | nonseitu   |

  @BPINPROAND-1834
  Scenario Outline: Login con posteID dopo app in background ed in modalita aereo per 15 minuti
    Verifica gestione perdita di connessione dopo 15 minuti

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck   | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | <testIdCheck> | <onboarding> |
    And viene messo il device in modalita offline
    And viene messa in background l'applicazione per quindici minuti
    And viene ripristinata la connessione
    And viene fatto un resume dell'app
      | onboarding   |
      | <onboarding> |
    #And viene effettuata una navigazione dell'app
    Then viene mostrata la pagina di login
    And Viene chiusa l'app

    @BPINPROAND-1834_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | posteID | testIdCheck     | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | prisma  | BPINPROAND-1834 | nonseitu   |

    @BPINPROIOS-1684_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | posteID | testIdCheck     | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | prisma  | BPINPROIOS-1684 | nonseitu   |

    @BPINPROIOS-1684_fortunato
    Examples: 
      | username                | password        | loginType | userHeader | device        | posteID  | testIdCheck          | onboarding |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | poste.it  | Vincenzo   | samsungGalaxyS8_plus | prisma   | BPINPROIOS-1684      | nonseitu   |

  @BPINPROAND-1848 @Canceled
  Scenario Outline: Login credenziali SPID - non supportato
    Verificare che il servizio non supporta più PosteID abilitato a SPID

    Given L'app "android" viene avviata e viene eseguita la nuova login
      | username   | password   | loginType   | device   | testIdCheck     | onboarding   |
      | <username> | <password> | <loginType> | <device> | BPINPROAND-1848 | <onboarding> |
    Then la popup di errore credenziali viene mostrata
    And L utente clicca su recupera
    And Viene aperto il browser per permettere il recupero password
    Then Viene chiusa l'app

    @BPINPROAND-1848
    Examples: 
      | username                  | password        | loginType | device               | testIdCheck | onboarding |
      | petrucci.luca92@gmail.com | NLWbnsx6#st0nlw | poste.it  | samsungGalaxyS8_plus | PBPAPP-2940 | nonseitu   |

  @BPINPROAND-1242
  Scenario Outline: Login con credenziali poste.it - utente onboardato
    L'utente deve avere un conto bp onboardato

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck     | onboarding   | owner   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | BPINPROAND-1242 | <onboarding> | <owner> |
    Then Viene chiusa l'app

    @BPINPROAND-1242_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | posteID | testIdCheck    | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | prisma  | PINPROAND-1242 | nonseitu   |

    @BPINPROIOS-2215_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | posteID | testIdCheck     | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | prisma  | BPINPROIOS-2215 | nonseitu   |

  @BPINPROAND-1169 @Canceled
  Scenario Outline: Login con Fingerprint Android
    Viene verificato la login con fingerPrint

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck   | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | <testIdCheck> | <onboarding> |
    And Vengono cliccate le impostazioni dal menu
    And Viene cliccato Accesso e autorizzazioni
    And viene settato a "si" il toggle "fingerPrint"
      | posteid   |
      | <posteID> |
    #And viene inserito il posteId
    #| posteid   |
    #| <posteID> |
    And viene inserito il fingerPrint
      | fingerId   |
      | <fingerId> |
    And viene effettuato il logout
    And viene effettuato il login con fingerPrint
      | fingerId   | onboarding   |
      | <fingerId> | <onboarding> |
    And Vengono cliccate le impostazioni dal menu
    And Viene cliccato Accesso e autorizzazioni
    And viene settato a "no" il toggle "fingerPrint"
      | posteid   |
      | <posteID> |
    #And viene inserito il posteId
    #| posteid   |
    #| <posteID> |
    Then Viene chiusa l'app

    @BPINPROAND-1169
    Examples: 
      | username             | password  | loginType | userHeader | device               | posteID | testIdCheck     | fingerId | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Luca       | samsungGalaxyS8_plus | prisma  | BPINPROAND-1169 |        1 | accedi     |

  @BPINPROAND-1257 @BPINPROIOS-2230
  Scenario Outline: Login con PosteID
    Al tap l’utente accede alla pagina di login, la quale varierà in funzione del meccanismo 
    di accesso settato dall’utente (con codice PosteID o con impronta biometrica o faceID)

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | PBPAPP-3792 | <onboarding> |
    Then Viene chiusa l'app

    @BPINPROAND-1257_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | posteID | testIdCheck     | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | prisma  | BPINPROAND-1257 | nonseitu   |

    @BPINPROIOS-2230_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | posteID | testIdCheck     | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | prisma  | BPINPROIOS-2230 | nonseitu   |

  @BPINPROAND-1195
  Scenario Outline: Login con credenziali poste.it - utente non onboardato
    Verifica della prima pagina dell'onboarding con utente non onbordato

    Given L'app "android" viene avviata e viene eseguito il New Login no onboarding
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck     | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | BPINPROAND-1195 | <onboarding> |
    Then Viene chiusa l'app

    @BPINPROAND-1195_musella
    Examples: 
      | username                | password        | loginType | userHeader | device               | posteID | testIdCheck    | onboarding |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | poste.it  | Vincenzo   | samsungGalaxyS8_plus | prisma  | PINPROAND-1195 | nonseitu   |

    @BPINPROIOS-2168_musella
    Examples: 
      | username                | password        | loginType | userHeader | device               | posteID | testIdCheck    | onboarding |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | poste.it  | Vincenzo   | samsungGalaxyS8_plus | prisma  | PINPROIOS-2168 | nonseitu   |
