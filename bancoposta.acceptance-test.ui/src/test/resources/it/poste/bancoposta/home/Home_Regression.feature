@Home
Feature: Home
  L'utente dopo aver effettuato l'onboarding può verificare le voci del menu laterale

  @BPINPROAND-1482
  Scenario Outline: Verifica le voci menu laterale
    Verifica le voci menu laterale

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck     | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | BPINPROAND-1482 | <onboarding> |
    And L utente verifica le voci dell hamburger menu
      | version   | owner   | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck | onboarding   |
      | <version> | <owner> | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | PBPAPP-142  | <onboarding> |
    Then Viene chiusa l'app

    @BPINPROAND-1482_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | payWith            | iban         | city   | owner               | amount | description     | address                     | citySender | reference | refecenceEffective | quickOperationName | transactionDescription | messageText                                   | categoryTransation | editValue | posteID | commissioni | bonificoType | beneficiario  | paymentMethod | testIdCheck | version | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | conto;001044548509 | 001046919773 | Italia | Salvatore Musella |   0.01 | test automatico | Via degli Ulivi Gialli n.23 | Agrigento  | Luca      | Luca Petrucci      | test bonifico      | TRN                    | BancoPosta: Conferma Bonifico SEPA effettuato | Varie              |       0.2 | prisma  |         1.0 | bonifico     | PETRUCCI LUCA | conto         | BPINPROAND-1482  | prod    | nonseitu   |

  #  | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore   | samsungGalaxyS8_plus | conto;001044548509 | 001046919773 | Italia | vincenzo fortunato |   0.01 | test automatico | Via degli Ulivi Gialli n.23 | Agrigento  | Luca      | Luca Petrucci      | test bonifico      | TRN                    | BancoPosta: Conferma Bonifico SEPA effettuato | Varie              |       0.2 | prisma  |         1.0 | bonifico     | PETRUCCI LUCA | conto         | PBPAPP-142  | test    |nonseitu|
  # PER ESEGUIRE QUESTO TEST SELEZIONARE IN INPUT "prod" o "test" in base all'app utilizzata(al momento solo prod)
  @BPINPROAND-1627
  Scenario Outline: Verifica che e possibile cliccare sulle operazioni veloci di default
    Verifica che e possibile cliccare sulle operazioni veloci di default

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck     | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | BPINPROAND-1627 | <onboarding> |
    And L utente verifica le voci delle operazioni veloci
      | quickOperation      | page                          | payWith   |
      | Ricarica Postepay   | RicaricaPostepayPage          | <payWith> |
      | Ricarica telefonica | RicaricaTelefonicaPage        | <payWith> |
      | Bonifico            | SendBonificoPage              | <payWith> |
      | Postagiro           | SendPostagiroPage             | <payWith> |
      | Bollettino          | BollettinoChooseOperationPage | <payWith> |
      | PagoPA              |                               | <payWith> |
      | Girofondo           | GirofondoFormPage             | <payWith> |
    Then Viene chiusa l'app

    @BPINPROAND-1627_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | payWith            | iban         | city   | owner               | amount | description     | address                     | citySender | reference | refecenceEffective | quickOperationName | transactionDescription | messageText                                   | categoryTransation | editValue | posteID | commissioni | bonificoType | beneficiario  | paymentMethod | testIdCheck | version | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | conto;001044548509 | 001046919773 | Italia | vincenzo fortunato |   0.01 | test automatico | Via degli Ulivi Gialli n.23 | Agrigento  | Luca      | Luca Petrucci      | test bonifico      | TRN                    | BancoPosta: Conferma Bonifico SEPA effettuato | Varie              |       0.2 | prisma  |         1.0 | bonifico     | PETRUCCI LUCA | conto         | BPINPROAND-1627  | prod    | nonseitu   |
