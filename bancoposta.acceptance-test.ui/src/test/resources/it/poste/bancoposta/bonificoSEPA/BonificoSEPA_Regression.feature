@BonificoSEPA
Feature: Bonifico SEPA
  L'utente compila e sottomette un bonifico verso conte POSTE o NO POSTE, da sottomenu paga o da operazione veloce

  @BPINPROAND-1541
  Scenario Outline: Bonifico su conto POSTE da conto
      Viene effettuato un bonifico da conto

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck    | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | BPINPROAND-154 | <onboarding> |
    And Viene effettuato un bonifico
      | payWith   | iban   | city   | owner   | amount   | description   | address   | citySender   | reference   | refecenceEffective   | posteid   | commissioni   | testIdCheck | bonificoType   | beneficiario   | paymentMethod   |
      | <payWith> | <iban> | <city> | <owner> | <amount> | <description> | <address> | <citySender> | <reference> | <refecenceEffective> | <posteID> | <commissioni> | PBPAPP-2105 | <bonificoType> | <beneficiario> | <paymentMethod> |
    And Salvare l'operazione come "operazione veloce"
      | quickOperationName   | useAdb | device   |
      | <quickOperationName> | true   | <device> |
    And Viene controllato che la transazione sia stata registrata nella lista movimenti
      | conto     | amount   | operation | transactionType   | transactionDescription   | commissioni   |
      | <payWith> | <amount> | -         | <transactionType> | <transactionDescription> | <commissioni> |
    And Viene controllata la corretta categorizzazione della transazione
      | categoryTransation   | amount   | subCategoryTransaction | transactionType   | operation |
      | <categoryTransation> | <amount> | Da Categorizzare       | <transactionType> | -         |
    And Controlla che il saldo sia stato decrementato da "conto bancoposta"
      | conto     | amount   | operation | transactionType   | transactionDescription   | commissioni   |
      | <payWith> | <amount> | -         | <transactionType> | <transactionDescription> | <commissioni> |
    Then Viene controllata la ricezione della notifica
      | messageText   | testIdCheck | notifyType        |
      | <messageText> | -           | <transactionType> |
    And Viene chiusa l'app

    @BPINPROAND-1541_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | payWith            | iban                        | city   | owner         | amount | description   | address                     | citySender | reference | refecenceEffective | quickOperationName | transactionType | transactionDescription | messageText                                   | categoryTransation | editValue | posteID | commissioni | bonificoType | beneficiario  | paymentMethod | testIdCheck | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | conto;001044548509 | IT29R0760103400001046919773 | Italia | Luca Petrucci |   0.01 | postagiro due | Via degli Ulivi Gialli n.23 | Agrigento  | Salvatore | vincenzo fortunato | postagiro due      | POSTAGIRO       | TRN                    | BancoPosta: Conferma Bonifico SEPA effettuato | Varie              |       0.2 | prisma  |         1.0 | postagiro    | PETRUCCI LUCA | conto         | BPINPROAND-1541 | nonseitu   |

  #@BPINPROAND-1541_fortunato
  #Examples:
  #| username                | password        | loginType | userHeader | device               | payWith            | iban                        | city   | owner         | amount | description   | address                     | citySender | reference | refecenceEffective | quickOperationName | transactionType | transactionDescription | messageText                                   | categoryTransation | editValue | posteID | commissioni | bonificoType | beneficiario  | paymentMethod | testIdCheck | onboarding |
  #| vincenzo.fortunato-4869 | D@niela22Lili10 | poste.it  | Vincenzo   | samsungGalaxyS8_plus | conto;001044548509 | IT29R0760103400001046919773 | Italia | Luca Petrucci |   0.01 | postagiro due | Via degli Ulivi Gialli n.23 | Agrigento  | Salvatore | vincenzo fortunato | postagiro due      | POSTAGIRO       | TRN                    | BancoPosta: Conferma Bonifico SEPA effettuato | Varie              |       0.2 | prisma  |         1.0 | postagiro    | PETRUCCI LUCA | conto         | PBPAPP-2105 | nonseitu   |
  @BPINPROAND-1542
  Scenario Outline: Bonifico su conto POSTE da carta
       Viene effettuato un bonifico da carta postepay evolution

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck     | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | BPINPROAND-1542 | <onboarding> |
    And Viene effettuato un bonifico
      | payWith   | iban   | city   | owner   | amount   | description   | address   | citySender   | reference   | refecenceEffective   | posteid   | commissioni   | testIdCheck             | bonificoType   | beneficiario   | paymentMethod   |
      | <payWith> | <iban> | <city> | <owner> | <amount> | <description> | <address> | <citySender> | <reference> | <refecenceEffective> | <posteID> | <commissioni> | PBPAPP-2105;PBPAPP-2108 | <bonificoType> | <beneficiario> | <paymentMethod> |
    And Salvare l'operazione come "operazione veloce"
      | quickOperationName   |
      | <quickOperationName> |
    And Viene controllato che la transazione sia stata registrata nella lista movimenti
      | conto     | amount   | operation | transactionType   | transactionDescription   | commissioni   |
      | <payWith> | <amount> | -         | <transactionType> | <transactionDescription> | <commissioni> |
    And Viene controllata la corretta categorizzazione della transazione
      | categoryTransation   | amount   | subCategoryTransaction | transactionType   | operation |
      | <categoryTransation> | <amount> | Da Categorizzare       | <transactionType> | -         |
    And Controlla che il saldo sia stato decrementato da "conto bancoposta"
      | conto     | amount   | operation | transactionType   | transactionDescription   | commissioni   |
      | <payWith> | <amount> | -         | <transactionType> | <transactionDescription> | <commissioni> |
    Then Viene controllata la ricezione della notifica
      | messageText   | testIdCheck | notifyType        |
      | <messageText> | -           | <transactionType> |
    And Viene chiusa l'app

    @BPINPROAND-1542_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | payWith                   | iban                        | city   | owner              | amount | description | address                     | citySender | reference | refecenceEffective | quickOperationName | transactionType | transactionDescription | messageText                                   | categoryTransation | editValue | posteID | commissioni | bonificoType | beneficiario       | paymentMethod | testIdCheck | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | postepay;5333171077147177 | IT15L0760103400001044920229 | Italia | FORTUNATO VINCENZO |    0.5 | bonifico    | Via degli Ulivi Gialli n.23 | Agrigento  | Salvatore | Salvatore Musella  | bonifico           | BONIFICO        | TRN                    | BancoPosta: Conferma Bonifico SEPA effettuato | Varie              |       0.2 | prisma  |         1.0 | postagiro    | FORTUNATO VINCENZO | carta         | BPINPROAND-1542 | nonseitu   |

  #NON EFFETTUABILE PER MANCANZA DI POSTEPAY EVOLUTION
  @BPINPROAND-1544
  Scenario Outline: Bonifico su conto NO POSTE da conto
       Viene effettuato un bonifico da conto a conto NO POSTE

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck     | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | BPINPROAND-1544 | <onboarding> |
    And Viene effettuato un bonifico
      | payWith   | iban   | city   | owner   | amount   | description   | address   | citySender   | reference   | refecenceEffective   | posteid   | commissioni   | testIdCheck             | bonificoType   | beneficiario   | paymentMethod   |
      | <payWith> | <iban> | <city> | <owner> | <amount> | <description> | <address> | <citySender> | <reference> | <refecenceEffective> | <posteID> | <commissioni> | PBPAPP-2105;PBPAPP-2108 | <bonificoType> | <beneficiario> | <paymentMethod> |
    And Salvare l'operazione come "operazione veloce"
      | quickOperationName   |
      | <quickOperationName> |
    And Viene controllato che la transazione sia stata registrata nella lista movimenti
      | conto     | amount   | operation | transactionType   | transactionDescription   | commissioni   |
      | <payWith> | <amount> | -         | <transactionType> | <transactionDescription> | <commissioni> |
    And Viene controllata la corretta categorizzazione della transazione
      | categoryTransation   | amount   | subCategoryTransaction | transactionType   | operation |
      | <categoryTransation> | <amount> | Da Categorizzare       | <transactionType> | -         |
    And Controlla che il saldo sia stato decrementato da "conto bancoposta"
      | conto     | amount   | operation | transactionType   | transactionDescription   | commissioni   |
      | <payWith> | <amount> | -         | <transactionType> | <transactionDescription> | <commissioni> |
    Then Viene controllata la ricezione della notifica
      | messageText   | testIdCheck | notifyType        |
      | <messageText> | -           | <transactionType> |
    And Viene chiusa l'app

    @BPINPROAND-1544_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | payWith            | iban                        | city   | owner         | amount | description  | address                     | citySender | reference | refecenceEffective | quickOperationName | transactionType | transactionDescription | messageText                                   | categoryTransation | editValue | posteID | commissioni | bonificoType | beneficiario  | paymentMethod | testIdCheck | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | conto;001044548509 | IT60I0200805236000102962357 | Italia | Luca Petrucci |   2.00 | bonifico uno | Via degli Ulivi Gialli n.23 | Agrigento  | Salvatore | vincenzo fortunato | bonifico uno       | BONIFICO        | TRN                    | BancoPosta: Conferma Bonifico SEPA effettuato | Varie              |       0.2 | prisma  |         1.0 | bonifico     | Petrucci Luca | conto         | BPINPROAND-1544 | nonseitu   |

  @BPINPROIOS-1394
  Scenario Outline: Bonifico su conto NO POSTE da conto
       Viene effettuato un bonifico da conto a conto NO POSTE

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck     | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | BPINPROIOS-1394 | <onboarding> |
    And Viene effettuato un bonifico
      | payWith   | iban   | city   | owner   | amount   | description   | address   | citySender   | reference   | refecenceEffective   | posteid   | commissioni   | testIdCheck             | bonificoType   | beneficiario   | paymentMethod   |
      | <payWith> | <iban> | <city> | <owner> | <amount> | <description> | <address> | <citySender> | <reference> | <refecenceEffective> | <posteID> | <commissioni> | PBPAPP-2105;PBPAPP-2108 | <bonificoType> | <beneficiario> | <paymentMethod> |
    And Salvare l'operazione come "operazione veloce"
      | quickOperationName   |
      | <quickOperationName> |
    And Viene chiusa l'app

    @BPINPROIOS-1394_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | payWith            | iban                        | city   | owner         | amount | description  | address                     | citySender | reference | refecenceEffective | quickOperationName | transactionType | transactionDescription | messageText                                   | categoryTransation | editValue | posteID | commissioni | bonificoType | beneficiario  | paymentMethod | testIdCheck | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | conto;001044548509 | IT60I0200805236000102962357 | Italia | Luca Petrucci |   2.00 | bonifico uno | Via degli Ulivi Gialli n.23 | Agrigento  | Salvatore | vincenzo fortunato | bonifico uno       | BONIFICO        | TRN                    | BancoPosta: Conferma Bonifico SEPA effettuato | Varie              |       0.2 | prisma  |         1.0 | bonifico     | Petrucci Luca | conto         | BPINPROIOS-1394 | nonseitu   |

  @BPINPROAND-1548
  Scenario Outline: Bonifico su conto NO POSTE da postepay evolution
       Viene effettuato un bonifico da postepay a conto NO POSTE

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck     | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | BPINPROAND-1548 | <onboarding> |
    And Viene effettuato un bonifico
      | payWith   | iban   | city   | owner   | amount   | description   | address   | citySender   | reference   | refecenceEffective   | posteid   | commissioni   | testIdCheck             | bonificoType   | beneficiario   | paymentMethod   |
      | <payWith> | <iban> | <city> | <owner> | <amount> | <description> | <address> | <citySender> | <reference> | <refecenceEffective> | <posteID> | <commissioni> | PBPAPP-2105;PBPAPP-2108 | <bonificoType> | <beneficiario> | <paymentMethod> |
    And Salvare l'operazione come "operazione veloce"
      | quickOperationName   |
      | <quickOperationName> |
    And Viene controllato che la transazione sia stata registrata nella lista movimenti
      | conto     | amount   | operation | transactionType   | transactionDescription   | commissioni   |
      | <payWith> | <amount> | -         | <transactionType> | <transactionDescription> | <commissioni> |
    And Viene controllata la corretta categorizzazione della transazione
      | categoryTransation   | amount   | subCategoryTransaction | transactionType   | operation |
      | <categoryTransation> | <amount> | Da Categorizzare       | <transactionType> | -         |
    And Controlla che il saldo sia stato decrementato da "conto bancoposta"
      | conto     | amount   | operation | transactionType   | transactionDescription   | commissioni   |
      | <payWith> | <amount> | -         | <transactionType> | <transactionDescription> | <commissioni> |
    Then Viene controllata la ricezione della notifica
      | messageText   | testIdCheck | notifyType        |
      | <messageText> | -           | <transactionType> |
    And Viene chiusa l'app

    @BPINPROAND-1548_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | payWith                   | iban                        | city   | owner         | amount | description | address                     | citySender | reference | refecenceEffective | quickOperationName | transactionType | transactionDescription | messageText                                   | categoryTransation | editValue | posteID | commissioni | bonificoType | beneficiario  | paymentMethod | testIdCheck | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | postepay;5333171077147177 | IT60I0200805236000102962357 | Italia | Luca Petrucci |   2.00 | bonifico    | Via degli Ulivi Gialli n.23 | Agrigento  | Salvatore | vincenzo fortunato | bonifico cinque    | BONIFICO        | TRN                    | BancoPosta: Conferma Bonifico SEPA effettuato | Varie              |       0.2 | prisma  |         1.0 | bonifico     | Petrucci Luca | carta         | BPINPROAND-1548 | nonseitu   |

  #NON EFFETTUABILE PER MANCANZA DI POSTEPAY EVOLUTION
  @BPINPROAND-1556
  Scenario Outline: Bonifico Operazione veloce
     Viene effettuato un bonifico da operazione veloce

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck     | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | BPINPROAND-1556 | <onboarding> |
    #And Prende i dati del bonifico
    #| payWith   | iban   | city   | owner   | amount   | description   | address   | citySender   | reference   | refecenceEffective   | posteid   | commissioni   | testIdCheck | bonificoType   | beneficiario   | paymentMethod   |
    #| <payWith> | <iban> | <city> | <owner> | <amount> | <description> | <address> | <citySender> | <reference> | <refecenceEffective> | <posteID> | <commissioni> | PBPAPP-2121 | <bonificoType> | <beneficiario> | <paymentMethod> |
    #And Viene effettuata la transazione da operazione veloce
    #| quickOperationName   | transactionType   | testIdCheck | payWith   | iban   | city   | owner    | amount   | description   |
    #| <quickOperationName> | <transactionType> | PBPAPP-2121 | <payWith> | <iban> | <city> | <owner > | <amount> | <description> |
    And accede ad operazione veloce e seleziona Bonifico
      | quickOperationName   | payWith   | quickOperationSaved |
      | <quickOperationName> | <payWith> | n                   |
    And Viene effettuato un bonifico da operazione veloce
      | payWith   | iban   | city   | owner   | amount   | description   | address   | citySender   | reference   | refecenceEffective   | posteid   | commissioni   | testIdCheck | bonificoType   | beneficiario   |
      | <payWith> | <iban> | <city> | <owner> | <amount> | <description> | <address> | <citySender> | <reference> | <refecenceEffective> | <posteID> | <commissioni> | PBPAPP-2121 | <bonificoType> | <beneficiario> |
    #And Salvare l'operazione come "operazione veloce"
    #| quickOperationName    | fromQuickOperation |
    #| <quickOperationName>1 | y                  |
    And Viene controllato che la transazione sia stata registrata nella lista movimenti
      | conto     | amount   | operation | transactionType   | transactionDescription   | commissioni   |
      | <payWith> | <amount> | -         | <transactionType> | <transactionDescription> | <commissioni> |
    #And Controlla che il saldo sia stato decrementato da "conto bancoposta"
    # | conto     | amount   | operation | transactionType   | transactionDescription   | commissioni   |
    #| <payWith> | <amount> | -         | <transactionType> | <transactionDescription> | <commissioni> |
    Then Viene controllata la ricezione della notifica
      | messageText   | notifyType        |
      | <messageText> | <transactionType> |
    And Viene chiusa l'app

    @BPINPROAND-1556_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | payWith            | iban                        | city   | owner              | amount | description  | address                     | citySender | reference | refecenceEffective | quickOperationName | transactionType | transactionDescription | messageText                                   | categoryTransation | editValue | posteID | commissioni | bonificoType | beneficiario       | testIdCheck | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | conto;001044548509 | IT15L0760103400001044920229 | Italia | Vincenzo Fortunato |   0.01 | bonifico tre | Via degli Ulivi Gialli n.23 | Agrigento  | Salvatore | vincenzo fortunato | Bonifico           | BONIFICO        | TRN                    | BancoPosta: Conferma Bonifico SEPA effettuato | Varie              |       0.2 | prisma  |         1.0 | bonifico     | Fortunato Vincenzo | BPINPROAND-1556 | nonseitu   |

    @BPINPROAND-1556_fortunato
    Examples: 
      | username                | password        | loginType | userHeader | device               | payWith            | iban                        | city   | owner        | amount | description  | address                     | citySender | reference | refecenceEffective | quickOperationName | transactionType | transactionDescription | messageText                                   | categoryTransation | editValue | posteID | commissioni | bonificoType | beneficiario | testIdCheck | onboarding |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | poste.it  | Vincenzo   | samsungGalaxyS8_plus | conto;001044920229 | IT45W0200840190000420385026 | Italia | Gennaro Rega |   2.00 | bonifico tre | Via degli Ulivi Gialli n.23 | Agrigento  | Salvatore | vincenzo fortunato | Bonifico           | BONIFICO        | TRN                    | BancoPosta: Conferma Bonifico SEPA effettuato | Varie              |       0.2 | prisma  |         1.0 | bonifico     | Rega Gennaro | BPINPROAND-1556 | nonseitu   |

  @BPINPROAND-1469
  Scenario Outline: Bonifico SEPA - Verifica il corretto popolamento del campo Paese di residenza
     Bonifico SEPA - Verifica il corretto popolamento del campo Paese di residenza

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck     | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | BPINPROAND-1469 | <onboarding> |
    And Accede alla sezione Invia Bonifico
      | payWith   | iban   | city   | owner   | amount   | description   | address   | citySender   | reference   | refecenceEffective   | posteid   | commissioni   | testIdCheck             | bonificoType   | beneficiario   | paymentMethod   |
      | <payWith> | <iban> | <city> | <owner> | <amount> | <description> | <address> | <citySender> | <reference> | <refecenceEffective> | <posteID> | <commissioni> | PBPAPP-2105;PBPAPP-2108 | <bonificoType> | <beneficiario> | <paymentMethod> |
    Then il campo Paese di residenza viene prepopolato con i seguenti dati
      | city   |
      | <city> |
    And Viene chiusa l'app

    @BPINPROAND-1469_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | payWith            | iban                        | city   | owner         | amount | description  | address                     | citySender | reference | refecenceEffective | quickOperationName | transactionType | transactionDescription | messageText                                   | categoryTransation | editValue | posteID | commissioni | bonificoType | beneficiario  | testIdCheck | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | conto;001044548509 | IT60I0200805236000102962357 | Italia | Luca Petrucci |   0.01 | bonifico tre | Via degli Ulivi Gialli n.23 | Agrigento  | Salvatore | vincenzo fortunato | bonifico           | BONIFICO        | TRN                    | BancoPosta: Conferma Bonifico SEPA effettuato | Varie              |       0.2 | prisma  |         1.0 | bonifico     | Petrucci Luca | BPINPROAND-1469 | nonseitu   |

    @BPINPROAND-1469_fortunato
    Examples: 
      | username                | password        | loginType | userHeader | device               | payWith            | iban                        | city   | owner         | amount | description  | address                     | citySender | reference | refecenceEffective | quickOperationName | transactionType | transactionDescription | messageText                                   | categoryTransation | editValue | posteID | commissioni | bonificoType | beneficiario | testIdCheck | onboarding |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | poste.it  | Vincenzo   | samsungGalaxyS8_plus | conto;001044920229 | IT45W0200840190000420385026 | Italia | Luca Petrucci |   0.01 | bonifico tre | Via degli Ulivi Gialli n.23 | Agrigento  | Vincenzo  | vincenzo fortunato | bonifico           | BONIFICO        | TRN                    | BancoPosta: Conferma Bonifico SEPA effettuato | Varie              |       0.2 | prisma  |         1.0 | bonifico     | Rega Gennaro | BPINPROAND-1469 | nonseitu   |

  @BPINPROAND-4614
  Scenario Outline: Bonifico SEPA - Click sul toggle di attiva bonifico instantaneo da cruscotto Conto
    verificare che sia possibile abilitare il prodotto all'instant payment

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck     | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | BPINPROAND-4614 | <onboarding> |
		And Naviga verso la sezione conti e carte
		And Clicca sul pulsante dettaglio del conto
		And Viene cliccato il toogle di bonifico istantaneo
			| posteid    |
			| <posteID>  |
		Then Viene controllato il messaggio di attivazione del bonifico istantaneo 
		And Viene chiusa l'app
		
    @BPINPROAND-4614_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | payWith            | iban                        | city   | owner         | amount | description  | address                     | citySender | reference | refecenceEffective | quickOperationName | transactionType | transactionDescription | messageText                                   | categoryTransation | editValue | posteID | commissioni | bonificoType | beneficiario  | testIdCheck | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | conto;001044548509 | IT60I0200805236000102962357 | Italia | Luca Petrucci |   0.01 | bonifico tre | Via degli Ulivi Gialli n.23 | Agrigento  | Salvatore | vincenzo fortunato | bonifico           | BONIFICO        | TRN                    | BancoPosta: Conferma Bonifico SEPA effettuato | Varie              |       0.2 | prisma  |         1.0 | bonifico     | Petrucci Luca | BPINPROAND-4614 | nonseitu   |

    @BPINPROAND-4614_fortunato
    Examples: 
      | username                | password        | loginType | userHeader | device               | payWith            | iban                        | city   | owner         | amount | description  | address                     | citySender | reference | refecenceEffective | quickOperationName | transactionType | transactionDescription | messageText                                   | categoryTransation | editValue | posteID | commissioni | bonificoType | beneficiario | testIdCheck | onboarding |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | poste.it  | Vincenzo   | samsungGalaxyS8_plus | conto;001044920229 | IT45W0200840190000420385026 | Italia | Luca Petrucci |   0.01 | bonifico tre | Via degli Ulivi Gialli n.23 | Agrigento  | Vincenzo  | vincenzo fortunato | bonifico           | BONIFICO        | TRN                    | BancoPosta: Conferma Bonifico SEPA effettuato | Varie              |       0.2 | prisma  |         1.0 | bonifico     | Rega Gennaro | BPINPROAND-4614 | nonseitu   |
