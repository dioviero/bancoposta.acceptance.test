@RicaricaPostePay
Feature: Ricarica PostePay
  L'utente compila e sottomette una ricarica Postepay, da sottomenu paga o da operazione veloce

  @BPINPROAND-1549 @BPINPROIOS-1399
  Scenario Outline: Ricarica Postepay da conto
     L'utente deve avere un conto bp e la carta onboardati

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck     | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | BPINPROAND-1549 | <onboarding> |
    And Viene effettuata una ricarica postepay
      | payWith   | transferTo   | owner   | amount   | testIdCheck            | description   | posteid   | paymentMethod |
      | <payWith> | <transferTo> | <owner> | <amount> | PBPAPP-185;PBPAPP-2113 | <description> | <posteID> | conto         |
    And Salvare l'operazione come "operazione veloce"
      | quickOperationName   | useAdb | device   |
      | <quickOperationName> | true   | <device> |
    And Viene controllata la ricezione della notifica
      | messageText   | notifyType        |
      | <messageText> | <transactionType> |
    And Viene chiusa l'app

    @BPINPROAND-1549_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | payWith            | transferTo       | owner             | amount | messageText                                | quickOperationName | editValue | categoryTransation | month | categoryDestination | posteID | description                         | testIdCheck     | messageText                                | transactionType | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | conto;001044548509 | 5333171077147177 | salvatore musella |   0.01 | BancoPosta: Ricarica Carta Postepay da App | Ricarica Postep    |      0.02 | varie              | month | categoryDestination | prisma  | Verifica i dati prima di procedere. | BPINPROAND-1549 | BancoPosta: Ricarica Carta Postepay da App | RICARICA POSTEP | nonseitu   |

    @BPINPROIOS-1399_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | payWith            | transferTo       | owner             | amount | messageText                                | quickOperationName | editValue | categoryTransation | month | categoryDestination | posteID | description                         | testIdCheck     | messageText                                | transactionType | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | conto;001044548509 | 5333171077147177 | salvatore musella |   0,01 | BancoPosta: Ricarica Carta Postepay da App | Ricarica Postep    |      0.02 | varie              | month | categoryDestination | prisma  | Verifica i dati prima di procedere. | BPINPROIOS-1399 | BancoPosta: Ricarica Carta Postepay da App | RICARICA POSTEP | nonseitu   |

    @BPINPROIOS-1399_fortunato
    Examples: 
      | username                | password        | loginType | userHeader | device               | payWith            | transferTo       | owner              | amount | messageText                                | quickOperationName | editValue | categoryTransation | month | categoryDestination | posteID | description                         | testIdCheck     | messageText                                | transactionType | onboarding |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | poste.it  | Vincenzo   | samsungGalaxyS8_plus | conto;001044920229 | 5333171077144679 | Vincenzo fortunato |   0,01 | BancoPosta: Ricarica Carta Postepay da App | Ricarica Postep    |      0.02 | varie              | month | categoryDestination | prisma  | Verifica i dati prima di procedere. | BPINPROIOS-1399 | BancoPosta: Ricarica Carta Postepay da App | RICARICA POSTEP | nonseitu   |

  @PBPAPP-2113-BIS
  Scenario Outline: Ricarica Postepay da conto
    L'utente deve avere un conto bp e la carta onboardati

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck     | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | PBPAPP-2113-BIS | <onboarding> |
    And Controlla che l'operazione veloce sia editabile
      | quickOperationName   | testIdCheck | transactionType   |
      | <quickOperationName> | -           | <transactionType> |
    And Modifica "metodo di pagamento" dell'operazione veloce
      | editValue   | testIdCheck |
      | <editValue> | -           |
    And Viene effettuata la transazione da operazione veloce
      | quickOperationName   | testIdCheck |
      | <quickOperationName> | -           |
    And Viene controllata la ricezione della notifica
      | messageText   | testIdCheck |
      | <messageText> | -           |
    And Viene controllata la corretta categorizzazione della transazione
      | categoryTransation   | amount   | testIdCheck | transactionType   | subCategoryTransaction |
      | <categoryTransation> | <amount> | -           | <transactionType> | Da Categorizzare       |
    Then Viene controllata la ricezione della notifica
      | messageText   | testIdCheck | notifyType        |
      | <messageText> | -           | <transactionType> |
    And Viene chiusa l'app

    @PBPAPP-2113-BIS
    Examples: 
      | username             | password  | loginType | userHeader | device               | payWith            | transferTo       | owner              | amount | messageText                                | quickOperationName | editValue | categoryTransation | month | categoryDestination | posteID | description                         | testIdCheck | transactionType  | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | conto;001044548509 | 5333171077147177 | vincenzo fortunato |   0.01 | BancoPosta: Ricarica Carta Postepay da App | Ricarica Postep    |      0.02 | varie              | month | categoryDestination | prisma  | Verifica i dati prima di procedere. | PBPAPP-2113 | RICARICAPOSTEPAY | nonseitu   |

  @BPINPROAND-1537
  Scenario Outline: Ricarica Postepay da carta
     Viene effettuata una ricarica Postepay da carta Postepay

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck     | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | BPINPROAND-1537 | <onboarding> |
    And Viene effettuata una ricarica postepay
      | payWith   | transferTo   | owner   | amount   | testIdCheck            | description   | posteid   | paymentMethod   |
      | <payWith> | <transferTo> | <owner> | <amount> | PBPAPP-185;PBPAPP-2113 | <description> | <posteID> | <paymentMethod> |
    And Salvare l'operazione come "operazione veloce"
      | quickOperationName   | useAdb | device   |
      | <quickOperationName> | true   | <device> |
    And Viene controllata la ricezione della notifica
      | messageText   | testIdCheck | notifyType   |
      | <messageText> | -           | <searchType> |
    And Viene controllata la corretta categorizzazione della transazione
      | categoryTransation   | amount   | subCategoryTransaction | transactionType   | operation |
      | <categoryTransation> | <amount> | Da Categorizzare       | <transactionType> | -         |
    And Viene chiusa l'app

    @BPINPROAND-1537_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | payWith                   | transferTo       | owner              | amount | messageText             | quickOperationName | editValue | categoryTransation | month | categoryDestination | posteID | description                                   | testIdCheck     | messageText                                | transactionType | paymentMethod | searchType              | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | postepay;5333171077147177 | 5333171077144679 | Vincenzo Fortunato |   0.01 | Ricarica Carta Postepay | Ricarica Postep    |      0.02 | varie              | month | categoryDestination | prisma  | Verifica prima di procedere con il pagamento. | BPINPROAND-1537 | BancoPosta: Ricarica Carta Postepay da App | RICARICA POSTEP | carta         | ricarica carta postepay | nonseitu   |

    @BPINPROAND-1537_fortunato
    Examples: 
      | username                | password        | loginType | userHeader | device               | payWith                   | transferTo       | owner             | amount | messageText             | quickOperationName | editValue | categoryTransation | month | categoryDestination | posteID | description                                   | testIdCheck     | messageText                                | transactionType | paymentMethod | searchType              | onboarding |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | poste.it  | Vincenzo   | samsungGalaxyS8_plus | postepay;5333171077144679 | 5333171077147177 | Salvatore Musella |   0.01 | Ricarica Carta Postepay | Ricarica Postep    |      0.02 | varie              | month | categoryDestination | prisma  | Verifica prima di procedere con il pagamento. | BPINPROAND-1537 | BancoPosta: Ricarica Carta Postepay da App | RICARICA POSTEP | carta         | ricarica carta postepay | nonseitu   |

  @BPINPROAND-1871
  Scenario Outline: Ricarica postepay da libretto - in lavorazione OK
       Viene effettuata una ricarica postepay da libretto

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck     | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | BPINPROAND-1871 | <onboarding> |
    And Viene effettuata una ricarica postepay
      | payWith   | transferTo   | owner   | amount   | testIdCheck            | description   | posteid   |
      | <payWith> | <transferTo> | <owner> | <amount> | PBPAPP-185;PBPAPP-2113 | <description> | <posteID> |
    #Then viene visualizzato il messaggio La tua richiesta di Ricarica Postepay è in lavorazione!
    And Viene chiusa l'app

    @BPINPROAND-1871_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | payWith                     | transferTo       | owner              | amount | messageText     | quickOperationName | editValue | categoryTransation | month | categoryDestination | posteID | description                         | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | libretto smart;000049494216 | 5333171077147177 | PETRUCCI Salvatore |   0.01 | test automatico | Ricarica Postepay  | editValue | categoryTransation | month | categoryDestination | prisma  | Verifica i dati prima di procedere. | nonseitu   |

  @BPINPROAND-1522
  Scenario Outline: Operazione Veloce - Ricarica PostePay - Modifica modalità di pagamento
    L'utente effettua una ricarica Postepay da conto

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck     | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | BPINPROAND-1522 | <onboarding> |
    And Controlla che l'operazione veloce sia editabile
      | quickOperationName   | testIdCheck | transactionType   |
      | <quickOperationName> | -           | <transactionType> |
    And Modifica "metodo di pagamento" dell'operazione veloce
      | editValue   | testIdCheck |
      | <editValue> | -           |
    And Viene chiusa l'app

    @BPINPROAND-1522_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | payWith                     | transferTo       | owner              | amount | messageText             | quickOperationName | editValue | categoryTransation | month | categoryDestination | posteID | description                         | testIdCheck     | transactionType  | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | libretto smart;000049494216 | 5333171077147177 | PETRUCCI Salvatore |   0.01 | Ricarica Carta Postepay | Ricarica Postep  | editValue | categoryTransation | month | categoryDestination | prisma  | Verifica i dati prima di procedere. | BPINPROAND-1522 | RICARICAPOSTEPAY | nonseitu   |

    @BPINPROAND-1522_fortunato
    Examples: 
      | username                | password        | loginType | userHeader | device               | posteID | testIdCheck     | onboarding | biscotto         | editValue | quickOperationName | transactionType  | payWith            | transferTo       |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | poste.it  | Vincenzo   | samsungGalaxyS8_plus | prisma  | BPINPROAND-1522 | nonseitu   | Conto BancoPosta | editValue | Ricarica Postep  | RICARICAPOSTEPAY | conto;001044920229 | 5333171077144679 |

  @BPINPROAND-1520
  Scenario Outline: Operazione Veloce - Modifica dati Ricarica PostePay
    Vengono modificati i dati di una ricarica postepay da operazione veloce

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck     | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | BPINPROAND-1520 | <onboarding> |
    And Controlla che l'operazione veloce sia editabile
      | quickOperationName   | testIdCheck | transactionType   |
      | <quickOperationName> | -           | <transactionType> |
    And Modifica "dati ricarica" dell'operazione veloce
      | editValue   | testIdCheck |
      | <editValue> | -           |
    And Viene chiusa l'app

    #Si devono modificare i dati della postepay
    @BPINPROAND-1520_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | payWith                     | transferTo       | owner              | amount | messageText             | quickOperationName | editValue | categoryTransation | month | categoryDestination | posteID | description                         | transactionType  | testIdCheck | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | libretto smart;000049494216 | 5333171077147177 | PETRUCCI Salvatore |   0.01 | Ricarica Carta Postepay | Ricarica Postep    | editValue | categoryTransation | month | categoryDestination | prisma  | Verifica i dati prima di procedere. | RICARICAPOSTEPAY | PBPAPP-198  | nonseitu   |

  @BPINPROAND-1567
  Scenario Outline: Ricarica PostePay Operazione veloce
      L'utente effettua una ricarica PostePay tramite l operazione veloce

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck     | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | BPINPROAND-1567 | <onboarding> |
    And Viene effettuata una ricarica Postepay da operazione veloce
      | payWith   | transferTo   | owner   | amount   | description   | posteid   | quickOperationName   | transactionType   | testIdCheck | userHeader   |
      | <payWith> | <transferTo> | <owner> | <amount> | <description> | <posteID> | <quickOperationName> | <transactionType> | -           | <userHeader> |
    #And Salvare l'operazione come "operazione veloce"
    # | quickOperationName   |
    #| <quickOperationName> |
    And Utente clicca su chiudi della thankYouPage e torna in homePage
      | quickOperationName   |
      | <quickOperationName> |
    And Viene controllato che la transazione sia stata registrata nella lista movimenti
      | conto     | amount   | operation | transactionType   | transactionDescription   | commissioni   | paymentMethod   |
      | <payWith> | <amount> | -         | <transactionType> | <transactionDescription> | <commissioni> | <paymentMethod> |
    And Controlla che il saldo sia stato decrementato da "conto bancoposta"
      | conto     | amount   | operation | transactionType   | transactionDescription   | commissioni   | paymentMethod   | notifyType         |
      | <payWith> | <amount> | -         | <transactionType> | <transactionDescription> | <commissioni> | <paymentMethod> | <transactionType2> |
    Then Viene controllata la ricezione della notifica
      | messageText   | testIdCheck | notifyType        |
      | <messageText> | -           | <transactionType> |
    And Viene chiusa l'app

    @BPINPROAND-1567_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | payWith            | transferTo       | owner             | amount | messageText                                | quickOperationName | editValue | categoryTransation | month | categoryDestination | posteID | description                         | transactionType   | paymentMethod | testIdCheck | transactionType2  | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | conto;001044548509 | 5333171077147177 | Salvatore Musella |   0.01 | BancoPosta: Ricarica Carta Postepay da App | Ricarica Postepay  | editValue | Varie              | month | categoryDestination | prisma  | Verifica i dati prima di procedere. | RICARICA POSTEPAY | carta         | PBPAPP-2131 | ricarica postepay | nonseitu   |

    @BPINPROAND-1567_fortunato
    Examples: 
      | username                | password        | loginType | userHeader | device               | payWith            | transferTo       | owner              | amount | messageText                                | quickOperationName | editValue | categoryTransation | month | categoryDestination | posteID | description                         | transactionType   | paymentMethod | testIdCheck | transactionType2  | onboarding |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | poste.it  | Vincenzo   | samsungGalaxyS8_plus | conto;001044920229 | 5333171077144679 | Vincenzo Fortunato |   0.01 | BancoPosta: Ricarica Carta Postepay da App | Ricarica Postepay  | editValue | Varie              | month | categoryDestination | prisma  | Verifica i dati prima di procedere. | RICARICA POSTEPAY | carta         | PBPAPP-2131 | ricarica postepay | nonseitu   |

  @BPINPROAND-1870
  Scenario Outline: Ricarica postepay da libretto operazione veloce - in lavorazione OK
      Viene effettuata una ricarica postepay da libretto operazione veloce

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck     | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | BPINPROAND-1870 | <onboarding> |
    And Viene effettuata una ricarica Postepay da operazione veloce
      | payWith   | transferTo   | owner   | amount   | description   | posteid   | quickOperationName   | transactionType   | testIdCheck | userHeader   |
      | <payWith> | <transferTo> | <owner> | <amount> | <description> | <posteID> | <quickOperationName> | <transactionType> | -           | <userHeader> |
    Then Salvare l'operazione come "operazione veloce"
      | quickOperationName   |
      | <quickOperationName> |
    And Viene chiusa l'app

    @BPINPROAND-1870_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | payWith                     | transferTo       | owner              | amount | messageText       | quickOperationName | editValue | categoryTransation | month | categoryDestination | posteID | description                         | transactionType   | paymentMethod | testIdCheck     | transactionType2  | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | libretto smart;000049494216 | 5333171077147177 | vincenzo fortunato |   0.01 | Ricarica PostePay | Ricarica Postepay  | editValue | Varie              | month | categoryDestination | prisma  | Verifica i dati prima di procedere. | ricarica postepay | carta         | BPINPROAND-1870 | ricarica postepay | nonseitu   |

  #      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore       | samsungGalaxyS8_plus | conto;001046919773 | 5333171077147177 | Salvatore PETRUCCI |   0.01 | ricarica postepay | Salvatore      | Salvatore Petrucci      | ricarica postepay  | Ricarica Postepay | TRN                    | BancoPosta: Conferma Bonifico SEPA effettuato | Varie              |       0.2 | prisma  |         1.0 | bonifico     | PETRUCCI Salvatore | Verifica i dati prima di procedere. | PBPAPP-2573 |
  @BPINPROAND-1734
  Scenario Outline: Ricarica questa postepay
      Viene effettuata lìoperazione di ricarica questa postepay

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck     | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | BPINPROAND-1734 | <onboarding> |
    And Viene effettuata una ricarica questa postepay
      | payWith   | transferTo   | owner   | amount   | testIdCheck | description   | posteid   | paymentMethod   | owner2   |
      | <payWith> | <transferTo> | <owner> | <amount> | PBPAPP-2573 | <description> | <posteID> | <paymentMethod> | <owner2> |
    And Controlla che il saldo sia stato decrementato da "conto bancoposta"
      | conto     | amount   | operation | transactionType   | transactionDescription   | commissioni   |
      | <payWith> | <amount> | -         | <transactionType> | <transactionDescription> | <commissioni> |
    And Viene chiusa l'app

    @BPINPROAND-1734_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | payWith            | transferTo                | owner             | amount | description                                   | reference | refecenceEffective | quickOperationName | transactionType   | transactionDescription | messageText                                   | categoryTransation | editValue | posteID | commissioni | bonificoType | beneficiario      | description       | testIdCheck     | paymentMethod | owner2            | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | conto;001044548509 | postepay;5333171077147177 | salvatore musella |   0.01 | Verifica prima di procedere con il pagamento. | Salvatore | Salvatore Musella  | ricarica postepay  | Ricarica Postepay | TRN                    | BancoPosta: Conferma Bonifico SEPA effettuato | Varie              |       0.2 | prisma  |         1.0 | bonifico     | MUSELLA SALVATORE | ricarica postepay | BPINPROAND-1734 | carta         | MUSELLA SALVATORE | nonseitu   |

    @BPINPROAND-1734_fortunato
    Examples: 
      | username                | password        | loginType | userHeader | device               | payWith            | transferTo                | owner              | amount | description                                   | reference | refecenceEffective | quickOperationName | transactionType   | transactionDescription | messageText                                   | categoryTransation | editValue | posteID | commissioni | bonificoType | beneficiario       | description       | testIdCheck     | paymentMethod | owner2               | onboarding |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | poste.it  | Vincenzo   | samsungGalaxyS8_plus | conto;001044920229 | postepay;5333171077144679 | vincenzo fortunato |   0.01 | Verifica prima di procedere con il pagamento. | Vincenzo  | Vincenzo Fortunato | ricarica postepay  | Ricarica Postepay | TRN                    | BancoPosta: Conferma Bonifico SEPA effettuato | Varie              |       0.2 | prisma  |         1.0 | bonifico     | FORTUNATO VINCENZO | ricarica postepay | BPINPROAND-1734 | carta         | FORTUNATO   VINCENZO | nonseitu   |

  @BPINPROAND-1858
  Scenario Outline: Ricarica automatica a soglia da conto a pp evo propria
    Ricarica automatica a soglia da conto a pp evo propria

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck     | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | BPINPROAND-1858 | <onboarding> |
    And Viene effettuata una ricarica postepay ricarica automatica
      | payWith   | transferTo   | owner   | amount   | testIdCheck | description   | posteid   | paymentMethod   | saldominimo   | commissione   |
      | <payWith> | <transferTo> | <owner> | <amount> | PBPAPP-2959 | <description> | <posteID> | <paymentMethod> | <saldominimo> | <commissione> |
    And Viene chiusa l'app

    @BPINPROAND-1858
    Examples: 
      | username             | password  | loginType | userHeader | device               | payWith            | transferTo                 | owner             | amount | messageText                                | quickOperationName | editValue | categoryTransation | month | categoryDestination | posteID | description                                   | testIdCheck     | messageText                                | transactionType | saldominimo | commissione | paymentMethod | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | conto;001044548509 | evolution;5333171077147177 | salvatore musella |   2.00 | BancoPosta: Ricarica Carta Postepay da App | Ricarica Postep    |      0.02 | varie              | month | categoryDestination | prisma  | verifica prima di procedere con il pagamento. | BPINPROAND-1858 | BancoPosta: Ricarica Carta Postepay da App | RICARICA POSTEP |        0.01 |        1.00 | conto         | nonseitu   |

  #  OBSOLETO
  #  @PBPAPP-2960
  #  Scenario Outline: Revoca ricarica automatica per soglia
  #    Viene revocata la soglia per la ricarica automatica
  #
  #    Given L'app "android" viene avviata e viene eseguita la login
  #      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck   |
  #      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | <testIdCheck> |
  #    And Naviga verso la sezione conti e carte
  #    And Clicca sul pulsante dettaglio del conto
  #    And Clicca sul pulsante Ricariche automatiche attive
  #    And Clicca sul pulsante inizia nella pagina ricariche automatiche
  #    And Viene effettuata una ricarica postepay due
  #      | payWith   | transferTo   | owner   | amount   | testIdCheck            | description   | posteid   | paymentMethod |
  #      | <payWith> | <transferTo> | <owner> | <amount> | PBPAPP-185;PBPAPP-2113 | <description> | <posteID> | conto         |
  #    And Viene chiusa l'app
  #
  #    @Test-PBPAPP-2960
  #    Examples:
  #      | username                | password        | loginType | userHeader | device               | payWith            | transferTo       | owner              | amount | messageText     | quickOperationName | editValue | categoryTransation | month | categoryDestination | posteID | description                         | testIdCheck | messageText                                | transactionType |
  #      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore   | samsungGalaxyS8_plus | conto;001044548509 | 5333171077147177 | vincenzo fortunato |   0.01 | test automatico | Ricarica Postep    |      0.02 | varie              | month | categoryDestination | prisma  | Verifica i dati prima di procedere. | PBPAPP-2960 | BancoPosta: Ricarica Carta Postepay da App | RICARICA POSTEP |
  @BPINPROAND-1860
  Scenario Outline: Revoca ricarica automatica per soglia
    Viene revocata la soglia per la ricarica automatica

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck     | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | BPINPROAND-1860 | <onboarding> |
    And Naviga verso la sezione conti e carte
    And Clicca sul pulsante dettaglio del conto
    And Clicca sul pulsante Ricariche automatiche attive
    And Verifica dell eliminazione della ricarica
      | posteid   |
      | <posteID> |
    And Viene chiusa l'app

    @BPINPROAND-1860_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | payWith            | transferTo       | owner              | amount | messageText     | quickOperationName | editValue | categoryTransation | month | categoryDestination | posteID | description                         | testIdCheck     | messageText                                | transactionType | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | conto;001044548509 | 5333171077147177 | vincenzo fortunato |   0.01 | test automatico | Ricarica Postep    |      0.02 | varie              | month | categoryDestination | prisma  | Verifica i dati prima di procedere. | BPINPROAND-1860 | BancoPosta: Ricarica Carta Postepay da App | RICARICA POSTEP | nonseitu   |

  @BPINPROAND-1861
  Scenario Outline: Ricarica automatica a tempo da conto a pp evo propria - ricorrenza bisettimanale
     L'utente deve avere un conto bp e la carta pp evo onboardati

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck     | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | BPINPROAND-1861 | <onboarding> |
    And Viene effettuata una ricarica postepay ricarica automatica2
      | payWith   | transferTo   | owner   | amount   | testIdCheck     | description   | posteid   | paymentMethod   | saldominimo   | commissione   |
      | <payWith> | <transferTo> | <owner> | <amount> | BPINPROAND-1861 | <description> | <posteID> | <paymentMethod> | <saldominimo> | <commissione> |
    And Viene chiusa l'app

    @BPINPROAND-1861_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | payWith            | transferTo                 | owner              | amount | messageText                                | quickOperationName | editValue | categoryTransation | month | categoryDestination | posteID | description                                   | testIdCheck     | messageText                                | transactionType | saldominimo | commissione | paymentMethod | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | conto;001044548509 | evolution;5333171077147177 | vincenzo fortunato |   2.00 | BancoPosta: Ricarica Carta Postepay da App | Ricarica Postep    |      0.02 | varie              | month | categoryDestination | prisma  | verifica prima di procedere con il pagamento. | BPINPROAND-1861 | BancoPosta: Ricarica Carta Postepay da App | RICARICA POSTEP |        0.01 |        1.00 | conto         | nonseitu   |

  @BPINPROAND-1621
  Scenario Outline: Ricarica Postepay - Salvare l'operazione come Operazione veloce
     Ricarica Postepay - Salvare l'operazione come Operazione veloce

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck     | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | BPINPROAND-1621 | <onboarding> |
    And Viene effettuata una ricarica postepay
      | payWith   | transferTo   | owner   | amount   | testIdCheck            | description   | posteid   | paymentMethod |
      | <payWith> | <transferTo> | <owner> | <amount> | PBPAPP-185;PBPAPP-2113 | <description> | <posteID> | conto         |
    And Salvare l'operazione come "operazione veloce"
      | quickOperationName   | useAdb | device   |
      | <quickOperationName> | true   | <device> |
    And Viene chiusa l'app

    @BPINPROAND-1621_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | payWith            | transferTo       | owner             | amount | messageText                                | quickOperationName | editValue | categoryTransation | month | categoryDestination | posteID | description                         | testIdCheck     | messageText                                | transactionType | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | conto;001044548509 | 5333171077147177 | salvatore musella |   0.01 | BancoPosta: Ricarica Carta Postepay da App | Ricarica Postep    |      0.02 | varie              | month | categoryDestination | prisma  | Verifica i dati prima di procedere. | BPINPROAND-1621 | BancoPosta: Ricarica Carta Postepay da App | RICARICA POSTEP | nonseitu   |

  @BPINPROAND-1240
  Scenario Outline: CTA (INIZIA o RICARICA) presente nella pagina delle liste delle “Ricariche automatiche"
    dalla voce di menu “Ricariche Automatiche” presente nel dettaglio della singola carta Postepay - Utente senza app Bancoposta installata sul device - PP Standard

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck     | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | BPINPROAND-1240 | <onboarding> |
    And Naviga verso la sezione conti e carte
    And Clicca sul pulsante dettaglio del conto
    And Clicca sul pulsante Ricariche automatiche attive
    Then Verifica La presenza del tasto INIZIA o RICARICA
    And Viene chiusa l'app

    @BPINPROAND-1240_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | payWith            | transferTo       | owner              | amount | messageText     | quickOperationName | editValue | categoryTransation | month | categoryDestination | posteID | description                         | testIdCheck     | messageText                                | transactionType | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | conto;001044548509 | 5333171077147177 | vincenzo fortunato |   0.01 | test automatico | Ricarica Postep    |      0.02 | varie              | month | categoryDestination | prisma  | Verifica i dati prima di procedere. | BPINPROAND-1240 | BancoPosta: Ricarica Carta Postepay da App | RICARICA POSTEP | nonseitu   |

  #| musellasal@gmail.com      | Musabp67!    | poste.it  | Salvatore       | samsungGalaxyS8_plus | conto;001044548509 | 5333171077147177 | vincenzo fortunato |   0.01 | test automatico | Ricarica Postep    |      0.02 | varie              | month | categoryDestination | prisma  | Verifica i dati prima di procedere. | PBPAPP-3597 | BancoPosta: Ricarica Carta Postepay da App | RICARICA POSTEP | nonseitu   |
  @BPINPROAND-3605
  Scenario Outline: Obbligatorietà campo "Fino al" - ricarica a soglia
    Il cliente in fase di configurazione della operazione ricorrente potrà specificare una data di scadenza. 
    La data di scadenza non è una informazione obbligatoria per il cliente.

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck   | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | <testIdCheck> | <onboarding> |
    And Naviga verso la sezione conti e carte
    And Utente verifica la NON obbligatorietà del campo "Fino al"
      | transferTo   | owner   | amount   | description   | payWith   | saldominimo   | testIdCheck   | paymentMethod | posteid   |
      | <transferTo> | <owner> | <amount> | <description> | <payWith> | <saldominimo> | <testIdCheck> | conto         | <posteID> |
    And Verifica popup della sezione ricarica automatica
    And Viene chiusa l'app

    @BPINPROAND-3605_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | payWith            | transferTo                 | owner             | amount | posteID | testIdCheck     | saldominimo | onboarding | description |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | conto;001044548509 | evolution;5333171077147177 | salvatore musella |   0.01 | prisma  | BPINPROAND-3605 |        0.01 | nonseitu   | ricarica    |

    @BPINPROAND-3605_fortunato
    Examples: 
      | username                | password        | loginType | userHeader | device               | payWith            | transferTo                 | owner              | amount | posteID | testIdCheck     | saldominimo | onboarding | description |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | poste.it  | Vincenzo   | samsungGalaxyS8_plus | conto;001044920229 | evolution;5333171077144679 | vincenzo fortunato |   0.01 | prisma  | BPINPROAND-3605 |        0.01 | nonseitu   | ricarica    |
