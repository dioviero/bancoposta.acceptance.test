@BolloAuto
Feature: Bollo Auto
  L'utente compila e sottomette un Bollo Auto da Postepay Evolution

  @BPINPROAND-1218
  Scenario Outline: Bollo Auto da Carta Postepay Evolution
    Viene effettuato un Bollo Auto da Postepay Evolution

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck     | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | BPINPROAND-1218 | <onboarding> |
    And Naviga verso il Bollo Auto ed effettua il pagamento di un bollo auto
      | region   | licensePlate   | vehicle   | month   | year   | validityMonth   | amount   | payWith   | paymentMethod   | oldPayment   | payWith2   | commissione   | posteId |
      | <region> | <licensePlate> | <vehicle> | <month> | <year> | <validityMonth> | <amount> | <payWith> | <paymentMethod> | <oldPayment> | <payWith2> | <commissione> | prisma  |
    And Viene chiusa l'app

    @BPINPROAND-1218_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | payWith                   | paymentMethod | testIdCheck     | region           | licensePlate | vehicle     | month   | year | validityMonth | amount | posteID | oldPayment | payWith2           | commissione | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | postepay;5333171077147177 | carta         | BPINPROAND-1218 | Regione Campania | AA000BB      | Autoveicolo | Gennaio | 2020 | Dicembre      |   0.01 | prisma  | no         | evolution **** 7177 |        1.00 | nonseitu   |

    @BPINPROAND-1218_fortunato
    Examples: 
      | username                | password        | loginType | userHeader | device               | payWith                   | paymentMethod | testIdCheck     | region           | licensePlate | vehicle     | month   | year | validityMonth | amount | posteID | oldPayment | payWith2           | commissione | onboarding |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | poste.it  | Vincenzo   | samsungGalaxyS8_plus | postepay;5333171077144679 | carta         | BPINPROAND-1218 | Regione Campania | AA000BB      | Autoveicolo | Gennaio | 2020 | Dicembre      |   0.01 | prisma  | no         | evolution **** 4679 |        1.00 | nonseitu   |
# Il campo "oldPayment" può avere come parametri:
# - "old" se si deve fare il test su un pagamento pregresso
# - "check" se si devono semplicemente verificare i campi di un eventuale pagamento pregresso
