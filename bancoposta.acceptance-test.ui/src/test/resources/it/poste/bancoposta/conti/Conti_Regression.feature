@Conti
Feature: Conti
  L'utente dopo aver onboardato il conto ed essersi posizionato sul conto visualizza l'elenco dei movimenti presenti

  @BPINPROAND-1579
  Scenario Outline: Lista Movimenti Conto
    L'utente visualizza l'elenco dei movimenti del conto presenti

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck     | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | BPINPROAND-1579 | <onboarding> |
    And Viene controllato che la transazione sia stata registrata nella lista movimenti tre
      | conto     | transactionType   | commissioni   | amount   |
      | <payWith> | <transactionType> | <commissioni> | <amount> |
    And Viene cliccato il primo risultato disponibile su conto
    And L utente verifica il dettaglio del movimento due
    Then Viene chiusa l'app

    @BPINPROAND-1579_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | payWith            | iban         | city   | owner         | amount | description     | address                     | citySender | reference | refecenceEffective | quickOperationName | transactionDescription | messageText                                   | categoryTransation | editValue | posteID | commissioni | bonificoType | beneficiario  | paymentMethod | testIdCheck     | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | conto;001044548509 | 001046919773 | Italia | PETRUCCI LUCA |   0.01 | test automatico | Via degli Ulivi Gialli n.23 | Agrigento  | Luca      | Luca Petrucci      | test bonifico      | TRN                    | BancoPosta: Conferma Bonifico SEPA effettuato | Varie              |       0.2 | prisma  |         1.0 | bonifico     | PETRUCCI LUCA | conto         | BPINPROAND-1579 | nonseitu   |

		@BPINPROAND-1579_fortunato
    Examples: 
      | username                | password        | loginType | userHeader | device               | posteID | testIdCheck     | onboarding |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | poste.it  | Vincenzo   | samsungGalaxyS8_plus | prisma  | BPINPROAND-1579 | nonseitu   |
 
  @BPINPROAND-1335
  Scenario Outline: Modificare l impostazione degli Acquisti online di un conto
    Viene modificata l'impostazione degli acquisti online di un conto

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck     | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | BPINPROAND-1335 | <onboarding> |
    And Naviga verso la sezione conti e carte
    And Clicca sul pulsante dettaglio del conto
    And viene controllato il toggle acquistionline
      | posteid   |
      | <posteID> |
    And Viene chiusa l'app

    @BPINPROAND-1335_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | payWith            | iban                        | city   | posteID | testIdCheck     | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | conto;001044548509 | IT06W0760103400001044548509 | Italia | prisma  | BPINPROAND-1335 | nonseitu   |

    @BPINPROAND-1335_fortunato
    Examples: 
      | username                | password        | loginType | userHeader | device               | posteID | testIdCheck     | onboarding |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | poste.it  | Vincenzo   | samsungGalaxyS8_plus | prisma  | BPINPROAND-1335 | nonseitu   |
