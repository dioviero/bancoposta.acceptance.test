@VOL
Feature: VOL
  L utente compra un nuovo prodotto poste

  @BPINPROAND-2479
  Scenario Outline: Accesso da biscotto informativo in home libretto
      Verificare l accesso alla vetrina vol dal biscotto libretto in homepage

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck   | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | <testIdCheck> | <onboarding> |
    #And Naviga sul carosello dei biscotti verso
    #| biscotto   |
    #| <biscotto> |
    #And Clicca su Scopri dal biscotto
    #Then Viene mostrata la pagina di tutorial da biscotto
    #| tutorial       | header                                     | description                                     | button        |
    #| Libretto Smart | Scopri tutti i vantaggi del Libretto Smart | Puoi aprire Libretto Smart direttamente da App! | APRI LIBRETTO |
    #And clicca su Scopri da tutorial di biscotto
    And Utente accede alla sezione VOL da hamburger menu
      | biscotto |
      | Libretto |
    Then La vetrina vol viene mostrata
      | biscotto |
      | Libretto |
    And Viene chiusa l'app

    @BPINPROAND-2479_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | posteID | testIdCheck     | onboarding | biscotto                                           |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | prisma  | BPINPROAND-2479 | nonseitu   | Puoi aprire il Libretto Smart direttamente in App! |

  @BPINPROAND-2478
  Scenario Outline: Tutorial da biscotto informativo in home conto
      Verificare il layout del tutorial del biscotto informativo del conto in homepage

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck   | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | <testIdCheck> | <onboarding> |
    #And Naviga sul carosello dei biscotti verso
    #| biscotto   |
    #| <biscotto> |
    #And Clicca su Scopri dal biscotto
    #Then Viene mostrata la pagina di tutorial da biscotto
    #| tutorial         | header                                           | description                     | button           |
    #| Conto BancoPosta | Apri il tuo Conto BancoPosta direttamente da App | Scegli il Conto più adatto a te | SCOPRI L'OFFERTA |
    And Utente accede alla sezione VOL da hamburger menu
      | biscotto |
      | Conto    |
    And Viene chiusa l'app

    @BPINPROAND-2478_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | posteID | testIdCheck     | onboarding | biscotto         |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | prisma  | BPINPROAND-2478 | nonseitu   | Conto BancoPosta |

    @BPINPROAND-2478_fortunato
    Examples: 
      | username                | password        | loginType | userHeader | device               | posteID | testIdCheck     | onboarding | biscotto         |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | poste.it  | Vincenzo   | samsungGalaxyS8_plus | prisma  | BPINPROAND-2478 | nonseitu   | Conto BancoPosta |

  @BPINPROAND-2488 @BPINPROIOS-2548
  Scenario Outline: FID conto start
      Verificare il link al documento informativo delle spese per conto start

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck   | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | <testIdCheck> | <onboarding> |
    #And Naviga sul carosello dei biscotti verso
    #| biscotto   |
    #| <biscotto> |
    #And Clicca su Scopri dal biscotto
    #Then Viene mostrata la pagina di tutorial da biscotto
    #| tutorial         | header                                           | description                     | button           |
    #| Conto BancoPosta | Apri il tuo Conto BancoPosta direttamente da App | Scegli il Conto più adatto a te | SCOPRI L'OFFERTA |
    #And clicca su Scopri da tutorial di biscotto
    And Utente accede alla sezione VOL da hamburger menu
      | biscotto |
      | Conto    |
    Then La vetrina vol viene mostrata
      | biscotto |
      | Conto    |
    And Naviga verso il prodotto
      | biscotto | prodotto                                          |
      | Conto    | Semplice e affidabile per gestire le tue esigenze |
    And Da pagina di dettaglio prodotto vol clicca su FID
      | prodotto                                                     |
      | il conto semplice ed affidabile, per gestire le tue esigenze |
    Then Il documento FID viene mostrato
      | fidCode               | prodotto        |
      | FID_CBP_CONSNEW_START | FID Conto Start |
    And Viene chiusa l'app

    @BPINPROAND-2488_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | posteID | testIdCheck     | onboarding | biscotto         |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | prisma  | BPINPROAND-2488 | nonseitu   | Conto BancoPosta |

    @BPINPROAND-2488_fortunato
    Examples: 
      | username                | password        | loginType | userHeader | device               | posteID | testIdCheck     | onboarding | biscotto         |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | poste.it  | Vincenzo   | samsungGalaxyS8_plus | prisma  | BPINPROAND-2488 | nonseitu   | Conto BancoPosta |

    @BPINPROIOS-2548_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | posteID | testIdCheck     | onboarding | biscotto         |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | prisma  | BPINPROIOS-2548 | nonseitu   | Conto BancoPosta |

    @BPINPROIOS-2548_fortunato
    Examples: 
      | username                | password        | loginType | userHeader | device               | posteID | testIdCheck     | onboarding | biscotto         |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | poste.it  | Vincenzo   | samsungGalaxyS8_plus | prisma  | BPINPROIOS-2548 | nonseitu   | Conto BancoPosta |

  @BPINPROAND-2487 @BPINPROIOS-2547
  Scenario Outline: FID conto start giovani
      Verificare il link al documento informativo delle spese per conto start giovani

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck   | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | <testIdCheck> | <onboarding> |
    #And Naviga sul carosello dei biscotti verso
    #| biscotto   |
    #| <biscotto> |
    #And Clicca su Scopri dal biscotto
    #Then Viene mostrata la pagina di tutorial da biscotto
    #| tutorial         | header                                           | description                     | button           |
    #| Conto BancoPosta | Apri il tuo Conto BancoPosta direttamente da App | Scegli il Conto più adatto a te | SCOPRI L'OFFERTA |
    #And clicca su Scopri da tutorial di biscotto
    And Utente accede alla sezione VOL da hamburger menu
      | biscotto |
      | Conto    |
    Then La vetrina vol viene mostrata
      | biscotto |
      | Conto    |
    And Naviga verso il prodotto
      | biscotto | prodotto      |
      | Conto    | Start Giovani |
    And Da pagina di dettaglio prodotto vol clicca su FID
      | prodotto                                                     |
      | per giovani con meno di 30 anni che preferiscono il digitale |
    Then Il documento FID viene mostrato
      | fidCode                       | prodotto        |
      | FID_CBP_CONSNEW_START_GIOVANI | FID Conto Start |
    And Viene chiusa l'app

    @BPINPROAND-2487_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | posteID | testIdCheck     | onboarding | biscotto         |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | prisma  | BPINPROAND-2487 | nonseitu   | Conto BancoPosta |

    @BPINPROIOS-2547_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | posteID | testIdCheck     | onboarding | biscotto         |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | prisma  | BPINPROAND-2547 | nonseitu   | Conto BancoPosta |

    @BPINPROIOS-2547_fortunato
    Examples: 
      | username                | password        | loginType | userHeader | device               | posteID | testIdCheck     | onboarding | biscotto         |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | poste.it  | Vincenzo   | samsungGalaxyS8_plus | prisma  | BPINPROIOS-2547 | nonseitu   | Conto BancoPosta |

  @BPINPROAND-2490
  Scenario Outline: Link Scopri l intera offerta in vetrina VOL tab conto
      Verificare il link rimanda a https://www.poste.it/gamma/conti-correnti-bancoposta.html

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck   | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | <testIdCheck> | <onboarding> |
    #And Naviga sul carosello dei biscotti verso
    #| biscotto   |
    #| <biscotto> |
    #And Clicca su Scopri dal biscotto
    #Then Viene mostrata la pagina di tutorial da biscotto
    #| tutorial         | header                                           | description                     | button           |
    #| Conto BancoPosta | Apri il tuo Conto BancoPosta direttamente da App | Scegli il Conto più adatto a te | SCOPRI L'OFFERTA |
    #And clicca su Scopri da tutorial di biscotto
    And Utente accede alla sezione VOL da hamburger menu
      | biscotto |
      | Conto    |
    Then La vetrina vol viene mostrata
      | biscotto |
      | Conto    |
    And Clicca sul link Scopri l intera offerta
    Then la pagina web "https://www.poste.it/gamma/conti-correnti-bancoposta.html" viene mostrata
    And Viene chiusa l'app

    @BPINPROAND-2490_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | posteID | testIdCheck     | onboarding | biscotto         |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | prisma  | BPINPROAND-2490 | nonseitu   | Conto BancoPosta |

  @BPINPROAND-2491
  Scenario Outline: Link Approfondisci l intera gamma di offerte su poste.it in dettaglio prodotti conto della vetrina VOL
      Verificare il link rimanda a https://www.poste.it/gamma/conti-correnti-bancoposta.html

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck   | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | <testIdCheck> | <onboarding> |
    #And Naviga sul carosello dei biscotti verso
    #| biscotto   |
    #| <biscotto> |
    #And Clicca su Scopri dal biscotto
    #Then Viene mostrata la pagina di tutorial da biscotto
    #| tutorial         | header                                           | description                     | button           |
    #| Conto BancoPosta | Apri il tuo Conto BancoPosta direttamente da App | Scegli il Conto più adatto a te | SCOPRI L'OFFERTA |
    #And clicca su Scopri da tutorial di biscotto
    And Utente accede alla sezione VOL da hamburger menu
      | biscotto |
      | Conto    |
    Then La vetrina vol viene mostrata
      | biscotto |
      | Conto    |
    And Naviga verso il prodotto
      | biscotto | prodotto      |
      | Conto    | Start Giovani |
    And Clicca sul link Approfondisci l intera gamma di offerte
      | biscotto |
      | Conto    |
    Then la pagina web "https://www.poste.it/gamma/conti-correnti-bancoposta.html" viene mostrata
    And Viene chiusa l'app

    @BPINPROAND-2491_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | posteID | testIdCheck     | onboarding | biscotto         |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | prisma  | BPINPROAND-2491 | nonseitu   | Conto BancoPosta |

  @BPINPROAND-2492
  Scenario Outline: Link Approfondisci l intera gamma di offerte su poste.it in dettaglio prodotti libretto della vetrina VOL
      Verificare il link rimanda a https://www.poste.it/gamma/conti-correnti-bancoposta.html

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck   | onboarding   | debug |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | <testIdCheck> | <onboarding> | y     |
    #And Naviga sul carosello dei biscotti verso
    #| biscotto   |
    #| <biscotto> |
    #And Clicca su Scopri dal biscotto
    #Then Viene mostrata la pagina di tutorial da biscotto
    #| tutorial       | header                                     | description                                     | button        |
    #| Libretto Smart | Scopri tutti i vantaggi del Libretto Smart | Puoi aprire Libretto Smart direttamente da App! | APRI LIBRETTO |
    #And clicca su Scopri da tutorial di biscotto
    And Utente accede alla sezione VOL da hamburger menu
      | biscotto |
      | Libretto |
    Then La vetrina vol viene mostrata
      | biscotto |
      | Libretto |
    And Clicca sul link Approfondisci l intera gamma di offerte
      | biscotto |
      | Libretto |
    Then la pagina web "https://buonielibretti.poste.it/prodotti/libretto-postale-smart.html" viene mostrata
    And Viene chiusa l'app

    @BPINPROAND-2492_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | posteID | testIdCheck     | onboarding | biscotto                                           |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | prisma  | BPINPROAND-2492 | nonseitu   | Puoi aprire il Libretto Smart direttamente in App! |

  @BPINPROAND-1159
  Scenario Outline: Verifica la presenza della card in home  Conto in apertura dopo  aver iniziato il funnel di acquisto
      Verifica la presenza della card in home  Conto in apertura dopo  aver iniziato il funnel di acquisto

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck   | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | <testIdCheck> | <onboarding> |
    #And Naviga sul carosello dei biscotti verso
    #| biscotto   |
    #| <biscotto> |
    #And Clicca su Scopri dal biscotto
    #Then Viene mostrata la pagina di tutorial da biscotto
    #| tutorial         | header                                           | description                     | button           |
    #| Conto BancoPosta | Apri il tuo Conto BancoPosta direttamente da App | Scegli il Conto più adatto a te | SCOPRI L'OFFERTA |
    #And clicca su Scopri da tutorial di biscotto
    And Utente accede alla sezione VOL da hamburger menu
      | biscotto |
      | Conto    |
    Then La vetrina vol viene mostrata
      | biscotto |
      | Conto    |
    And Naviga verso il prodotto
      | biscotto | prodotto      |
      | Conto    | Start Giovani |
    And Clicca su richiedi da pagina di dettaglio vol
      | biscotto |
      | Conto    |
    Then La pagina di richiesta acquisto viene mostrata
      | biscotto |
      | Conto    |
    And Clicca su Continua da pagina di acquisto vol
      | biscotto |
      | Conto    |
    Then la pagina di riepilogo acquisto vol viene mostrata
    And Clicca su Annulla acquisto vol
    Then La card in home Conto in apertura viene mostrata
    And Clicca sulla card Conto in apertura
    And Clicca su Annulla Acquisto VOL
    And Viene chiusa l'app

    @BPINPROAND-1159_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | posteID | testIdCheck     | onboarding | biscotto         |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | prisma  | BPINPROAND-1159 | nonseitu   | Conto BancoPosta |

    @BPINPROAND-1159_fortunato
    Examples: 
      | username                | password        | loginType | userHeader | device               | posteID | testIdCheck     | onboarding | biscotto         |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | poste.it  | Vincenzo   | samsungGalaxyS8_plus | prisma  | BPINPROAND-1159 | nonseitu   | Conto BancoPosta |

  @BPINPROAND-2472 @BPINPROIOS-2532
  Scenario Outline: Scopri i prodotti da prelogin (utente con wallet)
      L'utente accede alla pagina di Acquista prodotto dalla PRELOGIN con wallet attivo

    Given L'app "android" viene avviata
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck   | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | <testIdCheck> | <onboarding> |
    And Clicca su Scopri i prodotti da prelogin
    Then La vetrina vol viene mostrata
      | biscotto |
      | Conto    |
    And Viene chiusa l'app

    @BPINPROAND-2472_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | posteID | testIdCheck     | onboarding | biscotto         |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | prisma  | BPINPROAND-2472 | nonseitu   | Conto BancoPosta |

    @BPINPROIOS-2532_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | posteID | testIdCheck     | onboarding | biscotto         |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | prisma  | BPINPROIOS-2532 | nonseitu   | Conto BancoPosta |

    @BPINPROIOS-2532_fortunato
    Examples: 
      | username                | password        | loginType | userHeader | device               | posteID | testIdCheck     | onboarding | biscotto         |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | poste.it  | Vincenzo   | samsungGalaxyS8_plus | prisma  | BPINPROIOS-2532 | nonseitu   | Conto BancoPosta |
