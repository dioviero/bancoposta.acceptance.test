@notifiche
Feature: Notifiche

  @BPINPROAND-1223
  Scenario Outline: Verifica l autorizzazione in app dal portale web BPOL da app aperta
    Viene effettuata una autorizzazione all utilizzo di bancoposta on line da app BancoPosta

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck     | onboarding   | webDriver    |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | BPINPROAND-1223 | <onboarding> | <deviceBPOL> |
    And L utente imposta come preferito il device
    	| posteid   |
    	| <posteID> |
    And utente Accede a BPOL
      | username   | password   | loginType   | userHeader   | device | posteid   | testIdCheck   |
      | <username> | <password> | <loginType> | <userHeader> | chrome | <posteID> | <testIdCheck> |
    And accede alla sezione Servizi online Bancoposta online
    And invia la notifica push all app
    Then la notifica push viene ricevuta dall app
      | device   |
      | <device> |
    And utente clicca su "AUTORIZZA"
      | posteid   |
      | <posteID> |
    Then dal portale BPOL si viene indirizzati alla homepage di bancoposta
    And Utente naviga il portale
    And Viene chiuso il portale
    And Viene chiusa l'app

    @BPINPROAND-1223_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | posteID | testIdCheck     | onboarding | deviceBPOL  |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | prisma  | BPINPROAND-1223 | nonseitu   | default_ita |

    @BPINPROAND-1223_fortunato
    Examples: 
      | username                | password        | loginType | userHeader | device               | posteID | testIdCheck     | onboarding | deviceBPOL  |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | poste.it  | Vincenzo   | samsungGalaxyS8_plus | prisma  | BPINPROAND-1223 | nonseitu   | default_ita |

  @BPINPROAND-1239 @BPINPROIOS-2212
  Scenario Outline: Verifica l autorizzazione in app dal portale web BPOL da app in background
    Viene effettuata una autorizzazione all utilizzo di bancoposta on line da app BancoPosta con app in background

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck     | onboarding   | webDriver    |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | BPINPROAND-1239 | <onboarding> | <deviceBPOL> |
    And L utente imposta come preferito il device
      | posteid   |
      | <posteID> |
    And L utente mette l app in background
    And utente Accede a BPOL
      | username   | password   | loginType   | userHeader   | device       | posteid   | testIdCheck   |
      | <username> | <password> | <loginType> | <userHeader> | <deviceBPOL> | <posteID> | <testIdCheck> |
    And accede alla sezione Servizi online Bancoposta online
    And invia la notifica push all app
    Then la notifica push viene ricevuta dall app
      | device   |
      | <device> |
    And utente clicca su "AUTORIZZA"
      | posteid   |
      | <posteID> |
    Then dal portale BPOL si viene indirizzati alla homepage di bancoposta
    And Utente naviga il portale
    And Viene chiuso il portale
    And Viene chiusa l'app

    @BPINPROAND-1239_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | posteID | testIdCheck     | onboarding | deviceBPOL  |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | prisma  | BPINPROAND-1239 | nonseitu   | default_ita |

    @BPINPROAND-1239_fortunato
    Examples: 
      | username                | password        | loginType | userHeader | device               | posteID | testIdCheck     | onboarding | deviceBPOL  |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | poste.it  | Vincenzo   | samsungGalaxyS8_plus | prisma  | BPINPROAND-1239 | nonseitu   | default_ita |

    @BPINPROIOS-2212_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | posteID | testIdCheck     | onboarding | deviceBPOL  |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | prisma  | BPINPROIOS-2212 | nonseitu   | default_ita |

    @BPINPROIOS-2212_fortunato
    Examples: 
      | username                | password        | loginType | userHeader | device               | posteID | testIdCheck     | onboarding | deviceBPOL  |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | poste.it  | Vincenzo   | samsungGalaxyS8_plus | prisma  | BPINPROIOS-2212 | nonseitu   | default_ita |

  @BPINPROAND-1193
  Scenario Outline: Verifica l autorizzazione in app dal portale web BPOL da app chiusa
    Viene effettuata una autorizzazione all utilizzo di bancoposta on line da app BancoPosta con app in background

    #Given L'app "android" viene avviata e viene eseguito il New Login
    #| username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck     | onboarding   |
    #| <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | BPINPROAND-1193 | <onboarding> |
    #And L utente imposta come preferito il device
    #| posteid   |
    #| <posteID> |
    #And L utente chiude l app
    Given L app viene avviata
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck     | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | BPINPROAND-1193 | <onboarding> |
    And utente Accede a BPOL
      | username   | password   | loginType   | userHeader   | device | posteid   | testIdCheck   |
      | <username> | <password> | <loginType> | <userHeader> | chrome | <posteID> | <testIdCheck> |
    And accede alla sezione Servizi online Bancoposta online
    And invia la notifica push all app
    Then la notifica push viene ricevuta dall app
      | device   |
      | <device> |
    And utente clicca su "AUTORIZZA"
      | posteid   |
      | <posteID> |
    Then dal portale BPOL si viene indirizzati alla homepage di bancoposta
    And Utente naviga il portale
    And Viene chiuso il portale
    And Viene chiusa l'app

    @BPINPROAND-1193_musella
    Examples: 
      | username             | password  | loginType | userHeader | device                     | posteID | testIdCheck     | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus_noApp | prisma  | BPINPROAND-1193 | nonseitu   |
