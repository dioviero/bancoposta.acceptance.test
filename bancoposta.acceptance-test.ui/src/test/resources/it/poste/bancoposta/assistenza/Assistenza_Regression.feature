@Assistenza
Feature: Assistenza
  L utente contatta l assistenza attraverso il pulsante chatta con noi

  @BPINPROAND-2247
  Scenario Outline: Chat - Prenota Richiamata
      Viene contattata l assistenza

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck     | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | BPINPROAND-2247 | <onboarding> |
    And Viene cliccato il pulsante dell'assistenza
    And Viene cliccato il pulsante chatta con noi
    And Viene inserito il numero e viene cliccato sul pulsante prenota chiamata
      | phoneNumber   |
      | <phoneNumber> |
    Then viene cliccato il tasto termina e torna alla homepage
    And Viene chiusa l'app

    @BPINPROAND-2247_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | posteID | testIdCheck     | phoneNumber | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | prisma  | BPINPROAND-2247 |  3333333333 | nonseitu   |

  @BPINPROAND-1616
  Scenario Outline: Verifica il layout della schermata numeri utili
     Viene controllato il layout della schermata numeri utili

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck     | onboarding   | owner   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | BPINPROAND-1616 | <onboarding> | <owner> |
    And Viene cliccato il pulsante dell'assistenza
    And Viene cliccato il pulsante contattaci
    And Vengono effettuati i controlli della pagina
    And Viene chiusa l'app

    @BPINPROAND-1616_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | posteID | testIdCheck     | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | prisma  | BPINPROAND-2247 | nonseitu   |
