@Libretti
Feature: Libretti
  L'utente compila e sottomette una ricarica Postepay da libretto

  @BPINPROAND-1871
  Scenario Outline: Ricarica PP da Libretto
     Viene effettuata una ricarica PP da libretto

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck     | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | BPINPROAND-1871 | <onboarding> |
    And Viene effettuata una ricarica postepay da libretto
      | payWith   | amount   | testIdCheck | posteid   | phoneNumber   | phoneCompaign   |
      | <payWith> | <amount> | -           | <posteID> | <phoneNumber> | <phoneCompaign> |
    And Viene controllata la ricezione della notifica
      | messageText   |
      | <messageText> |
    And Viene controllata la corretta categorizzazione della transazione
      | categoryTransation   | amount   | testIdCheck |
      | <categoryTransation> | <amount> | -           |
    And Viene chiusa l'app

    @BPINPROAND-1871_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | payWith            | transferTo       | owner             | amount | messageText             | quickOperationName | editValue | categoryTransation | month | categoryDestination | posteID | description                         | testIdCheck | messageText                                | transactionType | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | conto;001044548509 | 5333171077147177 | Salvatore Musella |   0.01 | Ricarica Carta Postepay | Ricarica Postep    |      0.02 | varie              | month | categoryDestination | prisma  | Verifica i dati prima di procedere. | BPINPROAND-1871  | BancoPosta: Ricarica Carta Postepay da App | RICARICA POSTEP | nonseitu   |

  @BPINPROAND-1517 @BPINPROIOS-1895
  Scenario Outline: Verificare i risultati di Cerca movimenti di un libretto smart
     Vengono verificati i risultati di cerca movimenti di un libretto smart

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck     | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | BPINPROAND-1517 | <onboarding> |
    And Naviga verso la sezione Buoni e Libretti e seleziona il libretto smart
      | payWith   | paymentMethod   |
      | <payWith> | <paymentMethod> |
    And Viene controllato che la transazione sia stata registrata nella lista movimenti libretto
      | conto     | amount   | operation | transactionType   | transactionDescription   |
      | <payWith> | <amount> | -         | <transactionType> | <transactionDescription> |
    And Viene chiusa l'app

    @BPINPROAND-1517_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | payWith                     | transferTo       | owner             | amount | messageText             | quickOperationName | editValue | categoryTransation | month | categoryDestination | posteID | description                         | testIdCheck | messageText                                | transactionType | paymentMethod | transactionDescription | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | libretto smart;000049494216 | 5333171077147177 | Salvatore Musella |   0.01 | Ricarica Carta Postepay | Ricarica Postep    |      0.02 | varie              | month | categoryDestination | prisma  | Verifica i dati prima di procedere. | BPINPROAND-1517  | BancoPosta: Ricarica Carta Postepay da App | GIROFONDO       | smart         | TRN                    | nonseitu   |

    @BPINPROAND-1517_fortunato
    Examples: 
      | username                | password        | loginType | userHeader | device               | payWith                     | amount | posteID | testIdCheck | transactionType | paymentMethod | transactionDescription | onboarding |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | poste.it  | Vincenzo   | samsungGalaxyS8_plus | libretto smart;000049494216 |   0.01 | prisma  | PBPAPP-531  | GIROFONDO       | smart         | TRN                    | nonseitu   |

    @BPINPROIOS-1895_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | payWith                     | transferTo       | owner             | amount | messageText             | quickOperationName | editValue | categoryTransation | month | categoryDestination | posteID | description                         | testIdCheck     | messageText                                | transactionType | paymentMethod | transactionDescription | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | libretto smart;000049494216 | 5333171077147177 | Salvatore Musella |   0.01 | Ricarica Carta Postepay | Ricarica Postep    |      0.02 | varie              | month | categoryDestination | prisma  | Verifica i dati prima di procedere. | BPINPROIOS-1895 | BancoPosta: Ricarica Carta Postepay da App | GIROFONDO       | smart         | TRN                    | nonseitu   |

  @BPINPROAND-1640
  Scenario Outline: Lista movimenti libretto onboardato e visualizzazione dettaglio movimento
     Lista movimenti libretto onboardato e visualizzazione dettaglio movimento

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck     | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | BPINPROAND-1640 | <onboarding> |
    And Naviga verso la sezione Buoni e Libretti e seleziona il libretto smart
      | payWith   | paymentMethod   |
      | <payWith> | <paymentMethod> |
    And Viene controllato che la transazione sia presente nel libretto
      | conto     | amount   | operation | transactionType   | transactionDescription   |
      | <payWith> | <amount> | -         | <transactionType> | <transactionDescription> |
    And Viene cliccato sulla transazione
      | conto     | amount   | operation | transactionType   | transactionDescription   |
      | <payWith> | <amount> | -         | <transactionType> | <transactionDescription> |
    Then la pagina di dettaglio movimenti viene mostrata
      | conto     | amount   | operation | transactionType   | transactionDescription   |
      | <payWith> | <amount> | -         | <transactionType> | <transactionDescription> |
    And Viene chiusa l'app

    @BPINPROAND-1640_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | payWith                     | transferTo       | owner             | amount | messageText             | quickOperationName | editValue | categoryTransation | month | categoryDestination | posteID | description                         | testIdCheck | messageText                                | transactionType | paymentMethod | transactionDescription | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | libretto smart;000049494216 | 5333171077147177 | Salvatore Musella |   0.01 | Ricarica Carta Postepay | Ricarica Postep    |      0.02 | varie              | month | categoryDestination | prisma  | Verifica i dati prima di procedere. | PBPAPP-531  | BancoPosta: Ricarica Carta Postepay da App | GIROFONDO       | smart         | TRN                    | nonseitu   |

    @BPINPROAND-1640_fortunato
    Examples: 
      | username                | password        | loginType | userHeader | device        | payWith                     | transferTo       | owner              | amount | messageText             | quickOperationName | editValue | categoryTransation | month | categoryDestination | posteID | description                         | testIdCheck | messageText                                | transactionType | paymentMethod | transactionDescription | onboarding |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | poste.it  | Vincenzo   | samsungGalaxyS8_plus | libretto smart;000049580610 | 5333171077147177 | Vincenzo Fortunato |   0.01 | Ricarica Carta Postepay | Ricarica Postep    |      0.02 | varie              | month | categoryDestination | prisma  | Verifica i dati prima di procedere. | PBPAPP-531  | BancoPosta: Ricarica Carta Postepay da App | GIROFONDO       | smart         | TRN                    | nonseitu   |
